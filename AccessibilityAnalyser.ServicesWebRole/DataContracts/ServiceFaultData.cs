﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

namespace AccessibilityAnalyser.Web.Services.DataContracts
{
    [DataContract]
    public class ServiceFaultData
    {
        
        [DataMember]
        public bool IsError { get; set; }
        [DataMember]
        public int ErrorCode { get; set; }
        [DataMember]
        public string ErrorMessage { get; set; }
    }


}