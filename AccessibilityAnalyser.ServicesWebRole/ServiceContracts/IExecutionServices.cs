﻿// -----------------------------------------------------------------------
// <copyright file="IExecutionServices.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Interfaces;
namespace AccessibilityAnalyser.Web.Services
{
    /// <summary>
    /// ExecutionServices  page
    /// </summary>
    [ServiceContract]
    public interface IExecutionServices
    {
        [OperationContract]
        List<ClientBO> GetClientList(string searchText);

        [OperationContract]
        List<SiteBO> GetSiteList(string searchText, int clientID);

        [OperationContract]
        List<BuildBO> GetBuildList(string searchText, int siteID);

        [OperationContract]
        List<RunBO> GetRunList(string searchText, int buildID);

        [OperationContract]
        int GetClientID(string clientName);

        [OperationContract]
        int GetSiteID(string url, int clientID);

        [OperationContract]
        int GetBuildID(string buildName, int siteID);

        [OperationContract]
        int GetRunID(string runName, int buildID, int clientID);

        [OperationContract]
        bool CreateClient(string clientName);

        [OperationContract]
        bool CreateSite(string siteName, int clientId);

        [OperationContract]
        bool CreateBuild(string buildName, int siteId);

        [OperationContract]
        bool CreateRun(int clientId, int buildId, string runName);

        [OperationContract]
        bool CreateTestCase(string testCaseName, int buildId);

        [OperationContract]
        bool CreateTestCaseMap(int ruleDetailId, int testCaseId);

        [OperationContract]
        List<TestCaseBO> GetAllDistinctTestCases();

        [OperationContract]
        List<RuleDetailBO> GetAllDistinctRuleDetails();

        [OperationContract]
        List<TestCaseBO> GetAllTestCases(int buildId);
    }
}
