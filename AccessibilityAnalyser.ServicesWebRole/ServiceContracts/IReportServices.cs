﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.RuleEngine;


namespace AccessibilityAnalyser.Web.Services.ServiceContracts
{
    /// <summary>
    /// IReportServices - Service contract
    /// </summary>
    [ServiceContract]
    public interface IReportServices
    {
        [OperationContract]
        IEnumerable<BuildBO> GetNoOfBuild(int clientId);

        [OperationContract]
        List<ReportingDetails> GetTestCaseRatioDetails(int clientId, int buildId);
        
        [OperationContract]
        List<ReportingDetails> GetLatestBuilds(int clientId);

        [OperationContract]
        int GetClientId(string clientName);

        [OperationContract]
        string GetClientName(int clientId);

        [OperationContract]
        ReportingDetails GetTotalTestCaseRatio(int clientId);

        [OperationContract]
        void GetData(int clientId);
        
    }
}
