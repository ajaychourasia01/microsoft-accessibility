﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AccessibilityAnalyser.RuleEngine;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "NewClientService" in code, svc and config file together.
    public class NewClientService : INewClientService
    {
        public List<ClientBO> GetClientList(string searchText)
        {
            return NewClient.GetClientList(searchText);
        }


        public List<SiteBO> GetSiteList(string searchText, int clientID)
        {
            return NewClient.GetSiteList(searchText, clientID);
        }


        public List<BuildBO> GetBuildList(string searchText, int siteID)
        {
            return NewClient.GetBuildList(searchText, siteID);
        }


        public List<RunBO> GetRunList(string searchText, int buildID)
        {
            return NewClient.GetRunList(searchText, buildID);
        }

        public int GetClientID(string clientName)
        {
            return NewClient.GetClientID(clientName);
        }


        public int GetSiteID(string url, int clientID)
        {
            return NewClient.GetSiteID(url, clientID);
        }


        public int GetBuildID(string buildName, int siteID)
        {
            return NewClient.GetBuildID(buildName, siteID);
        }


        public int GetRunID(string runName, int buildID, int clientID)
        {
            return NewClient.GetRunID(runName, buildID, clientID);
        }

        public bool CreateClient(string clientName)
        {
            return NewClient.CreateClient(clientName);
        }

        public bool CreateSite(string siteName, int clientId)
        {
            return NewClient.CreateSite(siteName, clientId);
        }

        public bool CreateBuild(string buildName, int siteId)
        {
            return NewClient.CreateBuild(buildName, siteId);
        }

        public bool CreateRun(int clientId, int buildId, string runName)
        {
            return NewClient.CreateRun(clientId, buildId, runName);
        }

        public bool CreateTestCase(string testCaseName, int buildId, string strTestcaseDesc)
        {
            return NewClient.CreateTestCase(testCaseName, buildId, strTestcaseDesc);
        }

        public bool CreateTestCaseMap(int ruleDetailId, int testCaseId,int iPriority)
        {
            return NewClient.CreateTestCaseMap(ruleDetailId, testCaseId, iPriority);
        }

        public List<TestCaseBO> GetAllDistinctTestCases()
        {
            return NewClient.GetAllDistinctTestCases();
        }

        public List<RuleDetailBO> GetAllDistinctRuleDetails()
        {
            return NewClient.GetAllDistinctRuleDetails();
        }

        public List<TestCaseBO> GetAllTestCases(int buildId)
        {
            return NewClient.GetAllTestCases(buildId);
        }
        
        public bool UpdateTestCase(TestCaseBO TestCaseData)
        {
            return NewClient.UpdateTestCase(TestCaseData);
        }

        public bool DeleteTestCaseRuleMap(TestCaseRuleMapBO TestCaseRuleMapData)
        {
            return NewClient.DeleteTestCaseRuleMap(TestCaseRuleMapData);
        }

        public bool DeleteMultipleTestCaseRuleMap(List<TestCaseRuleMapBO> TestCaseRuleMapData)
        {
            return NewClient.DeleteTestCaseRuleMap(TestCaseRuleMapData);
        }

        public List<TestCaseRuleMapBO> GetAllTestCaseRuleMap(int iTestCaseID)
        {
            return NewClient.GetAllTestCaseRuleMap(iTestCaseID);
        }

        public bool DeleteSingleTestCase(int iTestCaseId)
        {
            return NewClient.DeleteSingleTestCase(iTestCaseId);
        }

        public IEnumerable<StandardBO> GetAllGroups()
        {
            return Rules.GetAllGroups();
        }

        public List<RuleDetails> GetRuleDetails(string groupID)
        {
            return NewClient.GetRuleDetails(groupID);
        }

        public List<RuleDetails> GetAllRuleDetails()
        {
            return NewClient.GetAllRuleDetails();
        }

        public TestCaseBO GetSingleTestCase(int iTestcaseId)
        {
            return NewClient.GetSingleTestCase(iTestcaseId);
        }
    }
}
