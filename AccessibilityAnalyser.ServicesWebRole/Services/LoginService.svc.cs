﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AccessibilityAnalyser.Web.Services.ServiceContracts;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.RuleEngine;

namespace AccessibilityAnalyser.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "LoginService" in code, svc and config file together.
    public class LoginService : ILoginService
    {
        public List<LoginBO> GetUsers()
        {
            return Login.GetUsers();
        }
    }
}
