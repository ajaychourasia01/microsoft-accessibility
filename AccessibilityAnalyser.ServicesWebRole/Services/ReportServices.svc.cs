﻿using System.ServiceModel.Web;
using System.Text;
using System.Collections.Generic;

using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Interfaces;
using AccessibilityAnalyser.RuleEngine;
using AccessibilityAnalyser.Web.Services.ServiceContracts;


namespace AccessibilityAnalyser.Web.Services
{
    public class ReportServices : IReportServices
    {
        //Reporting reporting = new Reporting();

        public IEnumerable<BuildBO> GetNoOfBuild(int clientId)
        {

            return Reporting.GetNoOfBuilds(clientId);
        }

        public List<ReportingDetails> GetTestCaseRatioDetails(int clientId, int buildId)
        {
            return Reporting.GetTestCaseRatioDetails(clientId, buildId);
        }

        public List<ReportingDetails> GetLatestBuilds(int clientId)
        {
            return Reporting.GetLatestBuilds(clientId);
        }

        public int GetClientId(string clientName)
        {
            return Reporting.GetClientId(clientName);
        }

        public string GetClientName(int clientId)
        {
            return Reporting.GetClientName(clientId);
        }

        public ReportingDetails GetTotalTestCaseRatio(int clientId)
        {
            return Reporting.GetTotalTestCaseRatio(clientId);
        }

        public void GetData(int clientId)
        {
            Reporting.GetData(clientId);
        }
    }
}
