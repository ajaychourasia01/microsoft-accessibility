﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AccessibilityAnalyser.RuleEngine;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Interfaces;
namespace AccessibilityAnalyser.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ExecutionServices" in code, svc and config file together.
    public class ExecutionServices : IExecutionServices
    {

        public List<ClientBO> GetClientList(string searchText)
        {
            return Executions.GetClientList(searchText);
        }


        public List<SiteBO> GetSiteList(string searchText, int clientID)
        {
            return Executions.GetSiteList(searchText, clientID);
        }


        public List<BuildBO> GetBuildList(string searchText, int siteID)
        {
            return Executions.GetBuildList(searchText, siteID);
        }


        public List<RunBO> GetRunList(string searchText, int buildID)
        {
            return Executions.GetRunList(searchText, buildID);
        }

        public int GetClientID(string clientName)
        {
            return Executions.GetClientID(clientName);
        }


        public int GetSiteID(string url, int clientID)
        {
            return Executions.GetSiteID(url, clientID);
        }


        public int GetBuildID(string buildName, int siteID)
        {
            return Executions.GetBuildID(buildName, siteID);
        }


        public int GetRunID(string runName, int buildID, int clientID)
        {
            return Executions.GetRunID(runName, buildID, clientID);
        }

        public bool CreateClient(string clientName)
        {
            return Executions.CreateClient(clientName);
        }

        public bool CreateSite(string siteName, int clientId)
        {
            return Executions.CreateSite(siteName, clientId);
        }

        public bool CreateBuild(string buildName, int siteId)
        {
            return Executions.CreateBuild(buildName, siteId);
        }

        public bool CreateRun(int clientId, int buildId, string runName)
        {
            return Executions.CreateRun(clientId, buildId, runName);
        }

        public bool CreateTestCase(string testCaseName, int buildId)
        {
            return Executions.CreateTestCase(testCaseName, buildId);
        }

        public bool CreateTestCaseMap(int ruleDetailId, int testCaseId)
        {
            return Executions.CreateTestCaseMap(ruleDetailId, testCaseId);
        }

        public List<TestCaseBO> GetAllDistinctTestCases()
        {
            return Executions.GetAllDistinctTestCases();
        }

        public List<RuleDetailBO> GetAllDistinctRuleDetails()
        {
            return Executions.GetAllDistinctRuleDetails();
        }

        public List<TestCaseBO> GetAllTestCases(int buildId)
        {
            return Executions.GetAllTestCases(buildId);
        }
    }
}
