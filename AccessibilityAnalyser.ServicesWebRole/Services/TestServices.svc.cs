﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

using System.Threading.Tasks;
using System.Threading;

using RA.EnterpriseLibrary.Logging;

using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Interfaces;
using AccessibilityAnalyser.RuleEngine;
using AccessibilityAnalyser.Web.Services.DataContracts;
using AccessibilityAnalyser.Web.Services.ServiceContracts;
using System.Configuration;

namespace AccessibilityAnalyser.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.

    public class TestServices : ITestServices
    {
        ServiceFaultData serviceFault = null;
        List<ViolationBO> errorReport = new List<ViolationBO>();

        //static RA.EnterpriseLibrary.Logging.IRALogger serviceLogger = RA.EnterpriseLibrary.Logging.InfrastructureParts.Instance.Logger;
        string logFileType = ConfigurationManager.AppSettings["LogFileType"];

        public int TestValidation(List<string> urlNames, int clientId, int buildId, int runId)
        {

            CrawlerService crawlObj = new CrawlerService();
            try
            {
                //List<string> urlList = crawlObj.GetUrlList(urlName, depthOfLevel);

                //Instantiates rule engine for performing the validation.
                TestCaseCollection tc = new TestCaseCollection();
                PrepareRules ruleDetailsForValidation = new PrepareRules();

                // Get all testcases based on build
                IEnumerable<TestCaseBO> testcaseList = tc.GetRecords().Where(o => o.BuildId.Equals(buildId));
                RuleValidator validator = new RuleValidator();

                foreach (TestCaseBO item in testcaseList) 
                {
                    //Prepares rule details for list of Rules based on Build.  
                    IEnumerable<RuleDetailBO> ruleDetailsList = ruleDetailsForValidation.PrepareRulesToTest(buildId, item.TestCaseId);


                    //serviceLogger.Log("Validation Started for the crawled URLs");
                    //serviceLogger.Log("Validation Started for the crawled URLs", logFileType);
                    foreach (string name in urlNames)
                    {
                        //RuleValidator validator = new RuleValidator();

                        bool isDone = validator.ExecuteValidation(name, ruleDetailsList, ruleDetailsForValidation.TestCaseRuleMapList, clientId, buildId, runId);


                        foreach (ViolationBO v in validator.violationReport)
                        {
                            errorReport.Add(v);
                        }
                    }
                }

                validator.updateDecliendViolations(clientId, buildId, runId);
                
                //serviceLogger.Log("Validation completed for " + urlNames.Count + " URLs", logFileType);
            }
            catch (TimeoutException timeOutEx)
            {
                string innerExDetails = timeOutEx.Message;
                //serviceLogger.Log("Timeout Exception raised from TestServices " + innerExDetails, logFileType);
                serviceFault = new ServiceFaultData();
                serviceFault.ErrorMessage = "TestServices Failed";
                throw new FaultException<ServiceFaultData>(serviceFault, serviceFault.ErrorMessage);
            }
            catch (CommunicationException communicationEx)
            {
                string innerExDetails = communicationEx.Message;
                //serviceLogger.Log("Communication Exception raised from TestServices " + innerExDetails, logFileType);
                serviceFault = new ServiceFaultData();
                serviceFault.ErrorMessage = "TestServices Failed";
                throw new FaultException<ServiceFaultData>(serviceFault, serviceFault.ErrorMessage);
            }
            catch (Exception generalEx)
            {
                string innerExDetails = generalEx.Message;
                //serviceLogger.Log("Exception raised from TestServices " + innerExDetails, logFileType);
                serviceFault = new ServiceFaultData();
                serviceFault.ErrorMessage = "TestServices Failed";
                throw new FaultException<ServiceFaultData>(serviceFault, serviceFault.ErrorMessage);
            }

            if (errorReport.Count > 0)
                return errorReport.Count;
            else
                return 0;

        }

        public List<TestCase> GetTestCases(string clientName, string siteName, string multipleChoice, int runId, int buildId)
        {
            return Testing.GetTestCases(clientName, siteName, multipleChoice, runId, buildId);
        }

        public List<ClientBO> GetClients(string searchText)
        {
            return Testing.GetClients(searchText);
        }

        public List<ViolationBO> GetViolations(int clientId, int buildId, int runId, int ruleDetailId, string violationType)
        {
            return Testing.GetViolations(clientId, buildId, runId,ruleDetailId, violationType);
        }

        public bool UpdateViolation(string violationDescription, string violationRecommendation, int violationId, string status)
        {
            return Testing.UpdateViolation(violationDescription, violationRecommendation, violationId, status);
        }

        public bool UpdateViolationForManual(string violationDescription, string violationRecommendation, string violationName, string sourceURL, string sourceElement, int violationId, string status)
        {
            return Testing.UpdateViolationForManual(violationDescription, violationRecommendation, violationName, sourceURL, sourceElement, violationId, status);
        }

        public int GetViolationCount(int ClientID)
        {
            return Testing.GetViolationCount(ClientID);
        }

        public ViolationBO GetViolation(List<ViolationBO> violations, int violationId)
        {
            return Testing.GetViolation(violations, violationId);
        }

        public List<SiteBO> GetSiteList(string searchText, string clientName)
        {
            return Testing.GetSiteList(searchText, clientName);
        }

        public List<Build> GetBuilds(string clientName, string siteName)
        {
            return Testing.GetBuilds(clientName, siteName);
        }

        public bool CreateViolation(string violationName, string violationDescription, int runId, string siteName, string violationSourceElement,
                                   int violationSourceLineNumber, string violationRecommendation, string status, int clientId, int buildId)
        {
            return Testing.CreateViolation(violationName, violationDescription, runId,
                                    siteName, violationSourceElement, violationSourceLineNumber,
                                    violationRecommendation, status, clientId,
                                    buildId);
        }
    }
}
