﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AccessibilityServices;
using AccessibilityAnalyser.RuleEngine;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Web.Services.ServiceContracts;
using AccessibilityAnalyser.Common;
namespace AccessibilityAnalyser.Web.Services.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RuleServices" in code, svc and config file together.
    public class RuleServices : IRuleServices
    {
        public List<RuleDetails> GetAllRuleDetails()
        {
            List<RuleDetails> getallRuleDetails = new List<RuleDetails>();

            try
            {
                getallRuleDetails = Rules.GetAllRuleDetails();
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "RuleService", "GetAllRuleDetails");
            }

            return getallRuleDetails;
        }

        public IEnumerable<StandardBO> GetAllGroups()
        {
            IEnumerable<StandardBO> getallGroups = null;

            try
            {
                getallGroups = Rules.GetAllGroups();
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "RuleService", "GetAllGroups");
            }

            return getallGroups;
        }

        public List<RuleDetails> GetRuleDetails(string groupID)
        {
            List<RuleDetails> getRuleDeatils = new List<RuleDetails>();

            try
            {
                getRuleDeatils = Rules.GetRuleDetails(groupID);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "RuleService", "GetRuleDetails");
            }

            return getRuleDeatils;
        }
    }
}
