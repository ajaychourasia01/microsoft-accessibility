﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AccessibilityServices;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.RuleEngine;
using AccessibilityAnalyser.Web.Services.ServiceContracts;
using AccessibilityAnalyser.Common;

namespace AccessibilityAnalyser.Web.Services.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "NewClientService" in code, svc and config file together.
    public class NewClientService : INewClientService
    {
        public List<ClientBO> GetClientList(string searchText)
        {
            List<ClientBO> getClientList = new List<ClientBO>();

            try
            {
                getClientList = NewClient.GetClientList(searchText);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "GetClientList");
            }

            return getClientList;
        }


        public List<SiteBO> GetSiteList(string searchText, int clientID)
        {
            List<SiteBO> getsiteList = new List<SiteBO>();

            try
            {
                getsiteList = NewClient.GetSiteList(searchText, clientID);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "GetSiteList");
            }

            return getsiteList;
        }


        public List<BuildBO> GetBuildList(string searchText, int siteID)
        {
            List<BuildBO> getBuildList = new List<BuildBO>();

            try
            {
                getBuildList = NewClient.GetBuildList(searchText, siteID);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "GetBuildList");
            }

            return getBuildList;
        }


        public List<RunBO> GetRunList(string searchText, int buildID)
        {
            List<RunBO> getRunList = new List<RunBO>();

            try
            {
                getRunList = NewClient.GetRunList(searchText, buildID);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "GetRunList");
            }

            return getRunList;
        }

        public int GetClientID(string clientName)
        {
            int clientId = 0;

            try
            {
                clientId = NewClient.GetClientID(clientName);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "GetClientID");
            }
            return clientId;
        }


        public int GetSiteID(string url, int clientID)
        {
            int siteId = 0;

            try
            {
                siteId = NewClient.GetSiteID(url, clientID);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "GetSiteID");
            }
            return siteId;
        }


        public int GetBuildID(string buildName, int siteID)
        {
            int buildId = 0;
            try
            {
                buildId = NewClient.GetBuildID(buildName, siteID);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "GetBuildID");
            }
            return buildId;
        }


        public int GetRunID(string runName, int buildID, int clientID)
        {
            int runId = 0;
            try
            {
                runId = NewClient.GetRunID(runName, buildID, clientID);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "GetRunID");
            }
            return runId;
        }

        public bool CreateClient(string clientName)
        {
            bool isCreated = false;

            try
            {
                isCreated = NewClient.CreateClient(clientName);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "CreateClient");
            }
            return isCreated;
        }

        public bool CreateSite(string siteName, int clientId)
        {
            bool isCreated = false;
            try
            {
                isCreated = NewClient.CreateSite(siteName, clientId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "CreateSite");
            }

            return isCreated;
        }

        public bool CreateBuild(string buildName, int siteId)
        {
            bool isCreated = false;

            try
            {
                isCreated = NewClient.CreateBuild(buildName, siteId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "CreateBuild");
            }

            return isCreated;
        }

        public bool CreateRun(int clientId, int buildId, string runName)
        {
            bool isCreated = false;
            try
            {
                isCreated = NewClient.CreateRun(clientId, buildId, runName);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "CreateRun");
            }
            return isCreated;
        }

        public bool CreateTestCase(string testCaseName, int buildId, string strTestcaseDesc)
        {
            bool isCreated = false;

            try
            {
                isCreated = NewClient.CreateTestCase(testCaseName, buildId, strTestcaseDesc);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "CreateTestCase");
            }
            return isCreated;
        }

        public bool CreateTestCaseMap(int ruleDetailId, int testCaseId, int iPriority)
        {
            bool isCreated = false;

            try
            {
                isCreated = NewClient.CreateTestCaseMap(ruleDetailId, testCaseId, iPriority);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "CreateTestCaseMap");
            }
            return isCreated;
        }

        public List<TestCaseBO> GetAllDistinctTestCases()
        {
            List<TestCaseBO> getAllDistinctTestcases = new List<TestCaseBO>();

            try
            {
                getAllDistinctTestcases = NewClient.GetAllDistinctTestCases();
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "GetAllDistinctTestCases");
            }

            return getAllDistinctTestcases;
        }

        public List<RuleDetailBO> GetAllDistinctRuleDetails()
        {
            List<RuleDetailBO> getAllDistinctRuleDetails = new List<RuleDetailBO>();
            try
            {
                getAllDistinctRuleDetails = NewClient.GetAllDistinctRuleDetails();
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "GetAllDistinctRuleDetails");
            }
            return getAllDistinctRuleDetails;
        }

        public List<TestCaseBO> GetAllTestCases(int buildId)
        {
            List<TestCaseBO> getAllTestCases = new List<TestCaseBO>();

            try
            {
                getAllTestCases = NewClient.GetAllTestCases(buildId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "GetAllTestCases");
            }
            return getAllTestCases;
        }

        public bool UpdateTestCase(TestCaseBO TestCaseData)
        {
            bool isUpdated = false;

            try
            {
                isUpdated = NewClient.UpdateTestCase(TestCaseData);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "UpdateTestCase");
            }

            return isUpdated;
        }

        public bool DeleteTestCaseRuleMap(TestCaseRuleMapBO TestCaseRuleMapData)
        {
            bool isDeleled = false;

            try
            {
                isDeleled = NewClient.DeleteTestCaseRuleMap(TestCaseRuleMapData);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "DeleteTestCaseRuleMap");
            }

            return isDeleled;
        }

        public bool DeleteMultipleTestCaseRuleMap(List<TestCaseRuleMapBO> TestCaseRuleMapData)
        {
            bool isDeleled = false;
            try
            {
                isDeleled = NewClient.DeleteTestCaseRuleMap(TestCaseRuleMapData);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "DeleteMultipleTestCaseRuleMap");
            }
            return isDeleled;
        }

        public List<TestCaseRuleMapBO> GetAllTestCaseRuleMap(int iTestCaseID)
        {
            List<TestCaseRuleMapBO> getAllTestCaseRuleMap = new List<TestCaseRuleMapBO>();
            try
            {
                getAllTestCaseRuleMap = NewClient.GetAllTestCaseRuleMap(iTestCaseID);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "GetAllTestCaseRuleMap");
            }
            return getAllTestCaseRuleMap;
        }

        public bool DeleteSingleTestCase(int iTestCaseId)
        {
            bool isDeleted = false;

            try
            {
                isDeleted = NewClient.DeleteSingleTestCase(iTestCaseId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "DeleteSingleTestCase");
            }
            return isDeleted;
        }

        public IEnumerable<StandardBO> GetAllGroups()
        {
            IEnumerable<StandardBO> getAllgroups = null;

            try
            {
                getAllgroups = Rules.GetAllGroups();
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "GetAllGroups");
            }
            return getAllgroups;
        }

        public List<RuleDetails> GetRuleDetails(string groupID)
        {
            List<RuleDetails> ruleDetails = new List<RuleDetails>();

            try
            {
                ruleDetails = NewClient.GetRuleDetails(groupID);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "GetRuleDetails");
            }
            return ruleDetails;
        }

        public List<RuleDetails> GetAllRuleDetails()
        {
            List<RuleDetails> getAllRuleDeatils = new List<RuleDetails>();
            try
            {
                getAllRuleDeatils = NewClient.GetAllRuleDetails();
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "GetAllRuleDetails");
            }
            return getAllRuleDeatils;
        }

        public TestCaseBO GetSingleTestCase(int iTestcaseId)
        {
            TestCaseBO getTestCase = new TestCaseBO();

            try
            {
                getTestCase = NewClient.GetSingleTestCase(iTestcaseId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientService", "GetSingleTestCase");
            }
            return getTestCase;
        }
    }
}
