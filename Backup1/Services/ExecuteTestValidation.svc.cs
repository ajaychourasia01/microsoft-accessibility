﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.RuleEngine;
using HtmlAgilityPack;
using System.Text;
using System.Xml;

namespace AccessibilityAnalyser.Web.Services.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ExecuteTestValidation" in code, svc and config file together.
    
    public class ExecuteTestValidation : IExecuteTestValidation
    {
        public int ExecuteTestValidationParam(string selectedURLArray, int clientId, int buildId, int runId, int violationPerBatch, StringBuilder html)
        {
            List<string> selectedURLList = selectedURLArray.Substring(0, selectedURLArray.Length - 1).Split("~".ToCharArray()).ToList();
            string strHtml = html.ToString();
            try
            {
                RuleValidator ruleValidator = new RuleValidator();
                ruleValidator.TestValidation(selectedURLList, clientId, buildId, runId, violationPerBatch, strHtml);
            }
            catch (FaultException ex)
            {
                throw ex;
                //LogException.CatchException(ex, "ExecutionController", "Execution");
            }
            return 30;
     
        }

        public int GetHtmlExtracted(string selectedURLArray, int clientId, int buildId, int runId)
        {                      
            return 10;
        }



    }
}
