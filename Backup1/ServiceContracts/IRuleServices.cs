﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AccessibilityAnalyser.RuleEngine;
using AccessibilityAnalyser.BusinessEntities;

namespace AccessibilityAnalyser.Web.Services.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRuleServices" in both code and config file together.
    [ServiceContract]
    public interface IRuleServices
    {
        [OperationContract]
        List<RuleDetails> GetAllRuleDetails();
        
        [OperationContract]
        IEnumerable<StandardBO> GetAllGroups();

        [OperationContract]
        List<RuleDetails> GetRuleDetails(string groupID);
    }
}
