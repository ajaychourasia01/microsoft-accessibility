using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Threading.Tasks;
using System.Linq;

namespace AccessibilityAnalyser.Web.Services.Helper
{
    public class Crawler
    {
        public int MaxDepth { get; set; }
        private int ThreadCount = 100;
        public List<string> FinalInternalUrlList { get; set; }
        public List<string> FinalExternalUrlList { get; set; }
        public List<string> BadUrlList = new List<string>();
        public string AuthenticationToken { get; set; }

        public Crawler()
        {
            FinalInternalUrlList = new List<string>();
            FinalExternalUrlList = new List<string>();
        }

        #region Private Fields


        #endregion

        /// <summary>
        /// Extracts valid urls from the given web page
        /// </summary>
        /// <param name="url">UrlFormat object that is going to be read</param>
        /// <returns>List of crawled internal urls</returns>
        private List<UrlFormat> ExtractUrls(UrlFormat url)
        {
            CrawledData crawl = new CrawledData();

            string htmlText = GetWebText(url.UrlName);

            if (htmlText != string.Empty)
            {
                LinkParser linkParser = new LinkParser();
                linkParser.ParseLinks(htmlText, url);
                linkParser = AddUrlsToFinalList(linkParser);
                return linkParser.InternalUrls;
            }
            else
            {
                BadUrlList.Add(url.UrlName);
            }

            return null;
        }

        /// <summary>
        /// Adds unique urls only to the final list.
        /// </summary>
        /// <param name="parsedUrls">Contains all parsed valid urls</param>
        /// <returns>Updated LinkParser object by removing existing or invalid urls</returns>        
        private LinkParser AddUrlsToFinalList(LinkParser parsedUrls)
        {
            List<UrlFormat> ignoredUrls = new List<UrlFormat>();            

            foreach (UrlFormat url in parsedUrls.ExternalUrls)
            {
                if (url != null && !FinalExternalUrlList.Contains(url.UrlName))
                {
                    FinalExternalUrlList.Add(url.UrlName);
                }
            }            

            foreach (UrlFormat url in parsedUrls.InternalUrls)
            {
                if (url != null && !FinalInternalUrlList.Contains(url.UrlName))
                {
                    FinalInternalUrlList.Add(url.UrlName);
                }
                else
                {
                    ignoredUrls.Add(url);
                }
            }            

            //Removing existing or invalid URLs from the extracted list
            foreach (UrlFormat url in ignoredUrls)
            {
                parsedUrls.InternalUrls.Remove(url);
            }

            return parsedUrls;
        }

        /// <summary>
        /// Initiates crawler threads depending on the number of urls to crawl.
        /// Manages the performance by restricting the parallel threads to 100.
        /// </summary>
        /// <param name="crawledData">Contains the urls to crawl</param>
        /// <returns>Updated crawledData object with crawled urls</returns>
        private CrawledData StartCrawling(CrawledData crawledData)
        {

            CrawledData resultData = new CrawledData();

            //Check the url count for crawling
            if (crawledData.Urls.Count <= ThreadCount)
            {
                //Start 100 threads to crawl 100 urls
                List<UrlFormat> crawledUrlList = StartCrawlerThreads(0, crawledData.Urls.Count, crawledData.Urls);
                resultData.Urls.AddRange(crawledUrlList);
            }
            else
            {
                //Split the urls into sets of 100 each and start crawling 100 urls in each loop.
                for (int set = 0; set < crawledData.Urls.Count / ThreadCount; set++)
                {
                    List<UrlFormat> crawledLoopWiseUrlList = StartCrawlerThreads(set * ThreadCount, (set * ThreadCount) + ThreadCount, crawledData.Urls);
                    resultData.Urls.AddRange(crawledLoopWiseUrlList);
                }

                //Crawl the reminders
                List<UrlFormat> crawledReminderUrlList = StartCrawlerThreads((crawledData.Urls.Count / ThreadCount) * ThreadCount, ((crawledData.Urls.Count / ThreadCount) * ThreadCount) + (crawledData.Urls.Count % ThreadCount), crawledData.Urls);
                resultData.Urls.AddRange(crawledReminderUrlList);
            }

            resultData.Depth = crawledData.Depth + 1;
            return resultData;
        }

        /// <summary>
        /// Crawls set of urls asynchronously by running parallel tasks.
        /// </summary>
        /// <param name="startIndex">Start index of the list to crawl</param>
        /// <param name="endIndex">End index of the list to crawl</param>
        /// <param name="unCrawledUrlList">Uncrawled urls list</param>
        /// <returns>List of crawled urls out of each sub set of the actual set of urls</returns>
        public List<UrlFormat> StartCrawlerThreads(int startIndex, int endIndex, List<UrlFormat> unCrawledUrlList)
        {
            List<UrlFormat> resultData = new List<UrlFormat>();

            var asyncCrawling = new Action<UrlFormat>((urlFormat) =>
            {
                List<UrlFormat> extractedUrlList = ExtractUrls(urlFormat);
                urlFormat.IsCrawled = true;

                if (extractedUrlList != null)
                    resultData.AddRange(extractedUrlList);
            }
            );

            int cnt = 0;
            Task[] crawlTasks = new Task[endIndex - startIndex];

            for (int i = startIndex; i < endIndex; i++)
            {
                crawlTasks[cnt] = Task.Factory.FromAsync(asyncCrawling.BeginInvoke, asyncCrawling.EndInvoke, unCrawledUrlList[i], asyncCrawling);
                cnt += 1;
            }

            Task.WaitAll(crawlTasks);

            return resultData;
        }

        /// <summary>
        /// Returns unCrawled urls list
        /// </summary>
        /// <param name="crawledList">List that contains both crawled & uncrawled urls</param>
        /// <param name="isExternal">Boolean value to query the list for either internal or external urls</param>
        /// <returns>Uncrawled urls list</returns>
        private List<UrlFormat> GetUrls(List<UrlFormat> crawledList, bool isExternal)
        {
            List<UrlFormat> unCrawledUrls = (from url in crawledList
                                             where url.IsExternal == isExternal &&
                                             url.IsCrawled == false
                                             select url).Distinct().ToList();
            return unCrawledUrls;
        }

        /// <summary>
        /// Crawls a page.
        /// </summary>
        /// <param name="url">The url to crawl.</param>
        public void CrawlPage(CrawledData crawledData)
        {
            //This block will run if maximum depth is set to numeric value.
            if (MaxDepth > 0)
            {
                if (crawledData.Depth < MaxDepth)
                {
                    List<UrlFormat> internalUrls = GetUrls(crawledData.Urls, false);

                    List<UrlFormat> externalUrls = GetUrls(crawledData.Urls, true);

                    crawledData.Urls.Clear();
                    crawledData.Urls = internalUrls;

                    CrawledData crawlData = StartCrawling(crawledData);

                    crawledData.Urls.AddRange(externalUrls);

                    CrawlPage(crawlData);
                }
            }
            else  //This block will run if maximum depth is set to All.
            {
                if (crawledData.Urls.Count > 0)
                {
                    List<UrlFormat> internalUrls = GetUrls(crawledData.Urls, false);

                    List<UrlFormat> externalUrls = GetUrls(crawledData.Urls, true);

                    crawledData.Urls.Clear();
                    crawledData.Urls = internalUrls;

                    CrawledData crawlData = StartCrawling(crawledData);

                    crawledData.Urls.AddRange(externalUrls);

                    CrawlPage(crawlData);
                }
            }
        }

        /// <summary>
        /// Gets the response text for a given url.
        /// </summary>
        /// <param name="url">The url whose text needs to be fetched.</param>
        /// <returns>The text of the response.</returns>
        private string GetWebText(string url)
        {
            string html = string.Empty;
            using (WebClient client = new WebClient())
            {
                Uri innUri = null;
                Uri.TryCreate(url, UriKind.RelativeOrAbsolute, out innUri);

                try
                {
                    //client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 6.1; .NET CLR 1.0.3705;)");
                    client.Headers.Add("User-Agent: Other");
                    client.Headers.Add("webauthtoken", AuthenticationToken);

                    using (StreamReader str = new StreamReader(client.OpenRead(innUri)))
                    {
                        html = str.ReadToEnd();
                    }
                }
                catch (Exception)
                {
                    //return we.Message;
                    return html;
                }
                return html;
            }

            //string htmlText = string.Empty;
            //try
            //{
            //    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            //    // request.KeepAlive = false;
            //    request.Method = "GET";
            //    //request.ContentType = "text/html";                                

            //    if (url.StartsWith("https"))
            //    {
            //        //request.Headers.Add("webauthtoken", AuthenticationToken);
            //        request.AllowAutoRedirect = true;
            //        request.MaximumAutomaticRedirections = 4;
            //    }
            //    else
            //    {
            //        request.MaximumResponseHeadersLength = 4;
            //        request.AllowAutoRedirect = false;
            //    }

            //    //CookieContainer cookieContainer = new CookieContainer();
            //    //cookieContainer.Add(new Cookie("webauthtoken", AuthenticationToken));
            //    //request.CookieContainer = cookieContainer;

            //    WebResponse response = request.GetResponse();

            //    Stream stream = response.GetResponseStream();

            //    Encoding enc = System.Text.Encoding.GetEncoding(1252);
            //    StreamReader reader = null;

            //    if (url.StartsWith("https"))
            //    {
            //        reader = new StreamReader(response.GetResponseStream(), enc);
            //    }
            //    else
            //    {
            //        reader = new StreamReader(stream);
            //    }

            //    htmlText = reader.ReadToEnd();
            //    reader.Close();
            //    response.Close();

            //    return htmlText;
            //}
            // catch (WebException we)
            //{
            //    throw we;
            //}            
        }
    }
}
