﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Interfaces;
using AccessibilityAnalyser.Entities;
using System.Configuration;
using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.RuleEngine;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Web;


namespace AccessibilityAnalyser.CrawlExecutionSequencer
{
    public partial class CrawlExecutionService : ServiceBase
    {
        string _uploadedFilePath;
        readonly int violationsPerBatch = Convert.ToInt32(ConfigurationManager.AppSettings["ViolPerBatch"]);

        readonly double timeInterval = Convert.ToDouble(ConfigurationManager.AppSettings["TimeInterval"]);
        System.Timers.Timer _timer = new System.Timers.Timer();


        public CrawlExecutionService()
        {
            InitializeComponent();
           
        }

        protected override void OnStart(string[] args)
        {
            DateTime t = DateTime.Now.AddMinutes(timeInterval);
            TimeSpan ts = new TimeSpan();
            ts = t - System.DateTime.Now;

            _timer.Interval = ts.TotalMilliseconds;
            _timer.Elapsed += new System.Timers.ElapsedEventHandler(_timer_Elapsed);
            _timer.Enabled = true;                     
        }

        protected override void OnStop()
        {

        }

        // This method you can write in service class of windows service. 
        void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {                
                #region Process ProcessFeefoResults
                //_timer.Stop();
                //Call the ProcessFeefoResults                     

                WindowsServiceSchedler(_timer);                
                #endregion
            }

            catch (Exception)
            {
                _timer.Start();

            }

        }

        private List<string> ReadUrlsFromUploadedFile()
        {            
            List<string> urlList = new List<string>();

            if (System.IO.File.Exists(_uploadedFilePath))
            {
                try
                {
                    Excel.Workbook workbook;
                    Excel.Worksheet NwSheet;
                    Excel.Range ShtRange;
                    Excel.Application appExl = new Excel.Application();

                    workbook = appExl.Workbooks.Open(_uploadedFilePath, Type.Missing, Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    NwSheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets.get_Item(1);

                    ShtRange = NwSheet.UsedRange; //gives the used cells in sheet                    

                    Excel.Range urlRange = ShtRange.get_Range("B7", "B" + ShtRange.Rows.Count.ToString());

                    foreach (Excel.Range url in urlRange.Rows)
                    {
                        urlList.Add(url.Text);
                    }

                    workbook.Close(false, Type.Missing, Type.Missing);
                    workbook = null;

                    appExl.Quit();
                    appExl = null;
                }
                catch (Exception)
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                finally
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }

            }
            
            return urlList;
        }

        // This the static method  can be used for WindowsServiceSchedler 
        private void WindowsServiceSchedler(System.Timers.Timer _timer)
        {
            try
            {                
                var waitingUsersList = GetWaitingUsers();                
                bool updateLogSuccess = false;

                foreach (ServiceLogBO serviceLog in waitingUsersList)
                {
                    _uploadedFilePath = serviceLog.FileName;                  
                    List<string> urlList = ReadUrlsFromUploadedFile();                  

                    updateLogSuccess = UpdateServiceLog(serviceLog.ClientId, serviceLog.RunId, serviceLog.BuildId, "Inprogress", serviceLog.RequestTime);
                    
                    try
                    {
                        HTMLExtractor.AuthenticationToken = "eyJhbGciOiJIUzI1NiIsImtpZCI6IjAiLCJ0eXAiOiJKV1QifQ.eyJ2ZXIiOjEsImlzcyI6InVybjp3aW5kb3dzOmxpdmVpZCIsImV4cCI6MTM2NDg4MTQyMCwidWlkIjoiMDVlY2U4OTExZjFmMzRjZmViZmQ3ODJjNGRlYjVjZjAiLCJhdWQiOiJteXdlYmNyYXdsZXIuY29tIiwidXJuOm1pY3Jvc29mdDphcHB1cmkiOiJhcHBpZDovLzAwMDAwMDAwNDQwRThDQTgiLCJ1cm46bWljcm9zb2Z0OmFwcGlkIjoiMDAwMDAwMDA0NDBFOENBOCJ9.-8pKRB4q3G9f3DyI2qQ3WAGS23b3D0t3lolebz0qg88";
                        RuleValidator ruleValidator = new RuleValidator();
                        ruleValidator.TestValidation(urlList, serviceLog.ClientId, serviceLog.BuildId, serviceLog.RunId, violationsPerBatch);
                        updateLogSuccess = UpdateServiceLog(serviceLog.ClientId, serviceLog.RunId, serviceLog.BuildId, "Completed", serviceLog.RequestTime);
                    }
                    catch (Exception)
                    {
                        updateLogSuccess = UpdateServiceLog(serviceLog.ClientId, serviceLog.RunId, serviceLog.BuildId, "Failed", serviceLog.RequestTime);
                    }
                }
            }
            catch(Exception ex)
            {
                LogException.CatchException(ex, "CrawlExecutionSequencer", "WindowsServiceSchedler");
            }
        }

        /// <summary>
        /// Getting a list of all waiting users
        /// </summary>
        /// <returns></returns>
        public List<ServiceLogBO> GetWaitingUsers()
        {
            List<ServiceLogBO> getWaitingUsers = new List<ServiceLogBO>();
            try
            {                
                getWaitingUsers = Executions.GetWaitingUsers();
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "CrawlExecutionSequencer", "GetWaitingUsers");
            }
            return getWaitingUsers;
        }


        /// <summary>
        /// Update the status of the Service log
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="runId"></param>
        /// <param name="buildId"></param>
        /// <param name="status"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public bool UpdateServiceLog(int clientId, int runId, int buildId, string status, DateTime date)
        {
            bool isUpdated = false;

            try
            {
                isUpdated = Executions.UpdateServiceLog(clientId, runId, buildId, status, date);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "CrawlExecutionSequencer", "UpdateServiceLog");
            }
            return isUpdated;
        }        
    }
}
