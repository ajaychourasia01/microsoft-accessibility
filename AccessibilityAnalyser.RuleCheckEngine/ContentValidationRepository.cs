﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using AccessibilityAnalyser.BusinessEntities;
using HtmlAgilityPack;
using AccessibilityAnalyser.Common;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Net;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
namespace AccessibilityAnalyser.RuleEngine
{
    public class ContentValidationRepository
    {

        static ResourceManager resourceManager = new ResourceManager("AccessibilityAnalyser.Common.ApplicationResources", typeof(ApplicationResources).Assembly);

        /// <summary>
        /// Checks if mouse events has comparable key events : MAS 36
        /// </summary>
        /// <param name="htmlDom"></param>
        /// <returns></returns>
        public static List<ViolationBO> HasKeyEventsForMouseEvents(HtmlDocument htmlDom, Dictionary<string, string> classProperties, ref bool isPassed)
        {
            bool onMouseDown = false;
            bool onKeyDown = false;
            bool onMouseOut = false;
            bool onBlur = false;
            bool onMouseOver = false;
            bool onFocus = false;
            bool onMouseUp = false;
            bool onKeyUp = false;
            bool onClick = false;
            bool onKeyPress = false;

            string onMouseDownValue = string.Empty;
            string onKeyDownValue = string.Empty;
            string onMouseOutValue = string.Empty;
            string onBlurValue = string.Empty;
            string onMouseOverValue = string.Empty;
            string onFocusValue = string.Empty;
            string onMouseUpValue = string.Empty;
            string onKeyUpValue = string.Empty;
            string onClickValue = string.Empty;
            string onKeyPressValue = string.Empty;

            List<ViolationBO> violationList = new List<ViolationBO>();
            isPassed = true;

            try
            {
                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HtmlTag")) != null)
                {
                    foreach (HtmlNode node in htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HtmlTag")))
                    {
                        if (node != null)
                        {
                            foreach (HtmlNode descendentNode in node.Descendants())
                            {
                                bool avoidInputSubmit = (descendentNode.Attributes["type"] != null && descendentNode.Attributes["type"].Value.ToLower() == "submit") ? true : false;

                                if (descendentNode.Name != "a" && !avoidInputSubmit)
                                {
                                    onMouseDown = NodeValidationRepository.HasAttribute(descendentNode, resourceManager.GetString("MouseDown"));
                                    onKeyDown = NodeValidationRepository.HasAttribute(descendentNode, resourceManager.GetString("KeyDown"));
                                    onMouseOut = NodeValidationRepository.HasAttribute(descendentNode, resourceManager.GetString("MouseOut"));
                                    onBlur = NodeValidationRepository.HasAttribute(descendentNode, resourceManager.GetString("OnBlur"));
                                    onMouseOver = NodeValidationRepository.HasAttribute(descendentNode, resourceManager.GetString("MouseOver"));
                                    onFocus = NodeValidationRepository.HasAttribute(descendentNode, resourceManager.GetString("OnFocus"));
                                    onMouseUp = NodeValidationRepository.HasAttribute(descendentNode, resourceManager.GetString("MouseUp"));
                                    onKeyUp = NodeValidationRepository.HasAttribute(descendentNode, resourceManager.GetString("KeyUp"));
                                    onClick = NodeValidationRepository.HasAttribute(descendentNode, resourceManager.GetString("OnClick"));
                                    onKeyPress = NodeValidationRepository.HasAttribute(descendentNode, resourceManager.GetString("OnKeyPress"));

                                    onMouseDownValue = NodeValidationRepository.GetAttributeValueInLowerCase(descendentNode, resourceManager.GetString("MouseDown"));
                                    onKeyDownValue = NodeValidationRepository.GetAttributeValueInLowerCase(descendentNode, resourceManager.GetString("KeyDown"));
                                    onMouseOutValue = NodeValidationRepository.GetAttributeValueInLowerCase(descendentNode, resourceManager.GetString("MouseOut"));
                                    onBlurValue = NodeValidationRepository.GetAttributeValueInLowerCase(descendentNode, resourceManager.GetString("OnBlur"));
                                    onMouseOverValue = NodeValidationRepository.GetAttributeValueInLowerCase(descendentNode, resourceManager.GetString("MouseOver"));
                                    onFocusValue = NodeValidationRepository.GetAttributeValueInLowerCase(descendentNode, resourceManager.GetString("OnFocus"));
                                    onMouseUpValue = NodeValidationRepository.GetAttributeValueInLowerCase(descendentNode, resourceManager.GetString("MouseUp"));
                                    onKeyUpValue = NodeValidationRepository.GetAttributeValueInLowerCase(descendentNode, resourceManager.GetString("KeyUp"));
                                    onClickValue = NodeValidationRepository.GetAttributeValueInLowerCase(descendentNode, resourceManager.GetString("OnClick"));
                                    onKeyPressValue = NodeValidationRepository.GetAttributeValueInLowerCase(descendentNode, resourceManager.GetString("OnKeyPress"));


                                    if ((onMouseDown && !onKeyDown) || (!onMouseDown && onKeyDown))
                                    {
                                        ViolationBO violation = new ViolationBO();

                                        violation.SourceElement = (!string.IsNullOrEmpty(descendentNode.InnerHtml.Trim())) ? descendentNode.OuterHtml.Replace(descendentNode.InnerHtml, "")
                                           : descendentNode.OuterHtml;

                                        violation.SourceLineNumber = descendentNode.Line;
                                        violation.ViolationDescription = (onMouseDown) ? resourceManager.GetString("MissingKeyDownEvent") : resourceManager.GetString("MissingMouseDownEvent"); //RuleDetail.ErrorDescription

                                        violationList.Add(violation);
                                    }
                                    if (onMouseDownValue != onKeyDownValue)
                                    {
                                        ViolationBO violation = new ViolationBO();

                                        violation.SourceElement = (!string.IsNullOrEmpty(descendentNode.InnerHtml.Trim())) ? descendentNode.OuterHtml.Replace(descendentNode.InnerHtml, "")
                                            : descendentNode.OuterHtml;
                                        violation.SourceLineNumber = descendentNode.Line;
                                        violation.ViolationDescription = resourceManager.GetString("OnMouseDownOnKeyDownActionMismatch");

                                        violationList.Add(violation);
                                    }
                                    else if ((onMouseOut && !onBlur) || (!onMouseOut && onBlur))
                                    {
                                        ViolationBO violation = new ViolationBO();

                                        violation.SourceElement = (!string.IsNullOrEmpty(descendentNode.InnerHtml.Trim())) ? descendentNode.OuterHtml.Replace(descendentNode.InnerHtml, "")
                                           : descendentNode.OuterHtml;
                                        violation.SourceLineNumber = descendentNode.Line;
                                        violation.ViolationDescription = (onMouseOut) ? resourceManager.GetString("MissingOnBlurEvent") : resourceManager.GetString("MissingOnMouseOut");

                                        violationList.Add(violation);
                                    }
                                    else if (onMouseOutValue != onBlurValue)
                                    {
                                        ViolationBO violation = new ViolationBO();

                                        violation.SourceElement = (!string.IsNullOrEmpty(descendentNode.InnerHtml.Trim())) ? descendentNode.OuterHtml.Replace(descendentNode.InnerHtml, "")
                                           : descendentNode.OuterHtml;
                                        violation.SourceLineNumber = descendentNode.Line;
                                        violation.ViolationDescription = resourceManager.GetString("OnMouseOutOnBlurActionMismatch");

                                        violationList.Add(violation);
                                    }
                                    else if ((onMouseOver && !onFocus) || (!onMouseOver && onFocus))
                                    {
                                        ViolationBO violation = new ViolationBO();

                                        violation.SourceElement = (!string.IsNullOrEmpty(descendentNode.InnerHtml.Trim())) ? descendentNode.OuterHtml.Replace(descendentNode.InnerHtml, "")
                                           : descendentNode.OuterHtml;
                                        violation.SourceLineNumber = descendentNode.Line;
                                        violation.ViolationDescription = (onMouseOver) ? resourceManager.GetString("MissingOnFocusEvent") : resourceManager.GetString("MissingOnMouseOverEvent");

                                        violationList.Add(violation);
                                    }
                                    else if (onMouseOverValue != onFocusValue)
                                    {
                                        ViolationBO violation = new ViolationBO();

                                        violation.SourceElement = (!string.IsNullOrEmpty(descendentNode.InnerHtml.Trim())) ? descendentNode.OuterHtml.Replace(descendentNode.InnerHtml, "")
                                           : descendentNode.OuterHtml;
                                        violation.SourceLineNumber = descendentNode.Line;
                                        violation.ViolationDescription = resourceManager.GetString("OnMouseOverOnFocusActionMismatch");

                                        violationList.Add(violation);
                                    }
                                    else if ((onMouseUp && !onKeyUp) || (!onMouseUp && onKeyUp))
                                    {
                                        ViolationBO violation = new ViolationBO();

                                        violation.SourceElement = (!string.IsNullOrEmpty(descendentNode.InnerHtml.Trim())) ? descendentNode.OuterHtml.Replace(descendentNode.InnerHtml, "")
                                           : descendentNode.OuterHtml;
                                        violation.SourceLineNumber = descendentNode.Line;
                                        violation.ViolationDescription = (onMouseUp) ? resourceManager.GetString("MissingOnKeyUpEvent") : resourceManager.GetString("MissingOnMouseUpEvent");

                                        violationList.Add(violation);
                                    }
                                    else if (onMouseUpValue != onKeyUpValue)
                                    {
                                        ViolationBO violation = new ViolationBO();

                                        violation.SourceElement = (!string.IsNullOrEmpty(descendentNode.InnerHtml.Trim())) ? descendentNode.OuterHtml.Replace(descendentNode.InnerHtml, "")
                                           : descendentNode.OuterHtml;
                                        violation.SourceLineNumber = descendentNode.Line;
                                        violation.ViolationDescription = resourceManager.GetString("OnMouseUpOnKeyUpActionMismatch");

                                        violationList.Add(violation);
                                    }

                                    else if ((onClick && !onKeyPress) || (!onClick && onKeyPress))
                                    {
                                        ViolationBO violation = new ViolationBO();

                                        violation.SourceElement = (!string.IsNullOrEmpty(descendentNode.InnerHtml.Trim())) ? descendentNode.OuterHtml.Replace(descendentNode.InnerHtml, "")
                                           : descendentNode.OuterHtml;
                                        violation.SourceLineNumber = descendentNode.Line;
                                        violation.ViolationDescription = (onClick) ? resourceManager.GetString("MissingOnKeyPressEvent") : resourceManager.GetString("MissingOnClickEvent");

                                        violationList.Add(violation);
                                    }
                                    else if (onClickValue != onKeyPressValue)
                                    {
                                        ViolationBO violation = new ViolationBO();

                                        violation.SourceElement = (!string.IsNullOrEmpty(descendentNode.InnerHtml.Trim())) ? descendentNode.OuterHtml.Replace(descendentNode.InnerHtml, "")
                                           : descendentNode.OuterHtml;
                                        violation.SourceLineNumber = descendentNode.Line;
                                        violation.ViolationDescription = resourceManager.GetString("OnClickOnKeyPressActionMismatch");

                                        violationList.Add(violation);
                                    }
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasKeyEventsForMouseEvents");
                isPassed = false;
            }
            return violationList;
        }

        /// <summary>
        /// Checks if heading tags are present in proper hierarchy : MAS 20,Rule2
        /// ***Ajay***  Page headings and labels for form and interactive controls are informative. Avoid 
        ///             duplicating heading (e.g., "More Details") or label text (e.g., "First Name") unless the
        ///             structure provides adequate differentiation between them.
        /// </summary>
        /// <param name="htmlDom"></param>
        /// <returns></returns>
        public static List<ViolationBO> HeadingTagVerfication(HtmlDocument htmlDom, Dictionary<string, string> classProperties, ref bool isPassed)
        {
            ArrayList arrayList = new ArrayList();
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<ViolationBO> violationList_Final = new List<ViolationBO>();

            List<HtmlNode> lstHeaders = new List<HtmlNode>();
            List<HtmlNode> lstAllHeader = new List<HtmlNode>();
            dynamic temp = 0;
            isPassed = true;

            try
            {

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H7Tag")) != null)
                    lstHeaders.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H7Tag")));
                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H6Tag")) != null)
                    lstHeaders.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H6Tag")));
                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H5Tag")) != null)
                    lstHeaders.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H5Tag")));
                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H4Tag")) != null)
                    lstHeaders.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H4Tag")));
                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H3Tag")) != null)
                    lstHeaders.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H3Tag")));
                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H2Tag")) != null)
                    lstHeaders.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H2Tag")));
                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H1Tag")) != null)
                    lstHeaders.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H1Tag")));

                //Check for diplay : None for in-line and class 
                if (lstHeaders.Count > 0)
                {
                    lstAllHeader = RuleEngineHelper.getAllValidNodes(lstHeaders, classProperties);
                }

                var lstHeader_temp = (from node in lstAllHeader
                                      where (!string.IsNullOrEmpty(node.InnerText))
                                      select new
                                      {
                                          node.Name,
                                          node.Line,
                                          node.OuterHtml,
                                      }).ToList();
                arrayList.AddRange(lstHeader_temp);

                arrayList.Reverse();
                ArrayList tempList = new ArrayList(arrayList);

                if (arrayList.Count != 0)
                {
                    int j = 0;
                    List<int> headerList = null;
                    List<int> missingTags = null;
                    string[] actualArray1, tempArray1, actualArray2, tempArray2;
                    string node1, nodeNumber1 = string.Empty, nodeOuterHtml1 = string.Empty, tempNode1, tempNodeNumber1, tempNodeOuterHtml1;
                    string node2, tempNodeNumber2, tempNodeOuterHtml2;
                    headerList = new List<int>();

                    //Fetches tags status 1 - h1, 2 - h2 and so on in a list
                    for (int index = 0; index < arrayList.Count; index++)
                    {
                        actualArray2 = arrayList[index].ToString().Split(',');
                        node2 = actualArray2[0].Substring(10);
                        headerList.Add(Convert.ToInt32(node2));
                    }

                    //Checks if any tags are missing in the hierarchy.
                    if (headerList.Any())
                    {
                        //Fetches the max header from the list
                        int maxHeader = headerList.Max();

                        //Checks if the max header is greater than 1 to continue
                        if (maxHeader > 1)
                        {
                            StringBuilder builder = null;
                            missingTags = new List<int>();
                            for (int hIndex = 1; hIndex < maxHeader; hIndex++)
                            {
                                if (headerList.Contains(hIndex))
                                {
                                    continue;
                                }
                                else
                                {
                                    missingTags.Add(hIndex);
                                }
                            }

                            //Builds the string message if any tags are missing
                            if (missingTags.Any())
                            {
                                builder = new StringBuilder();
                                foreach (int tags in missingTags)
                                {
                                    builder = builder.Append(resourceManager.GetString("HeaderTag") + tags.ToString() + ",");
                                }

                                builder = builder.Remove(builder.Length - 1, 1);

                                //Geneates the violations for list of all the header tags
                                for (int d = 0; d < arrayList.Count; d++)
                                {
                                    ViolationBO violation = new ViolationBO();
                                    tempArray2 = tempList[d].ToString().Split(',');

                                    //Get the Current Header No.
                                    var headerNo = Convert.ToInt32(tempArray2[0].Substring(10));

                                    tempNodeOuterHtml2 = tempArray2[2].Substring(0, tempArray2[2].Length - 2).Substring(13);
                                    tempNodeNumber2 = tempArray2[1].Substring(7);

                                    //Check if Any of the Missing Tags is less than the current HeaderNo 
                                    if (missingTags.Any(i => i < headerNo))
                                    {
                                        violation.SourceElement = tempNodeOuterHtml2;
                                        violation.SourceLineNumber = Convert.ToInt32(tempNodeNumber2);
                                        violation.ViolationDescription = builder + " " + resourceManager.GetString("HTagNotFound");
                                        violationList.Add(violation);
                                    }
                                }
                            }
                        }
                    }


                    //Checks if header order appears in proper hierarchy based on line position
                    for (int i = 0; i <= arrayList.Count - 1; i++)
                    {
                        actualArray1 = arrayList[i].ToString().Split(',');
                        node1 = actualArray1[0].Substring(10);
                        nodeNumber1 = actualArray1[1].Substring(7);
                        tempArray1 = arrayList[j].ToString().Split(',');
                        tempNode1 = tempArray1[0].Substring(10);
                        tempNodeNumber1 = tempArray1[1].Substring(7);
                        tempNodeOuterHtml1 = tempArray1[2].Substring(0, tempArray1[2].Length - 2).Substring(13);

                        if (Convert.ToInt32(node1) == Convert.ToInt32(tempNode1))
                        {
                            continue;
                        }
                        else if (Convert.ToInt32(node1) == Convert.ToInt32(tempNode1) + 1)
                        {
                            j = i;
                            arrayList[j] = arrayList[i];
                            //checks if longer order tags exists prior to higher header tag
                            if (Convert.ToInt32(nodeNumber1) > Convert.ToInt32(tempNodeNumber1))
                            {
                                continue;
                            }
                            else
                            {
                                for (int b = j; b < arrayList.Count; b++)
                                {
                                    string[] loopArray1 = arrayList[b].ToString().Split(',');
                                    string loopLineNumber = loopArray1[1].Substring(7);
                                    string loopOuterHtml = loopArray1[2].Substring(0, loopArray1[2].Length - 2).Substring(13);

                                    ViolationBO violation = new ViolationBO();
                                    violation.SourceElement = loopOuterHtml;
                                    violation.SourceLineNumber = Convert.ToInt32(loopLineNumber);
                                    violation.ViolationDescription = resourceManager.GetString("HeadingTagsNotInOrder");
                                    violationList.Add(violation);
                                }
                                i = j = arrayList.Count;
                            }
                        }
                        else
                        {
                            int c = i;
                            int a = c;
                            for (; a < arrayList.Count; a++)
                            {
                                string[] loopArray1 = arrayList[a].ToString().Split(',');
                                string loopLineNumber = loopArray1[1].Substring(7);
                                string loopOuterHtml = loopArray1[2].Substring(0, loopArray1[2].Length - 2).Substring(13);

                                ViolationBO violation = new ViolationBO();
                                violation.SourceElement = loopOuterHtml;
                                violation.SourceLineNumber = Convert.ToInt32(loopLineNumber);
                                violation.ViolationDescription = resourceManager.GetString("HeadingTagsNotInOrder");
                                violationList.Add(violation);
                            }
                            i = a;
                        }
                    }

                    #region OldImplementation
                    //int j = 0, k = 0;
                    //string[] actualArray1, tempArray1, actualArray2, tempArray2;
                    //string node1, nodeNumber1 = string.Empty, nodeOuterHtml1 = string.Empty, tempNode1, tempNodeNumber1, tempNodeOuterHtml1;
                    //string node2, nodeNumber2, nodeOuterHtml2, tempNode2, tempNodeNumber2, tempNodeOuterHtml2;

                    //actualArray2 = tempList[tempList.Count - 1].ToString().Split(',');
                    //node2 = actualArray2[0].Substring(10);
                    //nodeNumber2 = actualArray2[1].Substring(7);
                    //nodeOuterHtml2 = actualArray2[2].Substring(0, actualArray2[2].Length - 2).Substring(13);

                    //tempArray2 = tempList[k].ToString().Split(',');
                    //tempNode2 = tempArray2[0].Substring(10);
                    //tempNodeNumber2 = tempArray2[1].Substring(7);
                    //tempNodeOuterHtml2 = tempArray2[2].Substring(0, tempArray2[2].Length - 2).Substring(13);

                    //if (!(Convert.ToInt32(node2) >= 1 && (Convert.ToInt32(tempNode2)) == 1))
                    //{
                    //    for (int d = 0; d < arrayList.Count; d++)
                    //    {
                    //        ViolationBO violation = new ViolationBO();
                    //        tempArray2 = tempList[d].ToString().Split(',');
                    //        tempNodeOuterHtml2 = tempArray2[2].Substring(0, tempArray2[2].Length - 2).Substring(13);
                    //        tempNodeNumber2 = tempArray2[1].Substring(7);

                    //        violation.SourceElement = tempNodeOuterHtml2;
                    //        violation.SourceLineNumber = Convert.ToInt32(tempNodeNumber2);
                    //        violation.ViolationDescription = "H" + (Convert.ToInt32(tempNode2) - 1).ToString() + resourceManager.GetString("HTagNotFound");
                    //        violationList.Add(violation);
                    //    }
                    //}
                    //else
                    //{
                    //    for (int i = 0; i <= arrayList.Count - 1; i++)
                    //    {
                    //        actualArray1 = arrayList[i].ToString().Split(',');
                    //        node1 = actualArray1[0].Substring(10);

                    //        tempArray1 = arrayList[j].ToString().Split(',');
                    //        tempNode1 = tempArray1[0].Substring(10);
                    //        tempNodeNumber1 = tempArray1[1].Substring(7);
                    //        tempNodeOuterHtml1 = tempArray1[2].Substring(0, tempArray1[2].Length - 2).Substring(13);

                    //        if (Convert.ToInt32(node1) == Convert.ToInt32(tempNode1))
                    //        {
                    //            continue;
                    //        }
                    //        else if (Convert.ToInt32(node1) == Convert.ToInt32(tempNode1) + 1)
                    //        {
                    //            j = i;
                    //            arrayList[j] = arrayList[i];
                    //        }
                    //        else
                    //        {
                    //            int c = i;
                    //            int a = c;
                    //            for (; a < arrayList.Count; a++)
                    //            {
                    //                string[] loopArray1 = arrayList[a].ToString().Split(',');
                    //                string loopLineNumber = loopArray1[1].Substring(7);
                    //                string loopOuterHtml = loopArray1[2].Substring(0, loopArray1[2].Length - 2).Substring(13);

                    //                ViolationBO violation = new ViolationBO();
                    //                violation.SourceElement = loopOuterHtml;
                    //                violation.SourceLineNumber = Convert.ToInt32(loopLineNumber);
                    //                violation.ViolationDescription = resourceManager.GetString("HeadingTagsNotInOrder");
                    //                violationList.Add(violation);
                    //            }
                    //            i = a;
                    //        }
                    //    }
                    //} 
                    #endregion
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HeadingTagVerfication");
                isPassed = false;
            }
            return violationList;
        }


        /// <summary>
        /// Checks if descriptive text is present for links,label and button : MAS 20,Rule3
        /// </summary>
        /// <param name="htmlDom"></param>
        /// <returns></returns>
        //public static List<ViolationBO> ValidateTextForLinkLabelButton(HtmlDocument htmlDom, ref bool isPassed)
        //{
        //    List<ViolationBO> violationList = new List<ViolationBO>();
        //    isPassed = true;

        //    try
        //    {
        //        HtmlNodeCollection hrefNode = htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HrefTag"));
        //        HtmlNodeCollection labelNode = htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("LabelTag"));
        //        HtmlNodeCollection buttonNode = htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("ButtonTag"));

        //        if (hrefNode != null)
        //        {
        //            foreach (HtmlNode node in hrefNode)
        //            {
        //                if (string.IsNullOrEmpty(node.InnerText))
        //                {
        //                    ViolationBO violation = new ViolationBO();

        //                    violation.SourceElement = node.OuterHtml;
        //                    violation.SourceLineNumber = node.Line;
        //                    violation.ViolationDescription = resourceManager.GetString("LinkTextIsNotDescriptive");

        //                    violationList.Add(violation);
        //                }
        //            }
        //        }

        //        if (labelNode != null)
        //        {
        //            foreach (HtmlNode node in labelNode)
        //            {
        //                string labelText = (node.Attributes["title"] != null) ? node.Attributes["title"].Value.Trim() : string.Empty;

        //                if (string.IsNullOrEmpty(node.InnerText) && string.IsNullOrEmpty(labelText))
        //                {
        //                    ViolationBO violation = new ViolationBO();

        //                    violation.SourceElement = node.OuterHtml;
        //                    violation.SourceLineNumber = node.Line;
        //                    violation.ViolationDescription = resourceManager.GetString("LabelTextIsNotDescriptive");

        //                    violationList.Add(violation);
        //                }
        //            }
        //        }

        //        if (buttonNode != null)
        //        {
        //            foreach (HtmlNode node in buttonNode)
        //            {
        //                string buttonText = (node.Attributes["value"] != null) ? node.Attributes["value"].Value.Trim() : string.Empty;

        //                if (string.IsNullOrEmpty(node.InnerText) && string.IsNullOrEmpty(buttonText))
        //                {
        //                    ViolationBO violation = new ViolationBO();

        //                    violation.SourceElement = node.OuterHtml;
        //                    violation.SourceLineNumber = node.Line;
        //                    violation.ViolationDescription = resourceManager.GetString("ButtonTextIsNotDescriptive");

        //                    violationList.Add(violation);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception generalEx)
        //    {
        //        LogException.CatchException(generalEx, "ContentValidationRepository", "ValidateTextForLinkLabelButton");
        //        isPass = false;
        //    }
        //    return violationList;
        //}

        /// <summary>
        /// Checks if same label text is repeated for different control : MAS 20 - Rule 4
        /// </summary>
        /// <param name="htmlDoc"></param>
        /// <returns></returns>
        public static List<ViolationBO> HasDuplicateLabelsForControls(HtmlDocument htmlDoc, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<HtmlNode> lstNodes = new List<HtmlNode>();
            List<HtmlNode> lstLabelSpanNodes = new List<HtmlNode>();
            List<HtmlNode> lstFinalNodes = new List<HtmlNode>();
            List<HtmlNode> LstNodes_Temp = new List<HtmlNode>();
            isPassed = true;

            try
            {
                Regex objAlphaNumericPattern = new Regex(resourceManager.GetString("AlphaNumericRegex"));

                //Checks if parent node is not h1-h6 and span or label for label and span respectively.
                lstLabelSpanNodes = HTMLExtractor.GetHtmlContentElements(htmlDoc, resourceManager.GetString("LabelTag"));
                if (lstLabelSpanNodes != null && lstLabelSpanNodes.Count > 0)
                {
                    lstLabelSpanNodes = (from nodes in lstLabelSpanNodes
                                         where (nodes.ParentNode.Name != resourceManager.GetString("HeadTags").Split(',')[0]
                                            && nodes.ParentNode.Name != resourceManager.GetString("HeadTags").Split(',')[1]
                                            && nodes.ParentNode.Name != resourceManager.GetString("HeadTags").Split(',')[2]
                                            && nodes.ParentNode.Name != resourceManager.GetString("HeadTags").Split(',')[3]
                                            && nodes.ParentNode.Name != resourceManager.GetString("HeadTags").Split(',')[4]
                                            && nodes.ParentNode.Name != resourceManager.GetString("HeadTags").Split(',')[5]
                                            && nodes.ParentNode.Name != resourceManager.GetString("SpanTagName"))
                                         select nodes).ToList();
                    lstNodes.AddRange(lstLabelSpanNodes);
                }

                lstLabelSpanNodes = HTMLExtractor.GetHtmlContentElements(htmlDoc, resourceManager.GetString("SpanTag"));
                if (lstLabelSpanNodes != null && lstLabelSpanNodes.Count > 0)
                {
                    lstLabelSpanNodes = (from nodes in lstLabelSpanNodes
                                         where (nodes.ParentNode.Name != resourceManager.GetString("HeadTags").Split(',')[0]
                                            && nodes.ParentNode.Name != resourceManager.GetString("HeadTags").Split(',')[1]
                                            && nodes.ParentNode.Name != resourceManager.GetString("HeadTags").Split(',')[2]
                                            && nodes.ParentNode.Name != resourceManager.GetString("HeadTags").Split(',')[3]
                                            && nodes.ParentNode.Name != resourceManager.GetString("HeadTags").Split(',')[4]
                                            && nodes.ParentNode.Name != resourceManager.GetString("HeadTags").Split(',')[5]
                                            && nodes.ParentNode.Name != resourceManager.GetString("LabelTagName"))
                                         select nodes).ToList();
                    lstNodes.AddRange(lstLabelSpanNodes);
                }


                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("H6Tag")) != null)
                    lstNodes.AddRange(htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("H6Tag")));

                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("H5Tag")) != null)
                    lstNodes.AddRange(htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("H5Tag")));

                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("H4Tag")) != null)
                    lstNodes.AddRange(htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("H4Tag")));

                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("H3Tag")) != null)
                    lstNodes.AddRange(htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("H3Tag")));

                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("H2Tag")) != null)
                    lstNodes.AddRange(htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("H2Tag")));

                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("H1Tag")) != null)
                    lstNodes.AddRange(htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("H1Tag")));


                //Filtering all links and buttons
                if (lstNodes.Count > 0)
                {
                    lstNodes = (from node in lstNodes
                                where node.ParentNode.Name != resourceManager.GetString("AnchorTagName") && node.ParentNode.Name != resourceManager.GetString("ButtonTagName")
                                select node).ToList();
                }


                //Check if the label and span having childnode of type  Text
                if (lstNodes.Count > 0)
                {
                    lstFinalNodes = (from nodes in lstNodes
                                     where nodes.Descendants().All(i => i.Name == "#text" && i.NodeType == HtmlNodeType.Text && !string.IsNullOrEmpty(i.InnerText.Trim().ToLower()))
                                     select nodes).ToList();
                }

                //Check for diplay : None for in-line and class 
                if (lstFinalNodes.Count > 0)
                {
                    LstNodes_Temp = RuleEngineHelper.getAllValidNodes(lstFinalNodes, classProp);
                }

                //Checks for duplicate label. Duplicate labels are added to another list and also with partial special characters        
                List<HtmlNode> lstDuplicateLabels_tmp = LstNodes_Temp.FindAll(delegate(HtmlNode i)
                {
                    return LstNodes_Temp.FindAll(delegate(HtmlNode j)
                    { return (j.InnerText == i.InnerText && objAlphaNumericPattern.IsMatch(i.InnerText.Trim())); }).Count() > 1;
                }).ToList();

                //Get distinct labels ( group by innertext)
                var lstDuplicateLabels = (from i in
                                              (from j in lstDuplicateLabels_tmp
                                               select new
                                               {
                                                   OuterHtml = j.OuterHtml,
                                                   Line = j.Line,
                                                   InnerText = j.InnerText
                                               })
                                          group i by new { i.InnerText } into g
                                          let Line = g.Min(i => i.Line)
                                          let OuterHtml = g.Min(i => i.OuterHtml)
                                          select new
                                          {
                                              g.Key.InnerText,
                                              Line,
                                              OuterHtml
                                          }).ToList();

                //Duplicate label informations are added to violation report
                violationList = (from lbl in lstDuplicateLabels
                                 where !string.IsNullOrEmpty(lbl.InnerText)
                                 select new ViolationBO()
                                 {
                                     SourceElement = lbl.OuterHtml,
                                     SourceLineNumber = lbl.Line,
                                     ViolationDescription = string.Format("{0} - {1}", lbl.InnerText, resourceManager.GetString("DuplicateLabel"))
                                 }).ToList();
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasDuplicateLabelsForControls");
                isPassed = false;
            }
            return violationList;
        }

        /// <summary>
        /// Checks if same link label exist in a page and if it points to different destination : MAS 20 - Rule 5
        /// </summary>
        /// <param name="htmlDoc"></param>
        /// <returns></returns>
        public static List<ViolationBO> HasDuplicateLinkLabels(HtmlDocument htmlDoc, Dictionary<string, string> classProp, string baseUrl, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<HtmlNode> linkLabellst = new List<HtmlNode>();
            List<HtmlNode> LstNodes_Temp = new List<HtmlNode>();
            isPassed = true;
            try
            {
                HtmlNodeCollection linkNodes = htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("HrefTag"));
                if (linkNodes != null)
                {
                    //List to get all the link labels
                    linkLabellst = (from link in linkNodes
                                    where link.Attributes["href"].Value != "#" && link.Attributes["href"].Value != "/" && (!string.IsNullOrEmpty(link.Attributes["href"].Value))
                                    select link).ToList();
                    if (linkLabellst.Count > 0)
                    {
                        LstNodes_Temp = RuleEngineHelper.getAllValidNodes(linkLabellst, classProp);
                    }

                    //Checks for duplicate label. Duplicate labels are added to another list
                    List<HtmlNode> lstDuplicateLink_tmp = LstNodes_Temp.FindAll(delegate(HtmlNode i)
                    {
                        return LstNodes_Temp.FindAll(delegate(HtmlNode j)
                        {
                            return j.InnerText.ToLower().Trim() == i.InnerText.ToLower().Trim() && j.Attributes["href"] != null
                             && i.Attributes["href"] != null && (j.Attributes["href"].Value.Split('?').First().ToLower().Trim() != i.Attributes["href"].Value.Split('?').First().ToLower().Trim());
                        }).Count() >= 1;
                    }).ToList();

                    lstDuplicateLink_tmp.ForEach(lnk =>
                    {
                        if (!lnk.Attributes["href"].Value.ToLower().Contains("http"))
                        {
                            //lnk.Attributes["href"].Value = baseUrl + lnk.Attributes["href"].Value;
                            lnk.Attributes["href"].Value = string.Format("{0} {1}", baseUrl, lnk.Attributes["href"].Value);
                        }
                    });

                    // Get distinct links ( group by innertext)
                    var lstDuplicateLink = (from i in
                                                (from j in lstDuplicateLink_tmp
                                                 select new
                                                 {
                                                     OuterHtml = j.OuterHtml,
                                                     Line = j.Line,
                                                     InnerText = j.InnerText.ToLower().Trim()
                                                 })
                                            group i by new { i.InnerText } into g
                                            let Line = g.Min(i => i.Line)
                                            let OuterHtml = g.Min(i => i.OuterHtml)
                                            select new
                                            {
                                                g.Key.InnerText,
                                                Line,
                                                OuterHtml
                                            }).ToList();


                    //Duplicate label informations are added to violation report
                    violationList = (from lnk in lstDuplicateLink
                                     where !string.IsNullOrEmpty(lnk.InnerText.ToString())
                                     select new ViolationBO
                                     {
                                         SourceElement = lnk.OuterHtml,
                                         SourceLineNumber = lnk.Line,
                                         ViolationDescription = string.Format("{0} {1}", lnk.InnerText, resourceManager.GetString("DuplicateLinkLabel"))
                                     }).ToList();
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasDuplicateLinkLabels");
                isPassed = false;
            }
            return violationList;
        }

        /// <summary>
        /// Checks if duplicate button are present : MAS 20,Rule6
        /// </summary>
        /// <param name="htmlDom"></param>
        /// <returns></returns>
        public static List<ViolationBO> HasDuplicateButtonForControls(HtmlDocument htmlDom, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<HtmlNode> buttonsList = new List<HtmlNode>();
            List<HtmlNode> lstSubmit = new List<HtmlNode>();
            int take = 0;
            isPassed = true;

            try
            {
                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("ButtonTag")) != null)
                    buttonsList.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("ButtonTag")).ToList());

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HtmlButtonTag")) != null)
                    buttonsList.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HtmlButtonTag")).ToList());

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("AspButtonTag")) != null)
                    buttonsList.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("AspButtonTag")).ToList());


                //get all Buttons with out Display : None
                var lstButtonNode = RuleEngineHelper.getAllValidNodes(buttonsList, classProp);

                //Checks for duplicate buttons. Duplicate buttons are added to another list         
                List<HtmlNode> lstDuplicateButtons_tmp = lstButtonNode.FindAll(delegate(HtmlNode i)
                {
                    return lstButtonNode.FindAll(delegate(HtmlNode j)
                    {
                        return ((j.Attributes["value"] != null && i.Attributes["value"] != null && j.Attributes["value"].Value.Trim().ToLower() == i.Attributes["value"].Value.Trim().ToLower())
                        || (j.Attributes["value"] == null && i.Attributes["value"] == null && j.InnerText.Trim().ToLower() == i.InnerText.Trim().ToLower())
                        || (j.Attributes["value"] != null && i.Attributes["value"] == null && j.Attributes["value"].Value.ToLower() == i.InnerText.ToLower())
                        || (j.Attributes["value"] == null && i.Attributes["value"] != null && j.InnerText.ToLower() == i.Attributes["value"].Value.ToLower()));
                    }).Count() > 1;
                }).ToList();

                //Need to check for Onclick() event of the buttons in case buttons are duplicate
                var lstBtn = (from b in lstDuplicateButtons_tmp
                              where b.Attributes[resourceManager.GetString("OnClick")] != null &&
                              !string.IsNullOrEmpty(b.Attributes[resourceManager.GetString("OnClick")].Value)
                              select b).ToList();

                //Check for Buttons without Onclick() events
                lstSubmit = (from b in lstDuplicateButtons_tmp
                             //where b.Attributes[resourceManager.GetString("OnClick")] == null &&
                             where b.Attributes["type"] != null && b.Attributes["type"].Value.ToLower() == "submit" && b.Attributes["OnClick"] == null
                             select b).ToList();

                //Incase there are more than 1 Submit buttons then will check for the <form> tags
                if (lstSubmit.Count > 1)
                {
                    //gets the list of {nodename,action,outhtml,line} only if in any of Submit button's -> Ancestors we have <form> tag as child node in the 
                    var lstAllSubmit = (from item in lstSubmit.Where(i => i.Ancestors().Count() > 0)
                                        where item.Ancestors().Where(j => j.ChildNodes.Any(k => k.Name.ToLower() == "form")).Count() > 0
                                        let x = item.Ancestors().SelectMany(j => j.ChildNodes.Where(k => k.Name.ToLower() == "form"))
                                        let y = x.FirstOrDefault(a => a.Name.ToLower() == "form")
                                        select new
                                        {
                                            node = item.Name,
                                            buttonValue = item.Attributes["Value"].Value.ToLower(),
                                            action = y.Attributes["action"].Value != null ? y.Attributes["action"].Value.ToLower() : string.Empty,
                                            Outhtml = item.OuterHtml,
                                            Line = item.Line,
                                            InnerText = item.InnerText
                                        }).ToList();

                    //Null Check
                    if (lstAllSubmit.Count > 0)
                    {
                        //Group the Button based on there actions if the count = 1 ,if it occurs once then pick  
                        var LstDistBtn = lstAllSubmit.GroupBy(s => s.action).Where(g => g.Count() == 1).Select(g => g.FirstOrDefault()).ToList();

                        //incase if the count >1 then reverse the list and take the totalcount - 1 , Reverse will take the elments which are at the bottom of the Hthml
                        if (LstDistBtn.Count() > 1)
                        {
                            int total = LstDistBtn.Count();
                            take = total - 1;
                            LstDistBtn.Reverse();
                        }

                        var LstFinal = LstDistBtn.Count() > 1 ? LstDistBtn.Take(take).ToList() : LstDistBtn;
                        violationList = (from i in LstFinal
                                         select new ViolationBO
                                         {
                                             SourceElement = i.Outhtml,
                                             SourceLineNumber = i.Line,
                                             ViolationDescription = i.node + resourceManager.GetString("DuplicateSubmitButtons")
                                         }).ToList();
                    }
                }
                //Collect the list of{OnClick,Value,item,line,Outhtml,InnerText} for the Buttons with Onclick.
                var Lsttemp = (from i in lstBtn
                               select new
                               {
                                   OnClick = i.Attributes[resourceManager.GetString("OnClick")].Value.ToLower(),
                                   Value = i.Attributes["value"] != null ? i.Attributes["value"].Value.ToLower() : string.Empty,
                                   item = i.Name,
                                   line = i.Line,
                                   Outhtml = i.OuterHtml,
                                   InnerText = i.InnerText
                               }).ToList();
                var list = (from l in Lsttemp
                            group l by new
                            {
                                l.Value,
                                l.OnClick
                            }
                                into newList
                                select new
                                {
                                    Name = newList.Key.Value,
                                    OnClick = newList.Key.OnClick,
                                    Count = Lsttemp.GroupBy(x => new { x.Value, x.OnClick }).Select(grou => new { Onclick = grou.Key.OnClick, Count = grou.Count() }).ToList().Where(x => x.Onclick == newList.Key.OnClick).Select(x => x.Count).FirstOrDefault(),
                                }).Where(x => x.Count == 1).ToList();

                var finalJoinLst = (from i in Lsttemp
                                    join j in list on i.OnClick equals j.OnClick
                                    select i).ToList().Distinct();


                //Duplicate button informations are added to violation report
                violationList.AddRange(from duplicateButton in finalJoinLst
                                       where (!string.IsNullOrEmpty(duplicateButton.InnerText.ToString()) || !string.IsNullOrEmpty(duplicateButton.Value))
                                       select new ViolationBO
                                       {
                                           SourceElement = duplicateButton.Outhtml,
                                           SourceLineNumber = duplicateButton.line,
                                           ViolationDescription = duplicateButton.InnerText + resourceManager.GetString("DuplicateButton")
                                       });

            }

            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasDuplicateButtonForControls");
                isPassed = false;
            }
            return violationList;
        }


        /// <summary>
        /// Button - use <button> rather than an <img onclick> or a styled div : MAS 40, Rule 6
        /// </summary>
        /// <param name="doc">HtmlDocument</param>
        /// <returns>Dictionary<string, string></returns>
        public static List<ViolationBO> GetAllImagesOrDivWithOnclickEvent(HtmlDocument doc, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<ViolationBO> lstViolation = new List<ViolationBO>();
            List<HtmlNode> lstImgAndDivNodes = new List<HtmlNode>();
            isPassed = true;

            try
            {
                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("ImageTag")) != null)
                    lstImgAndDivNodes = doc.DocumentNode.SelectNodes(resourceManager.GetString("ImageTag")).ToList();

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("DivTag")) != null)
                    lstImgAndDivNodes.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("DivTag")).ToList());

                if (lstImgAndDivNodes.Count > 0)
                {
                    var lstAllNode = (from node in lstImgAndDivNodes
                                      where (node.Attributes[resourceManager.GetString("OnClick")] != null
                                      && !string.IsNullOrEmpty(node.Attributes[resourceManager.GetString("OnClick")].Value))
                                      select node).ToList();

                    lstAllNode = RuleEngineHelper.getAllValidNodes(lstAllNode, classProp);

                    if (lstAllNode.Count > 0)
                    {
                        lstViolation = (from node in lstAllNode
                                        where (node.Attributes[resourceManager.GetString("OnClick")] != null
                                        && !string.IsNullOrEmpty(node.Attributes[resourceManager.GetString("OnClick")].Value))
                                        select new ViolationBO
                                        {
                                            SourceElement = node.OuterHtml,
                                            SourceLineNumber = node.Line,
                                            ViolationDescription = resourceManager.GetString("ImgOrDivWithOnClick")
                                        }).ToList();
                    }
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "GetAllImagesOrDivWithOnclickEvent");
                isPassed = false;
            }
            return lstViolation;
        }

        /// <summary>
        /// To verify that all the images on the webpage have an associated “Alt” attribute: MAS40, Rule 1
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<ViolationBO> ImagesWithAltAttribute(HtmlDocument doc, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<HtmlNode> imgNodes = new List<HtmlNode>();
            isPassed = true;

            try
            {
                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("ImageTag")) != null)
                    imgNodes = doc.DocumentNode.SelectNodes(resourceManager.GetString("ImageTag")).ToList();

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("InputImage")) != null)
                    imgNodes.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("InputImage")));

                //get All Decorative Img
                var lstImages = RuleEngineHelper.GetAllNonDecorativeImg(imgNodes, classProp);

                if (lstImages.Count > 0)
                {
                    violationList = (from node in lstImages
                                     where node.Attributes["alt"] == null || string.IsNullOrEmpty(node.Attributes["alt"].Value.ToLower().Trim()) || node.Attributes["alt"].Value.ToLower().Trim() == "*"
                                     select new ViolationBO
                                     {
                                         SourceElement = node.OuterHtml,
                                         SourceLineNumber = node.Line,
                                         ViolationDescription = node.Attributes["alt"] == null ? resourceManager.GetString("NullImageAltAttribute") : resourceManager.GetString("ImageAltAttributeVal")
                                     }).ToList();
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "ImagesWithAltAttribute");
                isPassed = false;
            }

            return violationList;
        }

        /// <summary>
        /// To verify that all the decorative images on the webpage have alt = "" attribute : MAS40, Rule 2
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<ViolationBO> DecorativeImagesWithAltAttribute(HtmlDocument doc, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<HtmlNode> imgNodes = new List<HtmlNode>();
            isPassed = true;

            try
            {
                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("ImageTag")) != null)
                    imgNodes.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("ImageTag")));

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("InputImage")) != null)
                    imgNodes.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("InputImage")));

                //get All Decorative Img
                var lstNonDecorativeImg = RuleEngineHelper.GetAllNonDecorativeImg(imgNodes, classProp);

                //Finally Remove all these items from the Main List
                if (lstNonDecorativeImg.Count > 0)
                {
                    imgNodes.RemoveAll(i => lstNonDecorativeImg.Contains(i));
                    lstNonDecorativeImg.Clear();
                }

                if (imgNodes.Count > 0)
                {
                    violationList = (from node in imgNodes
                                     where (node.Attributes["alt"] == null || (node.Attributes["alt"] != null && !string.IsNullOrEmpty(node.Attributes["alt"].Value)))
                                     select new ViolationBO
                                     {
                                         SourceElement = node.OuterHtml,
                                         SourceLineNumber = node.Line,
                                         ViolationDescription = node.InnerText + resourceManager.GetString("DecorativeImageAlt")
                                     }).ToList();
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "DecorativeImagesWithAltAttribute");
                isPassed = false;
            }

            return violationList;
        }

        /// <summary>
        /// Div Inner content exceeds 250 Characters: MAS 40,Rule 7
        /// </summary>
        /// <param name="doc">HtmlDocument</param>
        /// <returns>Dictionary<string, string></returns>
        public static List<ViolationBO> VerifyDivInnerContent(HtmlDocument doc, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            ViolationBO objViolationBO = new ViolationBO();
            List<HtmlNode> divNodes = new List<HtmlNode>();

            isPassed = true;
            try
            {
                //HtmlNodeCollection divNodes = doc.DocumentNode.SelectNodes(resourceManager.GetString("DivTag"));
                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("DivTag")) != null)
                    divNodes.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("DivTag")));

                //Get all nodes without display : none
                var lstDivNodes = RuleEngineHelper.getAllValidNodes(divNodes, classProp);

                if (lstDivNodes != null)
                {
                    foreach (HtmlNode item in lstDivNodes)
                    {
                        if (item.Descendants().Count() > 1)
                        {
                            int counter = 0;
                            foreach (var descNode in item.Descendants())
                            {
                                if (descNode.Name.ToLower().Trim() == resourceManager.GetString("SpanTagName")
                                    || descNode.Name.ToLower().Trim() == resourceManager.GetString("LabelTagName")
                                    || descNode.Name.ToLower().Trim() == resourceManager.GetString("AnchorTagName")
                                    || descNode.Name.ToLower().Trim() == resourceManager.GetString("HeadTags").Split(',')[0]
                                    || descNode.Name.ToLower().Trim() == resourceManager.GetString("HeadTags").Split(',')[1]
                                    || descNode.Name.ToLower().Trim() == resourceManager.GetString("HeadTags").Split(',')[2]
                                    || descNode.Name.ToLower().Trim() == resourceManager.GetString("HeadTags").Split(',')[3]
                                    || descNode.Name.ToLower().Trim() == resourceManager.GetString("HeadTags").Split(',')[4]
                                    || descNode.Name.ToLower().Trim() == resourceManager.GetString("HeadTags").Split(',')[5])
                                {
                                    //Discard the node if it has dropdown inside it.
                                    if (descNode.Descendants() != null && descNode.Descendants().Where(j => j.Name.ToLower().Trim() == resourceManager.GetString("DropDownTagName")).Count() > 0)
                                    {
                                        counter = 0;
                                    }
                                    else
                                    {
                                        counter += descNode.InnerText.Trim().ToString().Replace("  ", "").Length;
                                    }

                                }
                                else if (descNode.Name.ToLower().Trim() == "#text"
                                    && descNode.ParentNode.Name.ToLower().Trim() == resourceManager.GetString("DivTagName")
                                    && descNode.InnerText.Trim().Length > 0 && descNode.NextSibling != null)
                                {
                                    counter += descNode.InnerText.Trim().ToString().Replace("  ", "").Length;
                                }
                                else if (descNode.Name.ToLower().Trim() == resourceManager.GetString("DivTagName")
                                        || descNode.Name.ToLower().Trim() == resourceManager.GetString("TableTagName")
                                        || descNode.Name.ToLower().Trim() == resourceManager.GetString("ImgTagName"))
                                {
                                    break;
                                }
                                else if ((descNode.Name.ToLower().Trim() == resourceManager.GetString("BrTagName")
                                        && descNode.NextSibling.Name.ToLower().Trim() == resourceManager.GetString("BrTagName"))
                                        && counter < Convert.ToInt32(resourceManager.GetString("TextLimit")))
                                {
                                    counter = 0;
                                }
                                else if ((descNode.Name.ToLower().Trim() == resourceManager.GetString("ULTagName")
                                        || descNode.Name.ToLower().Trim() == resourceManager.GetString("LITagName"))
                                        || descNode.Name.ToLower().Trim() == resourceManager.GetString("ParagraphTagName")
                                        && counter < Convert.ToInt32(resourceManager.GetString("TextLimit")))
                                {
                                    counter = 0;
                                }

                                if (counter > Convert.ToInt32(resourceManager.GetString("TextLimit")))
                                {
                                    objViolationBO = new ViolationBO
                                    {
                                        SourceElement = item.OuterHtml,
                                        SourceLineNumber = item.Line,
                                        ViolationDescription = resourceManager.GetString("InnerTextExceeded"),
                                    };
                                    violationList.Add(objViolationBO);
                                    break;
                                }
                            }
                        }
                        else if (item.InnerText.Trim().Length > Convert.ToInt32(resourceManager.GetString("TextLimit")))
                        {
                            objViolationBO = new ViolationBO
                            {
                                SourceElement = item.OuterHtml,
                                SourceLineNumber = item.Line,
                                ViolationDescription = resourceManager.GetString("InnerTextExceeded"),
                            };
                            violationList.Add(objViolationBO);
                        }
                    }
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "VerifyDivInnerContent");
                isPassed = false;
            }
            return violationList;
        }

        /// <summary>
        /// To verify that  the page title is descriptive of its  content
        /// </summary>
        /// <param name="node">HtmlNode</param>
        /// <returns>Violations list</returns>
        public static List<ViolationBO> VerifyPageDescriptiveTitle(HtmlDocument doc, Dictionary<string, string> classProp, ref bool isPassed)
        {

            List<ViolationBO> violationList = new List<ViolationBO>();
            ViolationBO violation = new ViolationBO();
            isPassed = true;
            List<HtmlNode> titleNodes = new List<HtmlNode>();

            try
            {
                //Check if the Html Tag is Present 
                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("HtmlTag")) != null)
                {
                    //Check for Title Tag
                    if (doc.DocumentNode.SelectNodes(resourceManager.GetString("TitleTag")) != null)
                    {
                        titleNodes.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("TitleTag")));
                    }
                    //If Title Tag is present
                    if (titleNodes.Count > 0)
                    {
                        //Check for the valid title Tags
                        var lstTitleNodes = RuleEngineHelper.getAllValidNodes(titleNodes, classProp);
                        //Check which all Title Nodes have innertext
                        var lstofTitle = (from i in lstTitleNodes
                                          where ((!string.IsNullOrEmpty(i.InnerText) && !string.IsNullOrWhiteSpace(i.InnerText)))
                                          select i).ToList();
                        if (lstofTitle.Count == 0)
                        {
                            //Check which all Title Nodes donot have innertext
                            var lstofemptyTitle = (from i in lstTitleNodes
                                                   where (string.IsNullOrEmpty(i.InnerText) || string.IsNullOrWhiteSpace(i.InnerText))
                                                   select i).ToList();
                            var lstfinalNode = lstofemptyTitle.Take(1);
                            violationList = (from i in lstfinalNode
                                             select new ViolationBO
                                             {
                                                 SourceElement = i.OuterHtml,
                                                 SourceLineNumber = i.Line,
                                                 ViolationDescription = resourceManager.GetString("TitleMissing")
                                             }).ToList();

                        }
                    }
                    else
                    {
                        violation.SourceElement = string.Empty;
                        violation.SourceLineNumber = 0;
                        violation.ViolationDescription = resourceManager.GetString("TitleTagMissing");
                        violationList.Add(violation);
                    }
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "VerifyPageDescriptiveTitle");
                isPassed = false;
            }
            return violationList;
        }

        /// <summary>
        /// HTML tags for the following elements should be as follows
        /// Links - use <a href>, not <span onclick=”navigate()”>
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<ViolationBO> GetAllSpanWithOnclickEvent(HtmlDocument doc, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<HtmlNode> spanNodes = new List<HtmlNode>();
            isPassed = true;

            try
            {
                // HtmlNodeCollection spanNodeCollection = doc.DocumentNode.SelectNodes(resourceManager.GetString("SpanTag"));

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("SpanTag")) != null)
                {
                    spanNodes.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("SpanTag")));
                }

                if (spanNodes.Count > 0)
                {
                    var lstSpanNodes = (from node in spanNodes
                                        where node.Attributes.Contains(resourceManager.GetString("OnClick"))
                                        && !string.IsNullOrEmpty(node.Attributes[resourceManager.GetString("OnClick")].Value)
                                        select node).ToList();

                    //Get all valid Nodes
                    lstSpanNodes = RuleEngineHelper.getAllValidNodes(lstSpanNodes, classProp);

                    if (lstSpanNodes.Count > 0)
                    {
                        violationList = (from node in lstSpanNodes
                                         where node.Attributes.Contains(resourceManager.GetString("OnClick"))
                                         && !string.IsNullOrEmpty(node.Attributes[resourceManager.GetString("OnClick")].Value)
                                         select new ViolationBO
                                         {
                                             SourceElement = node.OuterHtml,
                                             SourceLineNumber = node.Line,
                                             ViolationDescription = resourceManager.GetString("SpanWithOnClick")
                                         }).ToList();
                    }
                }

            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "GetAllSpanWithOnclickEvent");
                isPassed = false;
            }
            return violationList;
        }

        /// <summary>
        ///  Input - use <input>, rather than a styled div
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        //public static List<ViolationBO> GetAllContentEditableDivTags(HtmlDocument doc, ref bool isPassed)
        //{
        //    List<ViolationBO> violationList = new List<ViolationBO>();
        //    isPassed = true;

        //    try
        //    {
        //        HtmlNodeCollection divNodeCollection = doc.DocumentNode.SelectNodes(resourceManager.GetString("DivTag"));
        //        if (divNodeCollection != null)
        //        {
        //            var divWithOnclickAttr = from i in divNodeCollection
        //                                     select new
        //                                     {
        //                                         HasContentEditable = i.Attributes.Contains(resourceManager.GetString("ContentEditable")),
        //                                         Line = i.Line,
        //                                         OuterHtml = i.OuterHtml
        //                                     };

        //            foreach (var item in divWithOnclickAttr)
        //            {
        //                if (item.HasContentEditable)
        //                {
        //                    ViolationBO violationBO = new ViolationBO
        //                    {
        //                        SourceElement = item.OuterHtml,
        //                        SourceLineNumber = item.Line,
        //                        ViolationDescription = resourceManager.GetString("ContentEditableStyledDiv")
        //                    };

        //                    violationList.Add(violationBO);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception generalEx)
        //    {
        //        LogException.CatchException(generalEx, "ContentValidationRepository", "GetAllContentEditableDivTags");
        //        isPassed = false;
        //    }
        //    return violationList;
        //}

        /// <summary>
        /// To verify visual focus moves to active web elements
        /// </summary>
        /// <param name="htmlDom"></param>
        /// <returns></returns>
        //public static List<ViolationBO> HasVisualFocusForActiveElements(HtmlDocument htmlDom, ref bool isPassed)
        //{
        //    List<ViolationBO> violationList = new List<ViolationBO>();
        //    isPassed = true;

        //    try
        //    {
        //        HtmlNodeCollection hrefNodes = htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HrefTag"));
        //        HtmlNodeCollection buttonNodes = htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("ButtonTag"));
        //        HtmlNodeCollection inputButtonNodes = htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("InputTag"));

        //        if (hrefNodes != null)
        //        {
        //            foreach (HtmlNode node in hrefNodes)
        //            {
        //                HtmlAttribute att = node.Attributes["href"];
        //                if (att.Value == "#" || att.Value == "/" || (string.IsNullOrEmpty(att.Value)))
        //                {
        //                    ViolationBO violation = new ViolationBO();

        //                    violation.SourceElement = node.OuterHtml;
        //                    violation.SourceLineNumber = node.Line;
        //                    violation.ViolationDescription = resourceManager.GetString("VisualFocus");
        //                    violationList.Add(violation);
        //                }
        //            }
        //        }

        //        if (buttonNodes != null)
        //        {
        //            foreach (HtmlNode node in buttonNodes)
        //            {
        //                if (node.Name == "button")
        //                {

        //                    bool onClick = NodeValidationRepository.HasAttribute(node, resourceManager.GetString("OnClick"));
        //                    bool onKeyPress = NodeValidationRepository.HasAttribute(node, resourceManager.GetString("OnKeyPress"));

        //                    if ((!onClick && !onKeyPress))
        //                    {
        //                        ViolationBO violation = new ViolationBO();

        //                        violation.SourceElement = node.OuterHtml;
        //                        violation.SourceLineNumber = node.Line;
        //                        violation.ViolationDescription = resourceManager.GetString("VisualFocus");

        //                        violationList.Add(violation);
        //                    }
        //                }
        //            }
        //        }

        //        if (inputButtonNodes != null)
        //        {
        //            foreach (HtmlNode node in inputButtonNodes)
        //            {
        //                if (node.Name == "input" && node.Attributes["type"] != null && node.Attributes["type"].Value.ToLower().Trim() == "button")
        //                {

        //                    bool onClick = NodeValidationRepository.HasAttribute(node, resourceManager.GetString("OnClick"));
        //                    bool onKeyPress = NodeValidationRepository.HasAttribute(node, resourceManager.GetString("OnKeyPress"));

        //                    if ((!onClick && !onKeyPress))
        //                    {
        //                        ViolationBO violation = new ViolationBO();

        //                        violation.SourceElement = node.OuterHtml;
        //                        violation.SourceLineNumber = node.Line;
        //                        violation.ViolationDescription = resourceManager.GetString("VisualFocus");

        //                        violationList.Add(violation);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception generalEx)
        //    {
        //        LogException.CatchException(generalEx, "ContentValidationRepository", "HasVisualFocusForActiveElements");
        //        isPassed = false;
        //    }
        //    return violationList;
        //}

        /// <summary>
        /// Checks for logical progression for tab order with tabindex : MAS 36 # 2
        /// </summary>
        /// <param name="htmlDom"></param>
        /// <returns></returns> 
        //public static List<ViolationBO> HasLogicalTabOrder_Old(HtmlDocument htmlDom, ref bool isPassed)
        //{
        //    List<ViolationBO> violationList = new List<ViolationBO>();
        //    List<int> tabIndexList = new List<int>();
        //    isPassed = true;

        //    try
        //    {
        //        HtmlNodeCollection htmlNodes = htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HtmlTag"));
        //        HtmlNodeCollection tabIndexNodes = new HtmlNodeCollection(new HtmlNode(HtmlNodeType.Document, htmlDom, 0));

        //        if (htmlNodes != null)
        //        {
        //            tabIndexNodes.Clear();

        //            foreach (HtmlNode node in htmlNodes)
        //            {
        //                if (node != null)
        //                {
        //                    foreach (HtmlNode descendentNode in node.Descendants())
        //                    {
        //                        if (descendentNode.Attributes["tabindex"] != null && !string.IsNullOrEmpty(descendentNode.Attributes["tabindex"].Value))
        //                        {
        //                            tabIndexNodes.Add(descendentNode);
        //                            if (Helper.IsNumeric(descendentNode.Attributes["tabindex"].Value))
        //                                tabIndexList.Add(Convert.ToInt32(descendentNode.Attributes["tabindex"].Value));
        //                        }
        //                    }
        //                }

        //            }
        //        }

        //        if (tabIndexNodes != null && tabIndexNodes.Count > 0 && tabIndexList.Count > 0)
        //        {
        //            List<int> violationIndex = new List<int>();
        //            int startIndex;

        //            for (startIndex = 0; startIndex < tabIndexList.Count; startIndex++)
        //            {
        //                for (int nextIndex = startIndex + 1; nextIndex < tabIndexList.Count; nextIndex++)
        //                {

        //                    if (tabIndexList[nextIndex] < tabIndexList[startIndex])
        //                    {
        //                        if (!violationIndex.Contains(tabIndexList[nextIndex]))
        //                        {
        //                            violationIndex.Add(tabIndexList[nextIndex]);
        //                            ViolationBO violationBO = new ViolationBO
        //                            {
        //                                SourceElement = tabIndexNodes[nextIndex].OuterHtml,
        //                                SourceLineNumber = tabIndexNodes[nextIndex].Line,
        //                                ViolationDescription = resourceManager.GetString("LogicalTabOrder")
        //                            };
        //                            violationList.Add(violationBO);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception generalEx)
        //    {
        //        LogException.CatchException(generalEx, "ContentValidationRepository", "HasLogicalTabOrder_Old");
        //        isPassed = false;
        //    }
        //    return violationList;
        //}

        /// <summary>
        /// Checks for logical progression for tab order with tabindex : MAS 36 # 2
        /// </summary>
        /// <param name="htmlDom"></param>
        /// <returns></returns> 
        public static List<ViolationBO> HasLogicalTabOrder(HtmlDocument htmlDom, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<HtmlNode> nodeList = new List<HtmlNode>();
            isPassed = true;

            try
            {
                HtmlNodeCollection htmlNodes = htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HtmlTag"));

                if (htmlNodes != null)
                {
                    //get All Descendants to List.
                    nodeList = htmlNodes.Descendants().ToList();

                    //Get All valid Descendants Nodes
                    //Commented this block since it consumes huge time.
                    //var lstDescendantNodes = RuleEngineHelper.getAllValidNodes(nodeList, classProp);

                    violationList.AddRange(RuleEngineHelper.CheckForLogicalTabOrder(violationList, nodeList, null));
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasLogicalTabOrder");
                isPassed = false;
            }
            return violationList;
        }

        /// <summary>
        /// The submenu list for the corresponding menu item is displayed : MAS 36 # 3
        /// </summary>
        /// <param name="htmlDom"></param>
        /// <returns>List<ViolationBO></returns> 
        public static List<ViolationBO> HasLogicalTabOrderForSubMenuItems(HtmlDocument htmlDom, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<HtmlNode> ULNodes = new List<HtmlNode>();
            isPassed = true;

            try
            {
                //HtmlNodeCollection htmlNodes = htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("ULTag"));
                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("ULTag")) != null)
                {
                    ULNodes.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("ULTag")));
                }
                //Get all Valid Nodes
                var lstULNodes = RuleEngineHelper.getAllValidNodes(ULNodes, classProp);

                if (lstULNodes.Count > 0)
                {
                    foreach (HtmlNode item in lstULNodes)
                    {
                        List<HtmlNode> allChildNodes = (from childItem in item.ChildNodes
                                                        where childItem.Name.ToLower().Trim().Equals("li")
                                                        select childItem).ToList();
                        if (allChildNodes != null)
                        {
                            violationList.AddRange(RuleEngineHelper.CheckForLogicalTabOrder(violationList, allChildNodes, "li"));
                        }
                    }
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasLogicalTabOrderForSubMenuItems");
                isPassed = false;
            }
            return violationList;
        }


        /// <summary>
        /// To verify that an option is available for the user to navigate from one container to another container : MAS 27 # 1
        /// 1. Get all div tags from the html dom.
        /// 2. Get all child div tags.
        /// 3. If tab order is not available for one of the div. we will consider for violation.
        /// 4. If tab order is not proper, we will consider for violation.
        /// Note: This rule is applicable only if there is atleast one child div tag has tabindex.
        /// </summary>
        /// <param name="htmlDom"></param>
        /// <returns>List<ViolationBO></returns> 
        public static List<ViolationBO> HasLogicalTabOrderForUserNavigation(HtmlDocument htmlDom, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            isPassed = true;

            try
            {
                HtmlNodeCollection htmlNodes = htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("ParentDivTags"));
                if (htmlNodes != null)
                {
                    foreach (HtmlNode item in htmlNodes)
                    {
                        List<HtmlNode> allChildNodes = (from childItem in item.Descendants()
                                                        where childItem.Name.ToLower().Trim().Equals("div")
                                                        select childItem).ToList();

                        //Get All Valid Nodes.
                        //Commented this block Since it consumes huge time
                        //var lstChidNodes = RuleEngineHelper.getAllValidNodes(allChildNodes, classProp);

                        violationList.AddRange(RuleEngineHelper.CheckForLogicalTabOrder(violationList, allChildNodes, "div"));
                    }
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasLogicalTabOrderForUserNavigation");
                isPassed = false;
            }

            return violationList;
        }

        /// <summary>
        /// If a page has a proper heading structure, this may be considered a sufficient technique instead of a "Skip to main content" link. Note that navigating by headings is not yet supported in all browsers.
        /// </summary>
        /// <param name="htmlDom"></param>
        /// <param name="classProp"></param>
        /// <param name="isPassed"></param>
        /// <returns></returns>
        //Added for Skip to Main Content - Ajay
        public static List<ViolationBO> HasSkipToMainContent(HtmlDocument htmlDoc, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<HtmlNode> lstNodes = new List<HtmlNode>();
            List<HtmlNode> lstLabelSpanNodes = new List<HtmlNode>();
            List<HtmlNode> lstFinalNodes = new List<HtmlNode>();
            List<HtmlNode> LstNodes_Temp = new List<HtmlNode>();
            isPassed = true;

            try
            {

                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("Header")) != null)
                    lstNodes.AddRange(htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("Header")));

                //Filtering all header Tags.
                if (lstNodes.Count > 0)
                {
                    lstNodes = (from node in lstNodes
                                where node.ParentNode.Name != resourceManager.GetString("Header")
                                select node).ToList();
                }

                //Check if the webelement contains text To identify the name and Nodetype and Innertext condition.
                if (lstNodes.Count > 0)
                {
                    lstFinalNodes = (from nodes in lstNodes
                                     where !(nodes.Descendants().All(i => i.InnerText.Trim().ToLower().Contains("skip to content") || i.InnerText.Trim().ToLower().Contains("skip to main content") || i.InnerText.Trim().ToLower().Contains("skip")))
                                     select nodes).ToList();
                }

               //4. Add the violation to violation tag.
                //Skip to content informations are added to violation report

                violationList = (from skipContent in lstFinalNodes
                                 where !string.IsNullOrEmpty(skipContent.InnerText)
                                 select new ViolationBO()
                                 {
                                     SourceElement = skipContent.OuterHtml,
                                     SourceLineNumber = skipContent.Line,
                                     ViolationDescription = string.Format("{0} - {1}", skipContent.InnerText, resourceManager.GetString("No 'Skip to content' available"))
                                 }).ToList();
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasDuplicateLabelsForControls");
                isPassed = false;
            }
            return violationList;
        }




        /// <summary>
        /// Programmatic Access MAS 40 #4 - use <input>, rather than a styled div
        /// </summary>
        /// <param name="htmlDom"></param>
        /// <returns></returns>
        public static List<ViolationBO> UseInputRatherThanStyledDiv(HtmlDocument htmlDom, Dictionary<string, string> classProperties, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<ViolationBO> violationBO = new List<ViolationBO>();
            List<HtmlNode> lstDivNodes = new List<HtmlNode>();
            List<HtmlNode> divNodes = new List<HtmlNode>();
            isPassed = true;

            try
            {
                //HtmlNodeCollection divNodes = htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("DivTag"));
                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("DivTag")) != null)
                {
                    lstDivNodes.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("DivTag")));
                }
                //Get all valid nodes
                divNodes = RuleEngineHelper.getAllValidNodes(lstDivNodes, classProperties);

                if (divNodes != null)
                {
                    //Checks if any styled div are used to get input text.
                    violationBO = (from item in divNodes
                                   where item.Attributes["contenteditable"] != null && item.Attributes["contenteditable"].Value == "true"
                                   select new ViolationBO
                                   {
                                       SourceElement = item.OuterHtml,
                                       SourceLineNumber = item.Line,
                                       ViolationDescription = resourceManager.GetString("UseInput")
                                   }).ToList();
                    violationList.AddRange(violationBO);
                    violationBO.Clear();

                    //checks if any div has ul tags-basically these are filtered to check if any styled divs are used instead of select tag.
                    var divChildNodes = (from div in divNodes
                                         where (div.HasChildNodes && div.ChildNodes.Any(x => x.Name == resourceManager.GetString("ULTag")))
                                         select div).ToList();

                    if (divChildNodes.Any())
                    {
                        //checks for style properties to see if any border is set
                        violationList = (from divChild in divChildNodes
                                         from child in divChild.DescendantsAndSelf().Where(x => (x.Attributes["style"] != null && !(string.IsNullOrEmpty(x.Attributes["style"].Value))))
                                         let key = RuleEngineHelper.GetStyleProperties(child.Attributes["style"].Value).SingleOrDefault(x => x.Key.Contains("border"))
                                         where ((!String.IsNullOrEmpty(key.Key)) && key.Key.Contains("border"))
                                         select new ViolationBO
                                         {
                                             SourceLineNumber = divChild.Line,
                                             SourceElement = divChild.OuterHtml,
                                             ViolationDescription = resourceManager.GetString("UseInput")
                                         }).ToList();

                        violationList.AddRange(violationBO);
                        violationBO.Clear();

                        //checks for class properties to see if any border is set
                        violationList = (from divChild in divChildNodes
                                         from child in divChild.DescendantNodesAndSelf().Where(x => (x.Attributes["class"] != null && !(string.IsNullOrEmpty(x.Attributes["class"].Value))))
                                         let key = (from classProp in classProperties where (classProp.Key == child.Attributes["class"].Value) select classProp)
                                         let value = (key.Count() != 0) ? RuleEngineHelper.GetStyleProperties(key.SingleOrDefault().Value).SingleOrDefault(y => y.Key.Contains("border")).Value : string.Empty
                                         where (!string.IsNullOrEmpty(value))
                                         select new ViolationBO
                                         {
                                             SourceLineNumber = divChild.Line,
                                             SourceElement = divChild.OuterHtml,
                                             ViolationDescription = resourceManager.GetString("UseInput")

                                         }).ToList();

                        violationList.AddRange(violationBO);
                        violationBO.Clear();
                    }
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "UseInputRatherThanStyledDiv");
                isPassed = false;
            }
            return violationList;
        }

        /// <summary>
        /// To check for Id Attribute in Embedded Contents(div,p,header,span) for Programmatic Access : MAS 40 # 3
        /// </summary>
        /// <param name="htmlDom"></param>
        /// <returns>Violations list</returns>
        public static List<ViolationBO> HasIdAttributeForElements(HtmlDocument htmlDom, Dictionary<string, string> classProperties, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<HtmlNode> allNodes = new List<HtmlNode>();
            isPassed = true;

            try
            {
                //allNodes.Add(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("DivTag")));
                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("SpanTag")) != null)
                    allNodes.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("SpanTag")));

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H1Tag")) != null)
                    allNodes.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H1Tag")));

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H2Tag")) != null)
                    allNodes.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H2Tag")));

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H3Tag")) != null)
                    allNodes.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H3Tag")));

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H4Tag")) != null)
                    allNodes.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H4Tag")));

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H5Tag")) != null)
                    allNodes.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H5Tag")));

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H6Tag")) != null)
                    allNodes.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("H6Tag")));

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("ParagraphTag")) != null)
                    allNodes.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("ParagraphTag")));

                if (allNodes.Count > 0)
                {
                    var lstAllNodes = (from i in allNodes
                                       where (!string.IsNullOrEmpty(i.InnerText) &&
                                       (i.Attributes["id"] == null || string.IsNullOrEmpty(i.Attributes["id"].Value)))
                                       select i).ToList();

                    if (lstAllNodes.Count > 0)
                    {
                        lstAllNodes = RuleEngineHelper.getAllValidNodes(lstAllNodes, classProperties);

                        violationList = (from i in lstAllNodes
                                         where (!string.IsNullOrEmpty(i.InnerText) &&
                                         (i.Attributes["id"] == null || string.IsNullOrEmpty(i.Attributes["id"].Value)))
                                         select new ViolationBO
                                         {
                                             SourceElement = i.OuterHtml,
                                             SourceLineNumber = i.Line,
                                             ViolationDescription = resourceManager.GetString("ProgrammaticAccess")
                                         }).ToList();
                    }
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasIdAttributeForElements");
                isPassed = false;
            }
            return violationList;
        }

        /// <summary>
        ///  MAS 20, Rule7 - Duplicate table headers are accepted if the data is also duplicate
        ///  Scenario 1: Checks for duplicate header tags(th)
        ///  Scenario 2: Checks for duplicate table data with same header
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>  
        public static List<ViolationBO> DuplicateTableHeaders(HtmlDocument htmlDoc, Dictionary<string, string> classProperties, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            ViolationBO objViolationBO;
            List<HtmlNode> lstTableNode = new List<HtmlNode>();
            int counter;
            isPassed = true;

            try
            {
                //gets list of tables from the html DOM
                //HtmlNodeCollection tableNodes = htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("TableTag"));

                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("TableTag")) != null)
                    lstTableNode.AddRange(htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("TableTag")));

                //Get all valid Nodes
                var lstNode = RuleEngineHelper.getAllValidNodes(lstTableNode, classProperties);


                //Violation for Scenario 1
                if (lstNode.Count > 0)
                {
                    foreach (HtmlNode tblnode in lstNode)
                    {
                        HtmlNodeCollection trNodes = null;
                        counter = 0;

                        if (tblnode.SelectNodes("." + resourceManager.GetString("THeadTrTag")) != null)
                        {
                            trNodes = tblnode.SelectNodes("." + resourceManager.GetString("THeadTrTag"));
                        }
                        else if (tblnode.SelectNodes("." + resourceManager.GetString("TableRowTag")) != null)
                        {
                            trNodes = tblnode.SelectNodes("." + resourceManager.GetString("TableRowTag"));
                        }

                        if (trNodes != null)
                        {
                            foreach (HtmlNode trNode in trNodes)
                            {
                                if ((from trChildNode in trNode.ChildNodes
                                     where trChildNode.Name.Equals(resourceManager.GetString("TableHeaderTag")) && !string.IsNullOrEmpty(trChildNode.InnerText.Trim())
                                     select trChildNode).Count() > 1)
                                    counter++;
                            }
                            if (counter > 1)
                            {
                                objViolationBO = new ViolationBO
                                {
                                    SourceElement = tblnode.OuterHtml,
                                    SourceLineNumber = tblnode.Line,
                                    ViolationDescription = resourceManager.GetString("DuplicateHeaderTags")
                                };
                                violationList.Add(objViolationBO);
                                counter = 0;
                            }
                        }
                    }
                }

                //Violation for Scenario 2
                if (lstNode.Count > 0)
                {
                    Dictionary<int, string> dHeader;
                    Dictionary<int, string> dBody;
                    HtmlNodeCollection tHead = null;
                    HtmlNodeCollection tBody = null;

                    //Declarations for Comparing Data
                    Dictionary<int, string> dHeaderCompare;
                    Dictionary<int, string> dBodyCompare;
                    HtmlNodeCollection tHeadCompare = null;
                    HtmlNodeCollection tBodyCompare = null;

                    //For avoiding multiple table display
                    List<Dictionary<int, string>> dList = new List<Dictionary<int, string>>();


                    foreach (HtmlNode tableNode in lstNode)
                    {
                        dHeader = new Dictionary<int, string>();
                        dBody = new Dictionary<int, string>();

                        /***************Block to Fill the data to header Dictionary********************/

                        tHead = RuleEngineHelper.TableHeaderCollection(tableNode);

                        if (tHead != null)
                        {
                            dHeader = RuleEngineHelper.FillData(tHead);
                        }
                        /***************Block to Fill the data to header Dictionary Ends here***********/


                        /***************Block to fill the data to Body Dictionary***********************/

                        tBody = RuleEngineHelper.TableBodyCollection(tableNode);

                        if (tBody != null)
                        {
                            dBody = RuleEngineHelper.FillData(tBody);
                        }
                        /***************Block to fill the data to Body Dictionary Ends here************/


                        /***************Block to compare the tables************************************/
                        foreach (HtmlNode tableNodeCompare in lstNode)
                        {
                            dHeaderCompare = new Dictionary<int, string>();
                            dBodyCompare = new Dictionary<int, string>();

                            /***************Block to Fill the data to header Dictionary to Compare********************/

                            tHeadCompare = RuleEngineHelper.TableHeaderCollection(tableNodeCompare);

                            if (tHeadCompare != null)
                            {
                                dHeaderCompare = RuleEngineHelper.FillData(tHeadCompare);
                            }
                            /***************Block to Fill the data to header Dictionary  to Compare Ends here***********/


                            /***************Block to fill the data to Body Dictionary  to Compare***********************/

                            int existCount = (from objDHeader in dList
                                              where objDHeader.Except(dHeader).Count() == 0
                                              select objDHeader).Count();

                            //if (!(dHeader.Except(dHeaderCompare).Count() > 0) && dHeader.Count > 0 && dHeaderCompare.Count > 0 && existCount == 0)
                            if (dHeader.Except(dHeaderCompare).Count() == 0 && dHeader.Count > 0 && dHeaderCompare.Count > 0 && existCount == 0)
                            {
                                tBodyCompare = RuleEngineHelper.TableBodyCollection(tableNodeCompare);

                                if (tBodyCompare != null)
                                {
                                    dBodyCompare = RuleEngineHelper.FillData(tBodyCompare);
                                }
                                /***************Block to fill the data to Body Dictionary Ends here  to Compare*************/

                                if (!(dHeader.Except(dHeaderCompare).Count() > 0) && dBody.Except(dBodyCompare).Count() > 0)
                                {
                                    objViolationBO = new ViolationBO()
                                    {
                                        SourceElement = tableNodeCompare.OuterHtml,
                                        SourceLineNumber = tableNodeCompare.Line,
                                        ViolationDescription = resourceManager.GetString("DuplicateTableBody")
                                    };
                                    violationList.Add(objViolationBO);
                                    dList.Add(dHeader);
                                }
                            }
                        }
                        /***************Block to compare the tables Ends here**************************/
                    }
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "DuplicateTableHeaders");
                isPassed = false;
            }
            return violationList;
        }

        /// <summary>
        /// MAS 13, #14 : verify the site utilizes Underline Access Keys
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<ViolationBO> CheckForAccessKeyAttribute(HtmlDocument htmlDoc, Dictionary<string, string> classProperties, ref bool isPassed)
        {
            List<HtmlNode> lstNodes = new List<HtmlNode>();
            List<HtmlNode> lstNodes_temp = new List<HtmlNode>();
            List<ViolationBO> violationBOList = new List<ViolationBO>();
            List<HtmlNode> lstBtnNodes = new List<HtmlNode>();
            List<HtmlNode> lstBtnNodesWithImg = new List<HtmlNode>();

            isPassed = true;

            try
            {
                //Check if the <Button> where inner text != null
                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("ButtonTag")) != null)
                {
                    var lstButton = htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("ButtonTag")).ToList();
                    lstNodes_temp = (from btn in lstButton
                                     where !string.IsNullOrEmpty(btn.InnerText)
                                     select btn).ToList();
                }

                //Check if the <input type="button"> where value != null
                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("HtmlButtonTag")) != null)
                {
                    var lstInputBtn = htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("HtmlButtonTag")).ToList();
                    lstNodes_temp.AddRange(from btn in lstInputBtn
                                           where (btn.Attributes["value"] != null && !string.IsNullOrEmpty(btn.Attributes["value"].Value.ToLower()))
                                           select btn);
                }

                //Check if the <input type="submit"> where value != null
                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("AspButtonTag")) != null)
                {
                    var lstInputSubmit = htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("AspButtonTag")).ToList();
                    lstNodes_temp.AddRange(from btn in lstInputSubmit
                                           where (btn.Attributes["value"] != null && !string.IsNullOrEmpty(btn.Attributes["value"].Value.ToLower()))
                                           select btn);
                }

                //Check if the <UL> which has associated onmouseover event 
                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("ULTag")) != null)
                {
                    var lstUL = htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("ULTag")).ToList();
                    lstNodes_temp.AddRange(from ul in lstUL
                                           where (ul.Attributes["onmouseover"] != null && !string.IsNullOrEmpty(ul.Attributes["onmouseover"].Value))
                                           select ul);
                }


                //If node has background image then ignore those nodes.
                lstBtnNodesWithImg = (from item in lstNodes_temp
                                      where (item.Attributes["class"] != null && !string.IsNullOrEmpty(item.Attributes["class"].Value.Trim()))
                                      let className = item.Attributes["class"] != null ? item.Attributes["class"].Value.ToLower().Trim() : string.Empty
                                      where !string.IsNullOrEmpty(className)
                                      let Style = classProperties.Where(i => i.Key == (resourceManager.GetString("Dot")) + className)
                                      from j in Style
                                      let key = RuleEngineHelper.GetStyleProperties(j.Value.ToLower().Trim()).FirstOrDefault(x => x.Key.ToLower().Trim() == "background")
                                      let VALUE = key.Value != null ? key.Value : string.Empty
                                      where !string.IsNullOrEmpty(VALUE)
                                      select item).ToList();

                //REMOVE FROM THE FINAL LIST
                lstNodes_temp.RemoveAll(i => lstBtnNodesWithImg.Contains(i));

                //get All Valid Nodes
                lstNodes = RuleEngineHelper.getAllValidNodes(lstNodes_temp, classProperties);

                //If the Access key is not available for the list of Nodes then Add it as violation.
                violationBOList = (from node in lstNodes
                                   where node.Attributes["accesskey"] == null || string.IsNullOrEmpty(node.Attributes["accesskey"].Value)
                                   select new ViolationBO
                                   {
                                       SourceElement = node.OuterHtml,
                                       SourceLineNumber = node.Line,
                                       ViolationDescription = resourceManager.GetString("BtnNeedAccessKey")
                                   }).ToList();

                //Check for the nodeList where AccessKey Attr's Value repeats.

                //1.get the Lst of Nodes where accesskey != null
                lstBtnNodes = (from n in lstNodes
                               where n.Attributes["accesskey"] != null && !string.IsNullOrEmpty(n.Attributes["accesskey"].Value)
                               select n).ToList();

                //2.Check for All Duplicate AccessKey  values
                if (lstBtnNodes.Count > 0)
                {
                    List<HtmlNode> lstDuplicate_tmp = lstBtnNodes.FindAll(delegate(HtmlNode i)
                    {

                        return lstBtnNodes.FindAll(delegate(HtmlNode j)
                        { return (i.Attributes["accesskey"].Value.ToLower() == j.Attributes["accesskey"].Value.ToLower()); }).Count() > 1;

                    }).ToList();

                    //lst of Distinct AccessKey value elements (First Occurance)
                    var lstDistNode = lstDuplicate_tmp.GroupBy(p => p.Attributes["accesskey"].Value.ToLower()).Select(r => r.First());

                    //Remove the First Occurance Elements from the DuplicateNode List
                    lstDuplicate_tmp.RemoveAll(i => lstDistNode.Contains(i));

                    //Add the remaining Nodes to Violation
                    violationBOList.AddRange(from dupNode in lstDuplicate_tmp
                                             select new ViolationBO
                                             {
                                                 SourceElement = dupNode.OuterHtml,
                                                 SourceLineNumber = dupNode.Line,
                                                 ViolationDescription = resourceManager.GetString("DupAccesskey")
                                             });
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "CheckForAccessKeyAttribute");
                isPassed = false;
            }
            return violationBOList;
        }
       
        /// <summary>
        /// MAS 12 #1 - Audio Auto-Play
        /// We could check configured tags / attributes in the html
        /// </summary>
        /// <param name="htmlDom"></param>
        /// <returns>violationList</returns>
        public static List<ViolationBO> HasShowPauseForAudioPlay(HtmlDocument htmlDom, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<HtmlNode> lstChildNode_temp = new List<HtmlNode>();
            isPassed = true;

            try
            {
                string xmlpath = string.Format(@"{0}\{1}\MSAudioConfig.xml", AppDomain.CurrentDomain.BaseDirectory, @"\Helper");
                XDocument doc = XDocument.Load(xmlpath);
                var nodesDictionary = (from xmldoc in doc.Descendants("AutoPlayAudioTag")
                                       select new
                                       {
                                           tagname = xmldoc.Descendants("TagName").First().Value.ToLower(),
                                           attr = xmldoc.Descendants("Attribute").First().Value.ToLower()
                                       })
                        .ToDictionary(a => a.tagname, a => a.attr);

                HtmlNodeCollection htmlNodes = htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HtmlTag"));

                if (htmlNodes != null && htmlNodes.Count > 0)
                {
                    lstChildNode_temp = (from i in htmlNodes.Descendants()
                                         select i).ToList();
                }

                if (lstChildNode_temp != null && lstChildNode_temp.Count > 0)
                {
                    var lstChildNode = (from item in lstChildNode_temp
                                        join nodedict in nodesDictionary on item.Name equals nodedict.Key
                                        where item.Attributes[nodedict.Value] == null
                                        || string.IsNullOrEmpty(item.Attributes[nodedict.Value].Value)
                                        select item).ToList();

                    if (lstChildNode.Count > 0)
                    {
                        lstChildNode = RuleEngineHelper.getAllValidNodes(lstChildNode, classProp);
                        //need to work on this...
                        violationList = (from item in lstChildNode
                                         join nodedict in nodesDictionary on item.Name equals nodedict.Key
                                         //where (item.Attributes[nodedict.Value] == null
                                         //|| string.IsNullOrEmpty(item.Attributes[nodedict.Value].Value)) || (!item.Attributes[nodedict.Value].Equals("controls"))
                                         let check = Convert.ToBoolean(nodedict.Value.Equals("controls"))
                                         where check == false
                                         //(item.Attributes[nodedict.Value] == "controls'))  
                                         select new ViolationBO
                                         {
                                             SourceElement = item.OuterHtml,
                                             SourceLineNumber = item.Line,
                                             ViolationDescription = string.Format(resourceManager.GetString("MissingTag"), nodedict.Key, nodedict.Value)
                                         }).ToList();

                    }
                    else    //if condition and else condition with fucntionality is added/modified by Chaitanya G
                    {
                        var lstAudioNode = (from item in htmlNodes
                                            join nodedict in nodesDictionary on item.Name equals nodedict.Key
                                            where item.Attributes[nodedict.Value] == null
                                            || string.IsNullOrEmpty(item.Attributes[nodedict.Value].Value)
                                            select item).ToList();

                        lstAudioNode = RuleEngineHelper.getAllValidNodes(lstAudioNode, classProp);

                        violationList = (from item in lstAudioNode
                                         join nodedict in nodesDictionary on item.Name equals nodedict.Key
                                         where item.Attributes[nodedict.Value] == null
                                         || string.IsNullOrEmpty(item.Attributes[nodedict.Value].Value)
                                         select new ViolationBO
                                         {
                                             SourceElement = item.OuterHtml,
                                             SourceLineNumber = item.Line,
                                             ViolationDescription = string.Format(resourceManager.GetString("MissingTag"), nodedict.Key, nodedict.Value)
                                         }).ToList();

                    }




                }





            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasShowPauseForAudioPlay");
                isPassed = false;
            }
            return violationList;
        }

        /// <summary>
        /// MAS 27 #5 Efficient Navigation&Interaction
        /// check accesskey attribute in the commonly used elements for Efficent Navigation&Interaction
        /// </summary>
        /// <param name="htmlDom"></param>
        /// <returns>violationList</returns>
        public static List<ViolationBO> HasAccessKeyForHtml(HtmlDocument htmlDom, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<ViolationBO> violationBOList = new List<ViolationBO>();
            List<HtmlNode> nodeList = new List<HtmlNode>();
            List<HtmlNode> lst = new List<HtmlNode>();
            isPassed = true;

            try
            {
                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("ButtonTag")) != null)
                    nodeList.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("ButtonTag")).ToList());

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HtmlButtonTag")) != null)
                    nodeList.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HtmlButtonTag")).ToList());

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("AspButtonTag")) != null)
                    nodeList.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("AspButtonTag")).ToList());

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("SelectTag")) != null)
                    nodeList.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("SelectTag")).ToList());

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("TextAreaTag")) != null)
                    nodeList.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("TextAreaTag")).ToList());

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HtmlInputText")) != null)
                    nodeList.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HtmlInputText")).ToList());

                //get all valid nodes
                var lstAllNode = RuleEngineHelper.getAllValidNodes(nodeList, classProp);

                //Check for the nodeList where AccessKey Attr is not available / its value is null
                violationBOList = (from node in lstAllNode
                                   where node.Attributes["accesskey"] == null || string.IsNullOrEmpty(node.Attributes["accesskey"].Value)
                                   select new ViolationBO
                                   {
                                       SourceElement = node.OuterHtml,
                                       SourceLineNumber = node.Line,
                                       ViolationDescription = node.Attributes["accesskey"] == null ? resourceManager.GetString("AccessKeyAttNotAvailable") : resourceManager.GetString("AccessKeyAttNull")
                                   }).ToList();

                //Check for the nodeList where AccessKey Attr's Value repeats.
                //1.get the Lst of Nodes where accesskey != null
                lst = (from n in lstAllNode
                       where n.Attributes["accesskey"] != null && !string.IsNullOrEmpty(n.Attributes["accesskey"].Value)
                       select n).ToList();
                //2.Check for All Duplicate AccessKey  values
                if (lst.Count > 0)
                {
                    List<HtmlNode> lstDuplicate_tmp = lst.FindAll(delegate(HtmlNode i)
                    {

                        return lst.FindAll(delegate(HtmlNode j)
                        { return (i.Attributes["accesskey"].Value.ToLower() == j.Attributes["accesskey"].Value.ToLower()); }).Count() > 1;

                    }).ToList();

                    //lst of Distinct AccessKey value elements (First Occurance)
                    var lstDistNode = lstDuplicate_tmp.GroupBy(p => p.Attributes["accesskey"].Value.ToLower()).Select(r => r.First());

                    //Remove the First Occurance Elements from the DuplicateNode List
                    List<HtmlNode> lstDuplicate = lstDuplicate_tmp.Except(lstDistNode).ToList();

                    //Add the remaining Nodes to Violation
                    violationBOList.AddRange(from dupNode in lstDuplicate
                                             select new ViolationBO
                                             {
                                                 SourceElement = dupNode.OuterHtml,
                                                 SourceLineNumber = dupNode.Line,
                                                 ViolationDescription = resourceManager.GetString("DupAccesskey")
                                             });
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasAccessKeyForHtml");
                isPassed = false;
            }
            return violationBOList;
        }

        /// <summary>
        /// MAS 41 #2 HTML lang Attribute
        /// check the lang attribute in the <html> for Internalization/Localization
        /// </summary>
        /// <param name="htmlDom"></param>
        /// <returns>violationList</returns>
        public static List<ViolationBO> HasLangAttForHtml(HtmlDocument htmlDom, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<HtmlNode> lstHtml = new List<HtmlNode>();
            isPassed = true;

            try
            {
                //HtmlNodeCollection htmlNodes = htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HtmlTag"));

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HtmlTag")) != null)
                    lstHtml.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HtmlTag")).ToList());

                //get all valid nodes
                //var htmlNodes = RuleEngineHelper.getAllValidNodes(lstHtml, classProp);

                if (lstHtml.Count > 0 && (lstHtml.Any(i => i.Attributes["lang"] != null && !string.IsNullOrEmpty(i.Attributes["lang"].Value)) == false))
                {
                    if (lstHtml.Any(i => i.Attributes["xml:lang"] != null && !string.IsNullOrEmpty(i.Attributes["xml:lang"].Value)) == false)
                    {
                        List<ViolationBO> violationBOList = (from i in lstHtml
                                                             where i.Attributes["lang"] == null || string.IsNullOrEmpty(i.Attributes["lang"].Value)
                                                             select new ViolationBO
                                                             {
                                                                 SourceElement = "<html></html>",
                                                                 SourceLineNumber = i.Line,
                                                                 ViolationDescription = i.Attributes["lang"] == null ? resourceManager.GetString("LangAttrNotAvailable") : resourceManager.GetString("LangAttrNull")
                                                             }).ToList();

                        if (violationBOList.Any())
                        {
                            string langAttribute = htmlDom.DocumentNode.OuterHtml;
                            if (langAttribute.Contains("lang:"))
                            {
                                int indexLang = langAttribute.IndexOf("lang:");
                                langAttribute = langAttribute.Substring(indexLang, 15);
                                string[] lang = langAttribute.Split(':');
                                langAttribute = lang[1].Substring(lang[1].IndexOf('"'), 10);
                                if (string.IsNullOrEmpty(langAttribute))
                                {
                                    ViolationBO violation = new ViolationBO
                                    {
                                        SourceElement = violationBOList[0].SourceElement,
                                        SourceLineNumber = violationBOList[0].SourceLineNumber,
                                        ViolationDescription = resourceManager.GetString("LangAttrNotAvailable")
                                    };
                                    violationList.Add(violation);
                                }
                            }
                            else
                            {
                                violationList.AddRange(violationBOList);
                            }
                        }
                    }
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasLangAttForHtml");
                isPassed = false;
            }
            return violationList;
        }

        /// <summary>
        /// Gets violation for list of tags if its doesnot have default pointer set as per MAS 26 - Rule #2
        /// </summary>
        /// <param name="htmlDoc"></param>
        /// <param name="classProperties"></param>
        /// <returns></returns>
        public static List<ViolationBO> HasArrowForNormalSelection(HtmlDocument htmlDoc, Dictionary<string, string> classProperties, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<ViolationBO> violationBO = new List<ViolationBO>();
            List<HtmlNode> resultTags = new List<HtmlNode>();
            List<HtmlNode> resultTags_temp = new List<HtmlNode>();
            List<HtmlNode> inputNodes = new List<HtmlNode>();
            isPassed = true;

            try
            {
                //Gets button and select nodes
                string variousTags = string.Format("{0} {1} {2}", resourceManager.GetString("ButtonTag"), resourceManager.GetString("OrCondition"), resourceManager.GetString("SelectTag"));

                resultTags_temp = HTMLExtractor.GetHtmlContentElements(htmlDoc, variousTags);

                //Gets input nodes
                inputNodes = HTMLExtractor.GetHtmlContentElements(htmlDoc, resourceManager.GetString("InputTag"));

                //Checks if nodes are of type submit,checkbox and radio
                inputNodes = (from inputNode in inputNodes
                              where ((inputNode.Attributes["type"] != null && inputNode.Attributes["type"].Value != null) && ((inputNode.Attributes["type"].Value == "button") || (inputNode.Attributes["type"].Value == "submit") || (inputNode.Attributes["type"].Value == "checkbox") || (inputNode.Attributes["type"].Value == "radio")))
                              select inputNode).ToList();

                resultTags_temp.AddRange(inputNodes);

                //get all valid nodes
                resultTags = RuleEngineHelper.getAllValidNodes(resultTags_temp, classProperties);

                //Checking for style property for everynode and checks for cursor type. If its not default then adds to list
                violationBO = (from tags in resultTags.Where(x => (x.Attributes["style"] != null && !(string.IsNullOrEmpty(x.Attributes["style"].Value))))
                               let key = RuleEngineHelper.GetStyleProperties(tags.Attributes["style"].Value).SingleOrDefault(x => x.Key.ToLower().Trim() == "cursor")
                               where (!string.IsNullOrEmpty(key.Value) && key.Value.ToLower() != "default")
                               select new ViolationBO
                               {
                                   SourceLineNumber = tags.Line,
                                   SourceElement = tags.OuterHtml,
                                   ViolationDescription = resourceManager.GetString("DefaultCursor")
                               }).ToList();


                violationList.AddRange(violationBO);

                violationBO.Clear();

                //Checking for class property for everynode and checks for cursor type. If its not default then adds to list
                violationBO = (from tags in resultTags.Where(x => (x.Attributes["class"] != null && !(string.IsNullOrEmpty(x.Attributes["class"].Value))))
                               let key = (from classProp in classProperties where (classProp.Key == (resourceManager.GetString("Dot") + tags.Attributes["class"].Value)) select classProp)
                               let value = (key.Count() != 0) ? RuleEngineHelper.GetStyleProperties(key.SingleOrDefault().Value).SingleOrDefault(y => y.Key.ToLower().Trim() == "cursor").Value : string.Empty
                               where (!string.IsNullOrEmpty(value) && value.ToLower() != "default")
                               select new ViolationBO
                               {
                                   SourceLineNumber = tags.Line,
                                   SourceElement = tags.OuterHtml,
                                   ViolationDescription = resourceManager.GetString("DefaultCursor")
                               }).ToList();

                violationList.AddRange(violationBO);
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasArrowForNormalSelection");
                isPassed = false;
            }
            return violationList;

        }

        /// <summary>
        /// Gets violations for list of tags if it doesnot have I beam for normal texts MAS 26 - Rule #4
        /// </summary>
        /// <param name="htmlDoc"></param>
        /// <param name="classProperties"></param>
        /// <returns></returns>
        public static List<ViolationBO> HasIBeamForText(HtmlDocument htmlDoc, Dictionary<string, string> classProperties, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<HtmlNode> spanNodes = new List<HtmlNode>();
            List<HtmlNode> divNodes = new List<HtmlNode>();
            List<ViolationBO> violationBO = new List<ViolationBO>();
            List<HtmlNode> resultTags = new List<HtmlNode>();
            List<HtmlNode> resultTags_temp = new List<HtmlNode>();
            isPassed = true;

            try
            {
                //Gets h1,h2,h3,h4,h5,h6,h7,label and paragraph tags
                StringBuilder builder = new StringBuilder();
                builder = builder.Append(resourceManager.GetString("H1Tag"));
                builder = builder.Append(resourceManager.GetString("OrCondition"));
                builder = builder.Append(resourceManager.GetString("H2Tag"));
                builder = builder.Append(resourceManager.GetString("OrCondition"));
                builder = builder.Append(resourceManager.GetString("H3Tag"));
                builder = builder.Append(resourceManager.GetString("OrCondition"));
                builder = builder.Append(resourceManager.GetString("H4Tag"));
                builder = builder.Append(resourceManager.GetString("OrCondition"));
                builder = builder.Append(resourceManager.GetString("H5Tag"));
                builder = builder.Append(resourceManager.GetString("OrCondition"));
                builder = builder.Append(resourceManager.GetString("H6Tag"));
                builder = builder.Append(resourceManager.GetString("OrCondition"));
                //builder = builder.Append(resourceManager.GetString("LabelTag"));
                //builder = builder.Append(resourceManager.GetString("OrCondition"));
                builder = builder.Append(resourceManager.GetString("ParagraphTag"));

                string variousTags = builder.ToString();

                //Gets span nodes
                spanNodes = HTMLExtractor.GetHtmlContentElements(htmlDoc, resourceManager.GetString("SpanTag"));

                //Gets div nodes
                divNodes = HTMLExtractor.GetHtmlContentElements(htmlDoc, resourceManager.GetString("DivTag"));

                //Checks if only text exists inside span
                spanNodes = (from spanTags in spanNodes
                             where ((spanTags.Attributes["onclick"] == null) && (spanTags.HasChildNodes) && (spanTags.FirstChild.Name == "#text") && !(string.IsNullOrEmpty(spanTags.FirstChild.InnerText.ToString().Trim())) && !(string.IsNullOrEmpty(spanTags.InnerText)))
                             select spanTags).ToList();

                //Checks if only text exists inside Div
                divNodes = (from divTags in divNodes
                            where ((divTags.Attributes["onclick"] == null) && (divTags.HasChildNodes) && (divTags.FirstChild.Name == "#text") && !(string.IsNullOrEmpty(divTags.FirstChild.InnerText.ToString().Trim())) && !(string.IsNullOrEmpty(divTags.InnerText)))
                            select divTags).ToList();

                //Gets all various tags 
                resultTags_temp = HTMLExtractor.GetHtmlContentElements(htmlDoc, variousTags);


                //Checks if only text exists in all nodes
                resultTags_temp = (from tags in resultTags_temp
                                   where ((tags.HasChildNodes) && (tags.FirstChild.Name == "#text") && !(string.IsNullOrEmpty(tags.InnerText)))
                                   select tags).ToList();

                //Adds all the nodes together
                resultTags_temp.AddRange(spanNodes);
                resultTags_temp.AddRange(divNodes);

                //get all valid nodes
                resultTags = RuleEngineHelper.getAllValidNodes(resultTags_temp, classProperties);

                resultTags = (from i in resultTags.Where(x => !(string.IsNullOrEmpty(x.InnerText)))
                              where !string.IsNullOrEmpty(HttpUtility.HtmlDecode(i.InnerText).Trim())
                              select i).ToList();

                //-----------------------------------
                //Checking for style property for everynode and checks for cursor type. If its not auto then adds to list
                violationBO = (from tags in resultTags.Where(x => (x.Attributes["style"] != null && !(string.IsNullOrEmpty(x.Attributes["style"].Value))))
                               let key = RuleEngineHelper.GetStyleProperties(tags.Attributes["style"].Value).SingleOrDefault(x => x.Key.ToLower().Trim() == "cursor")
                               where (!string.IsNullOrEmpty(key.Value) && key.Value.ToLower() != "text")
                               select new ViolationBO
                               {
                                   SourceLineNumber = tags.Line,
                                   SourceElement = tags.OuterHtml,
                                   ViolationDescription = resourceManager.GetString("AutoCursor")
                               }).ToList();

                violationList.AddRange(violationBO);

                violationBO.Clear();

                //Checking for class property for everynode and checks for cursor type. If its not auto then adds to list
                violationBO = (from tags in resultTags.Where(x => (x.Attributes["class"] != null && !(string.IsNullOrEmpty(x.Attributes["class"].Value))))
                               let key = (from classProp in classProperties where (classProp.Key == (resourceManager.GetString("Dot") + tags.Attributes["class"].Value)) select classProp)
                               let value = (key.Count() != 0) ? RuleEngineHelper.GetStyleProperties(key.SingleOrDefault().Value).SingleOrDefault(y => y.Key.ToLower().Trim() == "cursor").Value : string.Empty
                               where (!string.IsNullOrEmpty(value) && value.ToLower() != "text")
                               select new ViolationBO
                               {
                                   SourceLineNumber = tags.Line,
                                   SourceElement = tags.OuterHtml,
                                   ViolationDescription = resourceManager.GetString("AutoCursor")
                               }).ToList();

                //All invalid node with different cursor types are grouped together
                violationList.AddRange(violationBO);
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasIBeamForText");
                isPassed = false;
            }
            return violationList;
        }

        /// <summary>MAS 19 #1 User Input Instructions
        ///  To verify that user is instructed about type of information that he/she should enter into a field
        /// </summary>
        /// <param name="htmlDoc"></param>
        /// <returns></returns>
        public static List<ViolationBO> HasForAttributeforInputFields(HtmlDocument htmlDoc, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<HtmlNode> nodeList = new List<HtmlNode>();
            List<ViolationBO> violationList = new List<ViolationBO>();
            ViolationBO violationBO = new ViolationBO();
            List<string> compStr = new List<string>();
            List<string> lstForAttr = new List<string>();
            List<HtmlNode> lstInputControls = new List<HtmlNode>();
            List<HtmlNode> labelNodes_temp = new List<HtmlNode>();
            List<HtmlNode> nodeList_temp = new List<HtmlNode>();
            isPassed = true;

            try
            {
                // HtmlNodeCollection labelNodes = htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("LabelTag"));
                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("LabelTag")) != null)
                    labelNodes_temp.AddRange(htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("LabelTag")).ToList());

                //Get al Valid Labels
                var labelNodes = RuleEngineHelper.getAllValidNodes(labelNodes_temp, classProp);

                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("TextAreaTag")) != null)
                    nodeList_temp.AddRange(htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("TextAreaTag")).ToList());

                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("HtmlInputText")) != null)
                    nodeList_temp.AddRange(htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("HtmlInputText")).ToList());

                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("InputPwdTag")) != null)
                    nodeList_temp.AddRange(htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("InputPwdTag")).ToList());


                //Get al ValidNodes
                nodeList = RuleEngineHelper.getAllValidNodes(nodeList_temp, classProp);


                //Collection of all comparable elements
                compStr.Add(resourceManager.GetString("LabelTagName"));
                compStr.Add(resourceManager.GetString("SpanTagName"));
                compStr.Add(resourceManager.GetString("TdTagName"));
                compStr.Add(resourceManager.GetString("TrTagName"));
                compStr.Add(resourceManager.GetString("HeadTags").Split(',')[0]);
                compStr.Add(resourceManager.GetString("HeadTags").Split(',')[1]);
                compStr.Add(resourceManager.GetString("HeadTags").Split(',')[2]);
                compStr.Add(resourceManager.GetString("HeadTags").Split(',')[3]);
                compStr.Add(resourceManager.GetString("HeadTags").Split(',')[4]);
                compStr.Add(resourceManager.GetString("HeadTags").Split(',')[5]);
                compStr.Add(resourceManager.GetString("ParagraphTagName"));
                compStr.Add(resourceManager.GetString("StrongTagName"));

                if (nodeList != null)
                {
                    //Collect all labels with for Attributes
                    if (labelNodes != null)
                    {
                        lstForAttr = (from node in labelNodes
                                      where node.Attributes["for"] != null && !string.IsNullOrEmpty(node.Attributes["for"].Value)
                                      select node.Attributes["for"].Value.Trim().ToString()).ToList();
                    }

                    //Collect all the node which do not map to any for attribute and controls without id's and controls without value attribute
                    lstInputControls = (from i in nodeList.Where(x => x.Attributes["id"] == null
                                        || string.IsNullOrEmpty(x.Attributes["id"].Value)
                                        || !lstForAttr.Contains(x.Attributes["id"].Value)
                                        && (x.Attributes["value"] == null
                                        || (x.Attributes["value"] != null && string.IsNullOrEmpty(x.Attributes["value"].Value)))
                                        && (x.Attributes["title"] == null
                                        || (x.Attributes["title"] != null && string.IsNullOrEmpty(x.Attributes["title"].Value)))
                                        )
                                        select i).ToList();

                    //Logic to generate the violations based on ancestoral checks
                    foreach (var node in lstInputControls)
                    {
                        HtmlNode compareNode = node;
                        int cntrlCount = 0;
                        bool flag = false;
                        bool parentNode = false;
                        int loopCounter = 0;
                        while (true)
                        {
                            if (parentNode)
                            {
                                parentNode = false;
                                if ((from n in compareNode.ChildNodes
                                     where compStr.Contains(n.Name.ToLower().Trim()) && !string.IsNullOrEmpty(n.InnerText.ToString())
                                     select n).Count() > 0)
                                {
                                    flag = true;
                                    break;
                                }
                            }
                            else
                            {
                                if (compareNode.PreviousSibling != null)
                                {
                                    if ((from n in compareNode.ChildNodes
                                         where compStr.Contains(n.Name.ToLower().Trim()) && !string.IsNullOrEmpty(n.InnerText.ToString())
                                         select n).Count() > 0)
                                    {
                                        flag = true;
                                        break;
                                    }
                                    else
                                        if (compStr.Contains(compareNode.PreviousSibling.Name.ToLower().Trim())
                                                && !string.IsNullOrEmpty(compareNode.PreviousSibling.InnerText.ToString()))
                                        {
                                            flag = true;
                                            break;
                                        }
                                        else if ((compareNode.PreviousSibling.Attributes["type"] != null
                                                && compareNode.PreviousSibling.Attributes["type"].Value.ToLower().Trim() == resourceManager.GetString("TextBoxTagName"))
                                                || compareNode.PreviousSibling.Name.ToLower().Trim() == resourceManager.GetString("TextAreaTagName")
                                                || compareNode.PreviousSibling.Name.ToLower().Trim() == resourceManager.GetString("DropDownTagName"))
                                        {
                                            if (cntrlCount < 2)
                                                cntrlCount += 1;
                                            else
                                            {
                                                flag = false;
                                                break;
                                            }
                                        }
                                }
                            }

                            if (compareNode.PreviousSibling != null && compareNode.PreviousSibling.PreviousSibling != null)
                            {
                                compareNode = compareNode.PreviousSibling;
                            }
                            else if (compareNode.ParentNode.Name.ToLower().Trim() == resourceManager.GetString("HtmlTagName"))
                            {
                                flag = false;
                                break;
                            }
                            else if (loopCounter > 15)
                            {
                                flag = false;
                                break;
                            }
                            else
                            {
                                parentNode = true;
                                compareNode = compareNode.ParentNode;
                            }
                            loopCounter++;
                        }

                        if (!flag)
                        {
                            violationBO = new ViolationBO
                            {
                                SourceElement = node.OuterHtml,
                                SourceLineNumber = node.Line,
                                ViolationDescription = resourceManager.GetString("ForAttrforInputFiled"),
                            };
                            violationList.Add(violationBO);
                        }
                    }
                }

            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasForAttributeforInputFields");
                isPassed = false;
            }
            return violationList;
        }

        /// <summary>MAS 19 #2 User Input Instructions
        ///  To verify that instructions are available on choosing an item from a select list
        /// </summary>
        /// <param name="htmlDoc"></param>
        /// <returns></returns>
        public static List<ViolationBO> HasForAttributeforSelectList(HtmlDocument htmlDoc, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            ViolationBO violationBO = new ViolationBO();
            List<string> compStr = new List<string>();
            List<string> lstForAttr = new List<string>();
            List<HtmlNode> lstInputControls = new List<HtmlNode>();
            List<HtmlNode> labelNodes_temp = new List<HtmlNode>();
            List<HtmlNode> selectNodes_temp = new List<HtmlNode>();
            isPassed = true;

            try
            {
                // HtmlNodeCollection labelNodes = htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("LabelTag"));
                // HtmlNodeCollection selectNodes = htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("SelectTag"));

                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("LabelTag")) != null)
                    labelNodes_temp.AddRange(htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("LabelTag")).ToList());
                if (htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("SelectTag")) != null)
                    selectNodes_temp.AddRange(htmlDoc.DocumentNode.SelectNodes(resourceManager.GetString("SelectTag")).ToList());
                //Get al Valid Labels
                var labelNodes = RuleEngineHelper.getAllValidNodes(labelNodes_temp, classProp);
                var selectNodes = RuleEngineHelper.getAllValidNodes(selectNodes_temp, classProp);

                //Collection of all comparable elements
                compStr.Add(resourceManager.GetString("LabelTagName"));
                compStr.Add(resourceManager.GetString("SpanTagName"));
                compStr.Add(resourceManager.GetString("TdTagName"));
                compStr.Add(resourceManager.GetString("TrTagName"));
                compStr.Add(resourceManager.GetString("HeadTags").Split(',')[0]);
                compStr.Add(resourceManager.GetString("HeadTags").Split(',')[1]);
                compStr.Add(resourceManager.GetString("HeadTags").Split(',')[2]);
                compStr.Add(resourceManager.GetString("HeadTags").Split(',')[3]);
                compStr.Add(resourceManager.GetString("HeadTags").Split(',')[4]);
                compStr.Add(resourceManager.GetString("HeadTags").Split(',')[5]);
                compStr.Add(resourceManager.GetString("ParagraphTagName"));

                //get List of all Labels having for attr
                if (selectNodes != null)
                {
                    if (labelNodes != null)
                    {
                        lstForAttr = (from node in labelNodes
                                      where node.Attributes["for"] != null && !string.IsNullOrEmpty(node.Attributes["for"].Value)
                                      select node.Attributes["for"].Value.Trim().ToString()).ToList();
                    }

                    //Collect all the node which do not map to any for attribute and controls without id's and controls without value attribute
                    lstInputControls = (from i in selectNodes.Where(x => x.Attributes["id"] == null
                                        || string.IsNullOrEmpty(x.Attributes["id"].Value)
                                        || !lstForAttr.Contains(x.Attributes["id"].Value))
                                        select i).ToList();


                    //Logic to generate the violations based on ancestoral checks
                    foreach (var node in lstInputControls)
                    {
                        HtmlNode compareNode = node;
                        int cntrlCount = 0;
                        bool flag = false;
                        bool parentNode = false;
                        int loopCounter = 0;
                        while (true)
                        {
                            if (parentNode)
                            {
                                parentNode = false;
                                if ((from n in compareNode.ChildNodes
                                     where compStr.Contains(n.Name.ToLower().Trim()) && !string.IsNullOrEmpty(n.InnerText.ToString())
                                     select n).Count() > 0)
                                {
                                    flag = true;
                                    break;
                                }
                            }
                            else
                            {
                                if (compareNode.PreviousSibling != null)
                                {
                                    if (compStr.Contains(compareNode.PreviousSibling.Name.ToLower().Trim())
                                        && !string.IsNullOrEmpty(compareNode.PreviousSibling.InnerText.ToString()))
                                    {
                                        flag = true;
                                        break;
                                    }
                                    else if ((compareNode.PreviousSibling.Attributes["type"] != null
                                            && compareNode.PreviousSibling.Attributes["type"].Value.ToLower().Trim() == resourceManager.GetString("TextBoxTagName"))
                                            || compareNode.PreviousSibling.Name.ToLower().Trim() == resourceManager.GetString("TextAreaTagName")
                                            || compareNode.PreviousSibling.Name.ToLower().Trim() == resourceManager.GetString("DropDownTagName"))
                                    {
                                        if (cntrlCount < 2)
                                            cntrlCount += 1;
                                        else
                                        {
                                            flag = false;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (compareNode.PreviousSibling != null && compareNode.PreviousSibling.PreviousSibling != null)
                            {
                                compareNode = compareNode.PreviousSibling;
                            }
                            else if (compareNode.ParentNode.Name.ToLower().Trim() == resourceManager.GetString("HtmlTagName"))
                            {
                                flag = false;
                                break;
                            }
                            else if (loopCounter > 15)
                            {
                                flag = false;
                                break;
                            }
                            else
                            {
                                parentNode = true;
                                compareNode = compareNode.ParentNode;
                            }
                            loopCounter++;
                        }

                        if (!flag)
                        {
                            violationBO = new ViolationBO
                            {
                                SourceElement = node.OuterHtml,
                                SourceLineNumber = node.Line,
                                ViolationDescription = resourceManager.GetString("ForAttrforSelectList"),
                            };
                            violationList.Add(violationBO);
                        }
                    }
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasForAttributeforSelectList");
                isPassed = false;
            }
            return violationList;
        }

        /// <summary>MAS 27 #3 Efficient Navigation and Interaction
        ///  To verify that an option is available for the user to navigate within a container(parent) element
        /// </summary>
        /// <param name="htmlDoc"></param>
        /// <returns></returns>
        public static List<ViolationBO> HasLogicalTabOrderinContainer(HtmlDocument htmlDom, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<HtmlNode> nodeList = new List<HtmlNode>();
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<string> lst = new List<string>();
            List<HtmlNode> nodeList_temp = new List<HtmlNode>();
            //collection of controls to check for tab order
            string allChildControlTags = "input,button,a,textarea,select";
            isPassed = true;

            try
            {
                lst = allChildControlTags.Split(",".ToCharArray()).ToList();

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("DivTag")) != null)
                    nodeList_temp.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("DivTag")));

                if (htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("TableTag")) != null)
                    nodeList_temp.AddRange(htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("TableTag")));

                //get all valid nodes
                //Commented since is consumes huge time.
                //nodeList = RuleEngineHelper.getAllValidNodes(nodeList_temp, classProp);

                if (nodeList_temp != null)
                {

                    List<HtmlNode> test = (from item in nodeList_temp
                                           where item.NodeType == HtmlNodeType.Element && item.ParentNode != null
                                           && item.ParentNode.Name.ToLower() != "div" && item.ParentNode.Name.ToLower() != "table"
                                           select item).ToList();

                    test.ForEach(r =>
                    {
                        List<HtmlNode> allChildNodes = (from childItem in r.Descendants()
                                                        where childItem.NodeType == HtmlNodeType.Element && lst.Contains(childItem.Name.ToLower().Trim())
                                                        select childItem).ToList();

                        //Check for all Input of type="hidden"
                        var lstHiddenNode = (from i in allChildNodes
                                             where i.Name == "input" && (i.Attributes["type"] != null && i.Attributes["type"].Value.ToLower().Trim() == "hidden")
                                             select i).ToList();

                        //If exist ? Remove all Input of type="hidden" from the List of childNodes
                        if (lstHiddenNode != null && lstHiddenNode.Count() > 0)
                        {
                            allChildNodes.RemoveAll(i => lstHiddenNode.Contains(i));
                        }

                        if (allChildNodes != null && allChildNodes.Count >= 2)
                        {
                            violationList.AddRange(RuleEngineHelper.CheckForLogicalTabOrder(violationList, allChildNodes, null, true));
                        }
                    });
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasLogicalTabOrderinContainer");
                isPassed = false;
            }
            return violationList;
        }

        /// <summary>MAS 26 #3 Consistent Navigation
        ///  To verify that the mouse utilizes a hand/pointer finger for link selection
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="classProp"></param>
        /// <returns></returns>
        public static List<ViolationBO> HasMousePointerForAnchorTag(HtmlDocument htmlDom, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<ViolationBO> violationBO = new List<ViolationBO>();
            isPassed = true;

            try
            {
                //HtmlNodeCollection aNodes = htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HrefTag"));
                var NodeList = htmlDom.DocumentNode.SelectNodes(resourceManager.GetString("HrefTag")).ToList();

                //get all valid nodes
                var aNodes = RuleEngineHelper.getAllValidNodes(NodeList, classProp);

                if (aNodes != null)
                {
                    //var lstAll = string.Empty;
                    //1.Check if Any Achor Tag has <label> as child node with style attr OR className attr
                    var lst = (from i in aNodes
                               //where i.Name.ToLower().Trim() == "label" && i.InnerText != string.Empty
                               where (i.Descendants().Where(j => j.Name.ToLower().Trim() == "label" && !string.IsNullOrEmpty(j.InnerText)).Count() > 0)
                               from descndnt in i.Descendants()
                               select new
                               {
                                   style = (descndnt.Attributes["style"] != null ? (descndnt.Attributes["style"].Value.Contains("cursor") ? descndnt.Attributes["style"].Value.ToString() : string.Empty) : string.Empty),
                                   className = descndnt.Attributes["class"] != null ? descndnt.Attributes["class"].Value.ToString() : string.Empty,
                                   NodeName = descndnt.Name,
                                   OuterHtml = descndnt.ParentNode.OuterHtml,
                                   Line = descndnt.Line,
                                   Message = "Label inside anchor tag without cursor pointer"
                               }).ToList();
                    //2.Check if Achor Tag has style attr OR className attr
                    lst.AddRange(from item in aNodes
                                 where (from i in item.ChildNodes where i.Name.ToLower() == "label" select i).Count() == 0 && item.Attributes["style"] != null || item.Attributes["class"] != null
                                 select new
                                 {
                                     style = (item.Attributes["style"] != null ? (item.Attributes["style"].Value.Contains("cursor") ? item.Attributes["style"].Value.ToString() : "") : ""),
                                     className = item.Attributes["class"] != null ? item.Attributes["class"].Value.ToString() : "",
                                     NodeName = item.Name,
                                     OuterHtml = item.OuterHtml,
                                     Line = item.Line,
                                     Message = ""
                                 });



                    //check for Label with style Attr OR ClassName
                    violationBO = (from item in lst.Where(x => x.NodeName.ToLower().Trim() == "label")
                                   where item.style == string.Empty || item.className == string.Empty
                                   select new ViolationBO
                                   {
                                       SourceElement = item.OuterHtml,
                                       SourceLineNumber = item.Line,
                                       ViolationDescription = (!string.IsNullOrEmpty(item.Message)) ? item.Message : resourceManager.GetString("CursorPointer")
                                   }).ToList();
                    violationList.AddRange(violationBO);

                    //3.Check for lst with Style attr && not label
                    violationBO = (from item in lst.Where(x => x.NodeName.ToLower().Trim() != "label" && x.style != string.Empty)
                                   let key = RuleEngineHelper.GetStyleProperties(item.style).SingleOrDefault(x => x.Key.ToLower().Trim() == "cursor")
                                   where (!string.IsNullOrEmpty(key.Value) && key.Value.ToLower().Trim() != "pointer")
                                   select new ViolationBO
                                   {
                                       SourceElement = item.OuterHtml,
                                       SourceLineNumber = item.Line,
                                       ViolationDescription = resourceManager.GetString("CursorPointer")
                                   }).ToList();
                    violationList.AddRange(violationBO);

                    //4.Check for lst with ClassName attr
                    violationBO = (from classLst in lst.Where(x => x.NodeName.ToLower().Trim() == "a" && !string.IsNullOrEmpty(x.className))
                                   let style = classProp.Where(i => i.Key == (resourceManager.GetString("Dot")) + classLst.className)
                                   from j in style
                                   let key = RuleEngineHelper.GetStyleProperties(j.Value.ToLower()).SingleOrDefault(x => x.Key.ToLower().Trim() == "cursor")
                                   where (!string.IsNullOrEmpty(key.Value) && key.Value.ToLower().Trim() != "pointer")
                                   select new ViolationBO
                                   {
                                       SourceElement = classLst.OuterHtml,
                                       SourceLineNumber = classLst.Line,
                                       ViolationDescription = resourceManager.GetString("CursorPointer")
                                   }).ToList();
                    violationList.AddRange(violationBO);
                }
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasMousePointerForAnchorTag");
                isPassed = false;
            }
            return violationList;
        }

        /// <summary>
        /// To verify the user is able to tab in Forward/Backward navigation and focus is not trapped : MAS07, Rule#1
        /// </summary>
        /// <param name="doc"></param>
        /// <returns>violationList</returns>
        public static List<ViolationBO> GetAllTagsWithFocusNotTrapped(HtmlDocument doc, Dictionary<string, string> classProp, string UrlName, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<HtmlNode> nodesList = new List<HtmlNode>();
            List<HtmlNode> nodesList_temp = new List<HtmlNode>();
            List<HtmlNode> finalNodeList = new List<HtmlNode>();
            List<HtmlNode> scriptNodes = new List<HtmlNode>();
            isPassed = true;
            #region Events to Be checked
            string strEvents = "onclick,onkeypress,onkeydown,onkeyup";
            List<string> lstEvnts = strEvents.Split(",".ToCharArray()).ToList();
            #endregion

            try
            {
                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("ULTag")) != null)
                    nodesList_temp.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("ULTag")).ToList());

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("ULTag")) != null)
                    nodesList_temp.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("ULTag")).ToList());

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("LITag")) != null)
                    nodesList_temp.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("LITag")).ToList());

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("LabelTag")) != null)
                    nodesList_temp.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("LabelTag")).ToList());

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("ParagraphTag")) != null)
                    nodesList_temp.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("ParagraphTag")).ToList());

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("TRTag")) != null)
                    nodesList_temp.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("TRTag")).ToList());

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("TDTag")) != null)
                    nodesList_temp.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("TDTag")).ToList());

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("H1Tag")) != null)
                    nodesList_temp.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("H1Tag")).ToList());

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("H2Tag")) != null)
                    nodesList_temp.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("H2Tag")).ToList());

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("H3Tag")) != null)
                    nodesList_temp.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("H3Tag")).ToList());

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("H4Tag")) != null)
                    nodesList_temp.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("H4Tag")).ToList());

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("H5Tag")) != null)
                    nodesList_temp.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("H5Tag")).ToList());

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("H6Tag")) != null)
                    nodesList_temp.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("H6Tag")).ToList());

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("HrefTag")) != null)
                    nodesList_temp.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("HrefTag")).ToList());


                //get all valid nodes
                nodesList = RuleEngineHelper.getAllValidNodes(nodesList_temp, classProp);


                List<HtmlNode> nodelst = (from node in nodesList
                                          where node.Descendants().Where(o => o.Name != "#text").Count() >= 1
                                          select node).ToList();

                if (nodelst != null)
                {
                    finalNodeList.AddRange((from node in nodelst
                                            where (
                                                from descnode in node.Descendants()
                                                where descnode.Name != "#text"
                                                && descnode.Name.Equals("label") || descnode.Name.Equals("span") || descnode.Name.Equals("p")
                                                || descnode.Name.Equals("tr") || descnode.Name.Equals("td") || descnode.Name.Equals("h1") || descnode.Name.Equals("h2")
                                                || descnode.Name.Equals("h3") || descnode.Name.Equals("h4") || descnode.Name.Equals("h5") || descnode.Name.Equals("h6")
                                                select descnode).Count() >= 1
                                            select node).ToList());
                }



                finalNodeList.AddRange((from node in nodesList
                                        where node.Descendants().Where(o => o.Name != "#text").Count() < 1
                                        select node).ToList());

                #region ScriptTags // Added by Chaitanya G
                //Gets all the script tags which has inner text - Basically to check for inline javascript functions/file references.
                scriptNodes.AddRange(HTMLExtractor.GetHtmlContentElements(doc, resourceManager.GetString("ScriptTag")));

                scriptNodes = (from nodes in scriptNodes.Where(x => x.Attributes["src"] != null)
                               where !string.IsNullOrEmpty(nodes.Name)
                               select nodes).ToList();

                /*scriptNodes = (from nodes in scriptNodes.Where(x => x.Attributes["type"] != null && !string.IsNullOrEmpty(x.Attributes["type"].Value) && (!string.IsNullOrEmpty(x.Attributes["src"].Value) || x.Attributes["type"].Value == "text/javascript"))
                               where !string.IsNullOrEmpty(nodes.InnerText)
                               select nodes).ToList(); */


                //List<HtmlNode> scriptnodelst = (from scriptnode in scriptNodes
                //                                where scriptnode.Descendants().Where(o => o.Name != "script").Count() >= 1
                //                                select scriptnode).ToList();

                //var responsefromJS = ProcessRequest();
                //Need to cpature the details of the nodes in HTML which has class files.



                //Need to check the javascript functions inthe javascript files.

                //Need to change the code for the url and needs to handle for the same.
                //2.Check for jquery Events Binded to any Active Controls
                var tempNodes = (from i in finalNodeList.Where(j => j.Attributes.Count > 0)
                                 from k in scriptNodes.Where(j => j.Attributes["src"] != null && j.Attributes["src"].Value != null)
                                 let check = ContentValidationRepository.ProcessRequest(i.Name + "." + i.InnerHtml, UrlName, k.Attributes["src"].Value)
                                 where check == true
                                 select new ViolationBO
                                 {
                                     SourceElement = i.OuterHtml,
                                     SourceLineNumber = i.Line,
                                     ViolationDescription = resourceManager.GetString("FocusTrapped")
                                 }
                                 ).ToList();
                violationList.AddRange(tempNodes);

                //3.Check for Custom Javascript Events Binded to any Active Controls and donot consider 'alert'
                //var tempNodes1 = (from i in nodesList.Where(j => !String.IsNullOrEmpty(j.Id))
                //                  from k in i.Attributes
                //                  where lstEvnts.Contains(k.Name.ToLower()) && !string.IsNullOrEmpty(k.Value.Trim()) && !k.Value.ToLower().Contains("alert")
                //                  let check = JSParser.CheckEventsName(scriptNodes, i.Id, k.Value)
                //                  where check == true
                //                  select new ViolationBO
                //                  {
                //                      SourceElement = i.OuterHtml,
                //                      SourceLineNumber = i.Line,
                //                      ViolationDescription = resourceManager.GetString("KeyBoardInterface_Descrip3") + ":" + k.Value.ToLower()
                //                  }).ToList();
                //violationList.AddRange(tempNodes1);

                #endregion

                /* Initial piece of code
                violationList = (from node in finalNodeList
                                 where node.Attributes[resourceManager.GetString("OnClick")] != null
                                 && !string.IsNullOrEmpty(node.Attributes[resourceManager.GetString("OnClick")].Value)
                                 select new ViolationBO
                                 {
                                     SourceElement = node.OuterHtml,
                                     SourceLineNumber = node.Line,
                                     ViolationDescription = resourceManager.GetString("FocusNotTrapped")
                                 }).ToList();

                */
                //Code needs to be modified here for handling the class with event prevent Default modified by Chaitanya G
                /*
                 violationList = (from node in finalNodeList
                                 //where node.Attributes["class"] != null //this gives the items with class names
                                 let key = (from clsProp in classProp where (clsProp.Key == (resourceManager.GetString("Dot") + node.Attributes["class"].Value)) select classProp)
                                 //let value = (key.Count() != 0) ? RuleEngineHelper.GetStyleProperties(key.SingleOrDefault()).SingleOrDefault(y => y.Key.ToLower().Trim() == "outline") : new KeyValuePair<string, string>()
                                 where node.Attributes["class"] != null 
                                 //where ((value.Key == null) || ((!string.IsNullOrEmpty(value.Value)) && (value.Value.ToLower() != "none" || value.Value.ToLower() != "0"));
                                 select new ViolationBO
                                 {
                                     SourceElement = node.OuterHtml,
                                     SourceLineNumber = node.Line,
                                     ViolationDescription = resourceManager.GetString("FocusTrapped")
                                 }).ToList();
                 */


            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "GetAllTagsWithFocusNotTrapped");
                isPassed = false;
            }

            return violationList;
        }

        public static bool ProcessRequest(string text, string url, string js)
        {

            WebClient webClient = new WebClient();
            //Need to maniipulate the URL String here..
            string Url = url;

            // Loop through all instances of the letter a.
            if (!js.Contains("jquery"))
            {
                while (js.Length > 0)
                {
                    int count = 0;
                    for (int j = 0; j < js.Length; j++)
                    {
                        if (js[j] == '/')
                        {
                            count++;
                        }
                    }
                    for (int i = 1; i <= count; i++)
                    {
                        int j = url.LastIndexOf('/');
                        url = url.Substring(0, j);
                    }
                    var url1 = url.ToString();
                    var finalURL = url1 + "/" + js;
                    string htmlCode = webClient.DownloadString(finalURL);

                    Regex re = new Regex("[;\\\\/:*#?\"$<>{},|&']");
                    string outputString = re.Replace(htmlCode, "");

                    outputString = Regex.Replace(outputString, @"\n", "");

                    StringReader r = new StringReader(outputString);

                    while (r.Peek() >= 0)
                    {
                        var line = r.ReadToEnd();
                        if (line.IndexOf(text) > -1)
                        {
                            var arr = line.Contains("event");
                            if (arr == true)
                            {

                                if (line.Contains("preventDefault"))
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }


        /// <summary>
        /// Method to check if focus rectangle property is set for the click able images which are not under anchor tags MAS 13- Rule 10
        /// </summary>
        /// <param name="htmlContent"></param>
        /// <param name="classProperties"></param>
        /// <returns></returns>
        public static List<ViolationBO> HasFocusRectangle(HtmlDocument htmlContent, Dictionary<string, string> classProperties, ref bool isPassed)
        {
            //Various lists to hold the htmlnodes and result nodes
            List<ViolationBO> violationlst = new List<ViolationBO>();
            List<ViolationBO> violationBO = new List<ViolationBO>();
            List<HtmlNode> imgNodes = new List<HtmlNode>();
            List<HtmlNode> imgDivSpanNodes = new List<HtmlNode>();
            isPassed = true;

            try
            {

                //Gets all the images nodes
                var lstImgNode = HTMLExtractor.GetHtmlContentElements(htmlContent, resourceManager.GetString("ImageTag"));
                //get all valid nodes
                imgNodes = RuleEngineHelper.getAllValidNodes(lstImgNode, classProperties);

                //Get list of image nodes with div and span as parent node
                imgDivSpanNodes = (from img in imgNodes
                                   where ((img.ParentNode.Name == "div") || (img.ParentNode.Name == "span"))
                                   select img.ParentNode).ToList();

                //Gets list of image nodes whose parents are not div and span
                imgNodes = (from img in imgNodes
                            where (!(img.ParentNode.Name == "div") && !(img.ParentNode.Name == "span"))
                            select img).ToList();

                #region ImageWithOnclicParents
                //checks if image parent has onclick property defined and fetches those nodes
                var divspanWithClick = (from node in imgDivSpanNodes.Where(x => (x.HasChildNodes == true && x.FirstChild.Name == "img" && x.Attributes["onclick"] != null)) select node).ToList();

                //Checks if style is defined, if yes then checks for outline property
                violationBO = (from tags in divspanWithClick.Where(x => (x.Attributes["style"] != null && !(string.IsNullOrEmpty(x.Attributes["style"].Value))))
                               let key = RuleEngineHelper.GetStyleProperties(tags.Attributes["style"].Value).SingleOrDefault(x => x.Key.ToLower().Trim() == "outline")
                               where ((key.Key == null) || ((!string.IsNullOrEmpty(key.Value)) && (key.Value.ToLower() != "none" || key.Value.ToLower() != "0")))
                               select new ViolationBO
                               {
                                   SourceLineNumber = tags.Line,
                                   SourceElement = tags.OuterHtml,
                                   ViolationDescription = resourceManager.GetString("FocusRectangle")
                               }).ToList();

                violationlst.AddRange(violationBO);
                violationBO.Clear();

                //Checks for class property and checks for outline property
                violationBO = (from tags in divspanWithClick.Where(x => (x.Attributes["class"] != null && !(string.IsNullOrEmpty(x.Attributes["class"].Value))))
                               let key = (from classProp in classProperties where (classProp.Key == (resourceManager.GetString("Dot") + tags.Attributes["class"].Value)) select classProp)
                               let value = (key.Count() != 0) ? RuleEngineHelper.GetStyleProperties(key.SingleOrDefault().Value).SingleOrDefault(y => y.Key.ToLower().Trim() == "outline") : new KeyValuePair<string, string>()
                               where ((value.Key == null) || ((!string.IsNullOrEmpty(value.Value)) && (value.Value.ToLower() != "none" || value.Value.ToLower() != "0")))
                               select new ViolationBO
                               {
                                   SourceLineNumber = tags.Line,
                                   SourceElement = tags.OuterHtml,
                                   ViolationDescription = resourceManager.GetString("FocusRectangle")
                               }).ToList();

                violationlst.AddRange(violationBO);
                violationBO.Clear();
                #endregion

                #region ImagesWithOnclickInsideParent
                //Checks if the parent has hild node and fetches those nodes.
                imgDivSpanNodes = (from node in imgDivSpanNodes.Where(x => x.HasChildNodes) select node).ToList();

                //Checks if img is the 1st child and fetches those image nodes
                var imgATags = (from node in imgDivSpanNodes select node.SelectNodes(resourceManager.GetString("ImageHref"))).ToList();

                //Checks if image has onclick defined
                imgATags = (from node in imgATags.Where(x => ((x.Count == 1) && (x.FirstOrDefault().Name == "img") && (x.FirstOrDefault().Attributes["onclick"] != null))) select node).ToList();

                //Checks for style property 
                violationBO = (from tags in imgATags.Where(x => (x.FirstOrDefault().Attributes["style"] != null && !(string.IsNullOrEmpty(x.FirstOrDefault().Attributes["style"].Value))))
                               let key = RuleEngineHelper.GetStyleProperties(tags.FirstOrDefault().Attributes["style"].Value).SingleOrDefault(x => x.Key.ToLower().Trim() == "outline")
                               where ((key.Key == null) || ((!string.IsNullOrEmpty(key.Value)) && (key.Value.ToLower() != "none" || key.Value.ToLower() != "0")))
                               select new ViolationBO
                               {
                                   SourceLineNumber = tags.FirstOrDefault().Line,
                                   SourceElement = tags.FirstOrDefault().OuterHtml,
                                   ViolationDescription = resourceManager.GetString("FocusRectangle")
                               }).ToList();

                violationlst.AddRange(violationBO);
                violationBO.Clear();

                //checks for class property
                violationBO = (from tags in imgATags.Where(x => (x.FirstOrDefault().Attributes["class"] != null && !(string.IsNullOrEmpty(x.FirstOrDefault().Attributes["class"].Value))))
                               let key = (from classProp in classProperties where (classProp.Key == (resourceManager.GetString("Dot") + tags.FirstOrDefault().Attributes["class"].Value)) select classProp)
                               let value = (key.Count() != 0) ? RuleEngineHelper.GetStyleProperties(key.SingleOrDefault().Value).SingleOrDefault(y => y.Key.ToLower().Trim() == "outline") : new KeyValuePair<string, string>()
                               where ((value.Key == null) || ((!string.IsNullOrEmpty(value.Value)) && (value.Value.ToLower() != "none" || value.Value.ToLower() != "0")))
                               select new ViolationBO
                               {
                                   SourceLineNumber = tags.FirstOrDefault().Line,
                                   SourceElement = tags.FirstOrDefault().OuterHtml,
                                   ViolationDescription = resourceManager.GetString("FocusRectangle")
                               }).ToList();

                violationlst.AddRange(violationBO);
                violationBO.Clear();
                #endregion

                #region IndependentImageWithOnclick
                //Checks for class property for the indepenedent images with onclick
                var classTag = (from n in
                                    (from hn in imgNodes
                                     where (hn.Attributes["onclick"] != null)
                                     select hn).ToList()
                                where (n.Attributes["class"] != null)
                                select n).ToList();

                violationBO = (from tag in classTag.Where(x => (x.Attributes["class"] != null && !(string.IsNullOrEmpty(x.Attributes["class"].Value))))
                               let classKey = (from classProp in classProperties where (classProp.Key == (resourceManager.GetString("Dot") + tag.Attributes["class"].Value)) select classProp)
                               let value = (classKey.Count() != 0) ? RuleEngineHelper.GetStyleProperties(classKey.SingleOrDefault().Value).SingleOrDefault(y => y.Key.ToLower().Trim() == "outline") : new KeyValuePair<string, string>()
                               where ((value.Key == null) || ((!string.IsNullOrEmpty(value.Value)) && (value.Value.ToLower() != "none" || value.Value.ToLower() != "0")))
                               select new ViolationBO
                               {
                                   SourceElement = tag.OuterHtml,
                                   SourceLineNumber = tag.Line,
                                   ViolationDescription = resourceManager.GetString("FocusRectangle")
                               }).ToList();

                violationlst.AddRange(violationBO);
                violationBO.Clear();

                //Checks for style property for the indepenedent images with onclick
                var styleTag = (from n in
                                    (from hn in imgNodes
                                     where (hn.Attributes["onclick"] != null)
                                     select hn).ToList()
                                where (n.Attributes["style"] != null)
                                select n).ToList();

                violationBO = (from tag in styleTag.Where(x => (x.Attributes["style"] != null && !(string.IsNullOrEmpty(x.Attributes["style"].Value))))
                               let key = RuleEngineHelper.GetStyleProperties(tag.Attributes["style"].Value).SingleOrDefault(x => x.Key.ToLower().Trim() == "outline")
                               where ((key.Key == null) || ((!string.IsNullOrEmpty(key.Value)) && (key.Value.ToLower() != "none" || key.Value.ToLower() != "0")))
                               select new ViolationBO
                               {
                                   SourceElement = tag.OuterHtml,
                                   SourceLineNumber = tag.Line,
                                   ViolationDescription = resourceManager.GetString("FocusRectangle")
                               }).ToList();

                violationlst.AddRange(violationBO);
                #endregion
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasFocusRectangle");
                isPassed = false;
            }
            return violationlst;
        }

        /// <summary>
        ///  Active UI elements should have a name and role  MAS 40 # 8
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<ViolationBO> ElementsWithoutNameAndRole(HtmlDocument doc, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<HtmlNode> nodelst = new List<HtmlNode>();
            ViolationBO violationBO = new ViolationBO();
            List<string> compStr = new List<string>();
            isPassed = true;

            try
            {

                //***************Block to check anchor tag elements***********************

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("AnchorTag")) != null)
                    nodelst = doc.DocumentNode.SelectNodes(resourceManager.GetString("AnchorTag")).ToList();

                if (nodelst.Count > 0)
                {
                    nodelst = RuleEngineHelper.getAllValidNodes(nodelst, classProp);

                    //Check for Title Attr && Role Attr && (Inner Text)
                    violationList = (from link in nodelst.Where(i => (i.Attributes["title"] == null || string.IsNullOrEmpty(i.Attributes["title"].Value.Trim())) && i.Attributes["role"] == null && (i.InnerText == null || string.IsNullOrEmpty(i.InnerText.Trim())))
                                     where link.Descendants().Any(j => j.Name.ToLower() == "img" && (j.Attributes["alt"] == null || string.IsNullOrEmpty(j.Attributes["alt"].Value.Trim())))
                                     select new ViolationBO
                                     {
                                         SourceElement = link.OuterHtml,
                                         SourceLineNumber = link.Line,
                                         ViolationDescription = resourceManager.GetString("ElementsNameRoleUnavailable")
                                     }).ToList();

                }

                //***************Block to check anchor tag elements ends******************


                //***************Block to check button elements***********************

                nodelst.Clear();
                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("ButtonTag")) != null)
                    nodelst = doc.DocumentNode.SelectNodes(resourceManager.GetString("ButtonTag")).ToList();

                if (nodelst.Count > 0)
                {
                    nodelst = RuleEngineHelper.getAllValidNodes(nodelst, classProp);

                    violationList.AddRange((from node in nodelst
                                            where (string.IsNullOrEmpty(node.InnerText.ToString().Trim()) && node.Attributes["title"] == null)
                                            || (string.IsNullOrEmpty(node.InnerText.ToString().Trim()) && node.Attributes["title"] != null
                                            && string.IsNullOrEmpty(node.Attributes["title"].Value.ToString().Trim()))
                                            select new ViolationBO
                                            {
                                                SourceElement = node.OuterHtml,
                                                SourceLineNumber = node.Line,
                                                ViolationDescription = resourceManager.GetString("ElementsNameRoleUnavailable")
                                            }).ToList());
                }
                //***************Block to check button elements ends***********************



                //***************Block to check input type = button/submit/reset elements ***********************
                nodelst.Clear();

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("AspButtonTag")) != null)
                    nodelst = doc.DocumentNode.SelectNodes(resourceManager.GetString("AspButtonTag")).ToList();

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("HtmlButtonTag")) != null)
                    nodelst.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("HtmlButtonTag")).ToList());

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("ResetButtonTag")) != null)
                    nodelst.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("ResetButtonTag")).ToList());

                if (nodelst.Count > 0)
                {
                    nodelst = RuleEngineHelper.getAllValidNodes(nodelst, classProp);

                    violationList.AddRange((from node in nodelst
                                            where node.Attributes["value"] == null
                                            || (node.Attributes["value"] != null && string.IsNullOrEmpty(node.Attributes["value"].Value))
                                            select new ViolationBO
                                            {
                                                SourceElement = node.OuterHtml,
                                                SourceLineNumber = node.Line,
                                                ViolationDescription = resourceManager.GetString("ElementsNameRoleUnavailable")
                                            }).ToList());
                }

                //***************Block to check input type = button/submit/reset elements ends***********************

                //***************Block to check input = checkbox/radio elements ***********************
                nodelst.Clear();

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("CheckBoxTag")) != null)
                    nodelst = doc.DocumentNode.SelectNodes(resourceManager.GetString("CheckBoxTag")).ToList();

                if (doc.DocumentNode.SelectNodes(resourceManager.GetString("RadioButtonTag")) != null)
                    nodelst.AddRange(doc.DocumentNode.SelectNodes(resourceManager.GetString("RadioButtonTag")).ToList());

                if (nodelst.Count > 0)
                {
                    nodelst = RuleEngineHelper.getAllValidNodes(nodelst, classProp);

                    compStr.Add(resourceManager.GetString("LabelTagName"));
                    compStr.Add(resourceManager.GetString("SpanTagName"));
                    compStr.Add(resourceManager.GetString("TdTagName"));
                    compStr.Add(resourceManager.GetString("TrTagName"));
                    compStr.Add(resourceManager.GetString("HeadTags").Split(',')[0]);
                    compStr.Add(resourceManager.GetString("HeadTags").Split(',')[1]);
                    compStr.Add(resourceManager.GetString("HeadTags").Split(',')[2]);
                    compStr.Add(resourceManager.GetString("HeadTags").Split(',')[3]);
                    compStr.Add(resourceManager.GetString("HeadTags").Split(',')[4]);
                    compStr.Add(resourceManager.GetString("HeadTags").Split(',')[5]);
                    compStr.Add(resourceManager.GetString("StrongTagName"));

                    foreach (var node in nodelst)
                    {
                        if (node.NextSibling.NodeType == HtmlNodeType.Text && string.IsNullOrEmpty(node.NextSibling.InnerText.Trim()))
                        {
                            if (node.NextSibling.NextSibling == null)
                            {
                                violationBO = new ViolationBO
                                {
                                    SourceElement = node.OuterHtml,
                                    SourceLineNumber = node.Line,
                                    ViolationDescription = resourceManager.GetString("ElementsNameRoleUnavailable"),
                                };
                                violationList.Add(violationBO);
                            }
                            else if (compStr.Contains(node.NextSibling.NextSibling.Name) && !string.IsNullOrEmpty(node.InnerText))
                            {
                                continue;
                            }
                        }
                        else if (node.NextSibling.NodeType == HtmlNodeType.Text && !string.IsNullOrEmpty(node.NextSibling.InnerText.Trim()))
                        {
                            continue;
                        }
                        else
                        {
                            violationBO = new ViolationBO
                            {
                                SourceElement = node.OuterHtml,
                                SourceLineNumber = node.Line,
                                ViolationDescription = resourceManager.GetString("ElementsNameRoleUnavailable"),
                            };
                            violationList.Add(violationBO);
                        }
                    }
                }
                //***************Block to check input type = button/submit/reset elements ***********************
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasForAttributeforInputFields");
                isPassed = false;
            }
            return violationList;
        }


        /// <summary>
        /// Checks for active tags to see if visual focus is set. If not lists for violations - MAS 05 Rule #1
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="classProp"></param>
        /// <returns></returns>
        public static List<ViolationBO> GetTagsWithoutFocus(HtmlDocument doc, Dictionary<string, string> classProp, ref bool isPassed)
        {
            List<HtmlNode> activeNodes = new List<HtmlNode>();
            List<HtmlNode> tempNodes = new List<HtmlNode>();
            List<HtmlNode> scriptNodes = new List<HtmlNode>();
            List<ViolationBO> ResultNodes = new List<ViolationBO>();
            isPassed = true;

            try
            {
                #region ScriptTags
                //Gets all the script tags which has inner text - Basically to check for inline javascript functions            
                scriptNodes.AddRange(HTMLExtractor.GetHtmlContentElements(doc, resourceManager.GetString("ScriptTag")));

                scriptNodes = (from nodes in scriptNodes.Where(x => x.Attributes["type"] != null && !string.IsNullOrEmpty(x.Attributes["type"].Value) && x.Attributes["type"].Value == "text/javascript")
                               where !string.IsNullOrEmpty(nodes.InnerText)
                               select nodes).ToList();
                #endregion


                #region ActiveNodes
                //Gets anchor tags
                activeNodes.AddRange(HTMLExtractor.GetHtmlContentElements(doc, resourceManager.GetString("HrefTag")));

                //Gets all the input tags
                tempNodes.AddRange(HTMLExtractor.GetHtmlContentElements(doc, resourceManager.GetString("InputTag")));

                //Filters the input tags of following types : button,submit,checkbox,text,radio etc.,
                tempNodes = (from inputNode in tempNodes
                             where ((inputNode.Attributes["type"] != null && inputNode.Attributes["type"].Value != null) && ((inputNode.Attributes["type"].Value == "button") || (inputNode.Attributes["type"].Value == "text") || (inputNode.Attributes["type"].Value == "submit") || (inputNode.Attributes["type"].Value == "checkbox") || (inputNode.Attributes["type"].Value == "radio")))
                             select inputNode).ToList();


                //Add all the nodes to activeNode list
                activeNodes.AddRange(tempNodes);
                tempNodes.Clear();

                //Filters the ValidNotes without display:None
                activeNodes = RuleEngineHelper.getAllValidNodes(activeNodes, classProp);
                #endregion

                ResultNodes = CheckTagsWithoutFocus(scriptNodes, activeNodes, classProp);
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "GetTagsWithoutFocus");
                isPassed = false;
            }

            return ResultNodes;
        }


        /// <summary>
        /// Collects the violations for the tags which doesnot have the visual focus set
        /// </summary>
        /// <param name="scriptNodes"></param>
        /// <param name="activeNodes"></param>
        /// <param name="classProp"></param>
        /// <returns></returns>
        public static List<ViolationBO> CheckTagsWithoutFocus(List<HtmlNode> scriptNodes, List<HtmlNode> activeNodes, Dictionary<string, string> classProp)
        {

            List<HtmlNode> tempNodes = new List<HtmlNode>();
            List<ViolationBO> ResultNodes = new List<ViolationBO>();

            try
            {
                #region DisabledNodes
                //Checks if the htmlNode has disabled attribute set. If yes then collects to list the violations
                tempNodes = (from nodes in activeNodes.Where(x => (x.Attributes["disabled"] != null && !string.IsNullOrEmpty(x.Attributes["disabled"].Value)))
                             where nodes.Attributes["disabled"].Value == resourceManager.GetString("Disabled")
                             select nodes).ToList();

                ResultNodes.AddRange(
                                      (from nodes in tempNodes
                                       select new ViolationBO
                                       {
                                           SourceElement = nodes.OuterHtml,
                                           SourceLineNumber = nodes.Line,
                                           ViolationDescription = nodes.Name + resourceManager.GetString("VisulaFocusDescription")
                                       }).ToList()
                                     );
                activeNodes.RemoveAll(n => tempNodes.Contains(n));
                tempNodes.Clear();
                #endregion

                #region CheckStyleForOpacity
                //Checks if the htmlNode has disabled attribute set. If yes then collects to list the violations
                tempNodes = (from nodes in activeNodes.Where(x => (x.Attributes["style"] != null && !string.IsNullOrEmpty(x.Attributes["style"].Value)))
                             let key = RuleEngineHelper.GetStyleProperties(nodes.Attributes["style"].Value).SingleOrDefault(x => x.Key.ToLower().Contains("opacity"))
                             where (!string.IsNullOrEmpty(key.Value) && key.Value == "0")
                             select nodes).ToList();
                ResultNodes.AddRange(
                                      (from nodes in tempNodes
                                       select new ViolationBO
                                       {
                                           SourceElement = nodes.OuterHtml,
                                           SourceLineNumber = nodes.Line,
                                           ViolationDescription = nodes.Name + resourceManager.GetString("VisulaFocusDescription")
                                       }).ToList()
                                     );

                tempNodes.Clear();
                #endregion

                #region CheckCssClassForOpacity
                //Checks if the htmlNode has disabled attribute set. If yes then collects to list the violations
                tempNodes = (from nodes in activeNodes.Where(x => (x.Attributes["class"] != null && !string.IsNullOrEmpty(x.Attributes["class"].Value)))
                             let key = RuleEngineHelper.GetStyleProperties(nodes.Attributes["class"].Value).SingleOrDefault(x => x.Key.ToLower().Contains("opacity"))
                             where (!string.IsNullOrEmpty(key.Value) && key.Value == "0")
                             select nodes).ToList();
                ResultNodes.AddRange(
                                      (from nodes in tempNodes
                                       select new ViolationBO
                                       {
                                           SourceElement = nodes.OuterHtml,
                                           SourceLineNumber = nodes.Line,
                                           ViolationDescription = nodes.Name + resourceManager.GetString("VisulaFocusDescription")
                                       }).ToList()
                                     );

                tempNodes.Clear();
                #endregion

                #region CheckJSFileForOpacityAndDisability
                tempNodes = (from nodes in activeNodes.Where(x => !String.IsNullOrEmpty(x.Id))
                             let disabled = JSParser.CheckFocusDisability(scriptNodes, nodes.Id, classProp)
                             where disabled == true
                             select nodes).ToList();

                ResultNodes.AddRange(
                                     (from nodes in tempNodes
                                      select new ViolationBO
                                      {
                                          SourceElement = nodes.OuterHtml,
                                          SourceLineNumber = nodes.Line,
                                          ViolationDescription = nodes.Name + resourceManager.GetString("VisulaFocusDescription")
                                      }).ToList()
                                    );
                #endregion
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "GetTagsWithoutFocus-CheckTagsWithoutFocus");
            }

            return ResultNodes;
        }

        /// <summary>
        /// Collects the violations for the Active Elemets which doesnot have the Key Board Interface -MAS 36 #4
        /// </summary> 
        /// <param name="scriptNodes"></param>
        /// <param name="activeNodes"></param>
        /// <param name="classProp"></param>
        /// <returns></returns>
        public static List<ViolationBO> HasKeyBoardInterface(HtmlDocument htmlContent, Dictionary<string, string> classProperties, ref bool isPassed)
        {
            isPassed = true;
            List<ViolationBO> violationList = new List<ViolationBO>();
            List<HtmlNode> tempnodelst = new List<HtmlNode>();
            List<HtmlNode> scriptNodes = new List<HtmlNode>();
            List<ViolationBO> ResultNodes = new List<ViolationBO>();
            #region Events to Be checked
            string strEvents = "onclick,onkeypress,onkeydown,onkeyup";
            List<string> lstEvnts = strEvents.Split(",".ToCharArray()).ToList();
            #endregion

            try
            {
                #region List Of Active Elements
                if (htmlContent.DocumentNode.SelectNodes(resourceManager.GetString("AnchorTag")) != null)
                    tempnodelst.AddRange(htmlContent.DocumentNode.SelectNodes(resourceManager.GetString("AnchorTag")).ToList());

                if (htmlContent.DocumentNode.SelectNodes(resourceManager.GetString("ButtonTag")) != null)
                    tempnodelst.AddRange(htmlContent.DocumentNode.SelectNodes(resourceManager.GetString("ButtonTag")).ToList());

                if (htmlContent.DocumentNode.SelectNodes(resourceManager.GetString("AspButtonTag")) != null)
                    tempnodelst.AddRange(htmlContent.DocumentNode.SelectNodes(resourceManager.GetString("AspButtonTag")).ToList());

                if (htmlContent.DocumentNode.SelectNodes(resourceManager.GetString("HtmlButtonTag")) != null)
                    tempnodelst.AddRange(htmlContent.DocumentNode.SelectNodes(resourceManager.GetString("HtmlButtonTag")).ToList());

                if (htmlContent.DocumentNode.SelectNodes(resourceManager.GetString("ResetButtonTag")) != null)
                    tempnodelst.AddRange(htmlContent.DocumentNode.SelectNodes(resourceManager.GetString("ResetButtonTag")).ToList());

                if (htmlContent.DocumentNode.SelectNodes(resourceManager.GetString("CheckBoxTag")) != null)
                    tempnodelst.AddRange(htmlContent.DocumentNode.SelectNodes(resourceManager.GetString("CheckBoxTag")).ToList());

                if (htmlContent.DocumentNode.SelectNodes(resourceManager.GetString("RadioButtonTag")) != null)
                    tempnodelst.AddRange(htmlContent.DocumentNode.SelectNodes(resourceManager.GetString("RadioButtonTag")).ToList());

                if (htmlContent.DocumentNode.SelectNodes(resourceManager.GetString("SelectTag")) != null)
                    tempnodelst.AddRange(htmlContent.DocumentNode.SelectNodes(resourceManager.GetString("SelectTag")).ToList());

                //Check for Valid Active Elements
                tempnodelst = RuleEngineHelper.getAllValidNodes(tempnodelst, classProperties);
                #endregion

                #region ScriptTags
                //Gets all the script tags which has inner text - Basically to check for inline javascript functions            
                scriptNodes.AddRange(HTMLExtractor.GetHtmlContentElements(htmlContent, resourceManager.GetString("ScriptTag")));

                scriptNodes = (from nodes in scriptNodes.Where(x => x.Attributes["type"] != null && !string.IsNullOrEmpty(x.Attributes["type"].Value) && x.Attributes["type"].Value == "text/javascript")
                               where !string.IsNullOrEmpty(nodes.InnerText)
                               select nodes).ToList();
                #endregion

                #region Check Points
                //1.Check for Title Attribute for all Active Elements
                //var lst = (from i in tempnodelst
                //           where i.Attributes["title"] == null || string.IsNullOrEmpty(i.Attributes["title"].Value.Trim())
                //           select new ViolationBO
                //           {
                //               SourceElement = i.OuterHtml,
                //               SourceLineNumber = i.Line,
                //               ViolationDescription = resourceManager.GetString("KeyBoardInterface_Descrip1")
                //           }).ToList();
                //violationList.AddRange(lst);

                //2.Check for jquery Events Binded to any Active Controls
                var tempNodes = (from i in tempnodelst.Where(j => !String.IsNullOrEmpty(j.Id))
                                 let check = JSParser.CheckEvents(scriptNodes, i.Id)
                                 where check == true
                                 select new ViolationBO
                                 {
                                     SourceElement = i.OuterHtml,
                                     SourceLineNumber = i.Line,
                                     ViolationDescription = resourceManager.GetString("KeyBoardInterface_Descrip2")
                                 }
                                 ).ToList();
                violationList.AddRange(tempNodes);

                //3.Check for Custom Javascript Events Binded to any Active Controls and donot consider 'alert'
                var tempNodes1 = (from i in tempnodelst.Where(j => !String.IsNullOrEmpty(j.Id))
                                  from k in i.Attributes
                                  where lstEvnts.Contains(k.Name.ToLower()) && !string.IsNullOrEmpty(k.Value.Trim()) && !k.Value.ToLower().Contains("alert")
                                  let check = JSParser.CheckEventsName(scriptNodes, i.Id, k.Value)
                                  where check == true
                                  select new ViolationBO
                                  {
                                      SourceElement = i.OuterHtml,
                                      SourceLineNumber = i.Line,
                                      ViolationDescription = resourceManager.GetString("KeyBoardInterface_Descrip3") + ":" + k.Value.ToLower()
                                  }).ToList();
                violationList.AddRange(tempNodes1);
                #endregion

            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "HasKeyBoardInterface");
                isPassed = false;
            }
            return violationList;
        }

        public static List<ViolationBO> GetAnimatedImages(HtmlDocument doc, Dictionary<string, string> classProperties, string baseUrl, ref bool isPassed)
        {
            List<ViolationBO> lstViolations = new List<ViolationBO>();
            List<HtmlNode> imageNodes = new List<HtmlNode>();
            isPassed = true;

            try
            {
                imageNodes = HTMLExtractor.GetHtmlContentElements(doc, resourceManager.GetString("ImageTag"));

                imageNodes = (from nodes in imageNodes.Where(x => x.Attributes["src"] != null && !String.IsNullOrEmpty(x.Attributes["src"].Value))
                              where nodes.Attributes["src"].Value.ToLower().Contains(".gif")
                              select nodes).ToList();

                imageNodes = RuleEngineHelper.getAllValidNodes(imageNodes, classProperties);

                lstViolations = (from violatedNodes in imageNodes
                                 let IsAnimated = CheckAnimationProperties(violatedNodes.Attributes["src"].Value, baseUrl)
                                 where IsAnimated
                                 select new ViolationBO
                                 {
                                     SourceElement = violatedNodes.OuterHtml,
                                     SourceLineNumber = violatedNodes.Line,
                                     ViolationDescription = resourceManager.GetString("AnimatedImages")
                                 }).ToList();
            }
            catch (Exception generalEx)
            {
                LogException.CatchException(generalEx, "ContentValidationRepository", "GetAnimatedImages");
                isPassed = false;
            }
            return lstViolations;
        }

        public static bool CheckAnimationProperties(string path, string baseUrl)
        {
            bool IsAnimated = false;
            //Checks if the path is valid.
            path = RuleEngineHelper.ValidateAnimatedImageUrl(path, baseUrl);
            if (!String.IsNullOrEmpty(path))
            {
                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(path);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream receiveStream = response.GetResponseStream();

                    //Loads the image from the the stream
                    using (System.Drawing.Image image = System.Drawing.Image.FromStream(receiveStream))
                    {
                        //Checks if the image is of type GIF 
                        if (image.RawFormat.Equals(ImageFormat.Gif))
                        {
                            //Checks if the image is animated
                            if (System.Drawing.ImageAnimator.CanAnimate(image))
                            {
                                FrameDimension frameDimension = new FrameDimension(image.FrameDimensionsList[0]);
                                int frameCount = image.GetFrameCount(frameDimension);

                                //20736 - PropertyID for PropertyTagFrameRate
                                int frameRate = image.GetPropertyItem(20736).Value[0];

                                //20737 - PropertyID for PropertyTagLoopCount
                                int numberOfRepitition = image.GetPropertyItem(20737).Value[0];

                                //If number of reptitions 0 implies, infinitely looped image.
                                if (numberOfRepitition > 0)
                                {
                                    //Checks if none of the value is 0
                                    if (frameCount > 0 && frameRate > 0)
                                    {
                                        //Product of frameCount, frameRate and numberOfRepitition gives the duration for which the image is animated in milli seconds.
                                        int animatedDuration = frameCount * frameRate * numberOfRepitition;

                                        //Checks if the animatedDuration is more than 5 secs - animatedDuration in milliseconds converted to seconds
                                        animatedDuration = animatedDuration / 1000;
                                        if (animatedDuration > 5)
                                        {
                                            IsAnimated = true;
                                        }
                                    }
                                }
                                //infinite animated duration
                                else if (numberOfRepitition == 0)
                                {
                                    IsAnimated = true;
                                }
                            }
                        }
                    }
                }
                catch (WebException webEx)
                {
                    if (webEx.Status == WebExceptionStatus.ProtocolError && webEx.Response != null)
                    {
                        var resp = (HttpWebResponse)webEx.Response;
                        if (resp.StatusCode == HttpStatusCode.NotFound) //HTTP 404
                        {
                            //the page was not found, continue with next in the for loop
                            IsAnimated = false;
                        }
                        if (resp.StatusCode == HttpStatusCode.Forbidden)
                        {
                            IsAnimated = false;
                        }
                    }
                }
            }
            return IsAnimated;
        }
    }
}
