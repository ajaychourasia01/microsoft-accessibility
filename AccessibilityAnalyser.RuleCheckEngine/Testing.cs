﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security;

using AccessibilityAnalyser.BusinessEntities;

namespace AccessibilityAnalyser.RuleEngine
{
    public static class Testing
    {

        #region Public Methods

        /// <summary>
        /// Get the test cases corresponding to the client, site and run selected
        /// </summary>
        /// <param name="clientName"></param>
        /// <param name="siteName"></param>
        /// <returns></returns>
        public static List<TestCase> GetTestCases(string clientName, string siteName, string multipleChoice, int runId, int buildId)
        {
            var testCaseCollection = new TestCaseCollection();
            var testCaseRuleMapCollection = new TestCaseRuleMapCollection();
            var ruleDetailCollection = new RuleDetailCollection();            
            var violationCollection = new ViolationCollection();
            var testCaseList = new List<TestCase>();
            if (!string.IsNullOrEmpty(multipleChoice))
            {                
                List<TestCaseBO> testCasesBoList = testCaseCollection.GetTestCaseByBuilId(buildId);

                testCaseList.AddRange(testCasesBoList.Select(testCaseBo => new TestCase()
                    {
                        TestCaseId = testCaseBo.TestCaseId, 
                        TestCaseName = testCaseBo.TestCaseName, 
                        BuildId = testCaseBo.BuildId, 
                        IsActive = testCaseBo.IsActive,
                        ruleDetails = ruleDetailCollection.GetRuleDetailsListByTestCase(testCaseBo.TestCaseId).Select(ruleDetailTemp => new RuleDetailBO()
                            {
                                RuleName = ruleDetailTemp.RuleName,
                                RuleDetailId = ruleDetailTemp.RuleDetailId,
                                RuleDetailName = ruleDetailTemp.RuleDetailName,
                                RuleDetailDescription = ruleDetailTemp.RuleDetailDescription,
                                StepsToPerform = ruleDetailTemp.StepsToPerform,
                                MethodToInvoke = ruleDetailTemp.MethodToInvoke,
                                RuleId = ruleDetailTemp.RuleId,
                                ElementToInspect = ruleDetailTemp.ElementToInspect,
                                ElementType = ruleDetailTemp.ElementType,
                                IsAutomated = ruleDetailTemp.IsAutomated,
                                IsActive = ruleDetailTemp.IsActive,
                                CreateDate = ruleDetailTemp.CreateDate,
                                CreateBy = ruleDetailTemp.CreateBy,
                                UpdateDate = ruleDetailTemp.UpdateDate,
                                UpdateBy = ruleDetailTemp.UpdateBy,
                                ViolationsCount = violationCollection.GetViolationsCountPerRule(buildId, runId, ruleDetailTemp.RuleDetailId, "Total", testCaseBo.TestCaseId)                                
                            }).ToList(), 
                        testrulemapDetails = testCaseRuleMapCollection.GetProrityList(testCaseBo.TestCaseId)
                    }));
                
            }           

            return testCaseList;
        }

        /// <summary>
        /// Get clients corresponding to the searchText
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public static List<ClientBO> GetClients(string searchText)
        {
            //Get the clients corresponding to the searchText
            ClientCollection clientCollection = new ClientCollection();

            if (searchText == "*")
            {
                return clientCollection.GetRecords().OrderByDescending(client => client.CreateDate).Take(2).ToList();
            }
            else
            {
                return clientCollection.GetRecords().Where(client => client.ClientName.ToLower().Contains(searchText.ToLower().Trim())).ToList();
            }
        }

        public static List<ClientBO> GetClientSingle(string ClientName)
        {
            //Get the client corresponding to the searchText
            ClientCollection clientCollection = new ClientCollection();
            return clientCollection.GetRecords().Where(client => client.ClientName.ToLower() == ClientName.ToLower().Trim()).ToList();
        }

        /// <summary>
        /// Get the Violation based on the RuleDetail selected
        /// </summary>
        /// <param name="ruleDetailId"></param>
        /// <returns></returns>
        public static Violations GetViolations(int clientId, int buildId, int runId, int ruleDetailId, string violationType,int skip,int testCaseId)
        {
            ////First: Get all the TestCaseRuleMappings ids based on the rule detail id 
            var violationCollection = new ViolationCollection();
            var violations = new Violations
                {
                    ViolationList =violationCollection.GetViolationList(clientId, buildId, runId, ruleDetailId, violationType, skip,testCaseId),
                    TotalViolationCount = violationCollection.GetViolationsCountPerRule(buildId, runId, ruleDetailId, "All", testCaseId),
                    AcceptedViolationCount = violationCollection.GetViolationsCountPerRule(buildId, runId, ruleDetailId, "Accepted", testCaseId),
                    DeclinedViolationCount = violationCollection.GetViolationsCountPerRule(buildId, runId, ruleDetailId, "Declined", testCaseId), 
                };

            return violations;
        }

        /// <summary>
        /// Update a violation
        /// </summary>
        /// <param name="violationDescription"></param>
        /// <param name="violationRecommendation"></param>
        /// <param name="violationId"></param>
        /// <param name="violationActive"></param>
        /// <returns></returns>
        public static bool UpdateViolation(string violationDescription, string violationRecommendation, int violationId, string status)
        {
            ViolationCollection violationCollection = new ViolationCollection();

            ViolationBO violation = new ViolationBO
            {
                ViolationId = violationId,
                ViolationDescription = violationDescription,
                Recomandation = violationRecommendation,
                Status = status
                
            };

            return violationCollection.UpdateDataSingle(violation);
        }

        public static bool UpdateViolationForManual(string violationDescription, string violationRecommendation, string violationName, string sourceURL, string sourceElement, int violationId, string status)
        {
            ViolationCollection violationCollection = new ViolationCollection();

            ViolationBO violation = new ViolationBO
            {
                ViolationId = violationId,
                ViolationDescription = violationDescription,
                Recomandation = violationRecommendation,
                Status = status,
                ViolationName = violationName,
                SourceElement = sourceElement,
                SourceURL = sourceURL

            };

            return violationCollection.UpdateDataSingleForManual(violation);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ClientID"></param>
        /// <returns></returns>
        public static int GetViolationCount(int ClientID)
        {
            ViolationCollection violationCollection = new ViolationCollection();

            return violationCollection.GetRecordCount(ClientID);
        }


        /// <summary>
        /// Get a violation based on ID
        /// </summary>
        /// <param name="violations"></param>
        /// <param name="violationId"></param>
        /// <returns></returns>
        public static ViolationBO GetViolation(List<ViolationBO> violations, int violationId)
        {
            if (violationId == 0)
            {
                return new ViolationBO();
            }
            else
            {
                if (violations.Where(violation => violation.ViolationId == violationId).ToList().Count > 0)
                {
                    return violations.Where(violation => violation.ViolationId == violationId).ToList().First();
                }
                else
                {
                    return new ViolationBO();
                }
            }
        }

        /// <summary>
        /// GetViolationRuleName
        /// </summary>
        /// <param name="violations"></param>
        /// <param name="violationId"></param>
        /// <returns></returns>
        
        ///Ajay
          public static ViolationBO GetViolationRuleName(int RuleDetailIdNew)
           {

               ViolationCollection violation = new ViolationCollection();
               RuleDetailBO rule = new RuleDetailBO();
              //violation.GetRecords
                //if (RuleDetailId != 0)
                //{
                //    violation.RuleName.Where(rule =>rule.RuleDetailId == RuleDetailIdNew);
                //    return new ViolationBO();
                //}
                //else
                //{
           
                  return new ViolationBO();
          
                //}
           }

        /// <summary>
        /// Get Site List
        /// </summary>
        /// <param name="searchText">string</param>
        /// <param name="clientID">int</param>
        /// <returns>List<SiteBO></returns>
        public static List<SiteBO> GetSiteList(string searchText, string clientName)
        {
            List<SiteBO> getSiteList = new List<SiteBO>();
            SiteCollection siteCollection = new SiteCollection();
            List<SiteBO> siteBoList = siteCollection.GetRecords();

            int clientID = GetClientId(clientName);

            try
            {
                if (searchText == "*" || searchText == null)
                {
                    return siteCollection.GetRecords().OrderByDescending(client => client.CreateDate).Take(2).ToList();
                }
                else
                {
                    var siteList = (from r in siteBoList
                                    where r.ClientId == clientID && r.SiteName.ToLower().Contains(searchText.ToLower())
                                    select new
                                    {
                                        r.SiteId,
                                        r.SiteName
                                    }).ToList();
                    foreach (var item in siteList)
                    {
                        SiteBO siteBoDatas = new SiteBO
                        {
                            SiteId = item.SiteId,
                            SiteName = item.SiteName
                        };
                        getSiteList.Add(siteBoDatas);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return getSiteList;
        }

        /// <summary>
        /// Get ClientId based on client name
        /// </summary>
        /// <param name="clientName">string</param>
        /// <returns>int</returns>
        public static int GetClientID(string clientName)
        {
            int clientId = 0;
            ClientCollection clientCollection = new ClientCollection();

            if (!string.IsNullOrEmpty(clientName))
            {
                try
                {
                    clientId = clientCollection.GetRecords().Where(client => client.ClientName.ToLower() == clientName.ToLower()).ToList().First().ClientId;
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
            }
            return clientId;
        }

        /// <summary>
        /// Get Site ID
        /// </summary>
        /// <param name="url">string</param>
        /// <returns>int</returns>
        public static int GetSiteID(string url, int clientID)
        {
            int siteId = 0;
            SiteCollection siteCollection = new SiteCollection();

            if (!string.IsNullOrEmpty(url))
            {
                try
                {
                    siteId = siteCollection.GetRecords().Where(site => site.SiteName.ToLower() == url.ToLower() && site.ClientId == clientID).ToList().First().SiteId;
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
            }
            return siteId;
        }

        /// <summary>
        /// Get builds and runs corresponding to the site
        /// </summary>
        /// <param name="clientName"></param>
        /// <param name="siteName"></param>
        /// <param name="multipleChoice"></param>
        /// <returns></returns>
        public static List<Build> GetBuilds(string clientName, string siteName)
        {
            int MAX = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MAX"].ToString());
            List<Build> builds = new List<Build>();
            BuildCollection buildCollection = new BuildCollection();
            RunDetailCollection runDetailsCollection = new RunDetailCollection();

            int clientId = GetClientID(clientName); 
            int siteId = GetSiteID(siteName, clientId);

            //First get all the builds
            List<BuildBO> buildsList = buildCollection.GetRecords();

            //Second: Get builds corresponding to the site
            buildsList = (from build in buildsList
                          where build.SiteId == siteId
                          select build).OrderByDescending(build => build.CreateDate).Take(MAX).ToList();

            //Third: Get Rundetails for each build and populate Build 
            foreach (BuildBO buildBO in buildsList)
            {
                //Get Rule details for each test case
                List<RunBO> runDetails = new List<RunBO>();

                runDetails = (from run in runDetailsCollection.GetRecords()
                              where run.BuildId == buildBO.BuildId
                              select run).OrderByDescending(build => build.CreatedDate).Take(MAX).ToList();

                if (runDetails.Count > 0)
                {
                    Build build = new Build
                    {
                        BuildId = buildBO.BuildId,
                        BuildName = buildBO.BuildName,
                        BuildDescription = buildBO.BuildDescription,
                        SiteId = buildBO.SiteId,
                        IsActive = buildBO.IsActive,
                        CreateDate = buildBO.CreateDate,
                        CreateBy = buildBO.CreateBy,
                        UpdateDate = buildBO.UpdateDate,
                        UpdateBy = buildBO.UpdateBy
                    };

                    foreach (RunBO runBO in runDetails)
                    {
                        build.AddRunDetail(runBO);
                    }
                    builds.Add(build);
                }
            }

            return builds;
        }

        /// <summary>
        /// Create Violation
        /// </summary>
        /// <param name="violationDescription"></param>
        /// <param name="violationRecommendation"></param>
        /// <param name="violationId"></param>
        /// <param name="violationActive"></param>
        /// <returns></returns>
        public static bool CreateViolation(string violationName, string violationDescription, int runId, string siteName, string violationSourceElement,
                                            int violationSourceLineNumber, string violationRecommendation, string status, int clientId, int buildId, string TestCaseName, int violationlogID)
        {
            ViolationCollection violationCollection = new ViolationCollection();

            ViolationBO violation = new ViolationBO
            {
                ViolationName = violationName,
                ViolationDescription = violationDescription,
                TRMappingId = GetTRMappingID(buildId, TestCaseName),
                RunId = runId,
                SourceURL = siteName,
                SourceElement = violationSourceElement,
                SourceLineNumber = violationSourceLineNumber,
                Recomandation = violationRecommendation,
                IsActive = true,
                CreateDate = DateTime.Now,
                CreateBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name,
                UpdateDate = DateTime.Now,
                UpdateBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name,
                Status = status,
                ClientId = clientId,
                BuildId = buildId,
                violationLogId = violationlogID
            };

            return violationCollection.CreateDataSingle(violation);
        }

        /// <summary>
        /// Get Status based on ViolationLogId
        /// </summary>
        /// <param name="ClientName"></param>
        /// <returns></returns>
        public static char GetViolationLogStatus()
        {
            //Get client based on the clientID            
            ViolationLogCollection violationLogColl = new ViolationLogCollection();

            return violationLogColl.GetViolationLogStatus();
        }

        #endregion Public Methods

        #region Private Methods
        /// <summary>
        /// Get Builds based on client and site
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        private static List<BuildBO> GetBuilds(int siteId)
        {
            //Getting builds based on site. 
            BuildCollection buildCollection = new BuildCollection();
            return buildCollection.GetRecords().Where(build => build.SiteId == siteId).ToList();
        }

        /// <summary>
        /// Get ClientID based on client name
        /// </summary>
        /// <param name="ClientName"></param>
        /// <returns></returns>
        private static int GetClientId(string ClientName)
        {
            //Get client based on the clientID
            ClientCollection clientCollection = new ClientCollection();
            if (clientCollection.GetRecords().Where(client => client.ClientName.ToLower() == ClientName.ToLower()).ToList().Count > 0)
            {
                return clientCollection.GetRecords().Where(client => client.ClientName.ToLower() == ClientName.ToLower()).ToList().First().ClientId;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Get TRMaapingID for manual test case
        /// </summary>
        /// <returns></returns>
        private static int GetTRMappingID(int buildId, string TestCaseName)
        {
            TestCaseRuleMapCollection trMapCollection = new TestCaseRuleMapCollection();
            TestCaseCollection tcCollection = new TestCaseCollection();
            RuleDetailCollection rdCollection = new RuleDetailCollection();

            //int iTestCaseID = tcCollection.GetRecords().Where(tc => tc.TestCaseName.ToLower() == "manualtestcase" && tc.BuildId == buildId).ToList().First().TestCaseId;
            //tcCollection.GetTestCaseByBuilId(buildId).First().
            int iTestCaseID = tcCollection.GetRecords().Where(tc => tc.TestCaseName == TestCaseName && tc.BuildId == buildId).ToList().First().TestCaseId;

            int iRuleDetailCollectionID = rdCollection.GetRecords().Where(rd => rd.RuleDetailName.ToLower() == "manual rule").ToList().First().RuleDetailId;
            return trMapCollection.GetRecords().Where(tr => tr.TestCaseId == iTestCaseID && tr.RuleDetailId == iRuleDetailCollectionID).ToList().First().TRMappingId;
        }


        #endregion

    }
}
