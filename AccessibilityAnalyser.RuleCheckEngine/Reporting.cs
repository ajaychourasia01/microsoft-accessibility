﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AccessibilityAnalyser.Common;

using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Interfaces;
using System.Globalization;

namespace AccessibilityAnalyser.RuleEngine
{
    public static class Reporting
    {
        #region Private Varibales

        static IEnumerable<TestCaseRuleMapBO> testCaseRuleMap = null;
        static IEnumerable<ViolationBO> violationDetails = null;

        static IEnumerable<TestCaseBO> testCaseDetails = null;
        static List<BuildBO> buildDetails = null;
        static List<ClientBO> clientDetail = null;
        static List<SiteBO> siteDetails = null;
        static SiteBO siteDetail = null;
        static List<RunBO> runDetails = null;
        static List<ReportingDetails> reportingDetails = null;


        #endregion Private Varibales

        #region Public Methods
        public static void GetData(int clientId)
        {
            // Load all business entity lists
            LoadAllBusinessEntityLists(clientId);
        }

        /// <summary>
        /// Get the starting 5 builds and latest 5 builds based on clientId
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public static IEnumerable<BuildBO> GetNoOfBuilds(int clientId)
        {
            //LoadAllBusinessEntityLists(clientId);

            IEnumerable<BuildBO> buildObjects = null;

            buildObjects = (IEnumerable<BuildBO>)(((from c in clientDetail
                                                    join s in siteDetails on c.ClientId equals s.ClientId
                                                    join b in buildDetails on s.SiteId equals b.SiteId
                                                    where c.ClientId == clientId
                                                    orderby b.CreateDate descending
                                                    //orderby b.BuildId
                                                    select new { b }.b).Take(3)
                              )
                                 );

            //buildObjects = (from c in clientDetail
            //                join s in siteDetails on c.ClientId equals s.ClientId
            //                join b in buildDetails on s.SiteId equals b.SiteId
            //                where c.ClientId == clientId
            //                orderby b.CreateDate descending
            //                select new BuildBO
            //                {
            //                    BuildId = b.BuildId,
            //                }).Take(3).ToList();

            //.OrderBy(r => r.BuildId);


            return buildObjects;
        }

        /// <summary>
        /// Get test case ratio (passed / failed)
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="buildId"></param>
        /// <returns></returns>
        public static List<ReportingDetails> GetTestCaseRatioDetails(int clientId, List<int> buildIDs)
        {
            //LoadAllBusinessEntityLists(clientId);
            List<ReportingDetails> getTestCaseRatioDetails = new List<ReportingDetails>();
            string runName = string.Empty;
            string buildName = string.Empty;
            
            try
            {
                // Get latest 3 runs of build
                buildIDs.ForEach(delegate(int buildId)
                {
                    ReportingDetails reportingDetail = new ReportingDetails();
                    RunDetailCollection runDetailsCollection = new RunDetailCollection();
                    BuildCollection buildCollection = new BuildCollection();
                    ViolationCollection violationCollection = new ViolationCollection();
                    reportingDetails = new List<ReportingDetails>();

                    int totalCount = 0;
                    int failCount = 0;
                    int failedViolationsCount = 0;
                    int totalViolationsCount = 0;
                    double failPerc = 0.0;
                    List<int> runDetail = new List<int>();
                    //Getting rundetail for each buildId
                    
                     runDetail = (from r in runDetails
                                     where r.BuildId == buildId
                                     where r.ClientId == clientId
                                     select  r.RunId).Distinct().ToList();

                    totalCount = (from tc in testCaseDetails
                                  where tc.BuildId == buildId
                                  select tc.TestCaseId).Distinct().Count();

                    buildName = buildCollection.GetRecords().Where(r => r.BuildId == buildId).ToList().First().BuildName;

                    runDetail.ForEach(delegate(int runItem)
                    //foreach (var runItem in runDetail)
                    {
                        List<ViolationCount> totalViolationsCountForATestCase = new List<ViolationCount>();
                        List<ViolationCount> failedViolationsCountForATestCase = new List<ViolationCount>();
                        failCount = 0;



                        totalViolationsCount = violationCollection.GetViolationCount(clientId, buildId, runItem);
                        failedViolationsCount = violationCollection.GetFailedViolationCount(clientId, buildId, runItem);

                        var testCaseID = violationCollection.GetTestCaseCount(clientId, buildId, runItem);
                       

                        totalViolationsCountForATestCase.AddRange((from tcId in testCaseID
                                                                   let violationCount = violationCollection.GetTestCaseViolationCount(tcId.TestCaseId, clientId, buildId, runItem)
                                                                   select new ViolationCount(int.Parse(tcId.TestCaseId.ToString()), violationCount)).ToList());
                        failedViolationsCountForATestCase.AddRange((from tcId in testCaseID
                                                                    let violationCount = violationCollection.GetFailedTestCaseViolationCount(tcId.TestCaseId, clientId, buildId, runItem)
                                                                    select new ViolationCount(int.Parse(tcId.TestCaseId.ToString()), violationCount)).ToList());

                        // test case is considered as passed, if total violations count is equals to passed violations count
                        var diff = totalViolationsCountForATestCase.Where(totalViolationItem => failedViolationsCountForATestCase.Any(failedViolationsCountItem => failedViolationsCountItem.TestCaseId == totalViolationItem.TestCaseId && failedViolationsCountItem.ViolationsCount != 0)).ToList();
                        failCount += diff.Count;
                        failPerc = Helper.Percent(failCount, totalCount, 2);

                        runName = runDetailsCollection.GetRecords().Where(r => r.RunId == runItem).ToList().First().RunName;
                      
                        reportingDetail = new ReportingDetails(buildId, runItem, runName, failedViolationsCount, totalViolationsCount, failCount, failPerc, totalCount, null, null, buildName);
                        reportingDetails.Add(reportingDetail);
                    });
                    getTestCaseRatioDetails.AddRange(reportingDetails);
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return getTestCaseRatioDetails;
        }

        /// <summary>
        /// Get total test cases count
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public static ReportingDetails GetTotalTestCaseRatio(int clientId)
        {
            //LoadAllBusinessEntityLists(clientId);

            int totalCount = 0;
            int failCount = 0;
            ReportingDetails reportingDetail = null;
            ViolationCollection violationCollection = new ViolationCollection();
            List<ViolationCount> totalViolationsCountForATestCase = new List<ViolationCount>();
            List<ViolationCount> failedViolationsCountForATestCase = new List<ViolationCount>();
            // get # of test cases from testcase table based on buildId
            totalCount = (from c in clientDetail
                          join s in siteDetails on c.ClientId equals s.ClientId
                          join b in buildDetails on s.SiteId equals b.SiteId
                          join t in testCaseDetails on b.BuildId equals t.BuildId
                          where c.ClientId == clientId
                          select t.TestCaseId).Distinct().Count();

            // get # of passed test cases
            var testCaseID = violationCollection.GetClientTestCaseCount(clientId);
            
           // Get total violations count for all test cases
           totalViolationsCountForATestCase.AddRange((from tcId in testCaseID
                                                      let violationCount = violationCollection.GetClientTestCaseViolationCount(tcId.TestCaseId, clientId)
                                                      select new ViolationCount(int.Parse(tcId.TestCaseId.ToString()), violationCount)).ToList());
            
            // Get passed violations count for all test cases
            failedViolationsCountForATestCase.AddRange((from tcId in testCaseID
                                                        let violationCount = violationCollection.GetClientFailedTestCaseViolationCount(tcId.TestCaseId, clientId)
                                                        select new ViolationCount(int.Parse(tcId.TestCaseId.ToString()), violationCount)).ToList());


            // test case is considered as passed, if total violations count is equals to passed violations count
            var diff = totalViolationsCountForATestCase.Where(totalViolationItem => failedViolationsCountForATestCase.Any(failedViolationsCountItem => failedViolationsCountItem.TestCaseId == totalViolationItem.TestCaseId && failedViolationsCountItem.ViolationsCount != 0)).ToList();
            failCount += diff.Count;
            double failPerc = Helper.Percent(failCount, totalCount, 2);

            reportingDetail = new ReportingDetails(0, 0, null, 0, 0, failCount, failPerc, totalCount, null, null, null);

            return reportingDetail;
        }

        /// <summary>
        /// Get all latest 3 runs based on client name
        /// </summary>
        /// <param name="clientName"></param>
        /// <returns></returns>
        public static List<ReportingDetails> GetLatestBuilds(int clientId)
        {
            //LoadAllBusinessEntityLists(clientId);

            List<ReportingDetails> getAllLatestBuilds = new List<ReportingDetails>();

            List<ReportingDetails> getLatestBuilds = (from c in clientDetail
                                                      join s in siteDetails on c.ClientId equals s.ClientId
                                                      join b in buildDetails on s.SiteId equals b.SiteId
                                                      where c.ClientId == clientId
                                                      orderby b.CreateDate descending
                                                      select new ReportingDetails
                                                      {
                                                          BuildId = b.BuildId,
                                                      }).Take(3).ToList();

            foreach (var item in getLatestBuilds)
            {
                // Get all latest build / runs
                getAllLatestBuilds.AddRange((from r in runDetails
                                             join b in buildDetails on r.BuildId equals b.BuildId
                                             where r.BuildId == item.BuildId
                                             orderby r.UpdateDate descending
                                             select new ReportingDetails
                                             {
                                                 BuildId = b.BuildId,
                                                 BuildName = b.BuildName,
                                                 BuildCreatedDate = b.UpdateDate,
                                                 RunId = r.RunId,
                                                 RunName = r.RunName,
                                                 RunCreatedDate = r.UpdateDate,
                                             }).Take(3).ToList());
            }

            return getAllLatestBuilds;
        }

        /// <summary>
        /// Get clientID based on client name
        /// </summary>
        /// <param name="ClientName"></param>
        /// <returns></returns>
        public static int GetClientId(string clientName)
        {
            //Get client based on the clientID
            int clientId = 0;

            ClientCollection clientCollection = new ClientCollection();
            //List<ClientBO> clientBOList = clientCollection.GetRecords().Where(client => client.ClientName.ToLower() == clientName.ToLower()).ToList();
            List<ClientBO> clientBOList = clientCollection.GetRecords(clientName);

            foreach (var item in clientBOList)
            {
                clientId = item.ClientId;
            }

            return clientId;
        }

        /// <summary>
        /// Get client name based on client id
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public static string GetClientName(int clientId)
        {
            //Get client name based on the clientID
            ClientCollection clientCollection = new ClientCollection();
            return clientCollection.GetRecords().Where(client => client.ClientId == clientId).ToList().First().ClientName;
        }
        #endregion Public Methods


        #region Private Methods
        /// <summary>
        /// Load all business entity lists
        /// </summary>
        private static void LoadAllBusinessEntityLists(int clientId)
        {
            ClientCollection clientCollection = new ClientCollection();
            //clientDetail = clientCollection.GetRecords().Where(c => c.ClientId == clientId).ToList();
            clientDetail = clientCollection.GetRecords(clientId);

            SiteCollection siteCollection = new SiteCollection();
            //siteDetails = siteCollection.GetRecords().Where(s => s.ClientId == clientId).ToList();
            siteDetails = siteCollection.GetRecords(clientId);

            BuildCollection buildCollection = new BuildCollection();
            buildDetails = buildCollection.GetRecords();

            TestCaseCollection testCaseCollection = new TestCaseCollection();
            testCaseDetails = testCaseCollection.GetRecords();

            TestCaseRuleMapCollection testCaseMapCollection = new TestCaseRuleMapCollection();
            testCaseRuleMap = testCaseMapCollection.GetRecords();

            RunDetailCollection runDetailsCollection = new RunDetailCollection();
            runDetails = runDetailsCollection.GetRecords();
        }
        #endregion
    }
}
