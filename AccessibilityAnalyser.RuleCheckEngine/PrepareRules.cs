﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.RuleEngine
{
    public class PrepareRules
    {

        #region Private Varibales

        private IEnumerable<RuleDetailBO> ruleDetailsList = null;
        private IEnumerable<TestCaseBO> testCaseList = null;
        private IEnumerable<TestCaseRuleMapBO> testCaseRuleMapList = null;

        #endregion Private Varibales

        #region Constructor

        public PrepareRules()
        {
        }

        public IEnumerable<TestCaseRuleMapBO> TestCaseRuleMapList
        {
            get { return testCaseRuleMapList; }
            set { testCaseRuleMapList = value; }
        }

        #endregion Constructor

        #region Public Methods

        /// <summary>
        /// Filters the rules for url validation based on ClientName and BuildNames input.
        /// BuildId is passed from UI.
        /// </summary>
        /// <param name="buildId"></param>
        public IEnumerable<RuleDetailBO> PrepareRulesToTest(int buildId, int testCaseId)
        {
            //Get build data from DB filtering om buildid input, mapping to business entity BuildBO.
            BuildCollection buildCollection = new BuildCollection();
            //BuildBO build = buildCollection.GetSingle(buildId);

            //Get TestCase collection data from DB mapping to business entity TextCaseBO.
            TestCaseCollection testCaseCollection = new TestCaseCollection();
            testCaseList = testCaseCollection.GetRecords();

            TestCaseRuleMapCollection testCaseMapCollection = new TestCaseRuleMapCollection();
            IEnumerable<TestCaseRuleMapBO> testCaseRuleMap = testCaseMapCollection.GetRecords();

            RuleDetailCollection ruleDetailCollection = new RuleDetailCollection();
            ruleDetailsList = ruleDetailCollection.GetRecords();

            testCaseRuleMapList = (IEnumerable<TestCaseRuleMapBO>)from tc in testCaseRuleMap
                                                         join t in testCaseList on tc.TestCaseId equals t.TestCaseId
                                                         where tc.TestCaseId == testCaseId && t.BuildId == buildId && t.IsActive == true
                                                         select new { tc }.tc;
            
            ruleDetailsList = from ruleDetail in ruleDetailsList
                              join tcasemap in testCaseRuleMapList on ruleDetail.RuleDetailId equals tcasemap.RuleDetailId
                              select new { ruleDetail }.ruleDetail;

            return ruleDetailsList;
        }

        #endregion Public Methods

    }

}
