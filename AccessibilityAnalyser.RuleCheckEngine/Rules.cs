﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AccessibilityAnalyser.BusinessEntities;

namespace AccessibilityAnalyser.RuleEngine
{
    public static class Rules
    {
        /// <summary>
        /// Get all the Rule name and Rules Details
        /// </summary>
        /// <returns>List<RulesAndRulesDetailsData></returns>
        public static List<RuleDetails> GetAllRuleDetails()
        {
            List<RuleDetails> getRuleDetailList = new List<RuleDetails>();
            RuleCollection objRules = new RuleCollection();
            RuleDetailCollection objRuleDetail = new RuleDetailCollection();

            List<RuleBO> ruleList = objRules.GetRecords();
            List<RuleDetailBO> ruleDetailsList = objRuleDetail.GetRecords();

            var result = (from r in ruleList
                          join rd in ruleDetailsList on r.RuleId equals rd.RuleId
                          where rd.RuleDetailName != "Manual Rule"
                          select new
                          {
                              r.RuleId,
                              r.RuleName,
                              rd.RuleDetailId,
                              rd.RuleDetailDescription
                          }).ToList();

            foreach (var item in result)
            {
                RuleDetails rulesandrulesdetailsdata = new RuleDetails
                {
                    RuleID = item.RuleId,
                    RuleName = item.RuleName,
                    RuleDetailsID = item.RuleDetailId,
                    RulesDescription = item.RuleDetailDescription
                };

                getRuleDetailList.Add(rulesandrulesdetailsdata);
            }

            return getRuleDetailList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<RuleDetails> GetRuleName(int ruleDetilID)
        {
            List<RuleDetails> getRuleDetailList = new List<RuleDetails>();
            RuleCollection objRules = new RuleCollection();
            RuleDetailCollection objRuleDetail = new RuleDetailCollection();

            List<RuleBO> ruleList = objRules.GetRecords();
            List<RuleDetailBO> ruleDetailsList = objRuleDetail.GetRecords();


            var result = (from r in ruleList
                          join rd in ruleDetailsList on r.RuleId equals rd.RuleId
                          where rd.RuleDetailId == ruleDetilID
                          select new
                          {
                              r.RuleId,
                              r.RuleName,
                              rd.RuleDetailId,
                              rd.RuleDetailDescription
                          }).ToList();
            foreach (var item in result)
            {
                RuleDetails rulesandrulesdetailsdata = new RuleDetails
                {
                    RuleID = item.RuleId,
                    RuleName = item.RuleName,
                    RuleDetailsID = item.RuleDetailId,
                    RulesDescription = item.RuleDetailDescription
                };

                getRuleDetailList.Add(rulesandrulesdetailsdata);
            }

            return getRuleDetailList;
        }
        /// <summary>
        /// Get all standards
        /// </summary>
        /// <returns></returns>
        public static List<StandardBO> GetAllGroups()
        {
            List<StandardBO> getAllGroups = new List<StandardBO>();
            StandardBO objStandard = new StandardBO();
            StandardCollection objStandardCollection = new StandardCollection();

            List<StandardBO> standardList = objStandardCollection.GetRecords();

            getAllGroups = (from stdList in standardList
                                select stdList).ToList();
            return getAllGroups;
        }

        /// <summary>
        /// Getting specific Rules and RulesDetails as per ruleID
        /// </summary>
        /// <param name="ruleID">int</param>
        /// <returns>List<RulesAndRulesDetailsData></returns>
        public static List<RuleDetails> GetRuleDetails(string groupID)
        {
            List<RuleDetails> getRuleDetailList = new List<RuleDetails>();
            List<StandardBO> getAllGroups = new List<StandardBO>();
            RuleCollection objRules = new RuleCollection();
            RuleDetailCollection objRuleDetail = new RuleDetailCollection();

            List<RuleBO> rulesList = objRules.GetRecords();
            List<RuleDetailBO> rulesDetailsList = objRuleDetail.GetRecords();

            if (!string.IsNullOrEmpty(groupID))
            {
                var result = (from r in rulesList
                              join rdl in rulesDetailsList on r.RuleId equals rdl.RuleId
                              where groupID.Contains(r.StandardId.ToString())
                              && rdl.RuleDetailName != "Manual Rule"
                              select new
                              {
                                  r.RuleId,
                                  r.StandardId,
                                  r.RuleName,
                                  rdl.RuleDetailId,
                                  rdl.RuleDetailDescription
                              }).ToList();


                foreach (var item in result)
                {
                    RuleDetails rulesandrulesdetailsdata = new RuleDetails
                    {
                        RuleID = item.RuleId,
                        RuleName = item.RuleName,
                        RuleDetailsID = item.RuleDetailId,
                        RulesDescription = item.RuleDetailDescription
                    };
                    
                    getRuleDetailList.Add(rulesandrulesdetailsdata);
                }
            }

            return getRuleDetailList;
        }
    }
}
