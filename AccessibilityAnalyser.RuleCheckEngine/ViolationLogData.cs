﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AccessibilityAnalyser.BusinessEntities;

namespace AccessibilityAnalyser.RuleEngine
{
    public class ViolationLogData
    {
        public string sourceURL { get; set; }
        public string invokedMethodName { get; set; }
        public int violationLogId { get; set; }
        public bool status { get; set; }

        #region Public Methods

        public static int GetIntialViolationLogID(int clientID)
        {
            int violationLogID = 0;
            ViolationLogCollection violLogCollection = new ViolationLogCollection();

            try
            {
                violationLogID = violLogCollection.GetIntialViolationLogId(clientID);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return violationLogID;
        }

        # endregion
    }
}
