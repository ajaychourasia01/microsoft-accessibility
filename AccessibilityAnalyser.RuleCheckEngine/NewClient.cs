﻿namespace AccessibilityAnalyser.RuleEngine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using AccessibilityAnalyser.BusinessEntities;
    using AccessibilityAnalyser.Interfaces;
    using AccessibilityAnalyser.DataAdapter;
    using System.Security.Principal;
 
    public static class NewClient
    {
        static string userName = WindowsIdentity.GetCurrent().Name;

        /// <summary>
        /// Get Client List
        /// </summary>
        /// <param name="searchText">string</param>
        /// <returns>List<ClientBO></returns>
        public static List<ClientBO> GetClientList(string searchText)
        {
            List<ClientBO> clientList = new List<ClientBO>();
            ClientCollection clientCollection = new ClientCollection();

            try
            {
                clientList = (from r in clientCollection.GetRecords()
                              where r.ClientName.ToLower().Contains(searchText.ToLower().Trim())
                              select new ClientBO
                              {
                                  ClientId = r.ClientId,
                                  ClientName = r.ClientName
                              }).ToList();

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return clientList;
        }

        public static List<ClientBO> GetClientList()
        {
            List<ClientBO> clientList = new List<ClientBO>();
            ClientCollection clientCollection = new ClientCollection();

            try
            {
                clientList = (from r in clientCollection.GetRecords()
                              select new ClientBO
                              {
                                  ClientId = r.ClientId,
                                  ClientName = r.ClientName
                              }).ToList();

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return clientList;
        }

        /// <summary>
        /// Get Site List
        /// </summary>
        /// <param name="searchText">string</param>
        /// <param name="clientID">int</param>
        /// <returns>List<SiteBO></returns>
        public static List<SiteBO> GetSiteList(string searchText, int clientID)
        {
            SiteCollection siteCollection = new SiteCollection();
            List<SiteBO> siteList = new List<SiteBO>();

            try
            {
                siteList = (from r in siteCollection.GetRecords()
                            where r.ClientId.Equals(clientID) && r.SiteName.ToLower().Contains(searchText.ToLower().Trim())
                            select new SiteBO
                            {
                                SiteId = r.SiteId,
                                SiteName = r.SiteName
                            }).ToList();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return siteList;
        }

        public static List<SiteBO> GetSiteList(int clientID)
        {
            SiteCollection siteCollection = new SiteCollection();
            List<SiteBO> siteList = new List<SiteBO>();

            try
            {
                siteList = (from r in siteCollection.GetRecords()
                            where r.ClientId.Equals(clientID)
                            select new SiteBO
                            {
                                SiteId = r.SiteId,
                                SiteName = r.SiteName
                            }).ToList();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return siteList;
        }

        /// <summary>
        /// Get Build List 
        /// </summary>
        /// <param name="searchText">string</param>
        /// <param name="siteID">int</param>
        /// <returns>List<BuildBO></returns>
        public static List<BuildBO> GetBuildList(string searchText, int siteID)
        {
            List<BuildBO> buildList = new List<BuildBO>();
            BuildCollection buildCollection = new BuildCollection();

            try
            {
                buildList = (from r in buildCollection.GetRecords()
                             where r.SiteId == siteID && r.BuildName.ToLower().Contains(searchText.ToLower().Trim())
                             select new BuildBO
                             {
                                 BuildId = r.BuildId,
                                 BuildName = r.BuildName,
                                 CreateDate = r.CreateDate
                             }).ToList();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return buildList;
        }

        public static List<BuildBO> GetBuildList(int siteID)
        {
            List<BuildBO> buildList = new List<BuildBO>();
            BuildCollection buildCollection = new BuildCollection();

            try
            {
                buildList = (from r in buildCollection.GetRecords()
                             where r.SiteId == siteID
                             select new BuildBO
                             {
                                 BuildId = r.BuildId,
                                 BuildName = r.BuildName,
                                 CreateDate = r.CreateDate
                             }).ToList();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return buildList;
        }

        /// <summary>
        /// Get Run List
        /// </summary>
        /// <param name="searchText">string</param>
        /// <param name="buildID">int</param>
        /// <returns> List<RunBO></returns>
        public static List<RunBO> GetRunList(string searchText, int buildID)
        {
            List<RunBO> runList = new List<RunBO>();
            RunDetailCollection runDetailCollection = new RunDetailCollection();

            try
            {
                runList = (from r in runDetailCollection.GetRecords()
                           where r.BuildId == buildID && r.RunName.ToLower().Contains(searchText.ToLower().Trim())
                           select new RunBO
                               {
                                   RunId = r.RunId,
                                   RunName = r.RunName
                               }).ToList();
            }
            catch (Exception Ex)
            {
                Ex.ToString();
            }
            return runList;
        }

        public static List<RunBO> GetRunList(int buildID)
        {
            List<RunBO> runList = new List<RunBO>();
            RunDetailCollection runDetailCollection = new RunDetailCollection();

            try
            {
                runList = (from r in runDetailCollection.GetRecords()
                           where r.BuildId == buildID
                           select new RunBO
                           {
                               RunId = r.RunId,
                               RunName = r.RunName
                           }).ToList();
            }
            catch (Exception Ex)
            {
                Ex.ToString();
            }
            return runList;
        }

        /// <summary>
        /// Get ClientId based on client name
        /// </summary>
        /// <param name="clientName">string</param>
        /// <returns>int</returns>
        public static int GetClientID(string clientName)
        {
            int clientId = 0;
            ClientCollection clientCollection = new ClientCollection();

            if (!string.IsNullOrEmpty(clientName))
            {
                try
                {
                    clientId = clientCollection.GetRecords().Where(client => client.ClientName.ToLower() == clientName.ToLower()).ToList().First().ClientId;
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
            }
            return clientId;
        }

        /// <summary>
        /// Get Site ID
        /// </summary>
        /// <param name="url">string</param>
        /// <returns>int</returns>
        public static int GetSiteID(string url, int clientID)
        {
            int siteId = 0;
            SiteCollection siteCollection = new SiteCollection();

            if (!string.IsNullOrEmpty(url))
            {
                try
                {
                    siteId = siteCollection.GetRecords().Where(site => site.SiteName.ToLower() == url.ToLower() && site.ClientId == clientID).ToList().First().SiteId;
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
            }
            return siteId;
        }

        /// <summary>
        /// Get Build ID
        /// </summary>
        /// <param name="buildName">string</param>
        /// <returns>int</returns>
        public static int GetBuildID(string buildName, int siteID)
        {
            int buildID = 0;
            BuildCollection buildcollection = new BuildCollection();

            if (!string.IsNullOrEmpty(buildName))
            {
                try
                {
                    buildID = buildcollection.GetRecords().Where(build => build.BuildName.ToLower() == buildName.ToLower() && build.SiteId == siteID).ToList().First().BuildId;
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
            }
            return buildID;
        }

        /// <summary>
        /// Get Run ID
        /// </summary>
        /// <param name="runName">string</param>
        /// <returns>int</returns>
        public static int GetRunID(string runName, int buildID, int clientID)
        {
            int runId = 0;
            RunDetailCollection runcollection = new RunDetailCollection();

            if (!string.IsNullOrEmpty(runName))
            {
                try
                {
                    runId = runcollection.GetRecords().Where(run => run.RunName.ToLower() == runName.ToLower() && run.ClientId == clientID && run.BuildId == buildID).ToList().First().RunId;
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
            }
            return runId;
        }

        public static bool CreateClient(string clientName)
        {
            ClientCollection clientCollection = new ClientCollection();

            ClientBO clientBO = new ClientBO
            {
                ClientName = clientName,
                ClientDescription = clientName,
                IsActive = true,
                CreateDate = DateTime.Now,
                CreateBy = userName,
                UpdateDate = DateTime.Now,
                UpdateBy = userName
            };

            return clientCollection.UpdateDataSingle(clientBO);
        }

        public static bool CreateSite(string siteName, int clientId)
        {
            SiteCollection siteCollection = new SiteCollection();

            SiteBO siteBO = new SiteBO
            {
                SiteName = siteName,
                SiteDescription = siteName,
                ClientId = clientId,
                IsActive = true,
                CreateDate = DateTime.Now,
                CreateBy = userName,
                UpdateDate = DateTime.Now,
                UpdateBy = userName
            };

            return siteCollection.UpdateDataSingle(siteBO);

        }

        public static bool CreateBuild(string buildName, int siteId)
        {
            BuildCollection buildCollection = new BuildCollection();

            BuildBO buildBO = new BuildBO
            {
                BuildName = buildName,
                BuildDescription = buildName,
                SiteId = siteId,
                IsActive = true,
                CreateDate = DateTime.Now,
                CreateBy = userName,
                UpdateDate = DateTime.Now,
                UpdateBy = userName
            };

            return buildCollection.UpdateDataSingle(buildBO);
        }

        public static bool CreateRun(int clientId, int buildId, string runName)
        {
            RunDetailCollection runCollection = new RunDetailCollection();

            RunBO runBO = new RunBO
            {
                ClientId = clientId,
                BuildId = buildId,
                RunName = runName,
                IsActive = true,
                CreatedDate = DateTime.Now,
                CreatedBy = userName,
                UpdateDate = DateTime.Now,
                UpdateBy = userName
            };

            return runCollection.UpdateDataSingle(runBO);
        }

        public static bool CreateTestCase(string testCaseName, int buildId, string strTestcaseDesc)
        {
            TestCaseCollection testCaseCollection = new TestCaseCollection();

            TestCaseBO testcaseBO = new TestCaseBO
            {
                TestCaseName = testCaseName,
                TestCaseDescription = strTestcaseDesc,
                BuildId = buildId,
                IsActive = true,
                CreateDate = DateTime.Now,
                CreateBy = userName,
                UpdateDate = DateTime.Now,
                UpdateBy = userName
            };

            return testCaseCollection.UpdateDataSingle(testcaseBO);
        }

        public static bool CreateTestCaseMap(int ruleDetailId, int testCaseId,int iPriority)
        {
            TestCaseRuleMapCollection testcaseRuleMapCollection = new TestCaseRuleMapCollection();

            TestCaseRuleMapBO testcaseRuleMapBO = new TestCaseRuleMapBO
            {
                RuleDetailId = ruleDetailId,
                TestCaseId = testCaseId,
                IsActive = true,
                CreateDate = DateTime.Now,
                CreateBy = userName,
                UpdateDate = DateTime.Now,
                UpdateBy = userName,
                Priority = iPriority
            };

            return testcaseRuleMapCollection.UpdateDataSingle(testcaseRuleMapBO);
        }

        public static List<TestCaseBO> GetAllDistinctTestCases()
        {
            TestCaseCollection testcaseCollection = new TestCaseCollection();
            List<TestCaseBO> distinctTestCases = null;

            distinctTestCases = (from i in
                                     (from j in testcaseCollection.GetRecords()
                                      select new
                                      {
                                          TestCaseName = j.TestCaseName
                                      })
                                 group i by new { i.TestCaseName } into g
                                 select new TestCaseBO
                                 {
                                     TestCaseName = g.Key.TestCaseName
                                 }).ToList();


            return distinctTestCases;
        }

        public static List<RuleDetailBO> GetAllDistinctRuleDetails()
        {
            RuleDetailCollection ruleDetailCollection = new RuleDetailCollection();

            List<RuleDetailBO> distinctRuleDetails = null;

            distinctRuleDetails = (from i in
                                       (from j in ruleDetailCollection.GetRecords().Where(r => r.IsActive == true)
                                        select new
                                        {
                                            RuleDetailId = j.RuleDetailId,
                                            RuleDetailName = j.RuleDetailName,
                                            RuleId = j.RuleId
                                        })
                                   group i by new { i.RuleDetailId, i.RuleDetailName, i.RuleId } into g
                                   select new RuleDetailBO
                                  {
                                      RuleDetailId = g.Key.RuleDetailId,
                                      RuleDetailName = g.Key.RuleDetailName,
                                      RuleId = g.Key.RuleId
                                  }).ToList();

            return distinctRuleDetails;
        }

        public static List<TestCaseBO> GetAllTestCases(int buildId)
        {
            TestCaseCollection testCaseCollection = new TestCaseCollection();

            return (from j in testCaseCollection.GetRecords()
                    where j.BuildId == buildId
                    select new TestCaseBO
                    {
                        TestCaseId = j.TestCaseId,
                        TestCaseName = j.TestCaseName,
                        BuildId = j.BuildId
                    }).OrderBy(o => o.TestCaseId).ToList();
        }

        /// <summary>
        /// Update a single record in database
        /// </summary>
        /// <param name="TestCaseData"></param>
        /// <returns></returns>
        public static bool UpdateTestCase(TestCaseBO TestCaseData)
        {
            TestCaseCollection testCaseCollection = new TestCaseCollection();
            TestCaseData.IsActive = true;
            return testCaseCollection.UpdateSingleTestCase(TestCaseData);
        }

        /// <summary>
        /// Delete the Single data from testcaserulemap table of database
        /// </summary>
        /// <param name="TestCaseRuleMapData"></param>
        /// <returns></returns>
        public static bool DeleteTestCaseRuleMap(TestCaseRuleMapBO TestCaseRuleMapData)
        {
            TestCaseRuleMapCollection TestCaseRuleMapCollection = new TestCaseRuleMapCollection();
            return TestCaseRuleMapCollection.DeleteDataSingle(TestCaseRuleMapData);
        }

        /// <summary>
        /// Delete the Multiple data from testcaserulemap table of database
        /// </summary>
        /// <param name="TestCaseRuleMapData"></param>
        /// <returns></returns>
        public static bool DeleteTestCaseRuleMap(List<TestCaseRuleMapBO> TestCaseRuleMapData)
        {
            TestCaseRuleMapCollection TestCaseRuleMapCollection = new TestCaseRuleMapCollection();
            return TestCaseRuleMapCollection.DeleteDataMultiple(TestCaseRuleMapData);
        }

        /// <summary>
        /// Gets the data from testcaserulemap table of database for a testcaseid
        /// </summary>
        /// <param name="TestCaseRuleMapData"></param>
        /// <returns></returns>
        public static List<TestCaseRuleMapBO> GetAllTestCaseRuleMap(int iTestCaseID)
        {
            TestCaseRuleMapCollection TestCaseRuleMapCollection = new TestCaseRuleMapCollection();
            List<TestCaseRuleMapBO> TestCaseRuleMapObject = (from j in TestCaseRuleMapCollection.GetRecords()
                                                             where j.TestCaseId == iTestCaseID
                                                             select j).OrderBy(o => o.TestCaseId).ToList();
            return TestCaseRuleMapObject;                                                                 
        }

        /// <summary>
        /// Delete Single testcase from Database
        /// </summary>
        /// <param name="TestCaseData"></param>
        /// <returns></returns>
        public static bool DeleteSingleTestCase(int iTestCaseId)
        {
            TestCaseCollection testCaseCollection = new TestCaseCollection();
            TestCaseBO TestCaseData = testCaseCollection.GetSingle(iTestCaseId);
            return testCaseCollection.DeleteSingleTestCase(TestCaseData);
        }

        /// <summary>
        /// gets the rule details for a standard id
        /// </summary>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public static List<RuleDetails> GetRuleDetails(string groupID)
        {
            return Rules.GetRuleDetails(groupID);
        }

        /// <summary>
        /// Get all Rules from database
        /// </summary>
        /// <returns></returns>
        public static List<RuleDetails> GetAllRuleDetails()
        {
            return Rules.GetAllRuleDetails();
        }

        /// <summary>
        /// Get a single testcase item
        /// </summary>
        /// <param name="iTestcaseId"></param>
        /// <returns></returns>
        public static TestCaseBO GetSingleTestCase(int iTestcaseId)
        {
            TestCaseCollection testCaseCollection = new TestCaseCollection();
            return testCaseCollection.GetSingle(iTestcaseId);
        }
    }
}