﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccessibilityAnalyser.Web.Services.Helper
{
    /// <summary>
    /// Contains URL data.
    /// </summary>
    public class UrlFormat
    {
        public string UrlName { get; set; }
        public bool IsCrawled { get; set; }
        public static string Authority { get; set; }
        public bool IsExternal { get; set; }

        public UrlFormat(string url)
        {
            UrlName = url;
            IsCrawled = false;
            IsExternal = false;
        }
    }

    /// <summary>
    /// Contains crawled URLs data
    /// </summary>
    public class CrawledData
    {
        private List<UrlFormat> _urls = new List<UrlFormat>();
        private int _depth;

        public List<UrlFormat> Urls
        {
            get
            {
                return _urls;
            }
            set
            {
                _urls = value;
            }
        }

        public int Depth
        {
            get
            {
                return _depth;
            }
            set
            {
                _depth = value;
            }
        }
    }
}
