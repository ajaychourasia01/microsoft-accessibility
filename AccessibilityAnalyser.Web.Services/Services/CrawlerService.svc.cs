﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AccessibilityAnalyser.Web.Services.ServiceContracts;
using AccessibilityAnalyser.Web.Services.DataContracts;
using AccessibilityServices;
using System.Configuration;
using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.Web.Services.Helper;

namespace AccessibilityAnalyser.Web.Services.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CrawlerService" in code, svc and config file together.
    public class CrawlerService : ICrawlerService
    {
        public List<string> GetUrlList(string siteUrl, int depthLevel, string authority, string crawlType)
        {
            CrawledData crawledData = new CrawledData();
            crawledData.Urls.Add(new UrlFormat(siteUrl));

            Crawler crawl = new Crawler();
            crawl.AuthenticationToken = "eyJhbGciOiJIUzI1NiIsImtpZCI6IjAiLCJ0eXAiOiJKV1QifQ.eyJ2ZXIiOjEsImlzcyI6InVybjp3aW5kb3dzOmxpdmVpZCIsImV4cCI6MTM2NDg4MTQyMCwidWlkIjoiMDVlY2U4OTExZjFmMzRjZmViZmQ3ODJjNGRlYjVjZjAiLCJhdWQiOiJteXdlYmNyYXdsZXIuY29tIiwidXJuOm1pY3Jvc29mdDphcHB1cmkiOiJhcHBpZDovLzAwMDAwMDAwNDQwRThDQTgiLCJ1cm46bWljcm9zb2Z0OmFwcGlkIjoiMDAwMDAwMDA0NDBFOENBOCJ9.-8pKRB4q3G9f3DyI2qQ3WAGS23b3D0t3lolebz0qg88";
            crawl.MaxDepth = depthLevel;
            UrlFormat.Authority = authority;
            crawl.CrawlPage(crawledData);

            if (siteUrl.EndsWith("/"))
                siteUrl = siteUrl.TrimEnd(new char[] { '/' });

            //Add base url to the final list if not present,
            //else move it to top of the list.
            if (!crawl.FinalInternalUrlList.Contains(siteUrl))
            {
                crawl.FinalInternalUrlList.Insert(0, siteUrl);
            }
            else if (crawl.FinalInternalUrlList.IndexOf(siteUrl) > 0)
            {
                crawl.FinalInternalUrlList.Remove(siteUrl);
                crawl.FinalInternalUrlList.Insert(0, siteUrl);
            }

            if (crawlType == "Internal")
            {
                return crawl.FinalInternalUrlList;
            }
            else if (crawlType == "External")
            {
                return crawl.FinalExternalUrlList;
            }
            else
            {
                List<string> tempUrlList = new List<string>();
                tempUrlList.AddRange(crawl.FinalInternalUrlList);
                tempUrlList.AddRange(crawl.FinalExternalUrlList);

                return tempUrlList;
            }
        }

        //public List<string> GetUrlList(string SiteUrl, int depthLevel, string allowExternal)
        //{
        //    List<string> urlList = new List<string>();
        //    Crawling crawlObj = new Crawling();

        //    try
        //    {
        //        urlList = crawlObj.GetUrlList(SiteUrl, depthLevel, allowExternal);

        //    }
        //    catch (TimeoutException timeOutEx)
        //    {
        //        LogException.CatchException(timeOutEx, "CrawlerService", "GetUrlList");
        //    }
        //    catch (CommunicationException communicationEx)
        //    {
        //        LogException.CatchException(communicationEx, "CrawlerService", "GetUrlList");
        //    }
        //    catch (Exception generalEx)
        //    {
        //        LogException.CatchException(generalEx, "CrawlerService", "GetUrlList");
        //    }

        //    return urlList;
        //}
    }
}
