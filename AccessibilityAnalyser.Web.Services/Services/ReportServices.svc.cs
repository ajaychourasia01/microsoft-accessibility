﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AccessibilityAnalyser.Web.Services.ServiceContracts;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.RuleEngine;
using AccessibilityAnalyser.Common;

namespace AccessibilityAnalyser.Web.Services.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ReportServices" in code, svc and config file together.
    public class ReportServices : IReportServices
    {
        public IEnumerable<BuildBO> GetNoOfBuild(int clientId)
        {
            IEnumerable<BuildBO> getNoofBuilds = null;
            try
            {
                getNoofBuilds = Reporting.GetNoOfBuilds(clientId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ReportService", "GetNoOfBuild");
            }

            return getNoofBuilds;
        }

        public List<ReportingDetails> GetTestCaseRatioDetails(int clientId, List<int> buildIDs)
        {
            List<ReportingDetails> getTestCaseRationDetails = new List<ReportingDetails>();
            try
            {
                getTestCaseRationDetails = Reporting.GetTestCaseRatioDetails(clientId, buildIDs);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ReportService", "GetTestCaseRatioDetails");
            }

            return getTestCaseRationDetails;
        }

        public List<ReportingDetails> GetLatestBuilds(int clientId)
        {
            List<ReportingDetails> getLatestBuilds = new List<ReportingDetails>();
            try
            {
                getLatestBuilds = Reporting.GetLatestBuilds(clientId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ReportService", "GetLatestBuilds");
            }
            return getLatestBuilds;
        }

        public int GetClientId(string clientName)
        {
            int clientID = 0;

            try
            {
                clientID = Reporting.GetClientId(clientName);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ReportService", "GetClientId");
            }

            return clientID;

        }

        public string GetClientName(int clientId)
        {
            string clientName = string.Empty;

            try
            {
                clientName = Reporting.GetClientName(clientId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ReportService", "GetClientName");
            }

            return clientName;
        }

        public ReportingDetails GetTotalTestCaseRatio(int clientId)
        {
            ReportingDetails getTotalTestcaseRatio = new ReportingDetails();

            try
            {
                getTotalTestcaseRatio = Reporting.GetTotalTestCaseRatio(clientId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ReportService", "GetTotalTestCaseRatio");
            }
            return getTotalTestcaseRatio;
        }

        public void GetData(int clientId)
        {
            try
            {
                Reporting.GetData(clientId);
            }

            catch (Exception ex)
            {
                LogException.CatchException(ex, "ReportService", "GetData");
            }
        }
    }
}
