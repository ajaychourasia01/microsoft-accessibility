﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Interfaces;
using AccessibilityAnalyser.Web.Services.DataContracts;

namespace AccessibilityAnalyser.Web.Services.ServiceContracts
{
    [ServiceContract]
    public interface ICrawlerService
    {
        [OperationContract]
        [FaultContract(typeof(ServiceFaultData))]
        List<string> GetUrlList(string SiteUrl, int depthLevel,string authority, string crawlType);
    }
}
