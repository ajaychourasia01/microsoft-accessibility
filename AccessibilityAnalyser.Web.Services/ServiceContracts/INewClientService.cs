﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Interfaces;
using AccessibilityAnalyser.RuleEngine;

namespace AccessibilityAnalyser.Web.Services.ServiceContracts
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INewClientService" in both code and config file together.
    /// <summary>
    /// Interfaces for New Client Service
    /// </summary>
    [ServiceContract]
    public interface INewClientService
    {
        [OperationContract(Name = "GetClientList")]
        List<ClientBO> GetClientList(string searchText);

        [OperationContract(Name = "GetSiteList")]
        List<SiteBO> GetSiteList(string searchText, int clientID);

        [OperationContract(Name = "GetBuildList")]
        List<BuildBO> GetBuildList(string searchText, int siteID);

        [OperationContract(Name = "GetRunList")]
        List<RunBO> GetRunList(string searchText, int buildID);

        [OperationContract(Name = "GetAllClientList")]
        List<ClientBO> GetClientList();

        [OperationContract(Name = "GetAllSiteList")]
        List<SiteBO> GetSiteList(int clientID);

        [OperationContract(Name = "GetAllBuildList")]
        List<BuildBO> GetBuildList(int siteID);

        [OperationContract(Name = "GetAllRunList")]
        List<RunBO> GetRunList(int buildID);

        [OperationContract]
        int GetClientID(string clientName);

        [OperationContract]
        int GetSiteID(string url, int clientID);

        [OperationContract]
        int GetBuildID(string buildName, int siteID);

        [OperationContract]
        int GetRunID(string runName, int buildID, int clientID);

        [OperationContract]
        bool CreateClient(string clientName);

        [OperationContract]
        bool CreateSite(string siteName, int clientId);

        [OperationContract]
        bool CreateBuild(string buildName, int siteId);

        [OperationContract]
        bool CreateRun(int clientId, int buildId, string runName);

        [OperationContract]
        bool CreateTestCase(string testCaseName, int buildId, string strTestcaseDesc);

        [OperationContract]
        bool CreateTestCaseMap(int ruleDetailId, int testCaseId, int iPriority);

        [OperationContract]
        List<TestCaseBO> GetAllDistinctTestCases();

        [OperationContract]
        List<RuleDetailBO> GetAllDistinctRuleDetails();

        [OperationContract]
        List<TestCaseBO> GetAllTestCases(int buildId);

        [OperationContract]
        bool UpdateTestCase(TestCaseBO TestCaseData);

        [OperationContract]
        bool DeleteTestCaseRuleMap(TestCaseRuleMapBO TestCaseRuleMapData);

        [OperationContract]
        bool DeleteMultipleTestCaseRuleMap(List<TestCaseRuleMapBO> TestCaseRuleMapData);

        [OperationContract]
        List<TestCaseRuleMapBO> GetAllTestCaseRuleMap(int iTestCaseID);

        [OperationContract]
        bool DeleteSingleTestCase(int iTestCaseId);

        [OperationContract]
        IEnumerable<StandardBO> GetAllGroups();

        [OperationContract]
        List<RuleDetails> GetRuleDetails(string groupID);

        [OperationContract]
        List<RuleDetails> GetAllRuleDetails();

        [OperationContract]
        TestCaseBO GetSingleTestCase(int iTestcaseId);
    }
}
