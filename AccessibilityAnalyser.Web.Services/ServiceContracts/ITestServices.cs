﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Interfaces;
using AccessibilityAnalyser.Web.Services.DataContracts;
using AccessibilityAnalyser.RuleEngine;

namespace AccessibilityAnalyser.Web.Services.ServiceContracts
{
    /// <summary>
    /// ITestServices - Service contract
    /// </summary>
    [ServiceContract]
    public interface ITestServices
    {
        //[OperationContract]
        //[FaultContract(typeof(ServiceFaultData))]
        //int TestValidation(List<string> urlNames, int clientId, int buildId, int runId, int violationPerBatch);

        [OperationContract]
        [FaultContract(typeof(ServiceFaultData))]
        List<TestCase> GetTestCases(string clientName, string siteName, string multipleChoice, int runId, int buildId);

        [OperationContract]
        [FaultContract(typeof(ServiceFaultData))]
        List<ClientBO> GetClients(string searchText);

        [OperationContract]
        [FaultContract(typeof(ServiceFaultData))]
        List<ClientBO> GetClientSingle(string ClientName);

        [OperationContract]
        [FaultContract(typeof(ServiceFaultData))]
        Violations GetViolations(int clientId, int buildId, int runId, int ruleDetailId, string violationType, int skip, int testCaseId);

        [OperationContract]
        [FaultContract(typeof(ServiceFaultData))]
        int GetViolationCount(int ClientID);

        [OperationContract]
        [FaultContract(typeof(ServiceFaultData))]
        bool UpdateViolation(string violationDescription, string violationRecommendation, int violationId, string status);

        [OperationContract]
        [FaultContract(typeof(ServiceFaultData))]
        bool UpdateViolationForManual(string violationDescription, string violationRecommendation, string violationName, string sourceURL, string sourceElement, int violationId, string status);

        [OperationContract]
        [FaultContract(typeof(ServiceFaultData))]
        ViolationBO GetViolation(List<ViolationBO> violations, int violationId);

        [OperationContract]
        [FaultContract(typeof(ServiceFaultData))]
        List<SiteBO> GetSiteList(string searchText, string clientName);

        [OperationContract]
        [FaultContract(typeof(ServiceFaultData))]
        List<Build> GetBuilds(string clientName, string siteName);

        [OperationContract]
        [FaultContract(typeof(ServiceFaultData))]
        bool CreateViolation(string violationName, string violationDescription, int runId, string siteName, string violationSourceElement, int violationSourceLineNumber,
                             string violationRecommendation, string status, int clientId, int buildId,string TestCaseName,int violationlogID);

        [OperationContract]
        [FaultContract(typeof(ServiceFaultData))]
        char GetViolationLogStatus();
    }
}
