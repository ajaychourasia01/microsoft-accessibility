﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects.DataClasses;

using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.Entities.SQLServerDB;
using AccessibilityAnalyser.Interfaces;
using System.Data.SqlClient;
using SystemData = System.Data;

namespace AccessibilityAnalyser.DataAdapter
{
    public class SQLDataEntitiesStore : IDataAdapter
    {
        private DataStore dataConnection;
        private AccessibilityAnalyserDBEntities dbEntities;

        public SQLDataEntitiesStore()
        {
            dataConnection = new DataStore();
            dbEntities = dataConnection.SQLDBEntities;
        }

        /// <summary>
        /// Add Build object into DB entity
        /// </summary>
        /// <param name="buildData"></param>
        /// <returns></returns>
        public bool CreateBuild(IBuild buildData)
        {
            bool IsCreated = false;

            try
            {
                Build dbBuildEntity = new Build();
                Helper.CopyPropertyValues(buildData, dbBuildEntity);

                dbEntities.Build.AddObject(dbBuildEntity);
                dbEntities.SaveChanges();
                IsCreated = (!IsCreated);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IsCreated;
        }

        /// <summary>
        /// Add run object into DB entity
        /// </summary>
        /// <param name="runData"></param>
        /// <returns></returns>
        public bool CreateRun(IRun runData)
        {
            bool IsCreated = false;

            try
            {
                RunDetails dbBuildEntity = new RunDetails();
                Helper.CopyPropertyValues(runData, dbBuildEntity);

                dbEntities.RunDetails.AddObject(dbBuildEntity);
                dbEntities.SaveChanges();
                IsCreated = (!IsCreated);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IsCreated;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildID"></param>
        /// <returns></returns>
        public EntityObject GetBuild(int buildID)
        {
            EntityObject buildObject = null;
            buildObject = (EntityObject)dbEntities.Build.First<Build>(x => x.BuildId == buildID);

            return buildObject;
        }

        /// <summary>
        /// Get run based on clientid, buildid and runid
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="buildId"></param>
        /// <param name="runId"></param>
        /// <returns></returns>
        public EntityObject GetRun(int clientId, int buildId, int runId)
        {
            EntityObject rundObject = null;
            rundObject = (EntityObject)dbEntities.RunDetails.First<RunDetails>(x => x.ClientId == clientId && x.BuildId == buildId && x.RunId == runId);

            return rundObject;
        }

        public IEnumerable<EntityObject> GetBuilds()
        {
            IEnumerable<EntityObject> buildObjects = null;

            try
            {
                buildObjects = dbEntities.Build;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return buildObjects;
        }

        /// <summary>
        /// Get all runs
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EntityObject> GetRuns()
        {
            IEnumerable<EntityObject> runObjects = null;

            try
            {
                runObjects = dbEntities.RunDetails;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return runObjects;
        }
        /// <summary>
        /// Add Client object into DB entity
        /// </summary>
        /// <param name="clientData"></param>
        /// <returns></returns>
        public bool CreateClient(IClient clientData)
        {
            bool IsCreated = false;

            try
            {
                Clients dbClientEntity = new Clients();
                Helper.CopyPropertyValues(clientData, dbClientEntity);

                dbEntities.Clients.AddObject(dbClientEntity);
                dbEntities.SaveChanges();
                IsCreated = (!IsCreated);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IsCreated;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public EntityObject GetClient(int clientId)
        {
            EntityObject clientObject = null;
            clientObject = (EntityObject)dbEntities.Clients.First<Clients>(x => x.ClientId == clientId);

            return clientObject;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EntityObject> GetClients(string clientName)
        {
            IEnumerable<EntityObject> clientObjects = null;

            try
            {
                //clientObjects = dbEntities.Clients;
                clientObjects = (from c in dbEntities.Clients
                                 where c.ClientName == clientName
                                 select c).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return clientObjects;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EntityObject> GetClients(int clientID)
        {
            IEnumerable<EntityObject> clientObjects = null;

            try
            {
                //clientObjects = dbEntities.Clients;
                clientObjects = (from c in dbEntities.Clients
                                 where c.ClientId == clientID
                                 select c).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return clientObjects;
        }
        /// <summary>
        /// Add Site object into DB entity
        /// </summary>
        /// <param name="siteData"></param>
        /// <returns></returns>
        public bool CreateSite(ISite siteData)
        {
            bool IsCreated = false;

            try
            {
                Site dbSiteEntity = new Site();
                Helper.CopyPropertyValues(siteData, dbSiteEntity);

                dbEntities.Site.AddObject(dbSiteEntity);
                dbEntities.SaveChanges();
                IsCreated = (!IsCreated);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IsCreated;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>

        public EntityObject GetSite(int siteId)
        {
            EntityObject siteObject = null;
            siteObject = (EntityObject)dbEntities.Site.First<Site>(x => x.SiteId == siteId);

            return siteObject;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>

        public IEnumerable<EntityObject> GetSites()
        {
            IEnumerable<EntityObject> siteObjects = null;

            try
            {
                siteObjects = dbEntities.Site;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return siteObjects;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>

        public IEnumerable<EntityObject> GetSites(int clientID)
        {
            IEnumerable<EntityObject> siteObjects = null;

            try
            {
                //siteObjects = dbEntities.Site;
                siteObjects = (from c in dbEntities.Site
                               where c.ClientId == clientID
                               select c).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return siteObjects;
        }
        /// <summary>
        /// Add TestCase object into DB entity
        /// </summary>
        /// <param name="testcaseData"></param>
        /// <returns></returns>
        public bool CreateTestCase(ITestCase testcaseData)
        {
            bool IsCreated = false;

            try
            {
                TestCase dbTestCaseEntity = new TestCase();
                Helper.CopyPropertyValues(testcaseData, dbTestCaseEntity);

                dbEntities.TestCase.AddObject(dbTestCaseEntity);
                dbEntities.SaveChanges();
                IsCreated = (!IsCreated);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IsCreated;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="testcaseId"></param>
        /// <returns></returns>
        public EntityObject GetTestCase(int testcaseId)
        {
            EntityObject testcaseObject = null;
            testcaseObject = (EntityObject)dbEntities.TestCase.First<TestCase>(x => x.TestCaseId == testcaseId);

            return testcaseObject;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EntityObject> GetTestCases()
        {
            IEnumerable<EntityObject> testcaseObjects = null;

            try
            {
                testcaseObjects = dbEntities.TestCase;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return testcaseObjects;
        }
        /// <summary>
        /// Add Standard object into DB entity
        /// </summary>
        /// <param name="standardData"></param>
        /// <returns></returns>
        public bool CreateStandard(IStandard standardData)
        {
            bool IsCreated = false;

            try
            {
                Standards dbStandardsEntity = new Standards();
                Helper.CopyPropertyValues(standardData, dbStandardsEntity);

                dbEntities.Standards.AddObject(dbStandardsEntity);
                dbEntities.SaveChanges();
                IsCreated = (!IsCreated);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IsCreated;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="standardId"></param>
        /// <returns></returns>
        public EntityObject GetStandard(int standardId)
        {
            EntityObject standardObject = null;
            standardObject = (EntityObject)dbEntities.Standards.First<Standards>(x => x.StandardId == standardId);

            return standardObject;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EntityObject> GetStandards()
        {
            IEnumerable<EntityObject> standardObjects = null;

            try
            {
                standardObjects = dbEntities.Standards;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return standardObjects;
        }
        /// <summary>
        /// Add Rule object into DB entity
        /// </summary>
        /// <param name="ruleData"></param>
        /// <returns></returns>
        public bool CreateRule(IRule ruleData)
        {
            bool IsCreated = false;

            try
            {
                Rules dbRulesEntity = new Rules();
                Helper.CopyPropertyValues(ruleData, dbRulesEntity);

                dbEntities.Rules.AddObject(dbRulesEntity);
                dbEntities.SaveChanges();
                IsCreated = (!IsCreated);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IsCreated;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ruleId"></param>
        /// <returns></returns>
        public EntityObject GetRule(int ruleId)
        {
            EntityObject ruleObject = null;
            ruleObject = (EntityObject)dbEntities.Rules.First<Rules>(x => x.RuleId == ruleId);

            return ruleObject;
        }

        public IEnumerable<EntityObject> GetRules()
        {
            IEnumerable<EntityObject> ruleObjects = null;

            try
            {
                ruleObjects = dbEntities.Rules;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ruleObjects;
        }
        /// <summary>
        /// Add RuleDetail object into DB entity
        /// </summary>
        /// <param name="ruledetailData"></param>
        /// <returns></returns>
        public bool CreateRuleDetail(IRuleDetail ruledetailData)
        {
            bool IsCreated = false;

            try
            {
                RuleDetails dbRuleDetailsEntity = new RuleDetails();
                Helper.CopyPropertyValues(ruledetailData, dbRuleDetailsEntity);

                dbEntities.RuleDetails.AddObject(dbRuleDetailsEntity);
                dbEntities.SaveChanges();
                IsCreated = (!IsCreated);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IsCreated;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ruledetailId"></param>
        /// <returns></returns>
        public EntityObject GetRuleDetail(int ruledetailId)
        {
            EntityObject ruleDetailObject = null;
            ruleDetailObject = (EntityObject)dbEntities.RuleDetails.First<RuleDetails>(x => x.RuleDetailId == ruledetailId);

            return ruleDetailObject;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EntityObject> GetRuleDetails()
        {
            IEnumerable<EntityObject> ruleDetailsObjects = null;

            try
            {
                ruleDetailsObjects = dbEntities.RuleDetails;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ruleDetailsObjects;
        }
        /// <summary>
        /// Add TestCaseMap object into DB entity
        /// </summary>
        /// <param name="testcaseRuleMapData"></param>
        /// <returns></returns>
        public bool CreateTestCaseMap(ITestCaseRuleMap testcaseRuleMapData)
        {
            bool IsCreated = false;

            try
            {
                TestCaseRuleMappings dbtestcaseRuleMapEntity = new TestCaseRuleMappings();
                Helper.CopyPropertyValues(testcaseRuleMapData, dbtestcaseRuleMapEntity);

                dbEntities.TestCaseRuleMappings.AddObject(dbtestcaseRuleMapEntity);
                dbEntities.SaveChanges();
                IsCreated = (!IsCreated);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IsCreated;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="testcaseRuleMapId"></param>
        /// <returns></returns>
        public EntityObject GetTestCaseMapping(int testcaseRuleMapId)
        {
            EntityObject tcMapObject = null;
            tcMapObject = (EntityObject)dbEntities.TestCaseRuleMappings.First<TestCaseRuleMappings>(x => x.TRMappingId == testcaseRuleMapId);

            return tcMapObject;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EntityObject> GetTestCasesRuleMappings()
        {
            IEnumerable<EntityObject> testcaseRuleMapObjects = null;

            try
            {
                testcaseRuleMapObjects = dbEntities.TestCaseRuleMappings;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return testcaseRuleMapObjects;
        }
        /// <summary>
        /// Add violation object into DB entity
        /// </summary>
        /// <param name="violationData"></param>
        /// <returns></returns>
        public bool CreateViolation(IViolation violationData)
        {
            bool IsCreated = false;

            try
            {
                Violations dbViolationEntity = new Violations();
                Helper.CopyPropertyValues(violationData, dbViolationEntity);

                //dbViolationEntity.TestCaseRuleMappings = new TestCaseRuleMappings();


                dbViolationEntity.TestCaseRuleMappings =
                    dbEntities.TestCaseRuleMappings.First<TestCaseRuleMappings>(x => x.TRMappingId == violationData.TRMappingId);


                dbEntities.Violations.AddObject(dbViolationEntity);
                dbEntities.SaveChanges();


                IsCreated = (!IsCreated);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IsCreated;
        }

        /// <summary>
        /// Violations - Bulk upload
        /// </summary>
        /// <param name="violationList"></param>
        /// <returns></returns>
        public bool CreateViolations(IList<IViolation> violationList)
        {
            bool IsCreated = false;
            IList<IViolation> removedViolations = new List<IViolation>();

            try
            {
                var entityConn = dbEntities.Connection as System.Data.EntityClient.EntityConnection;
                var dbConn = entityConn.StoreConnection as SqlConnection;

                ////Finding existing records from DB and removing it from 'violationList'
                //violationList.ToList().ForEach(delegate(IViolation violation)
                //{
                //    var duplicateViolationList = (from violations in dbEntities.Violations
                //                                  where
                //                                         violations.ClientId.Equals(violation.ClientId) && violations.BuildId.Equals(violation.BuildId) &&
                //                                         violations.RunId.Equals(violation.RunId) && violations.ViolationName.Trim().Equals(violation.ViolationName.Trim()) &&
                //                                         violations.ViolationDescription.Trim().Equals(violation.ViolationDescription.Trim()) &&
                //                                         violations.SourceURL.Trim().Equals(violation.SourceURL.Trim()) && violations.SourceElement.Trim().Equals(violation.SourceElement.Trim())
                //                                  select violations).ToList();

                //    if (duplicateViolationList.Count != 0)
                //    {
                //        removedViolations.Add(violation);
                //    }

                //});

                //removedViolations.ToList().ForEach(delegate(IViolation removedViolation)
                //{
                //    violationList.Remove(removedViolation);
                //});

                using (dbConn)
                {
                    dbConn.Open();

                    SqlBulkCopy bulkInsert = new SqlBulkCopy(dbConn);
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("ViolationId", "ViolationId"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("ViolationName", "ViolationName"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("ViolationDescription", "ViolationDescription"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("TRMappingId", "TRMappingId"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("DupTRMappingId", "DupTRMappingId"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("RunId", "RunId"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("SourceURL", "SourceURL"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("SourceElement", "SourceElement"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("SourceLineNumber", "SourceLineNumber"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("Recomandation", "Recomandation"));

                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("IsActive", "IsActive"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("CreateDate", "CreateDate"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("CreateBy", "CreateBy"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("UpdateDate", "UpdateDate"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("UpdateBy", "UpdateBy"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("Status", "Status"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("ClientId", "ClientId"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("BuildId", "BuildId"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("ViolationLogId", "ViolationLogId"));
                    
                    bulkInsert.DestinationTableName = "Violations";

                    SystemData.DataTable violationTable = Helper.ToDataTable<IViolation>(violationList);
                    bulkInsert.WriteToServer(violationTable);
                }

                IsCreated = (!IsCreated);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IsCreated;
        }


        /// <summary>
        /// Get Violation entity
        /// </summary>
        /// <param name="violationId"></param>
        /// <returns></returns>
        public EntityObject GetViolation(int violationId)
        {
            EntityObject violationObject = null;
            violationObject = (EntityObject)dbEntities.Violations.First<Violations>(x => x.ViolationId == violationId);

            return violationObject;
        }

        /// <summary>
        /// Get Violation Collection
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EntityObject> GetViolations()
        {
            IEnumerable<EntityObject> violationObjects = null;

            try
            {
                violationObjects = dbEntities.Violations;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return violationObjects;
        }

        /// <summary>
        /// Get Violation Count
        /// </summary>
        /// <returns></returns>
        public int GetViolationCount(int clientId)
        {
            int violationCount = 0;

            try
            {
                violationCount = dbEntities.Violations.Where(v => v.ClientId == clientId).Count();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return violationCount;
        }


        /// <summary>
        /// Get Violation Collection
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EntityObject> GetViolations(int clientId)
        {
            IEnumerable<EntityObject> violationObjects = null;

            try
            {
                violationObjects = dbEntities.Violations.Where(v => v.ClientId.Equals(clientId));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return violationObjects;
        }

        /// <summary>
        /// Update a violation
        /// </summary>
        /// <param name="violationData"></param>
        /// <returns></returns>
        public bool UpdateViolation(IViolation violationData)
        {
            bool IsUpdated = false;

            try
            {
                Violations violationObject = (Violations)dbEntities.Violations.First<Violations>(x => x.ViolationId == violationData.ViolationId);

                violationObject.ViolationDescription = violationData.ViolationDescription;
                violationObject.Recomandation = violationData.Recomandation;
                violationObject.Status = violationData.Status;

                dbEntities.SaveChanges();
                IsUpdated = (!IsUpdated);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IsUpdated;
        }

        /// <summary>
        /// Update a violation
        /// </summary>
        /// <param name="violationData"></param>
        /// <returns></returns>
        public bool UpdateViolationForManual(IViolation violationData)
        {
            bool IsUpdated = false;

            try
            {
                Violations violationObject = (Violations)dbEntities.Violations.First<Violations>(x => x.ViolationId == violationData.ViolationId);

                violationObject.ViolationDescription = violationData.ViolationDescription;
                violationObject.Recomandation = violationData.Recomandation;
                violationObject.ViolationName = violationData.ViolationName;
                violationObject.SourceURL = violationData.SourceURL;
                violationObject.SourceElement = violationData.SourceElement;
                //violationObject.Status = violationData.Status;

                dbEntities.SaveChanges();
                IsUpdated = (!IsUpdated);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IsUpdated;
        }

        /// <summary>
        /// Update a single Testcase Object into Tescase Table of Database
        /// </summary>
        /// <param name="TestCaseData"></param>
        /// <returns></returns>
        public bool UpdateTestCase(ITestCase TestCaseData)
        {
            bool IsUpdated = false;
            try
            {
                TestCase TestCaseObject = (TestCase)dbEntities.TestCase.First<TestCase>(x => x.TestCaseId == TestCaseData.TestCaseId);

                TestCaseObject.TestCaseName = TestCaseData.TestCaseName;
                TestCaseObject.TestCaseDescription = TestCaseData.TestCaseDescription;
                TestCaseObject.IsActive = TestCaseData.IsActive;
                TestCaseObject.UpdateDate = TestCaseData.UpdateDate;
                TestCaseObject.UpdateBy = TestCaseData.UpdateBy;

                dbEntities.SaveChanges();
                IsUpdated = (!IsUpdated);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return IsUpdated;
        }

        /// <summary>
        /// Delete the testcaserulemapping for a testcase
        /// </summary>
        /// <param name="TestCaseRuleMap"></param>
        /// <returns></returns>
        public bool DeleteTestCaseRuleMap(ITestCaseRuleMap TestCaseRuleMap)
        {
            bool IsDeleted = false;
            try
            {
                TestCaseRuleMappings TestCaseRuleMapObject = (TestCaseRuleMappings)dbEntities.TestCaseRuleMappings.First<TestCaseRuleMappings>(x => x.TestCaseId == TestCaseRuleMap.TestCaseId);
                dbEntities.TestCaseRuleMappings.DeleteObject(TestCaseRuleMapObject);
                dbEntities.SaveChanges();
                IsDeleted = (!IsDeleted);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return IsDeleted;
        }

        /// <summary>
        /// Delete the TestCase from database
        /// </summary>
        /// <param name="TestCaseRuleMap"></param>
        /// <returns></returns>
        public bool DeleteTestCase(ITestCase TestCaseData)
        {
            bool IsDeleted = false;
            try
            {
                TestCase TestCaseObject = (TestCase)dbEntities.TestCase.First<TestCase>(x => x.TestCaseId == TestCaseData.TestCaseId);
                dbEntities.TestCase.DeleteObject(TestCaseObject);
                dbEntities.SaveChanges();
                IsDeleted = (!IsDeleted);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return IsDeleted;
        }

        /// <summary>
        /// Delete the Voilation from database
        /// </summary>
        /// <param name="TestCaseRuleMap"></param>
        /// <returns></returns>
        public bool DeleteViolation(IViolation ViolationData)
        {
            bool IsDeleted = false;
            try
            {
                Violations ViolationObject = (Violations)dbEntities.Violations.First<Violations>(x => x.ViolationId == ViolationData.ViolationId);
                dbEntities.Violations.DeleteObject(ViolationObject);
                dbEntities.SaveChanges();
                IsDeleted = (!IsDeleted);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return IsDeleted;
        }
        public IEnumerable<EntityObject> GetUsers()
        {
            IEnumerable<EntityObject> userdObjects = null;

            try
            {
                userdObjects = dbEntities.Users;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return userdObjects;
        }
        //public IEnumerable<EntityObject> GetUsers(string accountUserName)
        //{
        //    IEnumerable<EntityObject> userdObjects = null;

        //    try
        //    {
        //        //userdObjects = dbEntities.Users;
        //        userdObjects = (from r in dbEntities.Users
        //                        where r.UserName == accountUserName
        //                        select r).ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return userdObjects;
        //}

        public IEnumerable<EntityObject> GetViolationList(int clientId, int buildId, int runId, int ruleDetailId, string violationType, int testCaseId)
        {
            try
            {

                var testCaseRuleMappingIds = (from r in dbEntities.TestCaseRuleMappings
                                              where r.RuleDetailId == ruleDetailId && r.TestCaseId == testCaseId
                                              select r.TRMappingId).ToList();

                var violationList = (from violations in dbEntities.Violations
                                     where testCaseRuleMappingIds.Contains(violations.TRMappingId)
                                            && violations.ClientId.Equals(clientId) && violations.BuildId.Equals(buildId) &&
                                            violations.RunId.Equals(runId) && (violationType == "Accepted" ? violations.Status.ToUpper().Trim() == "F" : violationType == "Declined" ? violations.Status.ToUpper().Trim() == "D" : (violations.Status.ToUpper().Trim() == "D" || violations.Status.ToUpper().Trim() == "F"))
                                     select violations);
                return violationList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetViolationCountPerRule(int buildId, int runId, int ruleDetailId, string typeCount, int testCaseId)
        {
            try
            {
                string status = (typeCount == "Accepted") ? "F" : (typeCount == "Declined") ? "D" : "Total";
                int violationCount = 0;
                var testCaseRuleMappingIds = new List<int>();
                // Get TestCaseRuleMappingIds
                if (testCaseId > 0)
                {                    
                    testCaseRuleMappingIds = (from r in dbEntities.TestCaseRuleMappings
                                              where r.RuleDetailId == ruleDetailId && r.TestCaseId == testCaseId
                                              select r.TRMappingId).ToList();
                }
                else
                {
                    testCaseRuleMappingIds = (from r in dbEntities.TestCaseRuleMappings
                                              where r.RuleDetailId == ruleDetailId
                                              select r.TRMappingId).ToList();
                }
                // get Client for BuildId
                var clientId = (from r in dbEntities.RunDetails
                                where r.BuildId == buildId
                                select r.ClientId).FirstOrDefault();

                if (status.Equals("Total"))
                {
                    violationCount = (from violations in dbEntities.Violations
                                      where testCaseRuleMappingIds.Contains(violations.TRMappingId)
                                            && violations.ClientId.Equals(clientId) &&
                                            violations.BuildId.Equals(buildId) &&
                                            violations.RunId.Equals(runId)
                                      select violations).Count();
                }
                else if (status.Equals("F") || status.Equals("D"))
                {
                    violationCount = (from violations in dbEntities.Violations
                                      where testCaseRuleMappingIds.Contains(violations.TRMappingId)
                                            && violations.ClientId.Equals(clientId) &&
                                            violations.BuildId.Equals(buildId) &&
                                            violations.RunId.Equals(runId)
                                      select violations).ToList().FindAll(x => x.Status == status).Count();

                }

                return violationCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Specific Violation Collection
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EntityObject> GetSpecificViolations(int clientId, int buildId, int runId)
        {
            IEnumerable<EntityObject> violationObjects = null;

            try
            {
                violationObjects = dbEntities.Violations.Where(x => x.ClientId.Equals(clientId) && x.BuildId.Equals(buildId) && x.RunId.Equals(runId));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return violationObjects;
        }

        /// <summary>
        /// Get Specific Violation Collection
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EntityObject> GetBuildSpecificViolations(int buildId)
        {
            IEnumerable<EntityObject> violationObjects = null;

            try
            {
                violationObjects = dbEntities.Violations.Where(x => x.BuildId.Equals(buildId));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return violationObjects;
        }

        public IEnumerable<EntityObject> GetTestCasesByBuildId(int buildId)
        {
            var testCaseList = (from t in dbEntities.TestCase
                                where t.BuildId == buildId && t.TestCaseRuleMappings.Any()
                                select t);

            return testCaseList;
        }

        public IEnumerable<EntityObject> GetRuleDetailsListByTestCase(int testCaseId)
        {
            var ruleDetailList = (from r in dbEntities.RuleDetails
                                  join tr in dbEntities.TestCaseRuleMappings on r.RuleDetailId equals tr.RuleDetailId
                                  where tr.TestCaseId == testCaseId
                                  select r);
            return ruleDetailList;
        }

        public IEnumerable<EntityObject> GetProrityList(int testCaseId)
        {
            var priorityList = (from tr in dbEntities.TestCaseRuleMappings
                                where tr.TestCaseId == testCaseId
                                select tr);

            return priorityList;
        }

        //public bool CreateTestCases(List<ITestCase> testcaseData)
        //{
        //    bool IsCreated = false;

        //    try
        //    {
        //        foreach (var item in testcaseData)
        //        {
        //            TestCase dbTestCaseEntity = new TestCase();
        //            Helper.CopyPropertyValues(item, dbTestCaseEntity);

        //            dbEntities.TestCase.AddObject(dbTestCaseEntity);
        //            dbEntities.SaveChanges();
        //        }
        //        IsCreated = (!IsCreated);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return IsCreated;
        //}


        //public bool CreateTestCaseMaps(List<ITestCaseRuleMap> testcaseRuleMapData)
        //{
        //    bool IsCreated = false;

        //    try
        //    {
        //        foreach (var item in testcaseRuleMapData)
        //        {
        //            TestCaseRuleMappings dbtestcaseRuleMapEntity = new TestCaseRuleMappings();
        //            Helper.CopyPropertyValues(item, dbtestcaseRuleMapEntity);

        //            dbEntities.TestCaseRuleMappings.AddObject(dbtestcaseRuleMapEntity);
        //            dbEntities.SaveChanges();    
        //        }

        //        IsCreated = (!IsCreated);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return IsCreated;
        //}

        /// <summary>
        /// Violations_Log - Bulk upload
        /// </summary>
        /// <param name="violationLogList"></param>
        /// <returns></returns>
        public bool CreateViolationsLog(IList<IViolationLog> violationLogList)
        {
            bool IsCreated = false;
            try
            {
                var entityConn = dbEntities.Connection as System.Data.EntityClient.EntityConnection;
                var dbConn = entityConn.StoreConnection as SqlConnection;

                using (dbConn)
                {
                    dbConn.Open();
                    SqlBulkCopy bulkInsert = new SqlBulkCopy(dbConn);
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("ViolationLogId", "ViolationLogId"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("ClientId", "ClientId"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("BuildId", "BuildId"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("RunId", "RunId"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("TestCaseId", "TestCaseId"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("SourceURL", "SourceURL"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("Status", "Status"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("CreateDate", "CreateDate"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("CreateBy", "CreateBy"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("UpdateDate", "UpdateDate"));
                    bulkInsert.ColumnMappings.Add(new SqlBulkCopyColumnMapping("UpdateBy", "UpdateBy"));

                    bulkInsert.DestinationTableName = "Violations_Log";
                    SystemData.DataTable violationLogTable = Helper.ToDataTable<IViolationLog>(violationLogList);
                    bulkInsert.WriteToServer(violationLogTable);
                }

                IsCreated = (!IsCreated);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return IsCreated;
        }

        /// <summary>
        /// Get ViolationLogId based on the inputs
        /// </summary>
        /// <returns></returns>
        public int GetViolationLogId(string urlName, int testCaseId, int clientId, int buildId, int runId)
        {
            int violationLogId = 0;

            try
            {
                violationLogId = dbEntities.Violations_Log.Where(v => v.ClientId == clientId && v.BuildId == buildId && v.RunId == runId && v.TestCaseId == testCaseId && v.SourceURL.ToLower() == urlName.ToLower()).Select(y => y.ViolationLogId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return violationLogId;
        }

        /// <summary>
        /// Update a violation
        /// </summary>
        /// <param name="violationData"></param>
        /// <returns></returns>
        public bool UpdateViolationLog(Dictionary<int, char> objStatusDictionay)
        {
            bool IsUpdated = false;
            try
            {
                foreach (var objStatus in objStatusDictionay)
                {
                    Violations_Log violationLogObj = (Violations_Log)dbEntities.Violations_Log.First<Violations_Log>(x => x.ViolationLogId == objStatus.Key);
                    violationLogObj.Status = Convert.ToString(objStatus.Value);
                    dbEntities.SaveChanges();
                }
                IsUpdated = (!IsUpdated);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IsUpdated;
        }

        public IEnumerable<EntityObject> GetClients()
        {
            IEnumerable<EntityObject> clientObjects = null;

            try
            {
                clientObjects = dbEntities.Clients;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return clientObjects;
        }

        public int GetViolationCount(int clientID, int buildID, int runID)
        {
            int violationCount = 0;
            try
            {
                violationCount = dbEntities.Violations.Where(v => v.ClientId == clientID && v.BuildId == buildID && v.RunId == runID).Count();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return violationCount;
        }

        public int GetFailedViolationCount(int clientID, int buildID, int runID)
        {
            int failedViolationCount = 0;
            try
            {
                failedViolationCount = dbEntities.Violations.Where(v => v.ClientId == clientID && v.BuildId == buildID && v.RunId == runID && v.Status == "F").Count();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return failedViolationCount;
        }

        public IEnumerable<EntityObject> GetTestCaseCount(int clientID, int buildID, int runID)
        {
            //IEnumerable<EntityObject> priorityList = null;
            var priorityList = (from c in dbEntities.Clients
                                join s in dbEntities.Site on c.ClientId equals s.ClientId
                                join b in dbEntities.Build on s.SiteId equals b.SiteId
                                join t in dbEntities.TestCase on b.BuildId equals t.BuildId
                                join tcrm in dbEntities.TestCaseRuleMappings on t.TestCaseId equals tcrm.TestCaseId
                                join v in dbEntities.Violations on tcrm.TRMappingId equals v.TRMappingId
                                where b.BuildId == buildID
                                where v.RunId == runID
                                where c.ClientId == clientID
                                select t).Distinct().ToList();
            return priorityList;
        }

        public int GetTestCaseViolationCount(int tcId, int clientId, int buildId, int runId)
        {
            int violationCount = 0;
            violationCount = (from t in dbEntities.TestCase
                              join tcrm in dbEntities.TestCaseRuleMappings on t.TestCaseId equals tcrm.TestCaseId
                              join v in dbEntities.Violations on tcrm.TRMappingId equals v.TRMappingId
                              where t.TestCaseId == tcId
                              where v.ClientId == clientId
                              where v.BuildId == buildId
                              where v.RunId == runId
                              select v).Count();

            return violationCount;
        }
        public int GetFailedTestCaseViolationCount(int tcId, int clientId, int buildId, int runId)
        {
            int violationCount = 0;
            violationCount = (from t in dbEntities.TestCase
                              join tcrm in dbEntities.TestCaseRuleMappings on t.TestCaseId equals tcrm.TestCaseId
                              join v in dbEntities.Violations on tcrm.TRMappingId equals v.TRMappingId
                              where t.TestCaseId == tcId
                              where v.ClientId == clientId
                              where v.BuildId == buildId
                              where v.RunId == runId
                              where v.Status.ToUpper().Trim() == "F"
                              select v).Count();

            return violationCount;
        }
        public IEnumerable<EntityObject> GetClientTestCaseCount(int clientID)
        {
            //IEnumerable<EntityObject> priorityList = null;
            var priorityList = (from c in dbEntities.Clients
                                join s in dbEntities.Site on c.ClientId equals s.ClientId
                                join b in dbEntities.Build on s.SiteId equals b.SiteId
                                join t in dbEntities.TestCase on b.BuildId equals t.BuildId
                                join tcrm in dbEntities.TestCaseRuleMappings on t.TestCaseId equals tcrm.TestCaseId
                                join v in dbEntities.Violations on tcrm.TRMappingId equals v.TRMappingId
                                where c.ClientId == clientID
                                select t).Distinct().ToList();
            return priorityList;
        }
        public int GetClientTestCaseViolationCount(int tcId, int clientId)
        {
            int violationCount = 0;
            violationCount = (from t in dbEntities.TestCase
                              join tcrm in dbEntities.TestCaseRuleMappings on t.TestCaseId equals tcrm.TestCaseId
                              join v in dbEntities.Violations on tcrm.TRMappingId equals v.TRMappingId
                              where t.TestCaseId == tcId
                              where v.ClientId == clientId
                              select v).Count();

            return violationCount;
        }
        public int GetClientFailedTestCaseViolationCount(int tcId, int clientId)
        {
            int violationCount = 0;
            violationCount = (from t in dbEntities.TestCase
                              join tcrm in dbEntities.TestCaseRuleMappings on t.TestCaseId equals tcrm.TestCaseId
                              join v in dbEntities.Violations on tcrm.TRMappingId equals v.TRMappingId
                              where t.TestCaseId == tcId
                              where v.ClientId == clientId
                              where v.Status.ToUpper().Trim() == ("F")
                              select v).Count();

            return violationCount;
        }

        /// <summary>
        /// Update violation_Log with Status as in Progress
        /// </summary>
        /// <param name="violationData"></param>
        /// <returns></returns>
        public bool UpdateViolationLogWithStatus(int violationLogId, string statusVal)
        {
            bool IsUpdated = false;

            try
            {
                Violations_Log violationsLogObject = (Violations_Log)dbEntities.Violations_Log.First<Violations_Log>(x => x.ViolationLogId == violationLogId);
                violationsLogObject.Status = statusVal;
                dbEntities.SaveChanges();
                IsUpdated = (!IsUpdated);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IsUpdated;
        }

        /// <summary>
        /// Update violation_Log with Status as in Progress
        /// </summary>
        /// <param name="violationData"></param>
        /// <returns></returns>
        public char GetViolationLogStatus()
        {
            char statusVal = 'C';
            int violationCount = 0;
            try
            {
                violationCount = dbEntities.Violations_Log.Where(v => v.Status.ToUpper() == "I").Count();
                if (violationCount > 0)
                    statusVal = 'I';                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return statusVal;
        }

        /// <summary>
        /// Get the Intial violation_LogID 
        /// </summary>
        /// <param name="violationData"></param>
        /// <returns></returns>
        public int GetInitialViolationLogId(int clientId)
        {
            int violationLogId = 0;

            try
            {
                violationLogId = dbEntities.Violations_Log.Where(v => v.ClientId == clientId).Select(y => y.ViolationLogId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return violationLogId;
        }

        /// <summary>
        /// Create a service log 
        /// </summary>
        /// <param name="urlsSelected"></param>
        /// <param name="userId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool CreateServiceLog(IServiceLog serviceLogData)
        {
            bool IsCreated = false;
            try
            {
                ServiceLog dbServiceLogEntity = new ServiceLog
                {
                    ClientId = serviceLogData.ClientId,
                    BuildId = serviceLogData.BuildId,
                    RunId = serviceLogData.RunId,
                    Status = serviceLogData.Status,
                    RequestTime = serviceLogData.RequestTime,
                    FileName = serviceLogData.FileName
                };

                dbEntities.ServiceLog.AddObject(dbServiceLogEntity);
                dbEntities.SaveChanges();
                IsCreated = (!IsCreated);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return IsCreated;
        }

        /// <summary>
        /// Update a service log
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="urlsSelected"></param>
        /// <returns></returns>
        public bool UpdateServiceLog(IServiceLog serviceLogData)
        {
            bool IsUpdated = false;

            try
            {
                var query = (from servicelogs in dbEntities.ServiceLog
                             where servicelogs.ClientId == serviceLogData.ClientId &&
                                    servicelogs.RunId == serviceLogData.RunId &&
                                    servicelogs.BuildId == serviceLogData.BuildId &&
                                    servicelogs.RequestTime.Day == serviceLogData.RequestTime.Day &&
                                    servicelogs.RequestTime.Month == serviceLogData.RequestTime.Month &&
                                    servicelogs.RequestTime.Year == serviceLogData.RequestTime.Year &&
                                    servicelogs.RequestTime.Hour == serviceLogData.RequestTime.Hour &&
                                    servicelogs.RequestTime.Minute == serviceLogData.RequestTime.Minute &&
                                    servicelogs.RequestTime.Second == serviceLogData.RequestTime.Second &&
                                    servicelogs.RequestTime.Millisecond == serviceLogData.RequestTime.Millisecond
                             select servicelogs);
                if (serviceLogData.Status != "New")
                {
                    if (query.Count() > 0)
                    {
                        ServiceLog serviceLogObject = (ServiceLog)query.OrderByDescending(x => x.RequestTime).First<ServiceLog>();
                        serviceLogObject.Status = serviceLogData.Status;
                        serviceLogObject.RequestTime = serviceLogData.RequestTime;
                    }
                }
                else
                {
                    ServiceLog dbServiceLogEntity = new ServiceLog
                    {
                        ClientId = serviceLogData.ClientId,
                        BuildId = serviceLogData.BuildId,
                        RunId = serviceLogData.RunId,
                        Status = serviceLogData.Status,
                        RequestTime = serviceLogData.RequestTime,
                        FileName = serviceLogData.FileName
                    };

                    dbEntities.ServiceLog.AddObject(dbServiceLogEntity);
                }
                dbEntities.SaveChanges();
                IsUpdated = (!IsUpdated);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IsUpdated;
        }

        /// <summary>
        /// Get the service log against the logId
        /// </summary>
        /// <param name="logId"></param>
        /// <returns></returns>
        public EntityObject GetServiceLog(int logId)
        {
            EntityObject tcMapObject = null;
            tcMapObject = (EntityObject)dbEntities.ServiceLog.First<ServiceLog>(x => x.LogId == logId);

            return tcMapObject;
        }

        /// <summary>
        /// Get ServiceLog Collection
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EntityObject> GetServiceLogs()
        {
            IEnumerable<EntityObject> serviceLogObjects = null;
            try
            {
                serviceLogObjects = dbEntities.ServiceLog.Where(serviceLogRecord => serviceLogRecord.Status == "New");
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return serviceLogObjects;
        }
    }
}
