﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects;
using System.Data.Objects.DataClasses;

using AccessibilityAnalyser.Entities.SQLServerDB;
using AccessibilityAnalyser.Entities.SQLAzureDB;
using AccessibilityAnalyser.Common;

namespace AccessibilityAnalyser.DataAdapter
{
    /// <summary>
    /// Keeping this as seperate class, to make this generic for all types of Data Store.Currently caters only to SQL Server.
    /// </summary>
    public class DataStore 
    {
        private string dbStoreType = string.Empty;
        private string dbConnectionString = string.Empty;
        private string dbConnectionName = string.Empty;

        private AccessibilityAnalyserDBEntities sqlDBEntities = null;
        private AccessibilityAnalyserAzureDBEntities sqlAzureDBEntities = null;

        private DataAdapterEnum selectedStoreType;

        public AccessibilityAnalyserDBEntities SQLDBEntities
        {
            get { return sqlDBEntities; }
            set { sqlDBEntities = value; }
        }

        public AccessibilityAnalyserAzureDBEntities SQLAzureDBEntities
        {
            get { return sqlAzureDBEntities; }
            set { sqlAzureDBEntities = value; }
        }

        public DataStore()
        {
            dbStoreType = ConfigurationHelper.GetDataBaseStoreType();
          
            if (Enum.IsDefined(typeof(DataAdapterEnum), dbStoreType))
            {
                selectedStoreType = (DataAdapterEnum)Enum.Parse(typeof(DataAdapterEnum), dbStoreType);
                GetEntity(selectedStoreType);
            }
            else
                throw new Exception("Unsupported data store");
        }

        /// <summary>
        /// This is not supporting generic DB entity type very specific to SQL Server
        /// </summary>
        /// <param name="connectionStoreType"></param>
        private void GetEntity(DataAdapterEnum connectionStoreType)
        {
            switch (connectionStoreType)
            {
                case DataAdapterEnum.SQLSERVER:
                    dbConnectionName = ConfigurationHelper.GetEntityContainerName("SQLServerDBConnectionName");
                    sqlDBEntities = new AccessibilityAnalyserDBEntities();
                    break;
                case DataAdapterEnum.SQLAZURE:
                    dbConnectionName = ConfigurationHelper.GetEntityContainerName("AzureDBConnectionName");
                    sqlAzureDBEntities = new AccessibilityAnalyserAzureDBEntities();
                    break;
                case DataAdapterEnum.NOSQL:
                    break;
                default:
                    //dbConnectionString = ConfigurationHelper.GetConnectionString(dbConnectionName);
                    //Create the respective entity-dbEntities object
                    break;
            }

        }
       
    }
}
