﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.DataAdapter
{
    public class DataStoreFactory
    {
        public IDataAdapter GetDataStore()
        {
            IDataAdapter dataStoreObject = null;
            string dbStoreType = ConfigurationHelper.GetDataBaseStoreType();
            DataAdapterEnum selectedStoreType = DataAdapterEnum.SQLSERVER;

            if (Enum.IsDefined(typeof(DataAdapterEnum), dbStoreType))
            {
                selectedStoreType = (DataAdapterEnum)Enum.Parse(typeof(DataAdapterEnum), dbStoreType);
            }

            switch (selectedStoreType)
            {
                case DataAdapterEnum.SQLSERVER:
                    //dbConnectionName = ConfigurationHelper.GetEntityContainerName("SQLServerDBConnectionName");
                    dataStoreObject = new SQLDataEntitiesStore();
                    break;
                case DataAdapterEnum.SQLAZURE:
                    //dbConnectionName = ConfigurationHelper.GetEntityContainerName("AzureDBConnectionName");
                    dataStoreObject = new SQLAzureDataEntitiesStore();
                    break;
                default:

                    break;
            }

            return dataStoreObject;
        }
    }
}
