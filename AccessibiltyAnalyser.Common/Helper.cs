﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Reflection;

namespace AccessibilityAnalyser.Common
{
    public static class Helper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        public static void CopyPropertyValues(object source, object destination)
        {

            # region Old_Approach
            //var destProperties = destination.GetType().GetProperties();

            //foreach (var sourceProperty in source.GetType().GetProperties())
            //{
            //    foreach (var destProperty in destProperties)
            //    {
            //        if (destProperty.Name.ToLower().Equals(sourceProperty.Name.ToLower()) &&
            //    destProperty.PropertyType.IsAssignableFrom(sourceProperty.PropertyType)
            //            )
            //        {
            //            destProperty.SetValue(destination, sourceProperty.GetValue(
            //                source, new object[] { }), new object[] { });
            //            break;
            //        }
            //    }
            //}
            #endregion

            PropertyInfo[] destinationProperties = destination.GetType().GetProperties();
            PropertyInfo[] sourceProperties = source.GetType().GetProperties().Take(destination.GetType().GetProperties().Count()).ToArray();
            PropertyInfo sourceValue = null;
            destinationProperties.ToList().ForEach(d =>
            {
                sourceValue = sourceProperties.Where(s => s.Name.ToLower().Equals(d.Name.ToLower())
                                                         && d.PropertyType.IsAssignableFrom(s.PropertyType)).FirstOrDefault();

                if (sourceValue != null)
                    d.SetValue(destination, sourceValue.GetValue(
                                source, new object[] { }), new object[] { });
            });
        }

        /// <summary>
        /// Convert List to datatable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));

            DataTable table = new DataTable();
            table.Columns.AddRange((from p in properties.OfType<PropertyDescriptor>() select new DataColumn(p.Name)).ToArray());           
            DataRow row = null;

            foreach (T item in data)
            {
                row = table.NewRow();
                properties.OfType<PropertyDescriptor>().ToList().ForEach(p => row[p.Name] = p.GetValue(item) ?? DBNull.Value);
                table.Rows.Add(row);
            }

            #region Old Implementation
            //foreach (PropertyDescriptor prop in properties)
            //    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);

            //foreach (T item in data)
            //{
            //    DataRow row = table.NewRow();
            //    foreach (PropertyDescriptor prop in properties)
            //        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
            //    table.Rows.Add(row);
            //}

            #endregion

            return table;
        }

        public static double Percent(
          this double actual,
          int oneHundredPercent,
          int decimalPlaces = 0)
        {
            double value = Math.Round(((double)actual / (double)oneHundredPercent) * 100, decimalPlaces);

            if (double.IsNaN(value))
            {
                value = 0;
            }

            return value;
        }
        public static double Percent(
           this int actual,
           int oneHundredPercent,
           int decimalPlaces = 0)
        {
            return Convert.ToDouble(actual).Percent(oneHundredPercent, decimalPlaces);
        }

        public static string PercentString(
           this double actual,
           int oneHundredPercent = 100,
           int decimalPlaces = 0)
        {
            return ((double)actual).Percent(oneHundredPercent, decimalPlaces).ToString() + "%";
        }

        public static bool IsNumeric(string s)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(s, @"^[-+]?\d+\.?\d*$"))
                return true;
            else
                return false;
        }

        public static bool IsPositiveNum(string s)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(s, @"^[+]?\d+\.?\d*$"))
                return true;
            else
                return false;
        }

    }
}
