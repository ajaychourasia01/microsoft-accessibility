﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Configuration;

namespace AccessibilityAnalyser.Common
{
    public class ConfigurationHelper
    {
        /// <summary>
        /// Gets the Config setting for the Database Persistence Mode.Ex: SQL, Oracle, Azure.
        /// </summary>
        /// <returns></returns>
        public static string GetDataBaseStoreType()
        {
            return ConfigurationManager.AppSettings["DBStoreType"];
        }


        /// <summary>
        /// Gets the connection string from configuration setting. 
        /// </summary>
        /// <returns></returns>
        public static string GetConnectionString(string connectionStringNameKey)
        {
            return ConfigurationManager.ConnectionStrings[connectionStringNameKey].ConnectionString;
        }

        /// <summary>
        /// Gets the Entity Contanier Name.
        /// </summary>
        /// <returns></returns>
        public static string GetEntityContainerName(string containerKey)
        {
            return ConfigurationManager.AppSettings[containerKey];
        }

    }
}
