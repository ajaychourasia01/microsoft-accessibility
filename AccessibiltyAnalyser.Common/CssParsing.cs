﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;


namespace AccessibilityAnalyser.Common
{
    public class CssParsing
    {       
        /// <summary>
        /// Method to get list of all the stylesheets/css classes from the url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetStyleSheetClasses(HtmlDocument htmlDoc,string BaseAddress)
        {
            List<string> listStyleSheets = new List<string>();
            Dictionary<string, string> classProperties = new Dictionary<string, string>();
            WebClient client = null;
            string tempCss = string.Empty;

            //Gets the style sheet nodes
            HtmlNodeCollection colStyle = htmlDoc.DocumentNode.SelectNodes("//style");
            HtmlNodeCollection colLink = htmlDoc.DocumentNode.SelectNodes("//link");


            if (colStyle != null)
            {
                //Checks for stylesheets with attributes and all the stylesheet of a url is loaded to a list
                listStyleSheets = (from nd in colStyle
                                   where (nd.Attributes["type"] != null)
                                   select nd.InnerHtml.Replace("\n", string.Empty).Replace("\r", string.Empty).Replace("\t", string.Empty).ToString()).ToList();
            }


            if (colLink != null)
            {
                client = new WebClient();

                var hrefStyles = (from st in colLink
                                  where (st.Attributes["href"] != null &&
                                        (((st.Attributes["type"] != null) && (st.Attributes["type"].Value.ToLower().Trim() == "text/css")) || ((st.Attributes["rel"] != null) && (st.Attributes["rel"].Value.ToLower().Trim() == "stylesheet")))
                                 )
                                  select st.Attributes["href"].Value.ToString()).ToList();

                hrefStyles = (from links in hrefStyles
                              let url = ValidateUrl(links, BaseAddress)
                              where !(string.IsNullOrEmpty(url))
                              select url).ToList();


                foreach (string url in hrefStyles)
                {
                    try
                    {
                        tempCss = client.DownloadString(url);
                        tempCss = Regex.Replace(tempCss, @"\t|\n|\r", string.Empty);
                    }
                    catch (WebException ex)
                    {
                        if (ex.Status == WebExceptionStatus.ProtocolError && ex.Response != null)
                        {
                            var resp = (HttpWebResponse)ex.Response;
                            if (resp.StatusCode == HttpStatusCode.NotFound) //HTTP 404
                            {
                                //the page was not found, continue with next in the for loop
                                continue;
                            }
                        }  
                    }
                    if (!(string.IsNullOrEmpty(tempCss)))
                    {
                        listStyleSheets.Add(tempCss);
                    }
                }


                listStyleSheets.AddRange(hrefStyles);
            }

            classProperties = GetClassProperties(listStyleSheets);

            return classProperties;
        }



        /// <summary>
        /// Gets the css classes and its properties and returns it in form of dictionary
        /// </summary>
        /// <param name="lstStyleSheets"></param>
        /// <param name="className"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetClassProperties(List<string> lstStyleSheets)
        {
            string[] parts;

            Dictionary<string, string> classProperties = new Dictionary<string, string>();

            //iterates each style sheets to gets all the styles
            foreach (string styles in lstStyleSheets)
            {
                parts = styles.Split('}');

                var style = (from val in parts
                             let ssc = val.Split('{')
                             select ssc).ToArray();

                //gets only the css class
                var thisStyle = (from classProp in style
                                 where (classProp[0].IndexOf('.') == 0)
                                 select classProp).ToList().GroupBy(pair => pair[0].ToString()).Select(group => group.First()).ToDictionary(p => p[0].ToString(), p => p[1].ToString());
                                            

                //combines all the classes together
                classProperties = classProperties.Concat(thisStyle).GroupBy(pair => pair.Key).Select(group => group.First()).ToDictionary(x => x.Key, x => x.Value);

            }           

            return classProperties;
        }

        public static string ValidateUrl(string urls,string baseAddress)
        {
            string validUrls = string.Empty;
            string regex = @"^((http)://|(https)://|(www)\.)[a-z0-9-]+(\.[a-z0-9-]+)+([/?].*)?$";
            Match urlMatch = Regex.Match(urls, regex);
            if (urlMatch.Success)
            {
                validUrls = urls;
            }
            else
            {
                if (urls.Contains("//") && urls.IndexOf("//") == 0)
                {
                    validUrls = "http:" + urls;
                }

                else if (urls.Contains("/") && urls.IndexOf("/") == 0)
                {
                    if (!string.IsNullOrEmpty(baseAddress))
                    {
                        validUrls = "https://" + baseAddress + urls;
                    }
                }
                else if (urls.StartsWith(".."))
                {
                    urls = urls.Remove(0, 2);
                    validUrls = "http://" + baseAddress + urls;
                }
                else
                {
                    validUrls = string.Empty;
                }
            }
            return validUrls;
        }       

    }
}
