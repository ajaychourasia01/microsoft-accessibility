﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects.DataClasses;

using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class ClientCollection : IEntityCollection<ClientBO>
    { 
        IDataAdapter entityStore = null;

        List<ClientBO> clientInfo = null;

        public ClientCollection()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }

        public ClientBO GetSingle(int ObjectId)
        {
            EntityObject clientObject = entityStore.GetClient(ObjectId);

            ClientBO client = new ClientBO();
            Helper.CopyPropertyValues(clientObject, client);

            return client;
        }

        public List<ClientBO> GetRecords(string clientName)
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetClients(clientName);
            clientInfo = new List<ClientBO>();

            foreach (EntityObject eo in entityEnumerable)
            {
                ClientBO client = new ClientBO();
                Helper.CopyPropertyValues(eo, client);
                clientInfo.Add(client);
            }

            return clientInfo;
        }


        public bool UpdateData(List<ClientBO> entityData)
        {
            throw new NotImplementedException();
        }

        public bool UpdateDataSingle(ClientBO entityData)
        {
            return entityStore.CreateClient(entityData);
        }


        public List<ClientBO> GetRecords()
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetClients();
            clientInfo = new List<ClientBO>();

            foreach (EntityObject eo in entityEnumerable)
            {
                ClientBO client = new ClientBO();
                Helper.CopyPropertyValues(eo, client);
                clientInfo.Add(client);
            }

            return clientInfo;
        }

        public List<ClientBO> GetRecords(int clientID)
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetClients(clientID);
            clientInfo = new List<ClientBO>();
            foreach (EntityObject eo in entityEnumerable)
            {
                ClientBO client = new ClientBO();
                Helper.CopyPropertyValues(eo, client);
                clientInfo.Add(client);
            }

            return clientInfo;
        }
    }
}
