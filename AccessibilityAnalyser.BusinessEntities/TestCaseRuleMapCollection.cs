﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects.DataClasses;

using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class TestCaseRuleMapCollection : IEntityCollection<TestCaseRuleMapBO>
    {
        IDataAdapter entityStore = null;
        List<TestCaseRuleMapBO> testcaseRuleMapInfo = null;

        public TestCaseRuleMapCollection()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }

        public TestCaseRuleMapBO GetSingle(int ObjectId)
        {
            EntityObject testcaseRuleMapObject = entityStore.GetTestCaseMapping(ObjectId);

            TestCaseRuleMapBO testcaseRuleMap = new TestCaseRuleMapBO();
            Helper.CopyPropertyValues(testcaseRuleMapObject, testcaseRuleMap);

            return testcaseRuleMap;
        }

        public List<TestCaseRuleMapBO> GetRecords()
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetTestCasesRuleMappings();

            testcaseRuleMapInfo = new List<TestCaseRuleMapBO>();
            TestCaseRuleMapBO testcasemap = null;

            entityEnumerable.ToList().ForEach(eo =>
            {
                testcasemap = new TestCaseRuleMapBO();
                Helper.CopyPropertyValues(eo, testcasemap);
                testcaseRuleMapInfo.Add(testcasemap);
            });

            //foreach (EntityObject eo in entityEnumerable)
            //{
            //    TestCaseRuleMapBO testcasemap = new TestCaseRuleMapBO();
            //    Helper.CopyPropertyValues(eo, testcasemap);
            //    testcaseRuleMapInfo.Add(testcasemap);
            //}

            return testcaseRuleMapInfo;
        }

        public bool UpdateData(List<TestCaseRuleMapBO> entityData)
        {
            //return entityStore.CreateTestCaseMaps(entityData);
            throw new NotImplementedException();
        }

        /// <summary>
        /// Update a single record of TestCaseRuleMap table
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        public bool UpdateDataSingle(TestCaseRuleMapBO entityData)
        {
            return entityStore.CreateTestCaseMap(entityData);
        }

        /// <summary>
        /// Delete a single Data/row from TestCaseRuleMap table in Database
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        public bool DeleteDataSingle(TestCaseRuleMapBO entityData)
        {
            //ViolationCollection ViolationObject = new ViolationCollection();
            //List<ViolationBO> ViolationList = (from Violation in ViolationObject.GetRecords()
            //                                   where Violation.TRMappingId == entityData.TRMappingId
            //                                   select Violation).ToList();
            //ViolationObject.DeleteDataMultiple(ViolationList);
            return entityStore.DeleteTestCaseRuleMap(entityData);
        }

        /// <summary>
        /// Delete a Multiple Data/row from TestCaseRuleMap table in Database
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        public bool DeleteDataMultiple(List<TestCaseRuleMapBO> entityData)
        {
            bool IsDeleted = false;
            foreach (TestCaseRuleMapBO TestCaseRuleMapItem in entityData)
            {
                DeleteDataSingle(TestCaseRuleMapItem);
            }
            IsDeleted = !IsDeleted;
            return IsDeleted;
        }

        public List<TestCaseRuleMapBO> GetProrityList(int testCaseId)
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetProrityList(testCaseId);

            testcaseRuleMapInfo = new List<TestCaseRuleMapBO>();

            foreach (EntityObject eo in entityEnumerable)
            {
                var testcasemap = new TestCaseRuleMapBO();
                Helper.CopyPropertyValues(eo, testcasemap);
                testcaseRuleMapInfo.Add(testcasemap);
            }
            return testcaseRuleMapInfo;
        }


    }
}
