﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class RuleDetailBO : IRuleDetail
    {
        IDataAdapter entityStore = null;

        public RuleDetailBO()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }
        public string RuleName { get; set; }
        public int RuleDetailId { get; set; }
        public string RuleDetailName{ get; set; }
        public string RuleDetailDescription { get; set; }
        public string StepsToPerform { get; set; }
        public string MethodToInvoke { get; set; }
        public string ElementToInspect { get; set; }
        public string ElementType { get; set; }
        public int RuleId { get; set; }
        public bool IsAutomated { get; set; }
        
        public int ViolationsCount { get; set; }
        public int AcceptedViolationsCount { get; set; }
        public int DeclinedViolationsCount { get; set; }
        public int DuplicateRuleDetailId { get; set; }
        public bool IsDuplicate { get; set; }

        public bool IsBaseUrl { get; set; }

        public bool IsActive
        {
            get;
            set;
        }

        public string CreateBy
        {
            get;
            set;
        }

        public DateTime CreateDate
        {
            get;
            set;
        }

        public string UpdateBy
        {
            get;
            set;
        }

        public DateTime UpdateDate
        {
            get;
            set;
        }

        public bool CreateRuleDetail()
        {
            return entityStore.CreateRuleDetail(this);
        }
    }
}
