﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects.DataClasses;

using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class RuleCollection : IEntityCollection<RuleBO>
    {

        IDataAdapter entityStore = null;
        List<RuleBO> ruleInfo = null;

        public RuleCollection()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }

        public RuleBO GetSingle(int ObjectId)
        {
            EntityObject ruleObject = entityStore.GetRule(ObjectId);

            RuleBO rule = new RuleBO();
            Helper.CopyPropertyValues(ruleObject, rule);

            return rule;
        }

        public List<RuleBO> GetRecords()
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetRules();

            ruleInfo = new List<RuleBO>();

            foreach (EntityObject eo in entityEnumerable)
            {
                RuleBO rule = new RuleBO();
                Helper.CopyPropertyValues(eo, rule);
                ruleInfo.Add(rule);
            }

            return ruleInfo;
        }

        public bool UpdateData(List<RuleBO> entityData)
        {
            throw new NotImplementedException();
        }

        public bool UpdateDataSingle(RuleBO entityData)
        {
            throw new NotImplementedException();
        }
    }
}
