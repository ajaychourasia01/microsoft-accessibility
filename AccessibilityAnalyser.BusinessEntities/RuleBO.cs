﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class RuleBO : IRule
    {
        IDataAdapter entityStore = null;

        public RuleBO()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }


        public int RuleId { get; set; }
        public int StandardId { get; set; }
        public string RuleName { get; set; }
        public string RuleDescription { get; set; }

        public bool IsActive
        {
            get;
            set;
        }

        public string CreateBy
        {
            get;
            set;
        }

        public DateTime CreateDate
        {
            get;
            set;
        }

        public string UpdateBy
        {
            get;
            set;
        }

        public DateTime UpdateDate
        {
            get;
            set;
        }

        public bool CreateRule()
        {
            return entityStore.CreateRule(this);
        }
    }
}
