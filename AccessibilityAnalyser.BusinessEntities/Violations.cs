﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class Violations
    {
        public List<ViolationBO> ViolationList { get; set; }
        public int TotalViolationCount { get; set; }
        public int AcceptedViolationCount { get; set; }
        public int DeclinedViolationCount { get; set; }
    }
}
