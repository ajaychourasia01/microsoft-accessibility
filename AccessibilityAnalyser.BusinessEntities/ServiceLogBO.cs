﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class ServiceLogBO: IServiceLog
    {
        IDataAdapter entityStore = null;

        public ServiceLogBO()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }

        public int LogId { get; set; }
        public int ClientId { get; set; }
        public string Status { get; set; }
        public DateTime RequestTime { get; set; }
        public int RunId { get; set; }
        public int BuildId { get; set; }
        public string FileName { get; set; }
    }
}
