﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class ClientBO : IClient
    {
        IDataAdapter entityStore = null;

        public ClientBO()
        {
            //entityStore = new DataStoreFactory().GetDataStore();
        }

        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string ClientDescription { get; set; }
        public bool IsActive
        {
            get;
            set;
        }

        public string CreateBy
        {
            get;
            set;
        }

        public Nullable<DateTime> CreateDate
        {
            get;
            set;
        }

        public string UpdateBy
        {
            get;
            set;
        }

        public Nullable<DateTime> UpdateDate
        {
            get;
            set;
        }

        public bool CreateClient()
        {
            if(entityStore == null)
            {
                entityStore = new DataStoreFactory().GetDataStore();
            }
            return entityStore.CreateClient(this);
        }
    }
}
