﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;

using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class ViolationLogCollection 
    {
        IDataAdapter entityStore = null;       

        public ViolationLogCollection()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }

        /// <summary>
        /// Insert Data to ViolationLog table
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        public bool CreateViolationsLog(IList<IViolationLog> entityData)
        {
            return entityStore.CreateViolationsLog(entityData);
        }

        /// <summary>
        /// Get ViolationLogId from Violation_Log table based on the inputs provided
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        public int GetViolationLogId(string urlName, int testCaseId, int clientId, int buildId, int runId)
        {
            return entityStore.GetViolationLogId(urlName, testCaseId, clientId, buildId, runId);
        }

        /// <summary>
        /// Update violationLog data in violation Table Set the status attribute
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        public bool updateViolationLog(Dictionary<int, char> objStatusDictionay)
        {
            return entityStore.UpdateViolationLog(objStatusDictionay);
        }

        /// <summary>
        /// Update violationLog data in violation Table Set the status attribute to InProgress
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        public bool UpdateViolationLogWithStatus(int violationLogId, string statusVal)
        {
            return entityStore.UpdateViolationLogWithStatus(violationLogId,statusVal);
        }

        /// <summary>
        /// Update violationLog data in violation Table Set the status attribute to InProgress
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        public char GetViolationLogStatus()
        {
            return entityStore.GetViolationLogStatus();
        }

        /// <summary>
        /// Get Intial ViolationLogId from Violation_Log table based on the clientID provided
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        public int GetIntialViolationLogId(int clientId)
        {
            return entityStore.GetInitialViolationLogId(clientId);
        }
    }
}
