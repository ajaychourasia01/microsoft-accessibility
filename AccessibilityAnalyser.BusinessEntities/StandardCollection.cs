﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects.DataClasses;

using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class StandardCollection : IEntityCollection<StandardBO>
    {
        IDataAdapter entityStore = null;
        List<StandardBO> standardInfo = null;

        public StandardCollection()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }

        public StandardBO GetSingle(int ObjectId)
        {
            EntityObject standardObject = entityStore.GetStandard(ObjectId);

            StandardBO standard = new StandardBO();
            Helper.CopyPropertyValues(standardObject, standard);

            return standard;
        }

        public List<StandardBO> GetRecords()
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetStandards();

            standardInfo = new List<StandardBO>();

            foreach (EntityObject eo in entityEnumerable)
            {
                StandardBO standard = new StandardBO();
                Helper.CopyPropertyValues(eo, standard);
                standardInfo.Add(standard);
            }

            return standardInfo;
        }

        public bool UpdateData(List<StandardBO> entityData)
        {
            throw new NotImplementedException();
        }

        public bool UpdateDataSingle(StandardBO entityData)
        {
            throw new NotImplementedException();
        }
    }
}
