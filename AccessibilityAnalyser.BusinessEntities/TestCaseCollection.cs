﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects.DataClasses;

using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class TestCaseCollection : IEntityCollection<TestCaseBO>
    {
        IDataAdapter entityStore = null;
        List<TestCaseBO> testCaseInfo = null;

        public TestCaseCollection()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }

        public TestCaseBO GetSingle(int ObjectId)
        {
            EntityObject testcaseObject = entityStore.GetTestCase(ObjectId);

            TestCaseBO testcase = new TestCaseBO();
            Helper.CopyPropertyValues(testcaseObject, testcase);

            return testcase;
        }

        public List<TestCaseBO> GetRecords()
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetTestCases();

            testCaseInfo = new List<TestCaseBO>();
            TestCaseBO testcase = null;

            entityEnumerable.ToList().ForEach(eo =>
            {
                testcase = new TestCaseBO();
                Helper.CopyPropertyValues(eo, testcase);
                testCaseInfo.Add(testcase);
            });

            //foreach (EntityObject eo in entityEnumerable)
            //{
            //    TestCaseBO testcase = new TestCaseBO();
            //    Helper.CopyPropertyValues(eo, testcase);
            //    testCaseInfo.Add(testcase);
            //}

            return testCaseInfo;
        }

        public bool UpdateData(List<TestCaseBO> entityData)
        {
            //return entityStore.CreateTestCases(entityData);
            throw new NotImplementedException();
        }

        public bool UpdateDataSingle(TestCaseBO entityData)
        {
            return entityStore.CreateTestCase(entityData);
        }

        /// <summary>
        /// Update Single Record of TestCase into DataBase
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        public bool UpdateSingleTestCase(TestCaseBO entityData)
        {
            return entityStore.UpdateTestCase(entityData);
        }

        /// <summary>
        /// Delete a single record of Testcase in Database
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        public bool DeleteSingleTestCase(TestCaseBO entityData)
        {
            TestCaseRuleMapCollection TestCaseRuleMapObject = new TestCaseRuleMapCollection();
            ViolationCollection violationCollection = new ViolationCollection();


            List<TestCaseRuleMapBO> TestCaseRuleMapCollection = (from j in TestCaseRuleMapObject.GetRecords()
                                                                 where j.TestCaseId == entityData.TestCaseId
                                                                 select j).ToList();

            List<ViolationBO> lstViolation = violationCollection.GetBuildSpecificViolations(entityData.BuildId).ToList();

            lstViolation = (from violation in lstViolation
                            join trMapping in TestCaseRuleMapCollection
                            on violation.TRMappingId equals trMapping.TRMappingId
                            where trMapping.TestCaseId == entityData.TestCaseId
                            select violation).ToList();


            violationCollection.DeleteDataMultiple(lstViolation);
            TestCaseRuleMapObject.DeleteDataMultiple(TestCaseRuleMapCollection);
            return entityStore.DeleteTestCase(entityData);
        }


        public List<TestCaseBO> GetTestCaseByBuilId(int buildId)
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetTestCasesByBuildId(buildId);
            testCaseInfo = new List<TestCaseBO>();

            foreach (EntityObject eo in entityEnumerable)
            {
                var testcase = new TestCaseBO();
                Helper.CopyPropertyValues(eo, testcase);
                testCaseInfo.Add(testcase);
            }
            return testCaseInfo;
        }
    }
}
