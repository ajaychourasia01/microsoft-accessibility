﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects.DataClasses;

using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;
using System.IO;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class ServiceLogCollection : IEntityCollection<ServiceLogBO>
    {
        IDataAdapter entityStore = null;        

        public ServiceLogCollection()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }

        public ServiceLogBO GetSingle(int ObjectId)
        {
            EntityObject ServiceLogObject = entityStore.GetServiceLog(ObjectId);

            ServiceLogBO serviceLog = new ServiceLogBO();
            Helper.CopyPropertyValues(ServiceLogObject, serviceLog);

            return serviceLog;
        }

        public List<ServiceLogBO> GetRecords()
        {
            List<ServiceLogBO> serviceLogInfo = new List<ServiceLogBO>();

            IEnumerable<EntityObject> entityEnumerable = entityStore.GetServiceLogs();            
            foreach (EntityObject eo in entityEnumerable)
            {
                ServiceLogBO servicelog = new ServiceLogBO();
                Helper.CopyPropertyValues(eo, servicelog);
                serviceLogInfo.Add(servicelog);
            }


            return serviceLogInfo;
        }

        public bool UpdateData(List<ServiceLogBO> entityData)
        {
            //return entityStore.CreateTestCaseMaps(entityData);
            throw new NotImplementedException();
        }

        /// <summary>
        /// Update a single record of ServiceLog table
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        public bool UpdateDataSingle(ServiceLogBO entityData)
        {
            return entityStore.UpdateServiceLog(entityData);
        }
    }
}
