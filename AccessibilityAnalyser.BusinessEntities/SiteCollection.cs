﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects.DataClasses;

using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class SiteCollection : IEntityCollection<SiteBO>
    {
        IDataAdapter entityStore = null;
        List<SiteBO> siteInfo = null;

        public SiteCollection()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }

        public SiteBO GetSingle(int ObjectId)
        {
            EntityObject siteObject = entityStore.GetSite(ObjectId);

            SiteBO site = new SiteBO();
            Helper.CopyPropertyValues(siteObject, site);

            return site;
        }

        public List<SiteBO> GetRecords()
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetSites();

            siteInfo = new List<SiteBO>();

            foreach (EntityObject eo in entityEnumerable)
            {
                SiteBO site = new SiteBO();
                Helper.CopyPropertyValues(eo, site);
                siteInfo.Add(site);
            }

            return siteInfo;
        }

        public List<SiteBO> GetRecords(int ClientID)
        {
            IEnumerable<EntityObject> entityEnumerable = entityStore.GetSites(ClientID);

            siteInfo = new List<SiteBO>();

            foreach (EntityObject eo in entityEnumerable)
            {
                SiteBO site = new SiteBO();
                Helper.CopyPropertyValues(eo, site);
                siteInfo.Add(site);
            }

            return siteInfo;
        }
        public bool UpdateData(List<SiteBO> entityData)
        {
            throw new NotImplementedException();
        }

        public bool UpdateDataSingle(SiteBO entityData)
        {
            return entityStore.CreateSite(entityData);
        }
    }
}
