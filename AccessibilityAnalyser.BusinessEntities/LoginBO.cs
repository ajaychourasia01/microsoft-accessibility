﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class LoginBO : ILogin
    {
        IDataAdapter entityStore = null;

        public LoginBO()
        {
            entityStore = new DataStoreFactory().GetDataStore();
        }

        public int Id
        {
            get;
            set;
        }
        public string UserName
        {
            get;
            set;
        }
        public string Password
        {
            get;
            set;
        }
        public Nullable<DateTime> CreateDate
        {
            get;
            set;
        }
        public string CreateBy
        {
            get;
            set;
        }
        public Nullable<DateTime> UpdateDate
        {
            get;
            set;
        }
        public string UpdateBy
        {
            get;
            set;
        }
    }
}
