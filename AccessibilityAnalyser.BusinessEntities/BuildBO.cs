﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class BuildBO : IBuild
    {
        IDataAdapter entityStore = null;

         public BuildBO()
        {
            //entityStore = new DataStoreFactory().GetDataStore();
        }

        public int BuildId
        { 
            get; 
            set; 
        }

        public string BuildName 
        { 
            get; 
            set; 
        }

        public string BuildDescription 
        { 
            get; 
            set; 
        }

        public int SiteId
        { 
            get; 
            set; 
        }

        public bool IsActive
        {
            get;
            set;
        }

        public string CreateBy
        {
            get;
            set;
        }

        public Nullable<DateTime> CreateDate
        {
            get;
            set;
        }

        public string UpdateBy
        {
            get;
            set;
        }

        public DateTime UpdateDate
        {
            get;
            set;
        }

        public bool CreateBuild()
        {
            if (entityStore == null)
            {
                entityStore = new DataStoreFactory().GetDataStore();
            }
            return entityStore.CreateBuild(this);
        }

    }
}
