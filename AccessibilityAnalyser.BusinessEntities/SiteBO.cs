﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AccessibilityAnalyser.DataAdapter;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.BusinessEntities
{
    public class SiteBO : ISite
    {
         IDataAdapter entityStore = null;

         public SiteBO()
        {
            //entityStore = new DataStoreFactory().GetDataStore();
        }

        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public string SiteDescription { get; set; }
        public int ClientId { get; set; }

        public bool IsActive
        {
            get;
            set; 
        }

        public string CreateBy
        {
            get;
            set; 
        }

        public DateTime CreateDate
        {
            get;
            set; 
        }

        public string UpdateBy
        {
            get;
            set; 
        }

        public DateTime UpdateDate
        {
            get;
            set; 
        }

        public bool CreateSite()
        {
            if(entityStore == null)
            {
                entityStore = new DataStoreFactory().GetDataStore();
            }
            return entityStore.CreateSite(this);
        }
    }
}
