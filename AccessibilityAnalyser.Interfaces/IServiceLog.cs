﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessibilityAnalyser.Interfaces
{
    public interface IServiceLog
    {
        int LogId { get; set; }
        int ClientId { get; set; }
        int RunId { get; set; }
        int BuildId { get; set; }
        string Status { get; set; }
        DateTime RequestTime  {get;  set;}
        string FileName { get; set; }
    }
}
