﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessibilityAnalyser.Interfaces
{
    public interface IStandard
    {
        int StandardId { get; set; }
        string StandardTitle { get; set; }
        string StandardDescription { get; set; }
        bool IsActive
        {
            get;
            set;
        }

        string CreateBy
        {
            get;
            set;
        }

        DateTime CreateDate
        {
            get;
            set;
        }

        string UpdateBy
        {
            get;
            set;
        }

        DateTime UpdateDate
        {
            get;
            set;
        }

    }
}
