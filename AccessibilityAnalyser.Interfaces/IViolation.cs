﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessibilityAnalyser.Interfaces
{
    public interface IViolation
    {
        //Ajay Added
        string RuleName { get; set; }
        int ViolationId { get; set; }
        string ViolationName { get; set; }
        string ViolationDescription { get; set; }
        int TRMappingId { get; set; }
        string SourceURL { get; set; }
        string SourceElement { get; set; }
        int SourceLineNumber { get; set; }
        string Recomandation { get; set; }
        string Status { get; set; }
        int RunId { get; set; }
        int ClientId { get; set; }
        int BuildId { get; set; }
        int violationLogId { get; set; }
        int DupTRMappingId { get; set; }
        bool IsActive
        {
            get;
            set;
        }

        DateTime CreateDate
        {
            get;
            set;
        }

        string CreateBy
        {
            get;
            set;
        }

        DateTime UpdateDate
        {
            get;
            set;
        }

        string UpdateBy
        {
            get;
            set;
        }

        //ITestCaseRuleMap TestCaseRuleMap
        //{
        //    get;
        //    set;
        //}         
        
    }
}
