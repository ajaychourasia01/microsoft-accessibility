﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects.DataClasses;

namespace AccessibilityAnalyser.Interfaces
{
    public interface IDataAdapter
    {


        bool CreateClient(IClient clientData);
        EntityObject GetClient(int clientId);
        IEnumerable<EntityObject> GetClients(string clientName);
        IEnumerable<EntityObject> GetClients(int clientID);
        IEnumerable<EntityObject> GetClients();

        bool CreateSite(ISite siteData);
        EntityObject GetSite(int siteId);
        IEnumerable<EntityObject> GetSites();
        IEnumerable<EntityObject> GetSites(int clientID);

        bool CreateBuild(IBuild buildData);
        EntityObject GetBuild(int buildId);
        IEnumerable<EntityObject> GetBuilds();

        bool CreateTestCase(ITestCase testcaseData);
        //bool CreateTestCases(List<ITestCase> testcaseData);
        EntityObject GetTestCase(int testcaseId);
        IEnumerable<EntityObject> GetTestCases();

        bool CreateStandard(IStandard standardData);
        EntityObject GetStandard(int standardId);
        IEnumerable<EntityObject> GetStandards();

        bool CreateRule(IRule ruleData);
        EntityObject GetRule(int ruleId);
        IEnumerable<EntityObject> GetRules();

        bool CreateRuleDetail(IRuleDetail ruledetailData);
        EntityObject GetRuleDetail(int ruledetailId);
        IEnumerable<EntityObject> GetRuleDetails();

        bool CreateTestCaseMap(ITestCaseRuleMap testcaseRuleMapData);
        //bool CreateTestCaseMaps(List<ITestCaseRuleMap> testcaseRuleMapData);
        EntityObject GetTestCaseMapping(int testcaseRuleMapId);
        IEnumerable<EntityObject> GetTestCasesRuleMappings();

        bool CreateViolation(IViolation violationData);
        EntityObject GetViolation(int violationId);
        IEnumerable<EntityObject> GetViolations(int clientId);
        IEnumerable<EntityObject> GetViolations();
        bool UpdateViolation(IViolation violationData);
        bool UpdateViolationForManual(IViolation violationData);
        bool UpdateTestCase(ITestCase TestCaseData);
        bool DeleteTestCaseRuleMap(ITestCaseRuleMap TestCaseRuleMap);
        bool DeleteTestCase(ITestCase TestCaseData);
        bool DeleteViolation(IViolation ViolationData);

        bool CreateRun(IRun runData);
        EntityObject GetRun(int clientId, int buildId, int runId);
        IEnumerable<EntityObject> GetRuns();
       // IEnumerable<EntityObject> GetUsers(string accountUserName);
        IEnumerable<EntityObject> GetUsers();
        bool CreateViolations(IList<IViolation> violationList);
        int GetViolationCount(int clientId);
        int GetViolationCountPerRule(int buildId, int runId, int ruleDetailId, string typeCount, int testCaseId);

        IEnumerable<EntityObject> GetViolationList(int clientId, int buildId, int runId, int ruleDetailId,
                                                   string violationType, int testCaseId);

        IEnumerable<EntityObject> GetSpecificViolations(int clientId, int buildId, int runId);
        IEnumerable<EntityObject> GetBuildSpecificViolations(int buildId);
        IEnumerable<EntityObject> GetTestCasesByBuildId(int buildId);
        IEnumerable<EntityObject> GetRuleDetailsListByTestCase(int testCaseId);
        IEnumerable<EntityObject> GetProrityList(int testCaseId);


        bool CreateViolationsLog(IList<IViolationLog> violationLogList);

        int GetViolationLogId(string urlName, int testCaseId, int clientId, int buildId, int runId);

        bool UpdateViolationLog(Dictionary<int, char> objStatusDictionay);

        int GetViolationCount(int clientID, int buildID, int runID);
        int GetFailedViolationCount(int clientID, int builldID, int runID);
        IEnumerable<EntityObject> GetTestCaseCount(int clientID, int buildID, int runID);

        int GetTestCaseViolationCount(int tcId, int clientID, int buildID, int runID);
        int GetFailedTestCaseViolationCount(int tcId, int clientID, int buildID, int runID);

        IEnumerable<EntityObject> GetClientTestCaseCount(int clientID);
        int GetClientTestCaseViolationCount(int tcId, int clientID);
        int GetClientFailedTestCaseViolationCount(int tcId, int clientID);
        bool UpdateViolationLogWithStatus(int violationLogId, string statusVal);
        char GetViolationLogStatus();

        int GetInitialViolationLogId(int clientId);

        bool CreateServiceLog(IServiceLog clientData);
        EntityObject GetServiceLog(int logId);
        IEnumerable<EntityObject> GetServiceLogs();
        bool UpdateServiceLog(IServiceLog clientData);
    }
}
