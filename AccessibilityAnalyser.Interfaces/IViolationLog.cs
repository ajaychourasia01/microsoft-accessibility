﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessibilityAnalyser.Interfaces
{
    public interface IViolationLog
    {
        int ViolationLogId { get; set; }
        int ClientId { get; set; }
        int BuildId { get; set; }
        int RunId { get; set; }
        int TestCaseId { get; set; }
        string SourceURL { get; set; }
        char Status { get; set; }
        DateTime CreateDate { get; set; }
        string CreateBy { get; set; }
        DateTime UpdateDate { get; set; }
        string UpdateBy { get; set; }
    }
}
