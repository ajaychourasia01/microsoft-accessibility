﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessibilityAnalyser.Interfaces
{
    public interface IEntityCollection<T>
    {
        /// <summary>
        /// Get single entity
        /// </summary>
        /// <param name="ObjectId"></param>
        /// <returns></returns>
        T GetSingle(int ObjectId);
        
        /// <summary>
        /// Get entity collection
        /// </summary>
        /// <returns></returns>
        List<T> GetRecords();

        /// <summary>
        /// update entiy collection
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        bool UpdateData(List<T> entityData);

        /// <summary>
        /// update single entity object
        /// </summary>
        /// <param name="entityData"></param>
        /// <returns></returns>
        bool UpdateDataSingle(T entityData);
    }
}
