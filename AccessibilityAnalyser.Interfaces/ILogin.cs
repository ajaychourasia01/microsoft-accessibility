﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessibilityAnalyser.Interfaces
{
   public interface ILogin
    {
        int Id
        {
            get;
            set;
        }

        string UserName
        {
            get;
            set;
        }

        string Password
        {
            get;
            set;
        }

        Nullable<DateTime> CreateDate
        {
            get;
            set;
        }

        string CreateBy
        {
            get;
            set;
        }

        Nullable<DateTime> UpdateDate
        {
            get;
            set;
        }

        string UpdateBy
        {
            get;
            set;
        }
    }
}
