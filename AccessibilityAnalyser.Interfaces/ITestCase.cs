﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessibilityAnalyser.Interfaces
{
    public interface ITestCase
    {
        int TestCaseId { get; set; }
        string TestCaseName { get; set; }
        string TestCaseDescription { get; set; }
        int BuildId { get; set; }
        bool IsActive
        {
            get;
            set;
        }

        string CreateBy
        {
            get;
            set;
        }

        DateTime CreateDate
        {
            get;
            set;
        }

        string UpdateBy
        {
            get;
            set;
        }

        DateTime UpdateDate
        {
            get;
            set;
        }
    }
}
