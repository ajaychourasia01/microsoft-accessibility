﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessibilityAnalyser.Interfaces
{
    public interface IRun
    {
        int ClientId { get; set; }
        int BuildId { get; set; }
        int RunId { get; set; }
        string RunName { get; set; }
        string CreatedBy { get; set; }
        Nullable<DateTime> CreatedDate { get; set; }
        string UpdateBy { get; set; }
        Nullable<DateTime> UpdateDate { get; set; }
        bool IsActive { get; set; }
    }
}
