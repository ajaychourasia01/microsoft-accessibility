﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessibilityAnalyser.Interfaces
{
    public interface IRuleDetail
    {
        int RuleDetailId { get; set; }
        string RuleDetailName { get; set; }
        string RuleDetailDescription { get; set; }
        string StepsToPerform { get; set; }
        string MethodToInvoke { get; set; }
        string ElementToInspect { get; set; }
        string ElementType { get; set; }
        int RuleId { get; set; }
        bool IsAutomated { get; set; }
        
        int DuplicateRuleDetailId { get; set; }
        bool IsDuplicate { get; set; }
        bool IsBaseUrl { get; set; }

        bool IsActive
        {
            get;
            set;
        }

        string CreateBy
        {
            get;
            set;
        }

        DateTime CreateDate
        {
            get;
            set;
        }

        string UpdateBy
        {
            get;
            set;
        }

        DateTime UpdateDate
        {
            get;
            set;
        }
    }
}
