﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AccessibilityAnalyser.Interfaces
{
    public interface IClient
    {
        int ClientId { get; set; }
        string ClientName { get; set; }
        string ClientDescription { get; set; }

        bool IsActive
        {
            get;
            set;
        }

        string CreateBy
        {
            get;
            set;
        }

        Nullable<DateTime> CreateDate
        {
            get;
            set;
        }

        string UpdateBy
        {
            get;
            set;
        }

        Nullable<DateTime> UpdateDate
        {
            get;
            set;
        }
    }
}
