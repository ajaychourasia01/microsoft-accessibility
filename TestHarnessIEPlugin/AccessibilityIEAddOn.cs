﻿using Microsoft.Win32;
using mshtml;
using SHDocVw;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace AccessibilityAnalyser.BrowserPlugIn
{

    [ComVisible(true)]
    [ClassInterface(ClassInterfaceType.None)]
    [Guid("23c38df1-d924-49ac-87f4-b2629624ada8")]
    [ProgId("Brillio.AccessibilityTesting")]
    public class AccessibilityTestingBHO : IObjectWithSite, IOleCommandTarget
    {
        IWebBrowser2 browser;
        private object site;

        #region Load and Save Data
        [DllImport("ieframe.dll")]
        public static extern int IEGetWriteableHKCU(ref IntPtr phKey);

        #endregion

        [Guid("6D5140C1-7436-11CE-8034-00AA006009FA")]
        [InterfaceType(1)]
        public interface IServiceProvider
        {
            int QueryService(ref Guid guidService, ref Guid riid, out IntPtr ppvObject);
        }

        #region Implementation of IObjectWithSite
        int IObjectWithSite.SetSite(object site)
        {
            //System.Diagnostics.Debugger.Launch();
            this.site = site;

            if (site != null)
            {
                var serviceProv = (IServiceProvider)this.site;
                var guidIWebBrowserApp = Marshal.GenerateGuidForType(typeof(IWebBrowserApp)); // new Guid("0002DF05-0000-0000-C000-000000000046");
                var guidIWebBrowser2 = Marshal.GenerateGuidForType(typeof(IWebBrowser2)); // new Guid("D30C1661-CDAF-11D0-8A3E-00C04FC9E26E");
                IntPtr intPtr;
                serviceProv.QueryService(ref guidIWebBrowserApp, ref guidIWebBrowser2, out intPtr);

                browser = (IWebBrowser2)Marshal.GetObjectForIUnknown(intPtr);

                //((DWebBrowserEvents2_Event)browser).DocumentComplete +=
                //    new DWebBrowserEvents2_DocumentCompleteEventHandler(this.OnDocumentComplete);
                //(browser as DWebBrowserEvents2_Event).DocumentComplete += (this.OnDocumentComplete);
            }
            else
            {
                //(browser as DWebBrowserEvents2_Event).DocumentComplete -= (this.OnDocumentComplete);
                browser = null;
            }
            return 0;
        }

        int IObjectWithSite.GetSite(ref Guid guid, out IntPtr ppvSite)
        {
            IntPtr punk = Marshal.GetIUnknownForObject(browser);
            int hr = Marshal.QueryInterface(punk, ref guid, out ppvSite);
            Marshal.Release(punk);
            return hr;
        }
        #endregion
        #region Implementation of IOleCommandTarget
        int IOleCommandTarget.QueryStatus(IntPtr pguidCmdGroup, uint cCmds, ref OLECMD prgCmds, IntPtr pCmdText)
        {
            return 0;
        }

        int IOleCommandTarget.Exec(IntPtr pguidCmdGroup, uint nCmdID, uint nCmdexecopt, IntPtr pvaIn, IntPtr pvaOut)
        {
            System.Diagnostics.Debugger.Launch();
            
            try
            {
                // Accessing the document from the command-bar.
                var document = browser.Document as IHTMLDocument2;
                var window = document.parentWindow;
                //var result = window.execScript(@"alert('Inside execution');");
                
                var form = new AccessibilityOptions();
                form.BrowserUrl = document.url;
                form.PageHTML = (browser.Document as IHTMLDocument3).documentElement.innerHTML;
                if (form.ShowDialog() != DialogResult.Cancel)
                {
                    //result = window.execScript(@"alert('Good !!!');");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return 0;
        }
        #endregion

        #region Registering with regasm
        public static string RegBHO = "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Browser Helper Objects";
        public static string RegCmd = "Software\\Microsoft\\Internet Explorer\\Extensions";

        [ComRegisterFunction]
        public static void RegisterBHO(Type type)
        {
            string guid = type.GUID.ToString("B");
            string iconFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Brillio", "TestHarnessPlugin");

            // BHO
            {
                RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(RegBHO, true);
                if (registryKey == null)
                    registryKey = Registry.LocalMachine.CreateSubKey(RegBHO);
                RegistryKey key = registryKey.OpenSubKey(guid);
                if (key == null)
                    key = registryKey.CreateSubKey(guid);
                key.SetValue("Alright", 1);
                registryKey.Close();
                key.Close();
            }

            // Command
            {
                RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(RegCmd, true);
                if (registryKey == null)
                    registryKey = Registry.LocalMachine.CreateSubKey(RegCmd);
                RegistryKey key = registryKey.OpenSubKey(guid);
                if (key == null)
                    key = registryKey.CreateSubKey(guid);
                key.SetValue("ButtonText", "Accessibility Testing");
                key.SetValue("CLSID", "{1FBA04EE-3024-11d2-8F1F-0000F87ABD16}");
                key.SetValue("ClsidExtension", guid);
                //key.SetValue("Icon", Path.Combine(iconFilePath, "ConfigParams.ico"));
                //key.SetValue("HotIcon", Path.Combine(iconFilePath, "ConfigParams.ico"));

                key.SetValue("Icon",@"E:\Microsoft_Accessibility\TestHarness_OriginalCopy\TestHarnessIEPlugin\ConfigParams.ico");
                key.SetValue("HotIcon", @"E:\Microsoft_Accessibility\TestHarness_OriginalCopy\TestHarnessIEPlugin\ConfigParams_0.ico");

                key.SetValue("Default Visible", "Yes");
                key.SetValue("MenuText", "&Accessibility Testing");
                key.SetValue("ToolTip", "Accessibility Testing");
                //key.SetValue("KeyPath", "no");
                registryKey.Close();
                key.Close();
            }
        }

        [ComUnregisterFunction]
        public static void UnregisterBHO(Type type)
        {
            string guid = type.GUID.ToString("B");
            // BHO
            {
                RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(RegBHO, true);
                if (registryKey != null)
                    registryKey.DeleteSubKey(guid, false);
            }
            // Command
            {
                RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(RegCmd, true);
                if (registryKey != null)
                    registryKey.DeleteSubKey(guid, false);
            }
        }
        #endregion
    }
}
