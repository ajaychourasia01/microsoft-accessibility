﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.Text;

namespace AccessibilityAnalyser.BrowserPlugIn
{
    public class Helper
    {
        public static T CreateClient<T>(string serviceUri, string httpBindingName)
        {
            T obj = default(T);
            BasicHttpBinding basicHttpbinding = new BasicHttpBinding(BasicHttpSecurityMode.None);
            basicHttpbinding.Name = httpBindingName;//"BasicHttpBinding_INewClientService";
            basicHttpbinding.MessageEncoding = WSMessageEncoding.Mtom;
            basicHttpbinding.CloseTimeout = TimeSpan.FromMinutes(30);
            basicHttpbinding.OpenTimeout = TimeSpan.FromMinutes(30);
            basicHttpbinding.ReceiveTimeout = TimeSpan.FromMinutes(30);
            basicHttpbinding.SendTimeout = TimeSpan.FromMinutes(30);

            EndpointAddress endpointAddress = new EndpointAddress(serviceUri); //("http://localhost:55437/Services/NewClientService.svc");
            obj = (T)Activator.CreateInstance(typeof(T), basicHttpbinding, endpointAddress);
            return obj;
            //var serviceClient = new NewClientServiceReference.NewClientServiceClient(basicHttpbinding, endpointAddress);
            //return serviceClient;
        }

        public static ToolBarConfigEntries LoadConfigurationDetails()
        {
            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Config.dat";
            ToolBarConfigEntries config = null;
            if (File.Exists(filePath))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream fs = new FileStream(filePath, FileMode.Open);
                config = (ToolBarConfigEntries)bf.Deserialize(fs);
                fs.Close();
            }
            return config;
        }

        public static void SaveConfigurationDetails(ToolBarConfigEntries newConfig)
        {
            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Config.dat";
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            FileStream fs = new FileStream(filePath, FileMode.Create);

            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, newConfig);
            }
            catch (SerializationException ex)
            {
                throw ex;
            }
            finally
            {
                fs.Close();
            }
        }
    }

    [Serializable]
    public class ToolBarConfigEntries
    {
        public int ClientId { get; set; }

        public string ClientName { get; set; }
        public int SiteId { get; set; }

        public string SiteName { get; set; }
        public int BuildId { get; set; }

        public string BuildName { get; set; }
        public int RunId { get; set; }
        public string RunName { get; set; }
        
    }
}
