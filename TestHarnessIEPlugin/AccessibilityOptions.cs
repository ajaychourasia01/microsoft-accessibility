﻿using Accessibility.ServicesLayer;
using AccessibilityAnalyser.RuleEngine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.Text;
using System.Windows.Forms;

namespace AccessibilityAnalyser.BrowserPlugIn
{
    public enum Action
    {
        Execution,
        Configuration
    }

    public partial class AccessibilityOptions : Form
    {
        private const string EXECUTIONSERVICEURL = "http://localhost:55437/Services/ExecuteTestValidation.svc";
        private const string EXECUTIONSERVICEBINDINGNAME = "BasicHttpBinding_IExecuteTestValidation";
        private const string NEWCLIENTSERVICEURL = "http://localhost:55437/Services/NewClientService.svc";
        private const string NEWCLIENTSERVICEBINDINGNAME = "BasicHttpBinding_INewClientService";
        private const int VIOLATIONSPERBATCH = 100000;

        private string filePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\Config.dat";
        private ToolBarConfigEntries config;
        BackgroundWorker worker;

        public AccessibilityOptions()
        {
            InitializeComponent();
        }

        public string BrowserUrl { get; set; }
        public string PageHTML { get; set; }

        private void LoadConfigurationDetails()
        {
            config = Helper.LoadConfigurationDetails();
            if (config != null)
            {
                lblClientName.Text = config.ClientName;
                lblSiteName.Text = config.SiteName;
                lblBuildName.Text = config.BuildName;
                lblRunName.Text = config.RunName;
            }
        }

        private void cmbClientName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbClientName.SelectedIndex > -1)
            {
                NewClientServiceReference.NewClientServiceClient serviceClient = Helper.CreateClient<NewClientServiceReference.NewClientServiceClient>("http://localhost:55437/Services/NewClientService.svc", "BasicHttpBinding_INewClientService");
                var sites = serviceClient.GetAllSiteList((int)cmbClientName.SelectedValue);
                serviceClient.Close();
                BindingSource bindingSource1 = new BindingSource();
                bindingSource1.DataSource = sites;
                cmbSiteName.DisplayMember = "SiteName";
                cmbSiteName.ValueMember = "SiteId";
                cmbSiteName.DataSource = bindingSource1.DataSource;
                cmbSiteName.SelectedIndex = -1;
            }
        }

        private void cmbSiteName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbSiteName.SelectedIndex > -1)
            {
                NewClientServiceReference.NewClientServiceClient serviceClient = Helper.CreateClient<NewClientServiceReference.NewClientServiceClient>("http://localhost:55437/Services/NewClientService.svc", "BasicHttpBinding_INewClientService");
                var builds = serviceClient.GetAllBuildList((int)cmbSiteName.SelectedValue);
                serviceClient.Close();
                BindingSource bindingSource1 = new BindingSource();
                bindingSource1.DataSource = builds;
                cmbBuild.DisplayMember = "BuildName";
                cmbBuild.ValueMember = "BuildId";
                cmbBuild.DataSource = bindingSource1.DataSource;
                cmbBuild.SelectedIndex = -1;
            }
        }

        private void cmbBuild_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbBuild.SelectedIndex > -1)
            {
                NewClientServiceReference.NewClientServiceClient serviceClient = Helper.CreateClient<NewClientServiceReference.NewClientServiceClient>("http://localhost:55437/Services/NewClientService.svc", "BasicHttpBinding_INewClientService");
                var runs = serviceClient.GetAllRunList((int)cmbBuild.SelectedValue);
                serviceClient.Close();
                BindingSource bindingSource1 = new BindingSource();
                bindingSource1.DataSource = runs;
                cmbRun.DisplayMember = "RunName";
                cmbRun.ValueMember = "RunId";
                cmbRun.DataSource = bindingSource1.DataSource;
                cmbRun.SelectedIndex = -1;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            cmbRun.SelectedIndex = -1;
            cmbBuild.SelectedIndex = -1;
            cmbSiteName.SelectedIndex = -1;
            cmbClientName.SelectedIndex = -1;
            grpEditConfig.Visible = false;
            grpReadConfig.Visible = true;
            LoadConfigurationDetails();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(cmbClientName.Text))
            {
                if (!string.IsNullOrEmpty(cmbSiteName.Text))
                {
                    if (!string.IsNullOrEmpty(cmbBuild.Text))
                    {
                        if (!string.IsNullOrEmpty(cmbRun.Text))
                        {
                            var newConfig = new ToolBarConfigEntries()
                            {
                                ClientId = (int)cmbClientName.SelectedValue,
                                ClientName = cmbClientName.Text,

                                SiteId = (int)cmbSiteName.SelectedValue,
                                SiteName = cmbSiteName.Text,

                                BuildId = (int)cmbBuild.SelectedValue,
                                BuildName = cmbBuild.Text,

                                RunId = (int)cmbRun.SelectedValue,
                                RunName = cmbRun.Text
                            };
                            Helper.SaveConfigurationDetails(newConfig);

                        }
                    }
                }
            }

            grpReadConfig.Visible = true;
            grpEditConfig.Visible = false;
            LoadConfigurationDetails();
        }

        private void btnChangeConfig_Click(object sender, EventArgs e)
        {
            grpReadConfig.Visible = false;
            grpEditConfig.Visible = true;

            //var clients = NewClient.GetClientList();
            NewClientServiceReference.NewClientServiceClient serviceClient = Helper.CreateClient<NewClientServiceReference.NewClientServiceClient>(NEWCLIENTSERVICEURL, NEWCLIENTSERVICEBINDINGNAME);

            var clients = serviceClient.GetAllClientList().ToList();
            serviceClient.Close();

            BindingSource bindingSource1 = new BindingSource();
            bindingSource1.DataSource = clients;
            cmbClientName.DisplayMember = "ClientName";
            cmbClientName.ValueMember = "ClientId";
            cmbClientName.DataSource = bindingSource1.DataSource;
            cmbClientName.SelectedIndex = -1;
        }

        private void AccessibilityOptions_Load(object sender, EventArgs e)
        {
            grpEditConfig.Visible = false;
            grpReadConfig.Visible = true;
            LoadConfigurationDetails();
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            lblExecutionStatus.Text = "Execution In progress...";
            Cursor.Current = Cursors.WaitCursor;
            btnExecute.Enabled = false;
            btnChangeConfig.Enabled = false;
            worker = new BackgroundWorker();

            worker.RunWorkerCompleted += ((s, dwea) =>
            {
                lblExecutionStatus.Text = dwea.Result.ToString();
                Cursor.Current = Cursors.Default;
                btnChangeConfig.Enabled = true;
                btnExecute.Enabled = true;
            });

            worker.DoWork += Worker_DoWork;
            worker.RunWorkerAsync();


        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            ExecutionTestReference.ExecuteTestValidationClient execTestClient = Helper.CreateClient<ExecutionTestReference.ExecuteTestValidationClient>(EXECUTIONSERVICEURL, EXECUTIONSERVICEBINDINGNAME);
            try
            {
                string urls = string.Concat(this.BrowserUrl, "~");
                StringBuilder sbHtml = new StringBuilder(this.PageHTML);
                var status = execTestClient.ExecuteTestValidationParam(urls, config.ClientId, config.BuildId, config.RunId, VIOLATIONSPERBATCH, sbHtml);
                if (status == 30)
                {
                    e.Result = "Execution completed. Execution results can be viewed in the web application.";
                }
            }
            catch (Exception ex)
            {
                e.Result = ex.Message;
            }
            finally
            {
                if (execTestClient != null)
                {
                    execTestClient.Close();
                }
            }
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            throw new NotImplementedException();
        }
    }

}
