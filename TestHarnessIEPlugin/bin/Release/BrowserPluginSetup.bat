@echo Off
setlocal ENABLEEXTENSIONS
set "AssemblyPath=%ProgramFiles%\Brillio\TestHarness"
set GacVersion=
set "v4Folder=NETFX 4.5.1 Tools"
set businessEntitiesDll= "AccessibilityAnalyser.BusinessEntities"
set ruleEngineDll="AccessibilityAnalyser.RuleEngine"
set commonDll="AccessibilityAnalyser.Common"
set dataAdapterDll="AccessibilityAnalyser.DataAdapter"
set interfacesDll="AccessibilityAnalyser.Interfaces"
set servicesDll="AccessibilityAnalyser.Services"
set entitiesDll="AccessibilityAnalyser.Entities"
set EPCommonDll="Microsoft.Practices.EnterpriseLibrary.Common"
set EPLoggingDll="Microsoft.Practices.EnterpriseLibrary.Logging"
set serviceLocationDll="Microsoft.Practices.ServiceLocation"
set RALoggingDll="RA.EnterpriseLibrary.Logging"
set browserPlugInDll="AccessibilityAnalyser.BrowserPlugIn"

set businessEntities="%AssemblyPath%\AccessibilityAnalyser.BusinessEntities"
set ruleEngine="%AssemblyPath%\AccessibilityAnalyser.RuleEngine"
set common="%AssemblyPath%\AccessibilityAnalyser.Common"
set dataAdapter="%AssemblyPath%\AccessibilityAnalyser.DataAdapter"
set interfaces="%AssemblyPath%\AccessibilityAnalyser.Interfaces"
set services="%AssemblyPath%\AccessibilityAnalyser.Services"
set entities="%AssemblyPath%\AccessibilityAnalyser.Entities"
set EPCommon="%AssemblyPath%\Microsoft.Practices.EnterpriseLibrary.Common"
set EPLogging="%AssemblyPath%\Microsoft.Practices.EnterpriseLibrary.Logging"
set serviceLocation="%AssemblyPath%\Microsoft.Practices.ServiceLocation"
set RALogging="%AssemblyPath%\RA.EnterpriseLibrary.Logging"
set browserPlugIn="%AssemblyPath%\AccessibilityAnalyser.BrowserPlugIn"

@for /F "tokens=1,2*" %%i in ('reg query "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full" /v "InstallPath"') DO (
    if "%%i"=="InstallPath" (
        SET "INSTALLUTILDIR=%%k"
    )
)
::@echo %INSTALLUTILDIR%
IF DEFINED INSTALLUTILDIR GOTO GACCheck 

@for /F "tokens=1,2*" %%i in ('reg query "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\MSBuild\ToolsVersions\2.0" /v "MSBuildToolsPath"') DO (
    if "%%i"=="MSBuildToolsPath" (
        SET "INSTALLUTILDIR=%%k"
    )
)
@echo %INSTALLUTILDIR%
IF DEFINED INSTALLUTILDIR GOTO GACCheck  
GOTO NotInstalled

:GACCheck
	@for /F "tokens=1,2*" %%i in ('reg query "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v8.1A\WinSDK-NetFx40Tools-x64" /v "InstallationFolder" 2^>nul') DO (

		if "%%i"=="InstallationFolder" (
			SET "GACDirectory=%%k"
			SET GacVersion="2.0"
		)
				
	)

IF DEFINED GACDirectory GOTO InstallGAC 

	@for /F "tokens=1,2*" %%i in ('reg query "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v8.1A\WinSDK-NetFx40Tools" /v "InstallationFolder" 2^>nul') DO (

		if "%%i"=="InstallationFolder" (
			SET "GACDirectory=%%k"
			SET GacVersion="2.0"
		)
				
	)
IF DEFINED GACDirectory GOTO InstallGAC 

	@for /F "tokens=1,2*" %%i in ('reg query "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v8.1A" /v "InstallationFolder" 2^>nul') DO (

		if "%%i"=="InstallationFolder" (
			SET "GACDirectory=%%k"
			SET GacVersion="4.0"
		)
				
	)
IF DEFINED GACDirectory GOTO InstallGAC 

:Checkv4
	@for /F "tokens=1,2*" %%i in ('reg query "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v8.0A\WinSDK-NetFx35Tools" /v "InstallationFolder" 2^>nul') DO (
		
		if "%%i"=="InstallationFolder" (
			SET "GACDirectory=%%k"
			SET GacVersion="2.0"
		)
	)
IF DEFINED GACDirectory GOTO InstallGAC

:Checkv3

	@for /F "tokens=1,2*" %%i in ('reg query "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v8.0A\WinSDK-NetFx35Tools-x64" /v "InstallationFolder" 2^>nul') DO (
		
		if "%%i"=="InstallationFolder" (
			SET "GACDirectory=%%k"
			SET GacVersion="2.0"
		)
	)

	IF DEFINED GACDirectory GOTO InstallGAC
	GOTO End

:InstallGAC
	
	IF %GacVersion% == "4.0" (
	
	IF /I "%1"=="Install" (echo Installing assemblies into the GAC
	xcopy "." "%AssemblyPath%" /D /E /C /R /H /I /K /Y
	GOTO v4InstallToGAC
	)
	IF /I "%1"=="-i" (echo Installing assemblies into the GAC
	xcopy "." "%AssemblyPath%" /D /E /C /R /H /I /K /Y
	GOTO v4InstallToGAC
	)
	IF /I "%1"=="UnInstall" (echo UnInstalling assemblies from the GAC
	GOTO v4UnInstallFromGAC
	)
	)
	IF /I "%1"=="-u" (echo UnInstalling assemblies from the GAC
	GOTO v4UnInstallFromGAC
	)
	)
	
	IF %GacVersion% == "2.0" (
	
	IF /I "%1"=="Install" (echo Installing Assemblies into the GAC
	xcopy "." "%AssemblyPath%" /D /E /C /R /H /I /K /Y
	GOTO v2InstallToGAC
	)
	IF /I "%1"=="-i" (echo Installing assemblies into the GAC
	xcopy "." "%AssemblyPath%" /D /E /C /R /H /I /K /Y
	GOTO v2InstallToGAC
	)
	IF /I "%1"=="UnInstall" (echo UnInstalling Assemblies from the GAC
	GOTO v2UnInstallFromGAC
	)
	IF /I "%1"=="-u" (echo UnInstalling assemblies from the GAC
	GOTO v2UnInstallFromGAC
	)
	)
		
	GOTO RegAsm
:v4InstallToGAC
	
	"%GACDirectory%bin\%v4Folder%"\gacutil /i %businessEntities%.dll
	"%GACDirectory%bin\%v4Folder%"\gacutil /i %ruleEngine%.dll
	"%GACDirectory%bin\%v4Folder%"\gacutil /i %common%.dll
	"%GACDirectory%bin\%v4Folder%"\gacutil /i %dataAdapter%.dll
	"%GACDirectory%bin\%v4Folder%"\gacutil /i %interfaces%.dll
	"%GACDirectory%bin\%v4Folder%"\gacutil /i %services%.dll
	"%GACDirectory%bin\%v4Folder%"\gacutil /i %entities%.dll
	"%GACDirectory%bin\%v4Folder%"\gacutil /i %EPCommon%.dll
	"%GACDirectory%bin\%v4Folder%"\gacutil /i %EPLogging%.dll
	"%GACDirectory%bin\%v4Folder%"\gacutil /i %serviceLocation%.dll
	"%GACDirectory%bin\%v4Folder%"\gacutil /i %RALogging%.dll
	"%GACDirectory%bin\%v4Folder%"\gacutil /i %browserPlugIn%.dll
	GOTO RegAsm
	
:v4UnInstallFromGAC

	"%GACDirectory%bin\%v4Folder%"\gacutil /u %businessEntitiesDll%
	"%GACDirectory%bin\%v4Folder%"\gacutil /u %ruleEngineDll%
	"%GACDirectory%bin\%v4Folder%"\gacutil /u %commonDll%
	"%GACDirectory%bin\%v4Folder%"\gacutil /u %dataAdapterDll%
	"%GACDirectory%bin\%v4Folder%"\gacutil /u %interfacesDll%
	"%GACDirectory%bin\%v4Folder%"\gacutil /u %servicesDll%
	"%GACDirectory%bin\%v4Folder%"\gacutil /u %entitiesDll%
	"%GACDirectory%bin\%v4Folder%"\gacutil /u %EPCommonDll%
	"%GACDirectory%bin\%v4Folder%"\gacutil /u %EPLoggingDll%
	"%GACDirectory%bin\%v4Folder%"\gacutil /u %serviceLocationDll%
	"%GACDirectory%bin\%v4Folder%"\gacutil /u %RALoggingDll%
	"%GACDirectory%bin\%v4Folder%"\gacutil /u %browserPlugInDll%
	GOTO RegAsm
	
:v2InstallToGAC
	"%GACDirectory%"\gacutil /i %businessEntities%.dll
	"%GACDirectory%"\gacutil /i %ruleEngine%.dll
	"%GACDirectory%"\gacutil /i %common%.dll
	"%GACDirectory%"\gacutil /i %dataAdapter%.dll
	"%GACDirectory%"\gacutil /i %interfaces%.dll
	"%GACDirectory%"\gacutil /i %services%.dll
	"%GACDirectory%"\gacutil /i %entities%.dll
	"%GACDirectory%"\gacutil /i %EPCommon%.dll
	"%GACDirectory%"\gacutil /i %EPLogging%.dll
	"%GACDirectory%"\gacutil /i %serviceLocation%.dll
	"%GACDirectory%"\gacutil /i %RALogging%.dll
	"%GACDirectory%"\gacutil /i %browserPlugIn%.dll
	GOTO RegAsm
	
:v2UnInstallFromGAC
	"%GACDirectory%"\gacutil /u %businessEntitiesDll%
	"%GACDirectory%"\gacutil /u %ruleEngineDll%
	"%GACDirectory%"\gacutil /u %commonDll%
	"%GACDirectory%"\gacutil /u %dataAdapterDll%
	"%GACDirectory%"\gacutil /u %interfacesDll%
	"%GACDirectory%"\gacutil /u %servicesDll%
	"%GACDirectory%"\gacutil /u %entitiesDll%
	"%GACDirectory%"\gacutil /u %EPCommonDll%
	"%GACDirectory%"\gacutil /u %EPLoggingDll%
	"%GACDirectory%"\gacutil /u %serviceLocationDll%
	"%GACDirectory%"\gacutil /u %RALoggingDll%
	"%GACDirectory%"\gacutil /u %browserPlugInDll%
	GOTO RegAsm
	
:RegAsm

	IF DEFINED INSTALLUTILDIR GOTO InstallToRegAsm
	GOTO End

:InstallToRegAsm
	@echo RegAsmDirectory : %INSTALLUTILDIR%
	IF /I "%1"=="Install" (echo Registering Types
	%INSTALLUTILDIR%regasm %browserPlugIn%.dll
	)
	IF /I "%1"=="-i" (echo Registering Types
	%INSTALLUTILDIR%regasm %browserPlugIn%.dll
	)
	IF /I "%1"=="UnInstall" (echo UnRegistering Types
	%INSTALLUTILDIR%regasm %browserPlugIn%.dll /u
	@RD /S /Q "%AssemblyPath%"
	)
	IF /I "%1"=="-u" (echo UnRegistering Types
	%INSTALLUTILDIR%regasm %browserPlugIn%.dll /u
	@RD /S /Q "%AssemblyPath%"
	)
	GOTO End


:NotInstalled
	echo .Net has not been installed in the system
	GOTO End
:End
	exit /b 0

endlocal 
