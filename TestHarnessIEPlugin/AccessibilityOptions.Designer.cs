﻿namespace AccessibilityAnalyser.BrowserPlugIn
{
    partial class AccessibilityOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpEditConfig = new System.Windows.Forms.GroupBox();
            this.cmbRun = new System.Windows.Forms.ComboBox();
            this.cmbBuild = new System.Windows.Forms.ComboBox();
            this.cmbSiteName = new System.Windows.Forms.ComboBox();
            this.cmbClientName = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnChangeConfig = new System.Windows.Forms.Button();
            this.btnExecute = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblClientName = new System.Windows.Forms.Label();
            this.grpReadConfig = new System.Windows.Forms.GroupBox();
            this.lblChangeText = new System.Windows.Forms.Label();
            this.lblRunName = new System.Windows.Forms.Label();
            this.lblBuildName = new System.Windows.Forms.Label();
            this.lblSiteName = new System.Windows.Forms.Label();
            this.lblExecutionStatus = new System.Windows.Forms.Label();
            this.grpEditConfig.SuspendLayout();
            this.grpReadConfig.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpEditConfig
            // 
            this.grpEditConfig.Controls.Add(this.cmbRun);
            this.grpEditConfig.Controls.Add(this.cmbBuild);
            this.grpEditConfig.Controls.Add(this.cmbSiteName);
            this.grpEditConfig.Controls.Add(this.cmbClientName);
            this.grpEditConfig.Controls.Add(this.label5);
            this.grpEditConfig.Controls.Add(this.label4);
            this.grpEditConfig.Controls.Add(this.label3);
            this.grpEditConfig.Controls.Add(this.btnCancel);
            this.grpEditConfig.Controls.Add(this.btnOk);
            this.grpEditConfig.Controls.Add(this.label1);
            this.grpEditConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpEditConfig.Location = new System.Drawing.Point(11, 37);
            this.grpEditConfig.Name = "grpEditConfig";
            this.grpEditConfig.Size = new System.Drawing.Size(287, 181);
            this.grpEditConfig.TabIndex = 2;
            this.grpEditConfig.TabStop = false;
            this.grpEditConfig.Text = "Select new configuration";
            // 
            // cmbRun
            // 
            this.cmbRun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbRun.FormattingEnabled = true;
            this.cmbRun.Location = new System.Drawing.Point(80, 112);
            this.cmbRun.Name = "cmbRun";
            this.cmbRun.Size = new System.Drawing.Size(191, 21);
            this.cmbRun.TabIndex = 29;
            // 
            // cmbBuild
            // 
            this.cmbBuild.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbBuild.FormattingEnabled = true;
            this.cmbBuild.Location = new System.Drawing.Point(80, 84);
            this.cmbBuild.Name = "cmbBuild";
            this.cmbBuild.Size = new System.Drawing.Size(191, 21);
            this.cmbBuild.TabIndex = 28;
            this.cmbBuild.SelectedIndexChanged += new System.EventHandler(this.cmbBuild_SelectedIndexChanged);
            // 
            // cmbSiteName
            // 
            this.cmbSiteName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbSiteName.FormattingEnabled = true;
            this.cmbSiteName.Location = new System.Drawing.Point(80, 56);
            this.cmbSiteName.Name = "cmbSiteName";
            this.cmbSiteName.Size = new System.Drawing.Size(191, 21);
            this.cmbSiteName.TabIndex = 27;
            this.cmbSiteName.SelectedIndexChanged += new System.EventHandler(this.cmbSiteName_SelectedIndexChanged);
            // 
            // cmbClientName
            // 
            this.cmbClientName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbClientName.FormattingEnabled = true;
            this.cmbClientName.Location = new System.Drawing.Point(80, 28);
            this.cmbClientName.Name = "cmbClientName";
            this.cmbClientName.Size = new System.Drawing.Size(191, 21);
            this.cmbClientName.TabIndex = 26;
            this.cmbClientName.SelectedIndexChanged += new System.EventHandler(this.cmbClientName_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(46, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Run";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(43, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Build";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(48, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Site";
            // 
            // btnCancel
            // 
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Location = new System.Drawing.Point(208, 139);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(63, 23);
            this.btnCancel.TabIndex = 20;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Location = new System.Drawing.Point(139, 139);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(63, 23);
            this.btnOk.TabIndex = 18;
            this.btnOk.Text = "Save";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "ClientName";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "ClientName";
            // 
            // btnChangeConfig
            // 
            this.btnChangeConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChangeConfig.Location = new System.Drawing.Point(18, 188);
            this.btnChangeConfig.Name = "btnChangeConfig";
            this.btnChangeConfig.Size = new System.Drawing.Size(124, 23);
            this.btnChangeConfig.TabIndex = 18;
            this.btnChangeConfig.Text = "Change Configuration";
            this.btnChangeConfig.UseVisualStyleBackColor = true;
            this.btnChangeConfig.Click += new System.EventHandler(this.btnChangeConfig_Click);
            // 
            // btnExecute
            // 
            this.btnExecute.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExecute.Location = new System.Drawing.Point(155, 188);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(63, 23);
            this.btnExecute.TabIndex = 20;
            this.btnExecute.Text = "Execute";
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Visible = false;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(48, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Site";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(43, 95);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Build";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(46, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Run";
            // 
            // lblClientName
            // 
            this.lblClientName.ForeColor = System.Drawing.Color.Blue;
            this.lblClientName.Location = new System.Drawing.Point(98, 34);
            this.lblClientName.Name = "lblClientName";
            this.lblClientName.Size = new System.Drawing.Size(134, 14);
            this.lblClientName.TabIndex = 26;
            this.lblClientName.Text = "label9";
            // 
            // grpReadConfig
            // 
            this.grpReadConfig.Controls.Add(this.lblChangeText);
            this.grpReadConfig.Controls.Add(this.lblRunName);
            this.grpReadConfig.Controls.Add(this.lblBuildName);
            this.grpReadConfig.Controls.Add(this.lblSiteName);
            this.grpReadConfig.Controls.Add(this.lblClientName);
            this.grpReadConfig.Controls.Add(this.label2);
            this.grpReadConfig.Controls.Add(this.label6);
            this.grpReadConfig.Controls.Add(this.label7);
            this.grpReadConfig.Controls.Add(this.btnExecute);
            this.grpReadConfig.Controls.Add(this.btnChangeConfig);
            this.grpReadConfig.Controls.Add(this.label8);
            this.grpReadConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpReadConfig.Location = new System.Drawing.Point(12, 32);
            this.grpReadConfig.Name = "grpReadConfig";
            this.grpReadConfig.Size = new System.Drawing.Size(286, 221);
            this.grpReadConfig.TabIndex = 3;
            this.grpReadConfig.TabStop = false;
            this.grpReadConfig.Text = "Existing configuration";
            // 
            // lblChangeText
            // 
            this.lblChangeText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChangeText.Location = new System.Drawing.Point(12, 146);
            this.lblChangeText.Name = "lblChangeText";
            this.lblChangeText.Size = new System.Drawing.Size(249, 27);
            this.lblChangeText.TabIndex = 30;
            this.lblChangeText.Text = "To change the configuration details click the Change Configuration Button";
            // 
            // lblRunName
            // 
            this.lblRunName.ForeColor = System.Drawing.Color.Blue;
            this.lblRunName.Location = new System.Drawing.Point(98, 119);
            this.lblRunName.Name = "lblRunName";
            this.lblRunName.Size = new System.Drawing.Size(134, 14);
            this.lblRunName.TabIndex = 29;
            this.lblRunName.Text = "label9";
            // 
            // lblBuildName
            // 
            this.lblBuildName.ForeColor = System.Drawing.Color.Blue;
            this.lblBuildName.Location = new System.Drawing.Point(98, 93);
            this.lblBuildName.Name = "lblBuildName";
            this.lblBuildName.Size = new System.Drawing.Size(134, 14);
            this.lblBuildName.TabIndex = 28;
            this.lblBuildName.Text = "label9";
            // 
            // lblSiteName
            // 
            this.lblSiteName.ForeColor = System.Drawing.Color.Blue;
            this.lblSiteName.Location = new System.Drawing.Point(98, 60);
            this.lblSiteName.Name = "lblSiteName";
            this.lblSiteName.Size = new System.Drawing.Size(182, 33);
            this.lblSiteName.TabIndex = 27;
            this.lblSiteName.Text = "label9";
            // 
            // lblExecutionStatus
            // 
            this.lblExecutionStatus.ForeColor = System.Drawing.Color.Green;
            this.lblExecutionStatus.Location = new System.Drawing.Point(11, 260);
            this.lblExecutionStatus.Name = "lblExecutionStatus";
            this.lblExecutionStatus.Size = new System.Drawing.Size(287, 43);
            this.lblExecutionStatus.TabIndex = 4;
            // 
            // AccessibilityOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 312);
            this.Controls.Add(this.grpReadConfig);
            this.Controls.Add(this.lblExecutionStatus);
            this.Controls.Add(this.grpEditConfig);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AccessibilityOptions";
            this.Text = "AccessibilityOptions";
            this.Load += new System.EventHandler(this.AccessibilityOptions_Load);
            this.grpEditConfig.ResumeLayout(false);
            this.grpEditConfig.PerformLayout();
            this.grpReadConfig.ResumeLayout(false);
            this.grpReadConfig.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpEditConfig;
        private System.Windows.Forms.ComboBox cmbRun;
        private System.Windows.Forms.ComboBox cmbBuild;
        private System.Windows.Forms.ComboBox cmbSiteName;
        private System.Windows.Forms.ComboBox cmbClientName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpReadConfig;
        private System.Windows.Forms.Label lblClientName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.Button btnChangeConfig;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblBuildName;
        private System.Windows.Forms.Label lblSiteName;
        private System.Windows.Forms.Label lblRunName;
        private System.Windows.Forms.Label lblChangeText;
        private System.Windows.Forms.Label lblExecutionStatus;
    }
}