﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.RuleEngine;


namespace AccessibilityAnalyser.Web.Services.ServiceContracts
{
    /// <summary>
    /// IReportServices - Service contract
    /// </summary>
    public interface IReportServices
    {
        
        IEnumerable<BuildBO> GetNoOfBuild(int clientId);

        
        List<ReportingDetails> GetTestCaseRatioDetails(int clientId, List<int> buildIDs);
        
        
        List<ReportingDetails> GetLatestBuilds(int clientId);

        
        int GetClientId(string clientName);

        
        string GetClientName(int clientId);

        
        ReportingDetails GetTotalTestCaseRatio(int clientId);

        
        void GetData(int clientId);
        
    }
}
