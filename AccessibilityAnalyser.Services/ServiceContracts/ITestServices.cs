﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Interfaces;
using AccessibilityAnalyser.RuleEngine;

namespace AccessibilityAnalyser.Web.Services.ServiceContracts
{
    /// <summary>
    /// ITestServices - Service contract
    /// </summary>
    public interface ITestServices
    {
        //
        //
        //int TestValidation(List<string> urlNames, int clientId, int buildId, int runId, int violationPerBatch);

        List<TestCase> GetTestCases(string clientName, string siteName, string multipleChoice, int runId, int buildId);

        List<ClientBO> GetClients(string searchText);

        List<ClientBO> GetClientSingle(string ClientName);

        Violations GetViolations(int clientId, int buildId, int runId, int ruleDetailId, string violationType, int skip, int testCaseId);

        int GetViolationCount(int ClientID);

        
        
        bool UpdateViolation(string violationDescription, string violationRecommendation, int violationId, string status);

        
        
        bool UpdateViolationForManual(string violationDescription, string violationRecommendation, string violationName, string sourceURL, string sourceElement, int violationId, string status);

        
        
        ViolationBO GetViolation(List<ViolationBO> violations, int violationId);

        
        
        List<SiteBO> GetSiteList(string searchText, string clientName);

        
        
        List<Build> GetBuilds(string clientName, string siteName);

        
        
        bool CreateViolation(string violationName, string violationDescription, int runId, string siteName, string violationSourceElement, int violationSourceLineNumber,
                             string violationRecommendation, string status, int clientId, int buildId,string TestCaseName,int violationlogID);

        
        
        char GetViolationLogStatus();
    }
}
