﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
//using HtmlAgilityPack;
using System.Text;


namespace Accessibility.ServicesLayer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IExecuteTestValidation" in both code and config file together.
    public interface IExecuteTestValidation
    {
        int ExecuteTestValidationParam(string selectedURLArray, int clientId, int buildId, int runId, int violationPerBatch, StringBuilder html);

        int GetHtmlExtracted(string selectedURLArray, int clientId, int buildId, int runId);
    }
}
