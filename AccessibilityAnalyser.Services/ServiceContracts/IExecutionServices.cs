﻿// -----------------------------------------------------------------------
// <copyright file="IExecutionServices.cs" company="Microsoft">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Interfaces;
namespace AccessibilityAnalyser.Web.Services.ServiceContracts
{
    /// <summary>
    /// ExecutionServices  page
    /// </summary>
    public interface IExecutionServices
    {
        List<ClientBO> GetClientList(string searchText);

        List<SiteBO> GetSiteList(string searchText, int clientID);

        List<BuildBO> GetBuildList(string searchText, int siteID);

        List<RunBO> GetRunList(string searchText, int buildID);

        int GetClientID(string clientName);

        
        int GetSiteID(string url, int clientID);

        
        int GetBuildID(string buildName, int siteID);

        
        int GetRunID(string runName, int buildID, int clientID);

        
        bool CreateClient(string clientName);

        
        bool CreateSite(string siteName, int clientId);

        
        bool CreateBuild(string buildName, int siteId);

        
        bool CreateRun(int clientId, int buildId, string runName);

        
        bool CreateTestCase(string testCaseName, int buildId);

        
        bool CreateTestCaseMap(int ruleDetailId, int testCaseId);

        
        List<TestCaseBO> GetAllDistinctTestCases();

        
        List<RuleDetailBO> GetAllDistinctRuleDetails();

        
        List<TestCaseBO> GetAllTestCases(int buildId);

        
        bool CreateServiceLog(int clientId, int runId, int buildId, string status, ref DateTime date, string uploadedFileNames);
    }
}
