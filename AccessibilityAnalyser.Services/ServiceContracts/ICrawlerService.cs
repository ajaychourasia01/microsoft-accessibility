﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Interfaces;

namespace AccessibilityAnalyser.Web.Services.ServiceContracts
{
    public interface ICrawlerService
    {
        List<string> GetUrlList(string SiteUrl, int depthLevel,string authority, string crawlType);
    }
}
