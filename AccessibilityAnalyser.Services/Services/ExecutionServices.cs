﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
//using AccessibilityServices;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.RuleEngine;
using AccessibilityAnalyser.Web.Services.ServiceContracts;
using AccessibilityAnalyser.Common;

using System.ServiceProcess;
using Excel = Microsoft.Office.Interop.Excel;

namespace Accessibility.ServicesLayer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ExecutionServices" in code, svc and config file together.
    public class ExecutionServices : IExecutionServices
    {
        ServiceController serviceObj = new ServiceController("CrawlExecutionSequencer");

        public List<ClientBO> GetClientList(string searchText)
        {
            List<ClientBO> getClientList = new List<ClientBO>();
            try
            {
                getClientList = Executions.GetClientList(searchText);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "GetClientList");
            }
            return getClientList;
        }


        public List<SiteBO> GetSiteList(string searchText, int clientID)
        {
            List<SiteBO> getSiteList = new List<SiteBO>();
            try
            {
                getSiteList = Executions.GetSiteList(searchText, clientID);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "GetSiteList");
            }
            return getSiteList;
        }


        public List<BuildBO> GetBuildList(string searchText, int siteID)
        {
            List<BuildBO> getBuildList = new List<BuildBO>();

            try
            {
                getBuildList = Executions.GetBuildList(searchText, siteID);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "GetBuildList");
            }
            return getBuildList;
        }


        public List<RunBO> GetRunList(string searchText, int buildID)
        {
            List<RunBO> getRunList = new List<RunBO>();

            try
            {
                getRunList = Executions.GetRunList(searchText, buildID);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "GetRunList");
            }
            return getRunList;
        }

        public int GetClientID(string clientName)
        {
            int clientID = 0;
            try
            {
                clientID = Executions.GetClientID(clientName);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "GetClientID");
            }
            return clientID;
        }


        public int GetSiteID(string url, int clientID)
        {
            int siteID = 0;

            try
            {
                siteID = Executions.GetSiteID(url, clientID);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "GetSiteID");
            }
            return siteID;
        }


        public int GetBuildID(string buildName, int siteID)
        {
            int buildID = 0;

            try
            {
                buildID = Executions.GetBuildID(buildName, siteID);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "GetBuildID");
            }
            return buildID;
        }


        public int GetRunID(string runName, int buildID, int clientID)
        {
            int runID = 0;

            try
            {
                runID = Executions.GetRunID(runName, buildID, clientID);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "GetRunID");
            }
            return runID;
        }

        public bool CreateClient(string clientName)
        {
            bool isCreated = false;

            try
            {
                isCreated = Executions.CreateClient(clientName);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "CreateClient");
            }
            return isCreated;
        }

        public bool CreateSite(string siteName, int clientId)
        {
            bool isCreated = false;

            try
            {
                isCreated = Executions.CreateSite(siteName, clientId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "CreateSite");
            }

            return isCreated;
        }

        public bool CreateBuild(string buildName, int siteId)
        {
            bool isCreated = false;

            try
            {
                isCreated = Executions.CreateBuild(buildName, siteId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "CreateBuild");
            }

            return isCreated;
        }

        public bool CreateRun(int clientId, int buildId, string runName)
        {
            bool isCreated = false;

            try
            {
                isCreated = Executions.CreateRun(clientId, buildId, runName);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "CreateRun");
            }
            return isCreated;
        }

        public bool CreateTestCase(string testCaseName, int buildId)
        {
            bool isCreated = false;

            try
            {
                isCreated = Executions.CreateTestCase(testCaseName, buildId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "CreateTestCase");
            }
            return isCreated;
        }

        public bool CreateTestCaseMap(int ruleDetailId, int testCaseId)
        {
            bool isCreated = false;

            try
            {
                isCreated = Executions.CreateTestCaseMap(ruleDetailId, testCaseId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "CreateTestCaseMap");
            }
            return isCreated;
        }

        /// <summary>
        /// Create the a new service log entry 
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="runId"></param>
        /// <param name="buildId"></param>
        /// <param name="status"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public bool CreateServiceLog(int clientId, int runId, int buildId, string status, ref DateTime date, string uploadedFileNames)
        {
            bool isCreated = false;

            try
            {
                isCreated = Executions.CreateServiceLog(clientId, runId, buildId, status, ref date, uploadedFileNames);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "CreateServiceLog");
            }
            return isCreated;
        }

        /// <summary>
        /// Update the status of the Service log
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="runId"></param>
        /// <param name="buildId"></param>
        /// <param name="status"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public bool UpdateServiceLog(int clientId, int runId, int buildId, string status, DateTime date)
        {
            bool isUpdated = false;

            try
            {
                isUpdated = Executions.UpdateServiceLog(clientId, runId, buildId, status, date);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "UpdateServiceLog");
            }
            return isUpdated;
        }

        public List<TestCaseBO> GetAllDistinctTestCases()
        {
            List<TestCaseBO> getAllDistinctTestCases = new List<TestCaseBO>();

            try
            {
                getAllDistinctTestCases = Executions.GetAllDistinctTestCases();
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "GetAllDistinctTestCases");
            }

            return getAllDistinctTestCases;
        }

        public List<RuleDetailBO> GetAllDistinctRuleDetails()
        {
            List<RuleDetailBO> getAllDistinctRuleDetails = new List<RuleDetailBO>();

            try
            {
                getAllDistinctRuleDetails = Executions.GetAllDistinctRuleDetails();
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "GetAllDistinctRuleDetails");
            }
            return getAllDistinctRuleDetails;
        }

        public List<TestCaseBO> GetAllTestCases(int buildId)
        {
            List<TestCaseBO> getAllTestCases = new List<TestCaseBO>();

            try
            {
                getAllTestCases = Executions.GetAllTestCases(buildId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "GetAllTestCases");
            }
            return getAllTestCases;
        }

        /// <summary>
        /// Getting a list of all waiting users
        /// </summary>
        /// <returns></returns>
        public List<ServiceLogBO> GetWaitingUsers()
        {
            List<ServiceLogBO> getWaitingUsers = new List<ServiceLogBO>();
            try
            {
                getWaitingUsers = Executions.GetWaitingUsers();
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ExecutionService", "GetWaitingUsers");
            }
            return getWaitingUsers;
        }
    }
}
