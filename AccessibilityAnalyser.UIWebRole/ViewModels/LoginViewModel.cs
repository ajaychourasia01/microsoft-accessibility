﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AccessibilityAnalyser.Web.UI.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Please Enter UserName")]
        public string AccountUserName { get; set; }

        [Required(ErrorMessage = "Please Enter password")]
        public string AccountUserPassword { get; set; }
    }
}