﻿


    var CommFunc = new Utils();
    var count = 0;    
    $(document).ready(function () {         

         $('#TestingPageLink').click(function () {
            window.location.href = "/Testing/Index?clientName=" + $('input[name=hdnClientName]').val();
         });

         $('#ExecutionPageLink').click(function (event) {
            window.location.href = $(this).attr('href')+"?clientName=" + $('input[name=hdnClientName]').val();
            return false;
         });

        //show or hide the left bar(New Client)
        $(".newclient_trigger").click(function () {
            $(".divpanel").toggle("slow");
            $(this).toggleClass("active");
             
             if (count == 0) {
                    $("#divLogo").removeClass('logo');
                    $("#divLogo").addClass('logo_Expand');
                    $(".newclient_col_two").addClass("newclient_col_two_expand");
                    //$(".client_editrules_panel").addClass("client_editrules_panel_expand");
                    count++;
             }
             else if (count != 0) {
                    $("#divLogo").removeClass('logo_Expand');
                    $("#divLogo").addClass('logo');
                    $(".newclient_col_two").removeClass("newclient_col_two_expand");
                    //$(".client_editrules_panel").removeClass("client_editrules_panel_expand");
                    count = 0;
                }
            return false;
        });

        //autocomplete for Client's Name
        $("#txtDetailclientName").autocomplete({ source: "/Testing/QuickSearchClient" });
        $("#txtclientName").autocomplete({ source: "/Testing/QuickSearchClient" });

        //autocomplete for Url which will showed based on the text present in Client's Name
        $("#txtDetailurlName").autocomplete({
			source : function (request, response) {
				$.ajax({
					url : "/Testing/QuickSearchUrlName",
					type : "POST",
					dataType : "json",
					data : {
						searchText : request.term,
						clientName : $("#txtDetailclientName").val()
					},
					success : function (data) {
						var result = data;
						if (result !== null) {
							var a = [];
							for (var i = 0; i < result.length; i++) {
								a.push({
									label : result[i].label
								});
							}
							response(a);
						} else {
							response(null);
						}
					}
				})
			}
		});

        //autocomplete for Url which will showed based on the text present in Client's Name
        $("#txturlName").autocomplete({
			source : function (request, response) {
				$.ajax({
					url : "/Testing/QuickSearchUrlName",
					type : "POST",
					dataType : "json",
					data : {
						searchText : request.term,
						clientName : $("#txtclientName").val()
					},
					success : function (data) {
						var result = data;
						if (result !== null) {
							var a = [];
							for (var i = 0; i < result.length; i++) {
								a.push({
									label : result[i].label
								});
							}
							response(a);
						} else {
							response(null);
						}
					}
				})
			}
		});
       
        //autocomplete for BuildName which will showed based on the text present in Client's Name and Url
        $("#txtBuildName").autocomplete({
			source : function (request, response) {
				$.ajax({
					url : "/Execution/QuickSearchBuildName",
					type : "POST",
					dataType : "json",
					data : {
						searchText : request.term,
						urlName : $("#txturlName").val(),
						clientName : $("#txtclientName").val()
					},
					success : function (data) {
                        SelBuildName = false;
						var result = data;
						if (result !== null) {
							var a = [];
							for (var i = 0; i < result.length; i++) {
								a.push({
									label : result[i].label
								});
							}
							response(a);
						} else {
							response(null);
						}
					},
				})
			}
		});

        //autocomplete for RunName which will showed based on the text present in Client's Name, Url and BuildName
        $("#txtRunName").autocomplete({
			source : function (request, response) {
				$.ajax({
					url : "/Execution/QuickSearchRunName",
					type : "POST",
					dataType : "json",
					data : {
						searchText : request.term,
						buildName : $("#txtBuildName").val(),
						urlName: $("#txturlName").val(),
                        clientName : $("#txtclientName").val()
					},
					success : function (data) {
						var result = data;
                        SelRunName = false;
						if (result !== null) {
							var a = [];
							for (var i = 0; i < result.length; i++) {
								a.push({
									label : result[i].label
								});
							}
							response(a);
						} else {
							response(null);
						}
					}
				})
			}
		});

        //if add new Client is clicked then show add new client screen
        $("#AddClient").click(function () {
            if($("#AddClientDialog").is(":visible")){
                $("#clientactionscreen").slideDown();
                $(this).parent().children()[0].innerText = 'Client';
                $(this).removeClass("edit_client_Btn").addClass("add_client_Btn");
                $(this).parent().parent().removeClass("client_div_height"); 
                $(this).parent().parent().children("div:nth-child(2)").removeClass("HideUI");
                $(this).parent().parent().children("div:nth-child(3)").addClass("HideUI");
            } else{
                $("#clientactionscreen").slideUp(); 
                $(this).parent().children()[0].innerText = 'New Client';
                $(this).removeClass("add_client_Btn").addClass("edit_client_Btn");
                $(this).parent().parent().addClass("client_div_height"); 
                $(this).parent().parent().children("div:nth-child(2)").addClass("HideUI"); 
                ClearControls("Client");       
                $(this).parent().parent().children("div:nth-child(3)").removeClass("HideUI");
            }
        });

        //if Add button is clicked then Save the values into database
        $("#AddBtn").click(function () {
            if(ValidateControls("AddClient") == "validated"){ 
                var url = $("#txturlName").val();
                url = Fix_url(url);                    
                var strClientName = $("#txtclientName").val();
                var strBuildName = $("#txtBuildName").val();
                var strRunName = $("#txtRunName").val();
                   
                $("#Loading").dialog("open");
                $.ajax({
                    url: "/NewClient/_CreateClient", 
                    type: "POST",
                    data: { 
                            ClientName: strClientName, 
                            UrlName: url,
                            BuildName: strBuildName, 
                            RunName: strRunName
                        },
                    success: function (data) {                           
                            $('input[name=hdnClientName]').val(strClientName);
                            $("#Loading").dialog("close");
                        if(data.Duplicate.toLowerCase() == "true"){
                            CommFunc.OpenOKDialog("divdialog","Details entered for the selected client already exists.",300,150);
                        }else{
                            $("#clientactionscreen").slideDown();
                            $("#ClientDetails").removeClass("HideUI");     //show the client screen
                            $("#AddClient").removeClass("edit_client_Btn").addClass("add_client_Btn");         //show the add client button
                            $("#AddClientDialog").addClass("HideUI");    //hide the add client dialog
                            $("#txtDetailclientName").val(strClientName);           //set the value of client name in client name textbox
                            $("#txtDetailurlName").val(url);                 //set the value of url name in client name textbox                                
                            $("#ClientDiv").removeClass("client_div_height"); 
                            $("#Show_Result_Client_Add").attr("style", "display:block");    //display the result of the add client action
                            $("#Show_Result_Client_Add").delay(3000).fadeOut();             //the result will fade out after 3 sec              
                            ShowActionButtons();
                        }
                    }
                });
             }
        });

        //if Clear button is clicked then clear all the textboxes value
        $("#ClearBtn").click(function () {
            ClearControls("Client");
        });


        //water mark for all the textboxes in add new client and client detail screen
        $("#txtDetailclientName").watermark(" Client's Name");
        $("#txtDetailurlName").watermark(" Site Url");
        $("#tctxtTCName").watermark("Test Case Name");

        //returns jus the url deleting http or https
        function Fix_url(url) {
            if (url.substr(0, 7).toLowerCase() == 'http://') { return url; }
            if (url.substr(0, 8).toLowerCase() == 'https://') { return url; }
            return 'http://' + url;
        } 

        //validate the entered url
        function ValidateWebAddress(url) {
            //var webSiteUrlExp = /^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/;

            var webSiteUrlExp = /((http|https):\/\/(\w+:{0,1}\w*@)?(\S+)|)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
            if (webSiteUrlExp.test(url)) {
                return true;
            }
            else {
                return false;
            }
        }

        //check if anycontrol has blank value jus before adding data to database
        function ValidateControls(ActionScren) {
	        var CommFunc = new Utils();
	        if ($("#txtclientName").val() == '') {
		        CommFunc.OpenOKDialog("divdialog", "Please enter Client's Name", 250, 150);
	        } else {
		        if ($("#txturlName").val() == '') {
			        CommFunc.OpenOKDialog("divdialog", "Please enter Site Url", 250, 150);
		        } else if (!ValidateWebAddress($("#txturlName").val())) {
			        CommFunc.OpenOKDialog("divdialog", "Please enter the valid URL", 150, 150);
		        } else {
			        if ($("#txtBuildName").val() == '') {
				        CommFunc.OpenOKDialog("divdialog", "Please enter Build name", 250, 150);
			        } else {
				        if ($("#txtRunName").val() == '') {
					        CommFunc.OpenOKDialog("divdialog", "Please enter Run name", 250, 150);
				        } else {
					        return "validated";
				        }
			        }
		        }
	        }
        }
    });

    //clear the controls of a screen
    function ClearControls(ActionScreen)
    {
        if(ActionScreen == "Client"){
            $("#txtclientName").val('').watermark("Client's Name");
            $("#txtBuildName").val('').watermark("Build name");
            $("#txturlName").val('').watermark("Site Url");
            $("#txtRunName").val('').watermark("Run name");            
        }
    };
    
    //if search button is clicked then check if client name and url is filled in textbox
    function GetBuilds()
    {
        
            //check if Client name is blank
            if ($("#txtDetailclientName").val() == '') {
                CommFunc.OpenOKDialog("divdialog","Please enter Client's Name",250,150);
                return false;
            }
            
            //check if url name is blank
            if ($("#txtDetailurlName").val() == '') {
                CommFunc.OpenOKDialog("divdialog","Please enter Site Url",250,150);
                return false;
            }

            //load the build panel with data
            $("#build_panel").load("/NewClient/_BuildPanel",
            { clientName: $("#txtDetailclientName").val(), siteName: $("#txtDetailurlName").val() },
            function () { 
                TestcaseforFirstBuild();
            });
            
            //show loading screen and then render data
            //RenderURL($("#txtDetailurlName").val());
    }

    //function which renders build detail data 
    function RenderURL(renderUrl) {
        $("#Loading").dialog("open");          
        $.ajax({
            url: '../../Helper/PageHandler.ashx?url=' + renderUrl,
            success: function (data) {
                html_string = data;
                SetHTMlText();
            }
        });
    }

    function SetHTMlText() {
        // loading graphic               
            $("#Loading").dialog("close").dialog('destroy').remove();

        SetBaseHost();
        var iframe = document.getElementById('url_iframe');
        var iframedoc = iframe.contentWindow.document;
        if (iframedoc != null && iframedoc != undefined) {
            iframedoc.open();
            iframedoc.writeln(html_string);
            iframedoc.close();
        }
        else {
            alert('Preview not available!.');
        }
        html_string = '';
    }

    function SetBaseHost()
    {
        //alert($("#txtDetailurlName").val());
        var curURL = $("#txtDetailurlName").val();
        //var curURL = $("#siteName").val();
        var hostName = 'http://' + curURL.match(/:\/\/(.[^/]+)/)[1];
        var rootPos = html_string.indexOf('"/');
        var repHostName = '"' + hostName + '/';
        if (rootPos > 0)
        {
                while (rootPos > 0)
                {
                        html_string = html_string.replace('"/',repHostName);
                        rootPos = html_string.indexOf('"/');
                }
        }
    };

    function ValidateNameUrl()
    {
        //var webSiteUrlExp = /^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/;

        var webSiteUrlExp = /((http|https):\/\/(\w+:{0,1}\w*@)?(\S+)|)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        if ($("#txtDetailclientName").val() == '') {
            CommFunc.OpenOKDialog("divdialog","Please enter Client's Name",250,150);
            return false;
        }
            
        //check if url name is blank
        if ($("#txtDetailurlName").val() == '') {
            CommFunc.OpenOKDialog("divdialog","Please enter Site Url",250,150);
            return false;
        }
        //validate if url name is blank
        if(!webSiteUrlExp.test($("#txtDetailurlName").val())) {
			CommFunc.OpenOKDialog("divdialog", "Please enter the valid Url", 150, 150); 
            return false;
        }
        return true;
    }

    function ShowActionButtons()
    {
        if(ValidateNameUrl()){
            $("#actionbtns").removeClass("HideUI");
            $("#clientactionscreen").removeAttr("style");
            $("#clientactionscreen").addClass("HideUI");
        }
        else
           return false; 
    }

    function ShowTestcase()
    {
        if(ValidateNameUrl){
            $(".page_construction_label").removeAttr("style");
            ShowActionScreen('testcasepanel');
            GetBuilds();
        }
        else
           return false; 
    }

    function TestcaseforFirstBuild()
    {
        showRuns(0,true,true);  //Get the first Build and display   
    }

    function ShowActionScreen(ActionScreenId){
        $("#clientactionscreen").removeClass("HideUI");
        var DivId=$("#actionBar").parent();
        var ActiveDiv = DivId.find("div[loaded='true']");
        ActiveDiv.addClass("HideUI");
        ActiveDiv.attr("loaded","false");
        ActiveDiv = DivId.find("div[id='" + ActionScreenId + "']");
        ActiveDiv.removeClass("HideUI");
        ActiveDiv.attr("loaded","true");
    }

     function ShowDatasource(){
        $(".page_construction_label").attr("style", "display:block"); 
        $("#clientactionscreen").removeAttr("style");
        $("#clientactionscreen").addClass("HideUI");
        $(".page_construction_label").children().html("Datasource page is under construction");
        $(".page_construction_label").delay(3000).fadeOut();

    }
    //function to render the dataSource
    function getDataSource()
    {
        $("#datasource_panel").load("/NewClient/_DataSource",
        { clientName: $("#txtDetailclientName").val(), siteName: $("#txtDetailurlName").val() },
        function(){
            //alert('DataSource..');
        });
    }

    function ShowUser(){
        $(".page_construction_label").attr("style", "display:block"); 
        $("#clientactionscreen").removeAttr("style");
        $("#clientactionscreen").addClass("HideUI");
        $(".page_construction_label").children().html("Users page is under construction");
        $(".page_construction_label").delay(3000).fadeOut();
    }