﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using AccessibilityAnalyser.RuleEngine;

namespace AccessibilityAnalyser.Web.UI.Helper
{
    public class Paging
    {
        List<TestCase> testcases;
        int totalCount;
        int itemsPerPage;       
        static int currentItem;
        bool lastPage;
        bool firstPage;

        public Paging(List<TestCase> _testcases, int _itemsPerPage) 
        {
            testcases = _testcases;
            totalCount = _testcases.Count;
            itemsPerPage = _itemsPerPage;
        }

        public bool IsLastPage
        {
            get{return lastPage;}

            set{lastPage = value;}
        }

        public bool IsFirstPage
        {
            get{return firstPage;}
            set{firstPage = value;}
        }

        public List<TestCase> getFirst()
        {
            List<TestCase> testcaseList = new List<TestCase>();

            if (totalCount < itemsPerPage)
            {
                int i;
                for (i = 0; i < totalCount; i++)
                {
                    testcaseList.Add(testcases[i]);
                }
                currentItem = i;
                if (currentItem >= totalCount)
                {
                    IsLastPage = true;
                }
                else
                {
                    IsLastPage = false;
                }
                IsFirstPage = true;
            }
            else if (testcases.Count > 0)
            {
                int i;
                for (i = 0; i < itemsPerPage; i++)
                {
                    testcaseList.Add(testcases[i]);
                }
                currentItem = i;
                if (currentItem >= totalCount)
                {
                    IsLastPage = true;
                }
                else
                {
                    IsLastPage = false;
                }
                IsFirstPage = true;
            }

            return testcaseList;
        }

        public List<TestCase> getNext()
        {
            List<TestCase> testcaseList = new List<TestCase>();
            int j = currentItem;

            int currentStatus = (totalCount - (currentItem));

            if (currentStatus < itemsPerPage && currentStatus!=0)
            {
                for (j = currentItem; j < currentItem + currentStatus; j++)
                {
                    testcaseList.Add(testcases[j]);
                }
            }
            else if(currentStatus >= itemsPerPage)
            {
                for (j = currentItem; j < currentItem + itemsPerPage; j++)
                {
                    testcaseList.Add(testcases[j]);
                }               
            }    
       
            currentItem = j;

            if (currentItem >= totalCount)
            {
                IsLastPage = true;
            }
            else
            {
                IsLastPage = false;
            }

            IsFirstPage = false;

            return testcaseList;
        }

        public List<TestCase> getPrevious()
        {
            List<TestCase> testcaseList = new List<TestCase>();
            int k = currentItem;

            int currentStatus = (currentItem % itemsPerPage);

            if (currentStatus > 0)
            {
                for (k = currentItem - (itemsPerPage + currentStatus); k < currentItem - currentStatus; k++)
                {
                    testcaseList.Add(testcases[k]);
                } 
            }
            else
            {
                for (k = currentItem - (2 * itemsPerPage); k < currentItem - itemsPerPage; k++)
                {
                    testcaseList.Add(testcases[k]);
                }   
            }                  

            currentItem = k;

            if (currentItem <= itemsPerPage)
            {
                IsFirstPage = true;
            }
            else
            {
                IsFirstPage = false;
            }

            IsLastPage = false;

            return testcaseList;
        }
    }
}