﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using HtmlAgilityPack;


namespace AccessibilityAnalyser.Web.UI.Helper
{
    /// <summary>
    /// Summary description for PageHandler
    /// </summary>
    public class PageHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
            {
            context.Response.ContentType = "text/plain";
            string element = string.Empty;
            string divStyle = string.Empty;
            HtmlNode newNode = null;
            int lineNumber;

            try
            {
                //HtmlWeb web = new HtmlWeb();

                WebClient client = new WebClient();
                string webDocument = client.DownloadString(HttpUtility.UrlDecode(context.Request["url"]));

                element = HttpUtility.UrlDecode(context.Request["element"]);
                lineNumber = Convert.ToInt32(HttpUtility.UrlDecode(context.Request["LineNumber"]));

                //HtmlDocument doc = web.Load(HttpUtility.UrlDecode(context.Request["url"]));

                var doc = new HtmlDocument();
                doc.LoadHtml(webDocument);

                //checks if the loaded element is duplicated in the page and gets all similar elements
                var duplicateNodes = (from nodes in doc.DocumentNode.DescendantsAndSelf()
                                      where nodes.OuterHtml == element
                                      select nodes).ToList();

                if (element != null)
                {
                    //if (element.ToLower().Contains("</h1>") || element.ToLower().Contains("</h2>") || element.ToLower().Contains("</h3>") || element.ToLower().Contains("</h4>") || element.ToLower().Contains("</h5>") || element.ToLower().Contains("</h6>") || element.ToLower().Contains("<img"))
                    //{
                    //    divStyle = "<mark style='background: none repeat scroll 0 0 #f00;border-radius: 0.27em 0.27em 0.27em 0.27em;color: #FFFFFF; content: '';display: inline-block; font-size: 0.8751em; font-weight: 700; padding: 0.618em 0; position: absolute; right: -0.5em; text-align: center; text-shadow: 0 1px 0 rgba(0, 0, 0, 0.1); text-transform: uppercase; top: -0.5em; width: 3em; z-index: 2;'>V</mark><div style='border:3px #f00 dotted;overflow:auto;'>" + element + "</div>";
                    //}
                    //else
                    //{
                    //    divStyle = "<mark style='background: none repeat scroll 0 0 #f00;border-radius: 0.27em 0.27em 0.27em 0.27em;color: #FFFFFF; content: '';display: inline-block; font-size: 0.8751em; font-weight: 700; padding: 0.618em 0; position: absolute; right: -0.5em; text-align: center; text-shadow: 0 1px 0 rgba(0, 0, 0, 0.1); text-transform: uppercase; top: -0.5em; width: 3em; z-index: 2;'>V</mark><span style='border:3px #f00 dotted;overflow:auto;'>" + element + "</span>";
                    //}
                    divStyle = "<mark style='background: none repeat scroll 0 0 #f00;border-radius: 0.27em 0.27em 0.27em 0.27em;color: #FFFFFF; content: '';display: inline-block; font-size: 0.8751em; font-weight: 700; padding: 0.618em 0; position: absolute; right: -0.5em; text-align: center; text-shadow: 0 1px 0 rgba(0, 0, 0, 0.1); text-transform: uppercase; top: -0.5em; width: 3em; z-index: 2;'>V</mark><div style='border:3px #f00 dotted;overflow:auto;'>" + element + "</div>";
                }


                if (!string.IsNullOrEmpty(element))
                {
                    if (duplicateNodes.Count > 1)
                    {
                        divStyle = "<div style='overflow:auto'>" + divStyle + "</div>";

                        //newNode = HtmlNode.CreateNode(divStyle);
                        HtmlNode oldNode = duplicateNodes.Where(x => (x.OuterHtml == element)).FirstOrDefault();
                        string node = oldNode.OuterHtml.ToString();
                        context.Response.Write(doc.DocumentNode.OuterHtml.Replace(node, divStyle));
                    }
                    else if (duplicateNodes.Count == 1 || !string.IsNullOrEmpty(element))
                    {
                        context.Response.Write(doc.DocumentNode.OuterHtml.Replace(element, divStyle));
                    }
                    else
                    {
                        context.Response.Write(doc.DocumentNode.OuterHtml);
                    }
                }
                else
                {
                    context.Response.Write(doc.DocumentNode.OuterHtml);
                }

            }
            catch (WebException WebEx)
            {
                if (WebEx.Status == WebExceptionStatus.ProtocolError && WebEx.Response != null)
                {
                    context.Response.Write("Page not found. Please try other URL");
                }
            }
            catch (Exception ex)
            {
                context.Response.Write("Page cannot be loaded. Please try other URL");
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}