﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;

using Accessibility.ServicesLayer;
using AccessibilityAnalyser.Web.UI.ViewModels;
using AccessibilityAnalyser.Web.UI.Helper;
using AccessibilityAnalyser.Web.UI.Models;
using System.Configuration;
using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.RuleEngine;
using AccessibilityAnalyser.BusinessEntities;

namespace AccessibilityAnalyser.Web.UI.Controllers
{
    public class NewClientController : Controller
    {
        TestServices testClient = null;
        TestingViewModel testingViewModel = null;
        Paging paging = null;

        NewClientService newclientservice = new NewClientService();

        public NewClientController()
        {
            testClient = new TestServices();
            testingViewModel = new TestingViewModel();
        }

        //
        // GET: /NewClient/
        //todo call index on demand i.e on add client
        public ActionResult Index()
        {
            if (ViewBag.strRuleList == null)
            {
                Hashtable RuleName = new Hashtable();

                try
                {
                    List<RuleDetailBO> distinctRuleDetails = newclientservice.GetAllDistinctRuleDetails();
                    ViewBag.strRuleList = distinctRuleDetails;
                    IEnumerable<StandardBO> standardList = newclientservice.GetAllGroups();
                    this.ViewData["RuleStandard"] = standardList;
                    RuleName = ModelNewClient.BuildRuleandStandardMap(standardList);
                }
                catch (Exception ex)
                {
                    LogException.CatchException(ex, "NewClientController", "Index");
                }
                
                this.ViewData["RuleName"] = RuleName;
            }
            return View();
        }

        /// <summary>
        /// Get the build and run details for a url and a client
        /// </summary>
        /// <param name="testingViewModel"></param>
        /// <returns></returns>
        public ActionResult _BuildPanel(TestingViewModel testingViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //testingViewModel.builds = testClient.GetBuilds(testingViewModel.clientName, testingViewModel.siteName).ToList();
                    testingViewModel.builds = Testing.GetBuilds(testingViewModel.clientName, testingViewModel.siteName);
                    Session["TestingViewModel"] = testingViewModel;
                }
                catch (Exception ex)
                {
                    LogException.CatchException(ex, "NewClientController", "_BuildPanel");
                }
                
                return PartialView(testingViewModel);
            }
            else
            {
                return PartialView();
            }
        }

        /// <summary>
        /// Get the testcase details for a build
        /// </summary>
        /// <param name="testingViewModel"></param>
        /// <returns></returns>
        public ActionResult _TestCasePanel(TestingViewModel testingViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //testingViewModel.testCases = testClient.GetTestCases(testingViewModel.clientName, testingViewModel.siteName, testingViewModel.multipleChoice, testingViewModel.runId, testingViewModel.buildId).ToList();
                    testingViewModel.testCases = Testing.GetTestCases(testingViewModel.clientName, testingViewModel.siteName, testingViewModel.multipleChoice, testingViewModel.runId, testingViewModel.buildId).ToList();
                    
                    paging = new Paging(testingViewModel.testCases, Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["itemsPerPage"]));

                    testingViewModel.testCasesPaged = paging.getFirst();
                    testingViewModel.paging = paging;
                    Session["TestingViewModel"] = testingViewModel;
                }
                catch (Exception ex)
                {
                    LogException.CatchException(ex, "NewClientController", "_TestCasePanel");
                }

                return PartialView(testingViewModel);
            }
            else
            {
                return PartialView();
            }
        }

        /// <summary>
        /// create a new client,url,build and run if not present in database
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public ActionResult _CreateClient(FormCollection collection)
        {
            int iClientID = 0, iSiteId = 0, iBuildId = 0, iRunId = 0;
            bool dupData = false;

            string strClientName = collection["ClientName"];
            string strSiteName = collection["UrlName"];
            string strBuildName = collection["BuildName"];
            string strRunName = collection["RunName"];

            try
            {
                //create new client. Add new client name to database
                iClientID = newclientservice.GetClientID(strClientName);
                iSiteId = newclientservice.GetSiteID(strSiteName, iClientID);
                iBuildId = newclientservice.GetBuildID(strBuildName, iSiteId);
                iRunId = newclientservice.GetRunID(strRunName, iBuildId, iClientID);

                if (iClientID != 0 && iSiteId != 0 && iBuildId != 0 && iRunId != 0)
                {
                    dupData = true;
                }
                else
                {
                    if (iClientID == 0)
                    {
                        newclientservice.CreateClient(strClientName);
                        iClientID = newclientservice.GetClientID(strClientName);
                    }

                    // get siteId for newly added site
                    if (iSiteId == 0)
                    {
                        newclientservice.CreateSite(strSiteName, iClientID);
                        iSiteId = newclientservice.GetSiteID(strSiteName, iClientID);
                    }

                    //add new build for a client and a url
                    if (iBuildId == 0)
                    {
                        newclientservice.CreateBuild(strBuildName, iSiteId);
                        iBuildId = newclientservice.GetBuildID(strBuildName, iSiteId);
                    }

                    //add new Run for a client a url and a 
                    if (iRunId == 0)
                    {
                        newclientservice.CreateRun(iClientID, iBuildId, strRunName);
                        iRunId = newclientservice.GetRunID(strRunName, iBuildId, iClientID);
                    }
                }
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientController", "_CreateClient");
            }

            return this.Json(new { Duplicate = dupData.ToString() });
        }

        /// <summary>
        /// Create or Update  Rules for Client
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public ActionResult _CreateOrUpdateRulesForClient(FormCollection collection)
        {
            int iTestCaseID = 0, iRuleId = 0, iPriority = 0;
            string[] RulePriority = new string[2];
            int iBuildId = int.Parse(collection["BuildID"]);
            string strTestCaseName = collection["TestCaseName"];
            string strTestCaseDesc = collection["TestCaseDesc"];
            string checkRunItems = collection["checkRunItems"].ToString().TrimEnd(',');
            bool IsAdd = Convert.ToBoolean(collection["AddAction"]);
            bool DupData = false;

            List<TestCaseBO> AllTestCases = newclientservice.GetAllTestCases(iBuildId);

            try
            {
                if (AllTestCases.Count > 0)
                {
                    if (IsAdd)
                    {
                        if (AllTestCases.FirstOrDefault(obj => obj.TestCaseName == strTestCaseName && obj.BuildId == iBuildId) != null)
                            DupData = true;
                        else
                            DupData = false;

                        if (!DupData)
                        {
                            newclientservice.CreateTestCase(strTestCaseName, iBuildId, strTestCaseDesc);
                            iTestCaseID = newclientservice.GetAllTestCases(iBuildId)
                                                        .Single(obj => obj.TestCaseName == strTestCaseName).TestCaseId;

                            List<RuleDetailBO> distinctAllRuleDetails = newclientservice.GetAllDistinctRuleDetails();
                            var RuleItem = new RuleDetailBO();

                            foreach (string strRuleId in checkRunItems.Split(','))
                            {
                                RulePriority = strRuleId.Split('~');
                                int.TryParse(RulePriority[0], out iRuleId);
                                if (RulePriority.Length > 1)
                                    int.TryParse(RulePriority[1], out iPriority);
                                else
                                    iPriority = 1;
                                newclientservice.CreateTestCaseMap(iRuleId, iTestCaseID, iPriority);
                            }
                        }
                    }
                    else
                    {
                        iTestCaseID = int.Parse(collection["TestCaseId"].ToString());

                        if (AllTestCases.FirstOrDefault(obj => obj.TestCaseName == strTestCaseName && obj.TestCaseId != iTestCaseID && obj.BuildId == iBuildId) != null)
                            DupData = true;
                        else
                            DupData = false;

                        if (!DupData)
                        {
                            TestCaseBO TestCaseItem = AllTestCases.Single(obj => obj.TestCaseId == iTestCaseID);
                            TestCaseItem.TestCaseName = strTestCaseName;
                            TestCaseItem.TestCaseDescription = strTestCaseDesc;
                            newclientservice.UpdateTestCase(TestCaseItem);

                            List<TestCaseRuleMapBO> TestCaseRuleMapCollection = newclientservice.GetAllTestCaseRuleMap(iTestCaseID);
                            newclientservice.DeleteMultipleTestCaseRuleMap(TestCaseRuleMapCollection);

                            foreach (string strRuleId in checkRunItems.Split(','))
                            {
                                RulePriority = strRuleId.Split('~');
                                int.TryParse(RulePriority[0], out iRuleId);
                                if (RulePriority.Length > 1)
                                    int.TryParse(RulePriority[1], out iPriority);
                                else
                                    iPriority = 1;
                                newclientservice.CreateTestCaseMap(iRuleId, iTestCaseID, iPriority);
                            }
                        }
                    }
                }
                else
                {
                    newclientservice.CreateTestCase(strTestCaseName, iBuildId, strTestCaseDesc);
                    iTestCaseID = newclientservice.GetAllTestCases(iBuildId)
                                                            .Single(obj => obj.TestCaseName == strTestCaseName).TestCaseId;

                    List<RuleDetailBO> distinctAllRuleDetails = newclientservice.GetAllDistinctRuleDetails();
                    var RuleItem = new RuleDetailBO();

                    foreach (string strRuleId in checkRunItems.Split(','))
                    {
                        RulePriority = strRuleId.Split('~');
                        int.TryParse(RulePriority[0], out iRuleId);
                        if (RulePriority.Length > 1)
                            int.TryParse(RulePriority[1], out iPriority);
                        else
                            iPriority = 1;
                        newclientservice.CreateTestCaseMap(iRuleId, iTestCaseID, iPriority);
                    }
                }
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientController", "_CreateOrUpdateRulesForClient");
            }

            return Json(new { Duplicate = DupData.ToString() });
        }

        /// <summary>
        /// Delete the selected testcase
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public ActionResult _DeleteTestCase(FormCollection collection)
        {
            try
            {
                int iTestCaseId = int.Parse(collection["TestCaseId"].ToString());
                newclientservice.DeleteSingleTestCase(iTestCaseId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "NewClientController", "_DeleteTestCase");
            }
            return RedirectToAction("Index", "NewClient");
        }

        /// <summary>
        /// Return partial view for create/edit testcase
        /// </summary>
        /// <param name="iTestCaseId"></param>
        /// <returns></returns>
        public ActionResult _EditTestcase(FormCollection collection)
        {
            int iTestCaseId = int.Parse(collection["TestcaseId"].ToString());
            TestCaseBO TestcaseObj = newclientservice.GetSingleTestCase(iTestCaseId);
            if (ViewBag.strRuleList == null)
            {
                try
                {
                    List<RuleDetailBO> distinctRuleDetails = newclientservice.GetAllDistinctRuleDetails();
                    ViewBag.strRuleList = distinctRuleDetails;
                    IEnumerable<StandardBO> standardList = newclientservice.GetAllGroups();
                    this.ViewData["RuleStandard"] = standardList;
                    Hashtable RuleName = ModelNewClient.BuildRuleandStandardMap(standardList);
                    this.ViewData["RuleName"] = RuleName;
                }
                catch (Exception ex)
                {
                    LogException.CatchException(ex, "NewClientController", "_EditTestcase");
                }
            }
            return PartialView(TestcaseObj);
        }
    }
}
