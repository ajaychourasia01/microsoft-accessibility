﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Deployment.WindowsInstaller;
using System.Diagnostics;
using System.IO;

namespace TestHarnessIEPluginSetupCustomAction
{
    public class CustomActions
    {
        [CustomAction]
        public static ActionResult IsIERunning(Session session)
        {
            Process[] process = Process.GetProcessesByName("Internet Explorer");

            if (process != null && process.Length > 0)
            {
                // Set the public property.
                session["IsIERunning"] = "1";
            }

            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult RegisterBHO(Session session)
        {
            string netFolder = session.CustomActionData["NETFOLDER"];
            string rootFolder = session.CustomActionData["INSTALLFOLDER"];
            string targetAssembly = Path.Combine(rootFolder, "AccessibilityAnalyser.BrowserPlugIn.dll");
            string regASMFile = Path.Combine(netFolder, "RegAsm.exe");

            session.Log(string.Format(".Net Folder = {0}, rootFolder={1}, targetAssembly={2}, regASMFile={3}", netFolder, rootFolder, targetAssembly, regASMFile));
            try
            {
                ProcessStartInfo processInfo = new ProcessStartInfo("\"" + regASMFile + "\"", "\"" + targetAssembly + "\"");
                Process.Start(processInfo);
            }
            catch(Exception ex)
            {
                session.Log(ex.Message);
                return ActionResult.Failure;
            }
            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult RegisterBHOUninstall(Session session)
        {
            string netFolder = session.CustomActionData["NETFOLDER"];
            string rootFolder = session.CustomActionData["INSTALLFOLDER"];
            string targetAssembly = Path.Combine(rootFolder, "AccessibilityAnalyser.BrowserPlugIn.dll");
            string regASMFile = Path.Combine(netFolder, "RegAsm.exe");

            session.Log(string.Format(".Net Folder = {0}, rootFolder={1}, targetAssembly={2}, regASMFile={3}", netFolder, rootFolder, targetAssembly, regASMFile));
            try
            {
                ProcessStartInfo processInfo = new ProcessStartInfo("\"" + regASMFile + "\"", "/unregister, \"" + targetAssembly + "\"");
                Process.Start(processInfo);
            }
            catch (Exception ex)
            {
                session.Log(ex.Message);
                return ActionResult.Failure;
            }
            return ActionResult.Success;
        }
    }
}
