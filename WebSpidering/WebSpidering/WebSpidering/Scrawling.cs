﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HtmlAgilityPack;
using System.Net;

namespace WebSpidering
{
    public class Scrawling
    {
        List<string> fileTypes = null;

        public List<urlFormat> GetUrls(urlFormat url)
        {
            List<urlFormat> hrefTags = new List<urlFormat>();
            if (url.IsScrawled == false)
            {                
                string baseSiteName = GetBaseSiteName(url.urlName);
                fileTypes = GetFileTypes();

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url.urlName);
                request.Proxy = WebRequest.DefaultWebProxy;
                request.Method = "GET";
                //request.Credentials = new NetworkCredential("user", "pwd", "domain");
                //request.Proxy.Credentials = new NetworkCredential("user", "pwd", "domain");    
                request.Credentials = CredentialCache.DefaultCredentials;
                request.KeepAlive = false;
                request.Timeout = 1000000;
                request.Proxy.Credentials = CredentialCache.DefaultCredentials;
                System.IO.Stream stream = null;
                HttpWebResponse response = null;
                while (response == null)
                {
                    try
                    {
                        response = (HttpWebResponse)request.GetResponse();
                        stream = response.GetResponseStream();
                    }
                    catch
                    {
                    }
                }

                var doc = new HtmlWeb().Load(url.urlName);


                foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//a[@href]"))
                {
                    HtmlAttribute att = link.Attributes["href"];
                    if (att.Value != "#")
                    {
                        string AbsoluteUrl = GetAbsoluteUrl(att.Value, url.urlName).Trim('/');
                        if (hrefTags.IndexOf(new urlFormat(){urlName = AbsoluteUrl}) == -1 &&
                            //&& AbsoluteUrl.IndexOf(baseSiteName.Substring(baseSiteName.IndexOf("://"))) >= 0                            
                            AbsoluteUrl.IndexOf("://" + baseSiteName) >= 0
                            && ValidUrl(AbsoluteUrl))
                        {
                            if (AbsoluteUrl == url.urlName)
                            {
                                urlFormat newUrl = new urlFormat() { urlName = AbsoluteUrl, IsScrawled = true };
                                hrefTags.Add(newUrl);
                            }
                            else
                            {
                                urlFormat newUrl = new urlFormat() { urlName = AbsoluteUrl, IsScrawled = false };
                                hrefTags.Add(newUrl);
                            }
                        }
                    }
                }

                //if (hrefTags.IndexOf(url) == -1)
                //{
                //    hrefTags.Insert(0, url);
                //}


                //hrefTags.Sort();                
            }
            return hrefTags;
        }

        public string GetBaseSiteName(string siteName)
        {
            string baseSiteAddress = string.Empty;
            try
            {
                string[] splitAddress = siteName.Split('/');
                //baseSiteAddress = splitAddress[0] + @"//" + splitAddress[2];
                baseSiteAddress = splitAddress[2];
            }
            catch { }
            return baseSiteAddress;
        }

        public bool ValidUrl(string url)
        {
            bool retVal = true;
            foreach (string fileType in fileTypes)
            {
                if (url.ToLower().IndexOf(fileType) >= 0)
                    retVal = false;
            }

            return retVal;
        }

        public List<string> GetFileTypes()
        {
            fileTypes = new List<string>();
            fileTypes.Add(".jpg");
            fileTypes.Add(".gif");
            fileTypes.Add(".pdf");
            fileTypes.Add(".xml");
            fileTypes.Add(".css");
            fileTypes.Add("javascript");
            fileTypes.Add(".js");
            fileTypes.Add("./");
            fileTypes.Add("mailto:");
            return fileTypes;
        }

        public static string GetAbsoluteUrl(string url, string Parenturl)
        {
            if (url.StartsWith("http://", StringComparison.OrdinalIgnoreCase)
                || url.StartsWith("https://", StringComparison.OrdinalIgnoreCase))
            {
                return url;
            }

            string strDomain = Parenturl.Substring(Parenturl.IndexOf("://") + 3);
            if (strDomain.Contains('/'))
            {
                if (url.StartsWith("/"))
                {
                    strDomain = strDomain.Substring(0, strDomain.IndexOf('/'));
                }
                else
                {
                    strDomain = strDomain.Substring(0, strDomain.LastIndexOf('/'));
                }
            }

            return "http://" + strDomain + "/" + url.TrimStart('/');
        }
    }
}