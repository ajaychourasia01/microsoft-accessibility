﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HtmlAgilityPack;
using System.Threading.Tasks;


namespace WebSpidering
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUrls_Click(object sender, EventArgs e)
        {
            Scrawling objScrawling = new Scrawling();
            //List<string> lstUrl = new List<string>();
            //List<string> lstFinalUrl = new List<string>();
            //List<string> lstInterUrl = new List<string>();

            List<urlFormat> lstUrl = new List<urlFormat>();
            List<urlFormat> lstFinalUrl = new List<urlFormat>();
            List<urlFormat> lstInterUrl = new List<urlFormat>();

            int count = Convert.ToInt32(txtLevel.Text);




            lstUrl = objScrawling.GetUrls(new urlFormat() { urlName = txtUrl.Text.ToString() });
            //lstFinalUrl = lstUrl;
            foreach (urlFormat urlItem in lstUrl)
            {
                if (!lstFinalUrl.Contains(new urlFormat() { urlName = urlItem.urlName }))
                {
                    lstFinalUrl.Add(urlItem);
                }
            }

           
            for (int i = 0; i < count; i++)
            {
                if (i == 0)
                {
                    lstInterUrl.Clear();
                    lstInterUrl = ParallelProcess(lstUrl);
                }
                else
                {
                    lstInterUrl = ParallelProcess(lstInterUrl);
                }


                foreach (urlFormat item in lstInterUrl)
                {
                    if (!(lstFinalUrl.Exists(x => x.urlName == item.urlName)))
                    {
                        lstFinalUrl.Add(item);
                    }
                }
            }


            txtUrlCount.Text = lstFinalUrl.Count.ToString();
            gdvUrl.DataSource = lstFinalUrl;
            gdvUrl.DataBind();
        }


        public List<urlFormat> ParallelProcess(List<urlFormat> lstInitialUrls)
        {
            List<urlFormat> lstInterUrls = new List<urlFormat>();
            List<urlFormat> lstResultUrls = new List<urlFormat>();
            Scrawling objScrawling = new Scrawling();

            Parallel.ForEach(lstInitialUrls, urlPage =>
            {
                lstInterUrls = objScrawling.GetUrls(urlPage);

                if (lstInterUrls.Count > 0)
                {
                    foreach (urlFormat urls in lstInterUrls)
                    {                        
                       lstResultUrls.Add(urls);   
                    }
                }
            });

            return lstResultUrls;
        }
    }

    public class urlFormat
    {
        public string urlName { get; set; }
        public bool IsScrawled { get; set; }
    }
}
