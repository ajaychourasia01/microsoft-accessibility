﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Accessibility.ServicesLayer;
using AccessibilityAnalyser.RuleEngine;
using System.ComponentModel.DataAnnotations;
using AccessibilityAnalyser.BusinessEntities;

namespace AccessibilityAnalyser.Web.UI.ViewModels
{
    public class HomeViewModel
    {
        public List<ClientBO> clients = new List<ClientBO>();
        public List<int> ViolationCount = new List<int>();
    }
}