//$j = jQuery.noConflict();


var elmHeight = "6";	// should be specified based on image size

// Extend JQuery Functionality For Custom Radio Button Functionality
$.fn.extend({
dgStyle: function()
{
	// Initialize with initial load time control state
	$.each($(this), function(){
		
		
		var elm	=	$(this).children().get(0);
		elmType = $(elm).attr("type");
		$(this).data('type',elmType);
		$(this).data('checked',$(elm).attr("checked"));
		$(this).dgClear();
		
	});
	
	$(this).mouseup(function() { $(this).dgHandle(); });	
},
dgClear: function()
{
	if($(this).data("checked") == true)
	{
		$(this).css("backgroundPosition","0 -"+(elmHeight*2)+"px");
		}
	else
	{
		$(this).css("backgroundPosition","0 4px");
		}	
},

dgHandle: function()
{
	var elm	=	$(this).children().get(0);
	if($(this).data("checked") == true)
		$(elm).dgUncheck(this);
	else
		$(elm).dgCheck(this);
	
	
},
dgCheck: function(div)
{
	$(this).attr("checked",true);
	$(div).data('checked',true).css({backgroundPosition:"0 -"+(elmHeight*2)+"px",color:"#007233"});
},
dgUncheck: function(div)
{
	$(this).attr("checked",false);
	if(div != -1)
		$(div).data('checked',false).css({backgroundPosition:"0 4px",color:"#000000"});
	else
		$(this).parent().data("checked",false).css({backgroundPosition:"0 4px",color:"#000000"});
}
});