﻿
$(document).ready(function () {

    function btnBack() {
        window.history.forward();
    }
    btnBack();
    window.onload = btnBack;
    window.onpageshow = function (evt) { if (evt.persisted) btnBack() }
    window.onunload = function () { void (0) }

    $("#txtUsername").watermark("UserName");
    $("#txtPassword").watermark("Password");

    var CommFunc = new Utils();

    $("#btnLogin").unbind();
    $("#btnLogin").bind('click', function (event) {
        var userName = $('#txtUsername').val();
        var password = $('#txtPassword').val();

        if (userName == "" && password == "") {
            CommFunc.OpenOKDialog("divdialog", "Please enter username/password", 300, 150);
            return false;
        }
        else if (password == "" && userName != "") {
            CommFunc.OpenOKDialog("divdialog", "Please enter username/password", 300, 150);
            return false;
        }
        else if (password != "" && userName == "") {
            CommFunc.OpenOKDialog("divdialog", "Please enter username/password", 300, 150);
            return false;
        }
        else {
            $.ajax({
                type: "POST",
                url: "/Login/UserAccountLogin",
                data: {
                    "LoginViewModel.AccountUserName": $('#txtUsername').val(),
                    "LoginViewModel.AccountUserPassword": $('#txtPassword').val()
                },
                success: function (data) {
                    //                    document.location.href = "/Home/Index";

                    if (data.success == false) {
                        CommFunc.OpenOKDialog("divdialog", "Please enter correct username/password", 300, 150);
                    }
                    else {
                        document.location.href = "/Home/Index";
//                        $("#dialog").dialog("close");
                    }

                }
            });
        }
    });
});