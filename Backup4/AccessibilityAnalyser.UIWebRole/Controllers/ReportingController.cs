﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Accessibility.ServicesLayer;
//using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.RuleEngine;
using System.Text;
using System.Configuration;
//using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.BusinessEntities;
using AccessibilityAnalyser.Common;


namespace AccessibilityAnalyser.Web.UI.Controllers
{
    public class ReportingController : Controller
    {
        // x-axis values for generating report
        StringBuilder run1FailPercValues = new StringBuilder();
        StringBuilder run2FailPercValues = new StringBuilder();
        StringBuilder run3FailPercValues = new StringBuilder();
        StringBuilder failPercValuesForAllRuns = new StringBuilder();
        StringBuilder failViolationsForAllRuns = new StringBuilder();
        StringBuilder totalViolationsForAllRuns = new StringBuilder();
        StringBuilder getAllRuns = new StringBuilder();
        ReportServices reportingService = new ReportServices();
        TestServices testClient = new TestServices();
       
        public ActionResult Index(int clientId = 0)
        {
            GetAllDataForReportView(clientId);

            if (clientId != 0)
            {
                ViewBag.ClientName = reportingService.GetClientName(clientId);
            }

            return View();
        }

        /// <summary>
        /// Get latest 3 runs
        /// </summary>
        /// <param name="clientName"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult _ReportView(FormCollection collection)
        {
            int clientId = 0;

            if (!string.IsNullOrEmpty(collection["clientName"]))
            {
                try
                {
                    clientId = reportingService.GetClientId(collection["clientName"]);
                    GetAllDataForReportView(clientId);
                }
                catch (Exception ex)
                {
                    LogException.CatchException(ex, "ReportingController", "_ReportView");
                }
                
            }

            ViewBag.ClientName = collection["clientName"];

            return PartialView();
        }

        /// <summary>
        /// Get pass percentage values for all runs across the builds
        /// </summary>
        /// <param name="getAllTestCaseRatioDetails"></param>
        private void GetFailPercValuesForAllBuilds(List<ReportingDetails> getAllTestCaseRatioDetails)
        {
            int counter = 0;
            int runIdCounterForBuild = 0;
            int buildId = 0;

            try
            {
                foreach (var testCaseItem in getAllTestCaseRatioDetails)
                {
                    int runCount = (from i in getAllTestCaseRatioDetails
                                    where i.BuildId == testCaseItem.BuildId
                                    select i.RunId).Count();

                    getAllRuns.AppendFormat("{0} - {1} ({2}),", testCaseItem.BuildName, testCaseItem.RunName, testCaseItem.TotalCount);

                    if (counter == 0 || buildId == testCaseItem.BuildId)
                    {

                        GetFailPercValuesForAllRuns(runCount, runIdCounterForBuild, testCaseItem.FailParcentage, testCaseItem.FailedViolationsCount, testCaseItem.TotalViolationsCount);
                        runIdCounterForBuild++;
                    }
                    else
                    {
                        runIdCounterForBuild = 0;
                        GetFailPercValuesForAllRuns(runCount, runIdCounterForBuild, testCaseItem.FailParcentage, testCaseItem.FailedViolationsCount, testCaseItem.TotalViolationsCount);
                        runIdCounterForBuild++;
                    }

                    buildId = testCaseItem.BuildId;
                    counter++;
                }

                if (!string.IsNullOrEmpty(failPercValuesForAllRuns.ToString()))
                {
                    this.ViewData["PassPercValuesForAllRuns"] = failPercValuesForAllRuns.ToString().Substring(0, failPercValuesForAllRuns.Length - 1);
                }

                if (!string.IsNullOrEmpty(failViolationsForAllRuns.ToString()))
                {
                    this.ViewData["FailedViolationsForAllRuns"] = failViolationsForAllRuns.ToString().Substring(0, failViolationsForAllRuns.Length - 1);
                }

                if (!string.IsNullOrEmpty(totalViolationsForAllRuns.ToString()))
                {
                    this.ViewData["TotalViolationsForAllRuns"] = totalViolationsForAllRuns.ToString().Substring(0, totalViolationsForAllRuns.Length - 1);
                }

                if (!string.IsNullOrEmpty(getAllRuns.ToString()))
                {
                    this.ViewData["GetAllRuns"] = getAllRuns.ToString().Substring(0, getAllRuns.Length - 1);
                }
            }
            catch (Exception ex)
            {   
                LogException.CatchException(ex, "ReportingController", "GetFailPercValuesForAllBuilds");
            }
        }

        /// <summary>
        /// Get pass percentage values for all runs based on build
        /// </summary>
        /// <param name="runCount"></param>
        /// <param name="runIdCounterForBuild"></param>
        /// <param name="passPerc"></param>
        private void GetFailPercValuesForAllRuns(int runCount, int runIdCounterForBuild, double failPerc, int failedViolationsCount, int totalViolationsCount)
        {
            try
            {
                failPercValuesForAllRuns.AppendFormat("{0},", failPerc);

                failViolationsForAllRuns.AppendFormat("{0},", failedViolationsCount);
                totalViolationsForAllRuns.AppendFormat("{0},", totalViolationsCount);

                switch (runCount)
                {
                    case 0:
                        {
                            run1FailPercValues.AppendFormat("0,");
                            run2FailPercValues.AppendFormat("0,");
                            run3FailPercValues.AppendFormat("0,");
                        }
                        break;
                    case 1:
                        {
                            if (runIdCounterForBuild == 0)
                            {
                                run1FailPercValues.AppendFormat(string.Format("{0},", failPerc));
                            }

                            run2FailPercValues.AppendFormat("0,");
                            run3FailPercValues.AppendFormat("0,");
                        }
                        break;
                    case 2:
                        {
                            switch (runIdCounterForBuild)
                            {
                                case 0:
                                    {
                                        run1FailPercValues.AppendFormat(string.Format("{0},", failPerc));
                                    }
                                    break;
                                case 1:
                                    {
                                        run2FailPercValues.AppendFormat(string.Format("{0},", failPerc));
                                        run3FailPercValues.AppendFormat("0,");
                                    }
                                    break;
                            }
                        }
                        break;
                    case 3:
                        {
                            switch (runIdCounterForBuild)
                            {
                                case 0:
                                    {
                                        run1FailPercValues.AppendFormat(string.Format("{0},", failPerc));
                                    }
                                    break;
                                case 1:
                                    {
                                        run2FailPercValues.AppendFormat(string.Format("{0},", failPerc));
                                    }
                                    break;
                                case 2:
                                    {
                                        run3FailPercValues.AppendFormat(string.Format("{0},", failPerc));
                                    }
                                    break;
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {   
                LogException.CatchException(ex, "ReportingController", "GetFailPercValuesForAllRuns");
            }
        }

        /// <summary>
        /// Get all data for displaying charts and run details
        /// </summary>
        private void GetAllDataForReportView(int clientId)
        {
            try
            {
                reportingService.GetData(clientId);

                // y-axis values for generating report
                StringBuilder allBuilds = new StringBuilder();
                List<int> buildIds = new List<int>();

                List<ReportingDetails> getTestCaseRatioDetails = new List<ReportingDetails>();

                // Y-axis values - get the list of latest 5 and starting 5 builds. 
                IEnumerable<BuildBO> buildDetails = reportingService.GetNoOfBuild(clientId);

                List<ReportingDetails> getAllTestCaseRatioDetails = new List<ReportingDetails>();

                // y-axis values - generate all builds
                foreach (var build in buildDetails)
                {
                    allBuilds.AppendFormat(string.Format("{0},", build.BuildName));
                }
                // x-axis values - get all test case ratio details
                buildDetails.ToList().ForEach(delegate(BuildBO build)
                {
                    buildIds.Add(build.BuildId);
                });
                getAllTestCaseRatioDetails.AddRange( reportingService.GetTestCaseRatioDetails(clientId, buildIds));
                buildDetails.ToList().ForEach(delegate(BuildBO build)
                {
                    // get all latest 3 runs across the builds
                    getAllTestCaseRatioDetails.AddRange((from j in
                                                             ((from i in getTestCaseRatioDetails
                                                               where i.BuildId == build.BuildId
                                                               orderby i.BuildId, i.RunId descending
                                                               select new
                                                               {
                                                                   BuildId = i.BuildId,
                                                                   BuildName = i.BuildName,
                                                                   RunId = i.RunId,
                                                                   FailParcentage = i.FailParcentage,
                                                                   RunName = i.RunName,
                                                                   FailCount = i.FailCount,
                                                                   TotalCount = i.TotalCount,
                                                                   TotalViolationsCount = i.TotalViolationsCount,
                                                                   FailedViolationsCount = i.FailedViolationsCount
                                                               }).ToList())
                                                         select new ReportingDetails
                                                         {
                                                             BuildId = j.BuildId,
                                                             BuildName = j.BuildName,
                                                             RunId = j.RunId,
                                                             RunName = j.RunName,
                                                             FailParcentage = j.FailParcentage,
                                                             FailCount = j.FailCount,
                                                             TotalCount = j.TotalCount,
                                                             TotalViolationsCount = j.TotalViolationsCount,
                                                             FailedViolationsCount = j.FailedViolationsCount
                                                         }).OrderBy(o => o.BuildId).OrderBy(o => o.RunId).Take(3).ToList());
                });
                ReportingDetails getTotalTCCount = reportingService.GetTotalTestCaseRatio(clientId);

                this.ViewData["FailCount"] = getTotalTCCount.FailCount;
                this.ViewData["TotalCount"] = getTotalTCCount.TotalCount;

                this.ViewData["FailPerc"] = Math.Round(Common.Helper.Percent(getTotalTCCount.FailCount, getTotalTCCount.TotalCount, 2));

                //X-axis values - get pass percentage values for all runs across the builds
                GetFailPercValuesForAllBuilds(getAllTestCaseRatioDetails);

                if (!string.IsNullOrEmpty(allBuilds.ToString()))
                {
                    this.ViewData["AllBuilds"] = allBuilds.ToString().Substring(0, allBuilds.Length - 1);
                }

                if (!string.IsNullOrEmpty(run1FailPercValues.ToString()))
                {
                    this.ViewData["Run1FailPercValues"] = run1FailPercValues.ToString().Substring(0, run1FailPercValues.Length - 1);
                }

                if (!string.IsNullOrEmpty(run2FailPercValues.ToString()))
                {
                    this.ViewData["Run2FailPercValues"] = run2FailPercValues.ToString().Substring(0, run2FailPercValues.Length - 1);
                }

                if (!string.IsNullOrEmpty(run3FailPercValues.ToString()))
                {
                    this.ViewData["Run3FailPercValues"] = run3FailPercValues.ToString().Substring(0, run3FailPercValues.Length - 1);
                }

                this.ViewData["GetLatestBuilds"] = reportingService.GetLatestBuilds(clientId);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "ReportingController", "GetAllDataForReportView");
            }
        }

        public ActionResult QuickSearchClient(string term)
        {
            var clients = testClient.GetClients(term)
            .Take(10)
            .OrderBy(client => client.ClientName)
            .Select(client => new { label = client.ClientName });

            return Json(clients, JsonRequestBehavior.AllowGet);
        }
       
    }
}
