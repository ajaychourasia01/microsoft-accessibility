﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Accessibility.ServicesLayer;
using AccessibilityAnalyser.Web.UI.ViewModels;
using AccessibilityAnalyser.Web.UI.Helper;
using AccessibilityAnalyser.Common;
using AccessBusEnt = AccessibilityAnalyser.BusinessEntities;
using AccessRuleEng = AccessibilityAnalyser.RuleEngine;
using System.Net;
using AccessibilityAnalyser.RuleEngine;



namespace AccessibilityAnalyser.Web.UI.Controllers
{
    public class TestingController : Controller
    {
        TestServices testClient = null;
        TestingViewModel testingViewModel = null;
        Paging paging = null;
        GetNextViolation getNextViolation = null;

        public TestingController()
        {
            testClient = new TestServices();
            testingViewModel = new TestingViewModel();
        }

        [HttpGet]
        public ActionResult Index(string clientName = null)
        {
            return View();
        }

        public ActionResult QuickSearchClient(string term)
        {
            var clients = testClient.GetClients(term)
            .Take(10)
            .OrderBy(client => client.ClientName)
            .Select(client => new { label = client.ClientName });

            return Json(clients, JsonRequestBehavior.AllowGet);
        }

        public JsonResult QuickSearchUrlName(string searchText, string clientName)
        {
            if (!string.IsNullOrEmpty(clientName))
            {
                var sites = testClient.GetSiteList(searchText, clientName)
                    .Take(10)
                    .OrderBy(client => client.SiteName)
                    .Select(client => new { label = client.SiteName });

                return Json(sites, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult _LeftPanel(TestingViewModel testingViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    testingViewModel.testCases = testClient.GetTestCases(testingViewModel.clientName, testingViewModel.siteName, testingViewModel.multipleChoice, testingViewModel.runId, testingViewModel.buildId).ToList();
                    paging = new Paging(testingViewModel.testCases, Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["itemsPerPage"]));
                    testingViewModel.testCasesPaged = paging.getFirst();
                    testingViewModel.paging = paging;
                    Session["TestingViewModel"] = testingViewModel;
                }
                catch (Exception ex)
                {
                    LogException.CatchException(ex, "TestingController", "_LeftPanel");
                }

                return PartialView(testingViewModel);
            }
            else
            {
                return PartialView();
            }
        }

        public ActionResult GetNext()
        {
            try
            {
                testingViewModel = (TestingViewModel)Session["TestingViewModel"];
                testingViewModel.testCasesPaged = testingViewModel.paging.getNext();
                Session["TestingViewModel"] = testingViewModel;
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestingController", "GetNext");
            }

            return PartialView("_LeftPanel", testingViewModel);
        }

        public ActionResult GetPrevious()
        {
            try
            {
                testingViewModel = (TestingViewModel)Session["TestingViewModel"];
                testingViewModel.testCasesPaged = testingViewModel.paging.getPrevious();
                Session["TestingViewModel"] = testingViewModel;
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestingController", "GetPrevious");
            }

            return PartialView("_LeftPanel", testingViewModel);
        }

        [HttpPost]
        public ActionResult _MiddlePanel(FormCollection collection)
        {
            testingViewModel = (TestingViewModel)Session["TestingViewModel"];
            int clientId = testClient.GetClients(testingViewModel.clientName).ToList().First().ClientId;
            int buildId = testingViewModel.buildId;
            int runId = testingViewModel.runId;
            var Violations = new AccessBusEnt.Violations();

            if (testingViewModel != null)
            {
                try
                {
                    if (Convert.ToInt32(collection["ruleDetailId"]) != 0)
                    {
                        testingViewModel.ruleDetailId = Convert.ToInt32(collection["ruleDetailId"]);
                        if (collection["ViolationType"] != null)
                        {
                            Violations = AccessRuleEng.Testing.GetViolations(clientId, buildId, runId, testingViewModel.ruleDetailId, collection["ViolationType"].ToString(), 0, testingViewModel.TestCaseId);
                            testingViewModel.violations = Violations.ViolationList;
                            testingViewModel.ViolationTypeSelected = collection["ViolationType"].ToString();
                        }
                    }
                    if (collection["getviolation"] != null)
                    {
                        testingViewModel.getviolation = collection["getviolation"].ToString();
                    }
                    testingViewModel.selectedRules = collection["selectedRules"];
                    Session["TestingViewModel"] = testingViewModel;
                }
                catch (Exception ex)
                {
                    LogException.CatchException(ex, "TestingController", "_MiddlePanel");
                }

                return PartialView(testingViewModel);
            }
            else
            {
                return PartialView();
            }
        }


        [HttpPost]
        public ActionResult _MiddlePanelViolations(FormCollection collection)
        {
            testingViewModel = (TestingViewModel)Session["TestingViewModel"];
            int clientId = testClient.GetClientSingle(testingViewModel.clientName).ToList().First().ClientId;
            int buildId = testingViewModel.buildId;
            int runId = testingViewModel.runId;
            int testCaseId = 0;
            if (collection["testCaseId"] != null)
                testCaseId = Convert.ToInt32(collection["testCaseId"]);
            else
                testCaseId = testingViewModel.TestCaseId;
            var violations = new AccessBusEnt.Violations();
            //int skip = testingViewModel.PageNo > 1 ? 0 : testingViewModel.PageNo*10;
            string refreshPanel = (collection["refreshPanel"]);

            if (testingViewModel != null)
            {
                try
                {
                    if (Convert.ToInt32(collection["ruleDetailId"]) != 0)
                    {
                        testingViewModel.ruleDetailId = Convert.ToInt32(collection["ruleDetailId"]);
                        if (collection["ViolationType"] != null)
                        {
                            violations = AccessRuleEng.Testing.GetViolations(clientId, buildId, runId, testingViewModel.ruleDetailId, collection["ViolationType"].ToString(), 0, testCaseId);
                            testingViewModel.violations = violations.ViolationList;
                            testingViewModel.ViolationTypeSelected = collection["ViolationType"].ToString();
                            testingViewModel.TestCaseId = testCaseId;
                            //Session["ViolationList"] = testingViewModel.violations;
                            testingViewModel.StartIndex = testingViewModel.violations.Count > 0 ? 1 : 0;
                            testingViewModel.EndIndex = testingViewModel.violations.Count >= 10 ? 10 : testingViewModel.violations.Count;
                            testingViewModel.PageNo = 0;
                            string violationtype = collection["ViolationType"];
                            testingViewModel.TotalFilteredRecords = (violationtype == "Accepted") ? violations.AcceptedViolationCount : (violationtype == "Declined") ? violations.DeclinedViolationCount : violations.TotalViolationCount;
                            testingViewModel.TotalPage = testingViewModel.TotalFilteredRecords > 0 ? testingViewModel.TotalFilteredRecords / 10 : 0;

                            if (violationtype == "All" || (refreshPanel != null))
                            {
                                testingViewModel.TotalRecords = violations.TotalViolationCount > 0 ? violations.TotalViolationCount : 0;
                                testingViewModel.AcceptedCount = violations.AcceptedViolationCount;
                                testingViewModel.DeclinedCount = violations.DeclinedViolationCount;
                            }

                        }
                    }
                    if (collection["getviolation"] != null)
                    {
                        testingViewModel.getviolation = collection["getviolation"].ToString();
                    }
                    testingViewModel.selectedRules = collection["selectedRules"];
                    Session["TestingViewModel"] = testingViewModel;
                }
                catch (Exception ex)
                {
                    LogException.CatchException(ex, "TestingController", "_MiddlePanelViolations");
                }

                return PartialView(testingViewModel);
            }
            else
            {
                return PartialView();
            }
        }

        public ActionResult _RightPanel(FormCollection collection)
        {
            testingViewModel = (TestingViewModel)Session["TestingViewModel"];
            if (testingViewModel != null)
            {
                try
                {
                    testingViewModel.violationId = Convert.ToInt32(collection["violationId"]);
                    testingViewModel.IsAutomated = Convert.ToBoolean(Session["IsAutomated"]);
                    testingViewModel.SearchViolation = "";
                    testingViewModel.violation = GetViolationDetail(testingViewModel.violations, testingViewModel.violationId);
                    getNextViolation = new Helper.GetNextViolation(testingViewModel.violations, testingViewModel.violations.FindIndex(violation => violation.ViolationId == testingViewModel.violationId));
                    testingViewModel.getNextViolation = getNextViolation;
                    Session["TestingViewModel"] = testingViewModel;
                }
                catch (Exception ex)
                {
                    LogException.CatchException(ex, "TestingController", "_RightPanel");
                }

                return PartialView(testingViewModel);
            }
            else
            {
                return PartialView();
            }
        }

        public ActionResult _RightPanelForGettingViolations(FormCollection collection)
        {
            testingViewModel = (TestingViewModel)Session["TestingViewModel"];
            if (testingViewModel != null)
            {
                try
                {
                    testingViewModel.violationId = Convert.ToInt32(collection["violationId"]);
                    testingViewModel.IsAutomated = Convert.ToBoolean(collection["IsAutomated"]);
                    Session["IsAutomated"] = Convert.ToBoolean(collection["IsAutomated"]);
                    testingViewModel.SearchViolation = "";
                    testingViewModel.violation = GetViolationDetail(testingViewModel.violations, testingViewModel.violationId);
                    getNextViolation = new Helper.GetNextViolation(testingViewModel.violations, testingViewModel.violations.FindIndex(violation => violation.ViolationId == testingViewModel.violationId));
                    testingViewModel.getNextViolation = getNextViolation;
                    Session["TestingViewModel"] = testingViewModel;
                }
                catch (Exception ex)
                {
                    LogException.CatchException(ex, "TestingController", "_RightPanelForGettingViolations");
                }

                return PartialView("_RightPanel", testingViewModel);
            }
            else
            {
                return PartialView();
            }
        }

        public ActionResult _RightPanelForSearchViolations(FormCollection collection)
        {
            testingViewModel = (TestingViewModel)Session["TestingViewModel"];
            if (testingViewModel != null)
            {
                try
                {
                    testingViewModel.violationId = Convert.ToInt32(collection["violationId"]);
                    testingViewModel.IsAutomated = Convert.ToBoolean(Session["IsAutomated"]);
                    testingViewModel.SearchViolation = "Search";
                    testingViewModel.violation = GetViolationDetail(testingViewModel.violations, testingViewModel.violationId);
                    getNextViolation = new Helper.GetNextViolation(testingViewModel.violations, testingViewModel.violations.FindIndex(violation => violation.ViolationId == testingViewModel.violationId));
                    testingViewModel.getNextViolation = getNextViolation;
                    Session["TestingViewModel"] = testingViewModel;
                }
                catch (Exception ex)
                {
                    LogException.CatchException(ex, "TestingController", "_RightPanelForSearchViolations");
                }

                return PartialView("_RightPanel", testingViewModel);
            }
            else
            {
                return PartialView();
            }
        }

        public ActionResult _RightPanelAfterSaveForManual(FormCollection collection)
        {
            testingViewModel = (TestingViewModel)Session["TestingViewModel"];
            if (testingViewModel != null)
            {
                try
                {
                    testingViewModel.violationId = Convert.ToInt32(collection["violationId"]);
                    testingViewModel.IsAutomated = Convert.ToBoolean(Session["IsAutomated"]);
                    testingViewModel.SearchViolation = "Save";
                    testingViewModel.violation = GetViolationDetail(testingViewModel.violations, testingViewModel.violationId);
                    getNextViolation = new Helper.GetNextViolation(testingViewModel.violations, testingViewModel.violations.FindIndex(violation => violation.ViolationId == testingViewModel.violationId));
                    testingViewModel.getNextViolation = getNextViolation;
                    Session["TestingViewModel"] = testingViewModel;
                }
                catch (Exception ex)
                {
                    LogException.CatchException(ex, "TestingController", "_RightPanelAfterSaveForManual");
                }

                return PartialView("_RightPanel", testingViewModel);
            }
            else
            {
                return PartialView();
            }
        }

        public ActionResult GetNextViolation()
        {
            testingViewModel = (TestingViewModel)Session["TestingViewModel"];
            testingViewModel.violation = testingViewModel.getNextViolation.getNext();
            testingViewModel.violationId = testingViewModel.violation.ViolationId;
            Session["TestingViewModel"] = testingViewModel;
            return PartialView("_RightPanel", testingViewModel);
        }

        public ActionResult GetPreviousViolation()
        {
            testingViewModel = (TestingViewModel)Session["TestingViewModel"];
            testingViewModel.violation = testingViewModel.getNextViolation.getPrevious();
            testingViewModel.violationId = testingViewModel.violation.ViolationId;
            Session["TestingViewModel"] = testingViewModel;
            return PartialView("_RightPanel", testingViewModel);
        }

        public ActionResult SaveBug(FormCollection collection)
        {
            testingViewModel = (TestingViewModel)Session["TestingViewModel"];
            testingViewModel.violationDescription = collection["violationDescription"];
            testingViewModel.violationRecommendation = collection["violationRecommendations"];

            bool status = testClient.UpdateViolation(testingViewModel.violationDescription, testingViewModel.violationRecommendation, testingViewModel.violationId, collection["status"]);

            return PartialView("_RightPanel", testingViewModel);
        }

        public ActionResult SaveBugForManual(FormCollection collection)
        {
            try
            {
                String violationSourceElement;
                // Decode the HTML string.
                violationSourceElement = WebUtility.HtmlDecode(collection["violationSourceElement"]);

                testingViewModel = (TestingViewModel)Session["TestingViewModel"];
                testingViewModel.violationDescription = collection["violationDescription"];
                testingViewModel.violationRecommendation = collection["violationRecommendations"];
                testingViewModel.violationName = collection["violationName"];
                testingViewModel.violationSourceElement = violationSourceElement;
                testingViewModel.siteName = collection["violationSourceURL"];

                testingViewModel.SearchViolation = "Save";

                bool status = Testing.UpdateViolationForManual(testingViewModel.violationDescription, testingViewModel.violationRecommendation, collection["violationName"], collection["violationSourceURL"], testingViewModel.violationSourceElement, testingViewModel.violationId, collection["status"]);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestingController", "SaveBugForManual");
            }

            return PartialView("_RightPanel", testingViewModel);
        }

        public ActionResult DeclineBug(FormCollection collection)
        {
            try
            {
                testingViewModel = (TestingViewModel)Session["TestingViewModel"];
                testingViewModel.violationDescription = collection["violationDescription"];
                testingViewModel.violationRecommendation = collection["violationRecommendations"];

                bool status = testClient.UpdateViolation(testingViewModel.violationDescription, testingViewModel.violationRecommendation, testingViewModel.violationId, collection["status"]);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestingController", "DeclineBug");
            }

            return PartialView("_RightPanel", testingViewModel);
        }

        public ActionResult DeclineBugForManual(FormCollection collection)
        {
            try
            {
                testingViewModel = (TestingViewModel)Session["TestingViewModel"];
                testingViewModel.violationDescription = collection["violationDescription"];
                testingViewModel.violationRecommendation = collection["violationRecommendations"];
                testingViewModel.SearchViolation = "Search";

                bool status = testClient.UpdateViolation(testingViewModel.violationDescription, testingViewModel.violationRecommendation, testingViewModel.violationId, collection["status"]);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestingController", "DeclineBugForManual");
            }

            return PartialView("_RightPanel", testingViewModel);
        }

        public ActionResult VerifyBug(FormCollection collection)
        {
            try
            {
                testingViewModel = (TestingViewModel)Session["TestingViewModel"];
                testingViewModel.violationDescription = collection["violationDescription"];
                testingViewModel.violationRecommendation = collection["violationRecommendations"];

                bool status = testClient.UpdateViolation(testingViewModel.violationDescription, testingViewModel.violationRecommendation, testingViewModel.violationId, collection["status"]);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestingController", "VerifyBug");
            }

            return PartialView("_RightPanel", testingViewModel);
        }

        public ActionResult VerifyBugForManual(FormCollection collection)
        {
            try
            {
                testingViewModel = (TestingViewModel)Session["TestingViewModel"];
                testingViewModel.violationDescription = collection["violationDescription"];
                testingViewModel.violationRecommendation = collection["violationRecommendations"];
                testingViewModel.SearchViolation = "Search";

                bool status = testClient.UpdateViolation(testingViewModel.violationDescription, testingViewModel.violationRecommendation, testingViewModel.violationId, collection["status"]);
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestingController", "VerifyBugForManual");
            }

            return PartialView("_RightPanel", testingViewModel);
        }

        public ActionResult SaveNewBug(FormCollection collection)
        {
            int intialClientID = 0;
            try
            {
                String violationSourceElement;
                // Decode the HTML string.
                violationSourceElement = WebUtility.HtmlDecode(collection["violationSourceElement"]);

                testingViewModel = (TestingViewModel)Session["TestingViewModel"];
                testingViewModel.violationName = collection["violationName"];
                testingViewModel.violationSourceElement = violationSourceElement;
                testingViewModel.violationSourceLineNumber = Convert.ToInt32(collection["violationSourceLineNumber"]);
                testingViewModel.violationDescription = collection["violationDescription"];
                testingViewModel.violationRecommendation = collection["violationRecommendations"];
                testingViewModel.siteName = collection["violationSourceURL"];
                testingViewModel.IsAutomated = Convert.ToBoolean(collection["isAutomated"]);
                testingViewModel.testcaseName = collection["testcaseName"];
                int vioaltionLogID = AccessRuleEng.ViolationLogData.GetIntialViolationLogID(intialClientID);
                bool status = testClient.CreateViolation(testingViewModel.violationName, testingViewModel.violationDescription, testingViewModel.runId,
                                        testingViewModel.siteName, testingViewModel.violationSourceElement, testingViewModel.violationSourceLineNumber,
                                        testingViewModel.violationRecommendation, "F", testClient.GetClientSingle(testingViewModel.clientName).ToList().First().ClientId,
                                        testingViewModel.buildId, testingViewModel.testcaseName, vioaltionLogID);

            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestingController", "SaveNewBug");
            }

            return PartialView("_RightPanel", testingViewModel);
        }

        public ActionResult QuickSearchViolation(string searchText)
        {
            testingViewModel = (TestingViewModel)Session["testingViewModel"];
            if (testingViewModel != null && testingViewModel.violations != null)
            {
                var violations = testingViewModel.violations.Where(violation => violation.ViolationId.ToString().Contains(searchText))
                .Take(10)
                .OrderBy(violation => violation.ViolationId)
                .Select(violation => new { label = violation.ViolationId });

                return Json(violations, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ValidateData(FormCollection collection)
        {
            bool DupData = true;
            int ClientID = 0;
            int siteId = 0;
            try
            {
                ClientID = testClient.GetClientSingle(collection["clientName"].ToString()).ToList().First().ClientId;
                ExecutionServices executionClient = new ExecutionServices();
                siteId = executionClient.GetSiteID(collection["site"].ToString(), ClientID);
                if (siteId > 0)
                {
                    DupData = false;
                }
            }
            catch (Exception ex)
            {
                
                LogException.CatchException(ex, "ExecutionController", "ValidateData");
            }

            return Json(new { Valid = DupData.ToString() });
        }


        public ActionResult _RightFilter(TestingViewModel testingViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //testingViewModel.builds = testClient.GetBuilds(testingViewModel.clientName, testingViewModel.siteName).ToList();
                    testingViewModel.builds = Testing.GetBuilds(testingViewModel.clientName, testingViewModel.siteName);
                    Session["TestingViewModel"] = testingViewModel;
                }
                catch (Exception ex)
                {
                    LogException.CatchException(ex, "TestingController", "_RightFilter");
                }

                return PartialView(testingViewModel);
            }
            else
            {
                return PartialView();
            }
        }

        private AccessBusEnt.ViolationBO GetViolationDetail(List<AccessBusEnt.ViolationBO> violations, int violationId)
        {
            if (violationId == 0)
            {
                return new AccessBusEnt.ViolationBO();
            }
            else
            {
                if (violations.Where(violation => violation.ViolationId == violationId).ToList().Count > 0)
                {
                    return violations.Where(violation => violation.ViolationId == violationId).ToList().First();
                }
                else
                {
                    return new AccessBusEnt.ViolationBO();
                }
            }
        }

        [HttpPost]
        public PartialViewResult GetPaginatedViolations(FormCollection collection)
        {
            testingViewModel = (TestingViewModel)Session["TestingViewModel"];
            int clientId = testClient.GetClientSingle(testingViewModel.clientName).ToList().First().ClientId;
            int buildId = testingViewModel.buildId;
            int runId = testingViewModel.runId;
            var totalPage = testingViewModel.TotalFilteredRecords / 10;
            var violations = new AccessBusEnt.Violations();
            if ((collection["paginate"].Equals("previous") && testingViewModel.PageNo > 0) || (collection["paginate"].Equals("next") && testingViewModel.PageNo < totalPage))
            {
                int skip = testingViewModel.PageNo * 10;
                violations = AccessRuleEng.Testing.GetViolations(clientId, buildId, runId, testingViewModel.ruleDetailId, testingViewModel.ViolationTypeSelected, skip, testingViewModel.TestCaseId);
                testingViewModel.violations = violations.ViolationList;
            }
            if (testingViewModel != null)
            {
                Session["TestingViewModel"] = testingViewModel;
                return PartialView("_MiddlePanelViolations", testingViewModel);
            }
            else
            {
                return PartialView();
            }
        }


        /// <summary>
        /// Get Status from Violations_Log table
        /// </summary>
        /// <param name=""></param>
        /// <returns>Status</returns>
        public ActionResult VerifyLogStatus()
        {
            char status = 'C';
            try
            {
                status = testClient.GetViolationLogStatus();
            }
            catch (Exception ex)
            {
                LogException.CatchException(ex, "TestingController", "VerifyLogStatus");
            }
            return Json(status);
        }

        public PartialViewResult _MiddlePanelViolationsPaging(FormCollection collection)
        {
            testingViewModel = (TestingViewModel)Session["TestingViewModel"];
            int clientId = testClient.GetClientSingle(testingViewModel.clientName).ToList().First().ClientId;
            int buildId = testingViewModel.buildId;
            int runId = testingViewModel.runId;
            int testCaseId = 0;
            if (collection["testCaseId"] != null)
                testCaseId = Convert.ToInt32(collection["testCaseId"]);
            else
                testCaseId = testingViewModel.TestCaseId;
            string refreshPanel = (collection["refreshPanel"]);
            var violations = new AccessBusEnt.Violations();
            if (testingViewModel != null)
            {
                try
                {
                    if (Convert.ToInt32(collection["ruleDetailId"]) != 0)
                    {
                        testingViewModel.ruleDetailId = Convert.ToInt32(collection["ruleDetailId"]);
                        if (collection["ViolationType"] != null)
                        {
                            violations = AccessRuleEng.Testing.GetViolations(clientId, buildId, runId, testingViewModel.ruleDetailId, collection["ViolationType"].ToString(), 0, testCaseId);
                            testingViewModel.violations = violations.ViolationList;
                            testingViewModel.ViolationTypeSelected = collection["ViolationType"].ToString();
                            testingViewModel.TestCaseId = testCaseId;
                            testingViewModel.StartIndex = testingViewModel.violations.Count > 0 ? 1 : 0;
                            testingViewModel.EndIndex = testingViewModel.violations.Count >= 10 ? 10 : testingViewModel.violations.Count;
                            testingViewModel.PageNo = 0;
                            string violationtype = collection["ViolationType"];
                            testingViewModel.TotalFilteredRecords = (violationtype == "Accepted") ? violations.AcceptedViolationCount : (violationtype == "Declined") ? violations.DeclinedViolationCount : violations.TotalViolationCount;
                            testingViewModel.TotalPage = testingViewModel.TotalFilteredRecords > 0 ? testingViewModel.TotalFilteredRecords / 10 : 0;
                        }
                    }
                    if (collection["getviolation"] != null)
                    {
                        testingViewModel.getviolation = collection["getviolation"].ToString();
                    }
                    testingViewModel.selectedRules = collection["selectedRules"];
                    Session["TestingViewModel"] = testingViewModel;
                }
                catch (Exception ex)
                {
                    LogException.CatchException(ex, "TestingController", "_MiddlePanelViolationsPaging");
                }
                return PartialView("_MiddlePanelViolationsPaging", testingViewModel);
            }
            else
            {
                return PartialView();
            }
        }

        public JsonResult GetPaginatedViolationsDetails(FormCollection collection)
        {
            Int32[] val = new Int32[5];
            testingViewModel = (TestingViewModel)Session["TestingViewModel"];
            int clientId = testClient.GetClientSingle(testingViewModel.clientName).ToList().First().ClientId;
            int buildId = testingViewModel.buildId;
            int runId = testingViewModel.runId;
            var totalPage = testingViewModel.TotalFilteredRecords / 10;
            var violations = new AccessBusEnt.Violations();
            if ((collection["paginate"].Equals("previous") && testingViewModel.PageNo > 0) || (collection["paginate"].Equals("next") && testingViewModel.PageNo < totalPage))
            {
                testingViewModel.PageNo = collection["paginate"] == "next" ? testingViewModel.PageNo + 1 : collection["paginate"] == "previous" ? testingViewModel.PageNo - 1 : testingViewModel.PageNo;
                int skip = testingViewModel.PageNo * 10;
                violations = AccessRuleEng.Testing.GetViolations(clientId, buildId, runId, testingViewModel.ruleDetailId, testingViewModel.ViolationTypeSelected, skip, testingViewModel.TestCaseId);
                testingViewModel.violations = violations.ViolationList;
                testingViewModel.StartIndex = (collection["paginate"] == "next") ? testingViewModel.StartIndex + 10 : testingViewModel.StartIndex - 10;
                testingViewModel.EndIndex = (collection["paginate"] == "next") ? testingViewModel.EndIndex + testingViewModel.violations.Count : testingViewModel.PageNo == totalPage - 1
                                                      ? testingViewModel.EndIndex - (testingViewModel.EndIndex % 10)
                                                      : testingViewModel.EndIndex - 10;

                val[0] = testingViewModel.StartIndex;
                val[1] = testingViewModel.EndIndex;
                val[2] = testingViewModel.PageNo;
                val[3] = testingViewModel.TotalFilteredRecords;
                val[4] = testingViewModel.TotalPage;
            }
            if (testingViewModel != null)
            {
                return Json(val);
            }
            else
            {
                return Json("");
            }

        }
    }
}
