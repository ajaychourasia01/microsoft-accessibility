﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Collections;
using System.Web.Security;
using AccessibilityAnalyser.Common;
using AccessibilityAnalyser.RuleEngine;
using Accessibility.ServicesLayer;
using AccessibilityAnalyser.Web.UI;
using System.Linq;
using AccessibilityAnalyser.BusinessEntities;

namespace AccessibilityAnalyser.Web.UI.Models
{
    public class ModelNewClient
    {
        static NewClientService newclientservice = new NewClientService();

        public static ArrayList GetRuleIdForABuild(int iBuildId)
        {
            ArrayList ArrayRuleDetailID = new ArrayList();
            List<TestCaseBO> TestCaseCollection = newclientservice.GetAllTestCases(iBuildId);
            List<TestCaseRuleMapBO> TestCaseRuleCollection = null;

            foreach (TestCaseBO TestcaseItem in TestCaseCollection)
            {
                TestCaseRuleCollection = newclientservice.GetAllTestCaseRuleMap(TestcaseItem.TestCaseId);
            
                foreach (TestCaseRuleMapBO TestcaseRulemapItem in TestCaseRuleCollection)
                {
                    if (!ArrayRuleDetailID.Contains(TestcaseRulemapItem.RuleDetailId))
                        ArrayRuleDetailID.Add(TestcaseRulemapItem.RuleDetailId);
                }
            }

            return ArrayRuleDetailID;
        }

        public static Hashtable BuildRuleandStandardMap(IEnumerable<StandardBO> standardList)
        {
            Hashtable RuleName = new Hashtable();
            List<RuleDetails> getRuleDetails = new List<RuleDetails>();
            foreach (var item in standardList)
            {
                getRuleDetails = newclientservice.GetRuleDetails(item.StandardId.ToString());
                foreach (var ruleDetail in (from i in getRuleDetails select new { i.RuleID, i.RuleName }).Distinct())
                {
                    if (RuleName.ContainsKey(item.StandardId))
                        RuleName[item.StandardId] += "," + ruleDetail.RuleName + "~" + ruleDetail.RuleID;
                    else
                        RuleName.Add(item.StandardId, ruleDetail.RuleName + "~" + ruleDetail.RuleID);
                }
            }
            return RuleName;
        }
    }
}
